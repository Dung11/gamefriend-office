import { fetchAPI } from "./BaseServices";
const ERROR_PREFIX = 'Api - ChatService - ';

export async function getMatching(params: any) {
    let paramStr: string;
    paramStr = '';
    for (const key in params) {
        if (Object.prototype.hasOwnProperty.call(params, key)) {
            if (paramStr == '') {
                paramStr = '?';
            } else {
                paramStr += '&';
            }
            paramStr += `${key}=${params[key]}`;
        }
    }
    try {
        const response: any = await fetchAPI({
            url: `/match${paramStr}`,
        });
        return response;

    } catch (error) {
        console.log(`${ERROR_PREFIX}getMatching`, error.message);
    }
}

export async function getStatusMatching() {
    try {
        const response: any = await fetchAPI({ url: '/match-status' });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getStatusMatching`, error.message);
    }
}

export async function stopMatching(params: any) {
    try {
        const response: any = await fetchAPI({
            url: `/match/${params.matchId}`,
            method: 'DELETE'
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}stopMatching`, error.message);
    }
}