import { keyValueStorage } from "../storage/keyValueStorage";
import { getRefreshData } from "../utils/UserUtils";
import { fetchAPI } from "./BaseServices";
import Url from '../config/UrlApi';

const ERROR_PREFIX = 'Api - AuthorService - ';
const PREFIX = Url.DEV.AUTH_SERVICE_BASE_URL;

export async function verifyToken() {
    try {
        const response: any = await fetchAPI({ url: `/auth/verify` });
        return response
    } catch (error) {
        console.log(`${ERROR_PREFIX}verifyToken`, error.message);
    }
}

export async function verifyEmail(email: string) {
    try {
        const response: any = await fetchAPI({
            url: `/auth/verify-email`,
            method: 'POST',
            body: {
                email
            }
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}verifyEmail`, error.message);
    }
}

export async function verifyEmailOtp(email: string, otp: string) {
    try {
        const response: any = await fetchAPI({
            url: `/auth/login/email-otp`,
            method: 'POST',
            body: {
                email,
                otp
            }
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}verifyEmail`, error.message);
    }
}

export async function refreshToken() {
    console.log('clm clm clm Author Services');
    let refreshToken: any = await keyValueStorage.get('refreshToken');
    if (!refreshToken) {
        return null;
    }
    let response: any = await fetchAPI({
        url: '/auth/refresh-token',
        method: 'POST',
        body: { refreshToken }
    });
    if (response.data) {
        await keyValueStorage.save('access-token', response.data.accessToken);
        return response.data.accessToken;
    }
    return null;
}

export async function getPersonality() {
    try {
        const response: any = await fetchAPI({ url: '/personality' });
        if (response.isFailed) {
            console.log('GET PERSONALITY ERROR: ', response);
        }
        return response
    } catch (error) {
        console.log(`${ERROR_PREFIX}getPersonality`, error.message);
    }
}

export async function checkEmailExist(username: string) {
    try {
        const response: any = await fetchAPI({ url: `/find/user?email=${username}` });
        if (response.isFailed) {
            console.log('GET CHECK EMAIL ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}checkEmailExist`, error.message);
    }
}

export async function checkPhoneNumberExist(value: string) {
    try {
        const response: any = await fetchAPI({ url: `/find/user?phoneNumber=${value}` });
        if (response.isFailed) {
            console.log('GET CHECK PHONE NUMBER EXITS ERROR: ', response);
        }
        return response
    } catch (error) {
        console.log(`${ERROR_PREFIX}checkPhoneNumberExist`, error.message);
    }
}

//login by email
export async function loginByEmail(email: string, password: string) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/login/email',
            method: 'POST',
            body: { email, password }
        });
        if (response.isFailed) {
            console.log('LOGIN BY EMAIL ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}loginByEmail`, error.message);
    }
}

// register by email
export async function resgiterByEmail(payLoad: any) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/register/email',
            method: 'POST',
            body: { ...payLoad }
        });
        if (response.isFailed) {
            console.log('REGISTER BY EMAIL ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}resgiterByEmail`, error.message);
    }
}

//verify otp
export async function phoneNumber(phoneNumber: string, accessToken: string) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/login/mobile',
            method: 'POST',
            body: { phoneNumber, accessToken }
        });
        if (response.isFailed) {
            console.log('PHONE NUMBER ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}resgiterByEmail`, error.message);
    }
}

//reset password
export async function newPassword(
    idUser: string,
    accessToken: string,
    password: string,
    confirmPassword: string) {
    try {
        let url = PREFIX + `/user/${idUser}/password`;
        let body = {
            password,
            confirmPassword,
            accessToken
        };

        const response: any = await fetch(url,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${accessToken}`,
                },
                method: 'PUT',
                body: JSON.stringify(body),
            });
        if (response.isFailed) {
            console.log('NEW PASSWORD ERROR: ', response);
        }
        return response.json();
    } catch (error) {
        console.log(`${ERROR_PREFIX}newPassword`, error.message);
    }
}

//login social
export async function loginFacebook(facebookId: string, accessToken: string) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/login/facebook',
            method: 'POST',
            body: { facebookId, accessToken }
        });
        if (response.isFailed) {
            console.log('LOGIN FB ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}loginFb`, error.message);
    }
}

export async function loginGoogles(googleId: string, accessToken: string) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/login/google',
            method: 'POST',
            body: { googleId, accessToken }
        });
        if (response.isFailed) {
            console.log('LOGIN GOOGLE ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}loginGoogle`, error.message);
    }
}

export async function loginKakaotalk(kakaoId: string, accessToken: string) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/login/kakao',
            method: 'POST',
            body: { kakaoId, accessToken }
        });
        if (response.isFailed) {
            console.log('LOGIN KAKAOTALK ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}loginKakaotalk`, error.message);
    }
}

export async function loginApple(appleId: string, firebaseId: string) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/login/firebase-apple',
            method: 'POST',
            body: { appleId, firebaseId }
        });
        if (response.isFailed) {
            console.log('LOGIN KAKAOTALK ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}loginApple`, error.message);
    }
}

export async function logout() {
    try {
        const response: any = await fetchAPI({
            url: '/auth/logout',
            method: 'POST',
        });
        if (response.isFailed) {
            console.log('LOGOUT ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}logout`, error.message);
    }
}

export async function checkNickname(nickname: string) {
    try {
        const response: any = await fetchAPI({ url: `/find/user?userName=${nickname}` });
        if (response.isFailed) {
            console.log('GET CHECK NICKNAME ERROR: ', response);
        }
        console.log('response: ', response);
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}checkNickname`, error.message);
    }
}

export async function getUserProfile(userid: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userid}/profile` });
        if (response.isFailed) {
            console.log('GET USER PROFILE ERROR: ', response);
            return null;
        }
        console.log('response:>>', response)

        return response
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserProfile`, error.message);
    }
}

export async function resetAccount() {

    const dataRf = await getRefreshData();
    let url = PREFIX + `/user/${dataRf.userId}/reset`;
    try {
        const response: any = await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${dataRf.resetToken}`,
            },
        });

        return response.json();
    } catch (error) {
        console.log(`${ERROR_PREFIX}resetAccount`, error.message);
    }
}

export async function updateProfile(payLoad: any, userId: string, refreshToken: string) {
    let url = PREFIX + `/user/${userId}/register-profile`;
    let body = JSON.stringify({ ...payLoad })

    try {
        const response: any = await fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${refreshToken}`,
            },
            method: 'PUT',
            body: body,

        })
        return response.json();
    } catch (error) {
        console.log(`${ERROR_PREFIX}resetAccount`, error.message);
    }
}

// register by kakao
export async function resgiterByKakao(payLoad: any) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/register/kakao',
            method: 'POST',
            body: { ...payLoad }
        });
        if (response.isFailed) {
            console.log('REGISTER KAKAOTALK ERROR: ', response);
            return null;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}resgiterByKakao`, error.message);
    }
}

// register by facebook
export async function resgiterByFacebook(payLoad: any) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/register/facebook',
            method: 'POST',
            body: { ...payLoad }
        });
        if (response.isFailed) {
            console.log('REGISTER FB ERROR: ', response);
            return null;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}resgiterByFacebook`, error.message);
    }
}

// register by google
export async function resgiterByGoogle(payLoad: any) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/register/google',
            method: 'POST',
            body: { ...payLoad }
        });
        if (response.isFailed) {
            console.log('REGISTER GOOGLE ERROR: ', response);
            return null;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}resgiterByGoogle`, error.message);
    }
}

// register by apple
export async function resgiterByApple(payLoad: any) {
    try {
        const response: any = await fetchAPI({
            url: '/auth/register/firebase-apple',
            method: 'POST',
            body: { ...payLoad }
        });
        if (response.isFailed) {
            console.log('REGISTER APPLE ERROR: ', response);
            return null;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}resgiterByApple`, error.message);
    }
}