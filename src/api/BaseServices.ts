import { RootStackParamList } from '../@types';
import { keyValueStorage } from './../storage/keyValueStorage';
import { DeviceEventEmitter } from 'react-native';
import Url from '../config/UrlApi';
import { getAccessToken, onLogout } from "../utils/UserUtils";
import { NavigationProp, useNavigation } from '@react-navigation/native';

interface FetchObject {
    url: string,
    method?: 'GET' | 'POST' | 'PUT' | 'DELETE',
    headers?: object,
    body?: object,
}

const PREFIX = Url.DEV.AUTH_SERVICE_BASE_URL;

export async function fetchAPI(params: FetchObject) {
    let { url, method = 'GET', body = '' }: any = params;
    url = PREFIX + url;
    console.log(`URL: ${url} | method: ${method}`);
    if (body) {
        body = JSON.stringify(body);
    }
    const token = await getAccessToken();
    return new Promise((resolve, reject) => {
        let statusCode = 200;
        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token ? `Bearer ${token}` : '',
            },
            method: method,
            body: body,
        }).then(async response => {
            statusCode = response.status;
            if (!response.ok) {
                errorHandler(response.status);
            }
            return response.json();
        }).then(async responseJson => {
            if (responseJson.status === 401) {
                let newAccessToken = await refreshToken();

                let fetchResponse = await fetch(url, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': newAccessToken ? `Bearer ${newAccessToken}` : '',
                    },
                    method: method,
                    body: body,
                });
                resolve(fetchResponse.json());
            } else {
                resolve(responseJson);
            }
        }).catch(error => {
            resolve(null);
            console.warn('FETCH API: ', error.message);
            reject(error);
        });
    })
}

export async function fetchFormdata(params: FetchObject) {
    try {
        let { url, method = 'GET', body = '' }: any = params;
        url = PREFIX + url;

        let response = await fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            method: method,
            body: body,
        });

        if (response.status === 200) {
            let res = await response.json();
            if (res.status === 401) {
                let newAccessToken = await refreshToken();
                let fetchResponse = await fetch(url, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': newAccessToken ? `Bearer ${newAccessToken}` : '',
                    },
                    method: method,
                    body: body,
                });
                return fetchResponse.json();
            }
            return res;
        }
        return null;
    } catch (error) {
        console.log('error', error.message);
        return error;
    }
}

async function errorHandler(errorCode: number) {
    switch (errorCode) {
        case 401:
            DeviceEventEmitter.emit('ShowAlert', {
                title: '세션이 만료되었습니다',
                body: '계속하려면 다시 로그인하세요.',
                errorCode,
            });
            console.error('Authenticate Error');
            break;

        case 500:
            console.error('Server Error');
            break;

        default:
            break;
    }
}

export function apiXMLRequest(params: FetchObject) {
    return new Promise((resolve, reject) => {
        try {
            let { url, method = 'GET', body = '' } = params;
            url = PREFIX + url;

            const xhr = new XMLHttpRequest();
            xhr.open(method, url);
            xhr.responseType = 'json';
            xhr.send(body);
            xhr.onreadystatechange = () => {
                if (xhr.readyState !== 4) {
                    return null;
                }
                if (xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    // console.log(xhr.response);
                    reject('잠시 후 다시 시도해주세요');
                }
            };

        } catch (error) {
            console.log('apiXMLRequest error', error);
            reject(error.message)
        }
    })
}

async function refreshToken() {
    try {
        console.log('clm clm clm Base Services');
        let refreshToken: any = await keyValueStorage.get('refreshToken');
        if (!refreshToken) {
            const navigation: NavigationProp<RootStackParamList, any> = useNavigation();
            await onLogout(navigation);
            return;
        }
        let url = `${PREFIX}/auth/refresh-token`;
        // console.log('refreshToken: ', refreshToken)
        let body = { refreshToken };
        let fetchResponse: any = await fetch(url,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify(body),
            });

        let response = await fetchResponse.json();
        // console.log('response', response)

        // console.log('refreshToken response: ', response.data);
        await keyValueStorage.save('access-token', response.data.accessToken);
        return response.data.accessToken;
    } catch (error) {
        console.log('doumaaaa', error)
    }
}

export const FETCH_SUCCESS = 'SUCCESS';
