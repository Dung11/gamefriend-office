import { apiXMLRequest } from "./BaseServices";

const ERROR_PREFIX = 'Api - ImageServices - ';

export async function uploadFile(data: any) {
    return new Promise<UploadFileResponse>((resolve, reject) => {
        try {
            apiXMLRequest({ url: '/upload/file', method: 'POST', body: data })
                .then((response: any) => {

                    console.log('uploadImages _ apiXMLRequest', response);
                    resolve(response);
                })
                .catch((error: Error) => {
                    console.log('uploadImages _ apiXMLRequest _ error', error.message);
                    reject(error.message);
                });
        } catch (error) {
            console.log('uploadImages_error', error);
        }
    })
}

interface UploadFileResponse {
    status: number,
    isFailed: boolean,
    data: {
        locations: Location[]
    }
}


type Location = {
    origin: string,
    thumbnail: string
}