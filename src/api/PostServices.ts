import { Alert } from "react-native";
import { fetchAPI } from "./BaseServices";

const ERROR_PREFIX = 'Api - PostService - ';

export async function getPostData(params: any = null) {
    try {
        let paramStr: string;
        paramStr = '';
        for (const key in params) {
            if (Object.prototype.hasOwnProperty.call(params, key)) {
                if (paramStr == '') {
                    paramStr = '?';
                } else {
                    paramStr += '&';
                }
                paramStr += `${key}=${params[key]}`;
            }
        }
        const response: any = await fetchAPI({ url: `/post${paramStr}` });
        if (response.isFailed) {
            console.log('GET POST DATA ERROR: ', response.error);
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getPostData`, error.message);
    }
}

export async function getUserPostData(userId: string, params: any = null) {
    try {
        let paramStr: string;
        paramStr = '';
        for (const key in params) {
            if (Object.prototype.hasOwnProperty.call(params, key)) {
                if (paramStr == '') {
                    paramStr = '?';
                } else {
                    paramStr += '&';
                }
                paramStr += `${key}=${params[key]}`;
            }
        }
        const response: any = await fetchAPI({ url: `/user/${userId}/post${paramStr}` });
        if (response.isFailed) {
            console.log('GET POST DATA ERROR: ', response.error);
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getPostData`, error.message);
    }
}

export async function getPostDetail(id: string, userId: string) {
    try {
        const response: any = await fetchAPI({ url: `/post/${id}` });
        if (response.isFailed) {
            console.log('GET POST DETAIL ERROR: ', response.errors);
        }
        return response.data.post;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getPostDetail`, error.message);
    }
}

export async function onLikePost(id: string) {
    try {
        const response: any = await fetchAPI({ url: `/post/${id}/like`, method: 'POST' });
        if (response.isFailed) {
            console.log('ON LIKE POST ERROR: ', response.errors);
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}onLikePost`, error.message);
    }
}

export async function onUnLikePost(id: string) {
    try {
        const response: any = await fetchAPI({ url: `/post/${id}/like`, method: 'DELETE' });
        if (response.isFailed) {
            console.log('ON UNLIKE POST ERROR: ', response.errors);
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}onUnLikePost`, error.message);
    }
}

export async function getComment(postId: string, page: number, limit: number) {
    try {
        const response: any = await fetchAPI({
            url: `/post/${postId}/comment?page=${page}&limit=${limit}`
        });
        if (response.isFailed) {
            Alert.alert('죄송합니다', response.errors);
            console.log('GET COMMENT ERROR: ', response);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getComment`, error.message);
    }
}

export async function addComment(postId: string, body: any) {
    try {
        const response: any = await fetchAPI({
            url: `/post/${postId}/comment`,
            method: 'POST',
            body
        });
        if (response.isFailed) {
            Alert.alert('죄송합니다', response.errors);
            console.log('ADD COMMENT ERROR: ', response);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}addComment`, error.message);
    }
}

export async function createNewPost(userId: string, body: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/post`,
            method: 'POST',
            body
        });
        if (response.isFailed) {
            Alert.alert('죄송합니다', response.errors);
            console.log('CREATE NEW POST ERROR: ', response);
            return response;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}createNewPost`, error.message);
    }
}

export async function deletePost(userId: string, postId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/post/${postId}`,
            method: 'DELETE'
        });
        if (response.isFailed) {
            Alert.alert('죄송합니다', response.errors);
            console.log('DELETE POST ERROR: ', response);
            return response;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}deletePost`, error.message);
    }
}

export async function getNumberOfComment(postId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/post/${postId}/comment-count`
        });
        if (response.isFailed) {
            Alert.alert('죄송합니다', response.errors);
            console.log('GET NUMBER OF COMMENT ERROR: ', response);
            return response;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getNumberOfComment`, error.message);
    }
}

export async function delComment(postId: string, commentId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/post/${postId}/comment/${commentId}`,
            method: 'DELETE'
        });
        if (response.isFailed) {
            Alert.alert('죄송합니다', response.errors);
            console.log('DELETE COMMENT ERROR: ', response);
            return response;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}delComment`, error.message);
    }
}

export async function getCommentSub(postId: string, commentId: string, page: number, limit: number) {
    try {
        const response: any = await fetchAPI({
            url: `/post/${postId}/comment/${commentId}/sub?page=${page}&limit=${limit}`
        });
        if (response.isFailed) {
            Alert.alert('죄송합니다', response.errors);
            console.log('GET COMMENT SUB ERROR: ', response);
            return null;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getComemntSub`, error.message);
    }
}
