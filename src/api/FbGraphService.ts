import { GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

class FbGraphService {

  getProfile = () => {
    return new Promise<any>((resolve, reject) => {
      const infoRequest = new GraphRequest('/me?fields=id,name,email,picture.type(large)', null, (error: any, result: any) => {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      });
      new GraphRequestManager().addRequest(infoRequest).start();
    });
  }
}

export const fbGraphService = new FbGraphService();
