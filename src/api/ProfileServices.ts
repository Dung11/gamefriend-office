import { apiXMLRequest } from "./BaseServices";

const ERROR_PREFIX = 'Api - AuthorService - ';

export  async function uploadImages(data: any) {
    return new Promise((resolve, reject) => {
        try {
            apiXMLRequest({ url: '/upload/file/thumbnail', method: 'POST', body: data })
                .then((response: any) => {
                    console.log('uploadImages _ apiXMLRequest', response);
                    resolve(response);
                })
                .catch((error: any) => {
                    console.log('uploadImages _ apiXMLRequest _ error', error);
                    reject(error);
                });
        } catch (error) {
            console.log('uploadImages_error', error);
        }
    })
}