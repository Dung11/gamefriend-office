import { fetchAPI } from "./BaseServices";

const ERROR_PREFIX = 'Api - RoomService - ';

export async function getLocalRoomStatus() {
    try {
        const response: any = await fetchAPI({ url: '/local-room-status' });
        if (response.isFailed) {
            console.log('GET LOCAL ROOM ERROR: ', response.error);
            return null;
        }
        const data = response.data.users;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}RoomService`, error.message);
    }
}