import { Platform } from "react-native";
import { keyValueStorage } from "../storage/keyValueStorage";
import { fetchAPI } from "./BaseServices";
const ERROR_PREFIX = 'Api - ChatService - ';

export async function getGameItem() {
    try {
        const response: any = await fetchAPI({ url: '/item-package' });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getGameItem`, error.message);
    }
}

export async function getTotalItem() {
    const userId = await keyValueStorage.get("userID") as any
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/item` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getTotalItem`, error.message);
    }
}

export async function purchaseGameItem(body: any) {
    const userId = await keyValueStorage.get("userID") as any
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/item`,
            method: 'PUT',
            body: {
                ...body,
                packageId: body.productId,
            },
        });
        return response
    } catch (error) {
        console.log(`${ERROR_PREFIX}saveUserTendency`, error.message);
    }
}

export async function updateReceipt(receipt: any) {
    try {
        const response: any = await fetchAPI({
            url: '/receipt',
            method: 'POST',
            body: {
                platform: Platform.OS.toLowerCase(),
                receipt,
            },
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}updateReceipt`, error.message)
    }
}
