import { fetchAPI } from "./BaseServices";

const ERROR_PREFIX = 'Api - GameService - ';

export async function getCharacterByType(type: 'mental' | 'personality' | 'teamwork') {
    try {
        const response: any = await fetchAPI({ url: `/character?type=${type}` });
        if (response.isFailed) {
            console.log('GET CHARACTER BY TYPE ERROR: ', response.error);
            return null;
        }
        const data = response.data.characters;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getCharacterByType`, error.message);
    }
}

export async function getGameFilter(userId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/game-filter` });
        if (response.isFailed) {
            console.log('GET GAME FILTER ERROR: ', response.error);
            return null;
        }
        const data = response.data.gameFilters;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getGameFilter`, error.message);
    }
}

export async function getUserItem(userId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/item` });
        if (response.isFailed) {
            console.log('GET USER ITEM ERROR: ', response.error);
            return null;
        }
        return response.data.items;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserItem`, error.message);
    }
}

export async function getListGame(platform: string, isHasTier?: boolean) {
    try {
        const response: any = await fetchAPI({ url: `/game?platform=${platform}${isHasTier ? '&hasTier=true' : ''}` });
        if (response.isFailed) {
            console.log('GET LIST GAME ERROR: ', response.error);
            return null;
        }
        const data = response;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getListGame`, error.message);
    }
}

export async function getTiesGame(gameId: string) {
    try {
        const response: any = await fetchAPI({ url: `/game/${gameId}` });
        if (response.isFailed) {
            console.log('GET TIES GAME ERROR: ', response.error);
            return null;
        }
        const data = response;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getTiesGame`, error.message);
    }
}

export async function deleteGameTag(userId: string, gameId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/game-filter/${gameId}`,
            method: 'DELETE'
        });
        if (response.isFailed) {
            console.log('DELETE GAME TAG ERROR: ', response.error);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}deleteGameTag`, error.message);
    }
}