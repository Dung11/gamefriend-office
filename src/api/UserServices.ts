import { Alert } from 'react-native';
import { fetchAPI } from "./BaseServices";

const ERROR_PREFIX = 'Api - UserService - ';

export async function getUserDetailProfile(id: string, isRecommended: boolean = false) {
    try {
        const response: any = await fetchAPI({ url: `/user/${id}/detail-profile?isRecommended=${isRecommended}` });
        if (response.isFailed) {
            console.log('GET USER DETAIL PROFILE ERROR: ', response.error);
            return null;
        }
        const data = response.data.profile;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserDetailProfile`, error.message);
    }
}

export async function getUserReview(id: string, page: number, limit: number) {
    try {
        const response: any = await fetchAPI({ url: `/user/${id}/received-review?page=${page}&limit=${limit}` });
        if (response.isFailed) {
            console.log('GET USER REVIEW ERROR: ', response.errors[0]);
            return null;
        }
        const data = response.data;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserReview`, error.message);
    }
}

export async function getUserRequests(id: string, page: number, limit: number) {
    try {
        const response: any = await fetchAPI({ url: `/user/${id}/received-friend-request?page=${page}&limit=${limit}` });
        if (response.isFailed) {
            console.log('GET USER REQUEST ERROR: ', response.error);
            return null;
        }
        const data = response.data;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserRequests`, error.message);
    }
}

export const getUserRequestsQuantity = async (payload: any) => {
    try {
        const response: any = await fetchAPI({ url: `/user/${payload?.data?.userId}/received-friend-request` });
        if (response.isFailed) {
            console.log('GET USER REQUEST QUANTITY ERROR: ', response.error);
            return null;
        }
        const data = response.data;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserRequestsQuantity`, error.message);
    }
}

export async function acceptFriendRequests(id: string, requestId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${id}/received-friend-request/${requestId}/accept`, method: 'POST' });
        if (response.isFailed) {
            console.log('ACCEPT FRIEND REQUEST ERROR: ', response);
            return null;
        }
        const data = response.data;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}acceptFriendRequests`, error.message);
    }
}

export async function denyFriendRequests(id: string, requestId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${id}/received-friend-request/${requestId}/deny`, method: 'POST' });
        if (response.isFailed) {
            console.log('DENY FRIEND REQUESTS ERROR: ', response.error);
            return null;
        }
        const data = response.data;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}denyFriendRequests`, error.message);
    }
}

export async function requestedUserInfo(id: string, requestId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/received-friend-request/${requestId}`
        });
        if (response.isFailed) {
            console.log('REQUESTED USER INFO ERROR: ', response.error);
            return null;
        }
        const data = response.data.request;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}requestedUserInfo`, error.message);
    }
}


export async function addReview(id: string, params: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/review`,
            body: params,
            method: 'POST'
        });
        if (response.isFailed) {
            console.log('ADD REVIEW ERROR: ', response);
            return null;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}addReview`, error.message);
    }
}

export async function addReport(id: string, params: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/report`,
            body: params,
            method: 'POST'
        });
        if (response.isFailed) {
            console.log('ADD REPORT ERROR: ', response.errors[0]);
            return null;
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}addReport`, error.message);
    }
}

export async function personalProfile(payload: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${payload?.data?.id}/personal-profile`
        });
        if (response.isFailed) {
            console.log('PERSONAL PROFILE ERROR: ', response);
            return null;
        }
        return response.data.profile;
    } catch (error) {
        console.log(`${ERROR_PREFIX}personalProfile`, error.message);
    }
}

export async function getGameFilterPayload(payload: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${payload?.data?.id}/default-game-filter`
        });
        if (response.isFailed) {
            console.log('GAME FILTER ERROR: ', response.error);
            return null;
        }
        return response.data.filters;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getGameFilterPayload`, error.message);
    }
}

export async function updateGameFilter(id: any, params: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/default-game-filter`,
            method: 'PUT',
            body: params
        });
        console.log('ressss: ', response)
        if (response.isFailed) {
            console.log('UPDATE GAME FILTER ERROR: ', response.error);
            return null;
        }
        return response.data.filters;
    } catch (error) {
        console.log(`${ERROR_PREFIX}updateGameFilter`, error.message);
    }
}

export async function personalProfileById(id: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/personal-profile`
        });
        if (response.isFailed) {
            console.log('PERSONAL PROFILE BY ID ERROR: ', response.error);
            return null;
        }
        return response.data.profile;
    } catch (error) {
        console.log(`${ERROR_PREFIX}personalProfileById`, error.message);
    }
}

export async function updatePersonalProfile(id: string, data: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/personal-profile`,
            method: 'PUT',
            body: data
        });
        if (response.isFailed) {
            console.log('UPDATE PERSONAL PROFILE ERROR: ', response.error);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}updatePersonalProfile`, error.message);
    }
}

export async function personalTendency(payload: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${payload?.data?.id}/tendency`
        });
        if (response.isFailed) {
            console.log('PERSONAL TENDENCY ERROR: ', response.error);
            return null;
        }
        return response.data.tendency;
    } catch (error) {
        console.log(`${ERROR_PREFIX}personalTendency`, error.message);
    }
}

export async function personalTendencyById(id: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/tendency`
        });
        if (response.isFailed) {
            console.log('PERSONAL TENDENCY ERROR: ', response.error);
            return null;
        }
        return response.data.tendency;
    } catch (error) {
        console.log(`${ERROR_PREFIX}personalTendency`, error.message);
    }
}

export async function saveUserTendency(id: string, data: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/tendency`,
            method: 'PUT',
            body: data
        });
        if (response.isFailed) {
            console.log('SAVE USER TENDENCY ERROR: ', response.error);
            return null;
        }
        return response.data.tendency;
    } catch (error) {
        console.log(`${ERROR_PREFIX}saveUserTendency`, error.message);
    }
}

export async function withdrawUser(id: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/withdraw`,
            method: 'POST',
        });
        if (response.isFailed) {
            console.log('WITHDRAW USER ERROR: ', response.error);
            return null;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}withdrawUser`, error.message);
    }
}

export async function recommendationStatus(id: string, status: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/recommendation-status/${status}`,
            method: 'PUT',
        });
        if (response.isFailed) {
            console.log('RECOMMENDATION STATUS ERROR: ', response.error);
            return null;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}recommendationStatus`, error.message);
    }
}

export async function hideReview(id: string, reviewId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/received-review/${reviewId}/hide`,
            method: 'POST',
        });
        if (response.isFailed) {
            console.log('HIDE REVIEW ERROR: ', response.error);
            return null;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}hideReview`, error.message);
    }
}

export async function feedback(params: any) {
    try {
        const response: any = await fetchAPI({
            url: '/feedback',
            method: 'POST',
            body: params
        });
        if (response.isFailed) {
            console.log('FEEDBACK ERROR: ', response);
            return null;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}feedback`, error.message);
    }
}

export async function blockUser(userId: string, toBlockUserId: string) {
    try {
        console.log(`userId: ${userId} | toBlockUserId: ${toBlockUserId}`)
        const response: any = await fetchAPI({
            url: `/user/${userId}/blocked-user`,
            method: 'POST',
            body: {
                userId: toBlockUserId
            }
        });
        if (response.isFailed) {
            console.log('BLOCK USER ERROR: ', response.error);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}blockUser`, error.message);
    }
}

export function getPrivacyPolicy() {
    return new Promise(async (resolve, reject) => {
        const response = await fetchAPI({
            url: '/privacy-policy'
        });
        resolve(response);
    });
}

export function getServicePolicy() {
    return new Promise(async (resolve, reject) => {
        const response = await fetchAPI({
            url: '/service-policy'
        });
        resolve(response);
    });
}

export function getBoardServicePolicy() {
    return new Promise(async (resolve, reject) => {
        const response = await fetchAPI({
            url: '/board-service'
        });
        resolve(response);
    });
}

export async function getUserPicture(id: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${id}/picture` });
        if (response.isFailed) {
            console.log('GET USER PICTURE ERROR: ', response.errors);
            Alert.alert('오류...', response.errors);
            return response;
        }
        return response.data.pictures;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserPicture`, error.message);
    }
}

export async function getNotification(userId: string, page: number, limit: number) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/notification?type=post&page=${page}&limit=${limit}`
        });
        if (response.isFailed) {
            Alert.alert('오류...', response.errors);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getNotification`, error.message);
    }
}