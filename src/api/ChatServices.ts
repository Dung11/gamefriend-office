import { Alert } from 'react-native';
import { apiXMLRequest, fetchAPI } from "./BaseServices";

const ERROR_PREFIX = 'Api - ChatService - ';

export async function getChattingData(
    userId: string,
    roomId: string,
    page: number,
    limit: number) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/room/${roomId}/chat?limit=${limit}&page=${page}` });
        if (response.isFailed) {
            console.log('GET CHATTING DATA ERROR: ', response.error);
            return null;
        }
        return response.data
    } catch (error) {
        console.log(`${ERROR_PREFIX}getChattingData`, error.message);
    }
}

export async function uploadImages(data: any) {
    return new Promise((resolve, reject) => {
        try {
            apiXMLRequest({ url: '/upload/file/thumbnail', method: 'POST', body: data })
                .then((response: any) => {
                    console.log('uploadImages _ apiXMLRequest', response);
                    resolve(response);
                })
                .catch((error: any) => {
                    console.log('uploadImages _ apiXMLRequest _ error', error);
                    reject(error);
                });
        } catch (error) {
            console.log('uploadImages_error', error);
        }
    })
}

export async function muteNoticationAction(
    userId: string,
    roomId: string,
    muteStatus: 'unmute' | 'mute',
) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/room/${roomId}/${muteStatus}`, method: 'POST' });
        if (response.isFailed) {
            console.log('MUTE NOTIFICATION ERROR: ', response);
            return null;
        }
        return !response.isFailed
    } catch (error) {
        console.log(`${ERROR_PREFIX}muteNoticationAction`, error.message);
    }
}

export async function muteNoticationLocalAction(
    roomId: string,
    muteStatus: 'unmute' | 'mute',
) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${roomId}/${muteStatus}`, method: 'POST' });
        if (response.isFailed) {
            console.log('MUTE NOTIFICATION LOCAL ACTION ERROR: ', response);
            return null;
        }
        return !response.isFailed
    } catch (error) {
        console.log(`${ERROR_PREFIX}muteNoticationLocalAction`, error.message);
    }
}

export async function getRoomInformation(userId: string, roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/room/${roomId}` });
        if (response.isFailed) {
            console.log('GET ROOM INFORMATIONN: ', response);
            return null;
        }
        return response.data.room;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRoomInformation`, error.message);
    }
}

export async function getRoomMemberInformation(payLoad: any) {
    try {
        const response: any = await fetchAPI({ url: `/user/${payLoad.userId}/room/${payLoad.roomId}/member` });
        if (response.isFailed) {
            console.log('GET ROOM INFORMATIONN MEMBER: ', response);
            return null;
        }
        return response.data.members;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRoomMemberInformation`, error.message);
    }
}

export async function getLocalRoomMemberInformation(roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${roomId}/member` });
        if (response.isFailed) {
            console.log('GET ROOM MEMBER INFORMATIONN: ', response);
            return null;
        }
        return response.data.members;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getLocalRoomMemberInformation`, error.message);
    }
}

export async function getLocalRoomInformation(roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${roomId}` });
        if (response.isFailed) {
            console.log('GET ROOM INFORMATIONN: ', response);
            return null;
        }
        return response.data.localRoom;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getLocalRoomInformation`, error.message);
    }
}

export async function getChatListData(userId: string, params: any = null) {
    try {
        let paramStr: string;
        paramStr = '';
        for (const key in params) {
            if (Object.prototype.hasOwnProperty.call(params, key)) {
                if (paramStr == '') {
                    paramStr = '?';
                } else {
                    paramStr += '&';
                }
                paramStr += `${key}=${params[key]}`;
            }
        }

        const response: any = await fetchAPI({ url: `/user/${userId}/room${paramStr}` });
        if (response.isFailed) {
            console.log('GET CHAT LIST DATA ERROR: ', response);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getChatListData`, error.message);
    }
}

export async function getTip() {
    try {
        const response: any = await fetchAPI({ url: '/tip' });
        if (response.isFailed) {
            console.log('GET TIP: ', response);
            return null;
        }
        return response.data.tips;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getTip`, error.message);
    }
}

export async function getChatListDataPayload(payload: any) {
    try {
        let paramStr: string;
        let params = payload.params;
        paramStr = '';
        for (const key in params) {
            if (Object.prototype.hasOwnProperty.call(params, key)) {
                if (paramStr == '') {
                    paramStr = '?';
                } else {
                    paramStr += '&';
                }
                paramStr += `${key}=${params[key]}`;
            }
        }

        const response: any = await fetchAPI({ url: `/user/${payload.userId}/room${paramStr}` });
        if (response.isFailed) {
            console.log('GET CHAT LIST DATA ERROR: ', response);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getChatListData`, error.message);
    }
}

export async function outRoomChat(userId: string, roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/room/${roomId}/leave`, method: 'POST' });
        if (response.isFailed) {
            console.log('OUT ROOM CHAT ERROR: ', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}outRoomChat`, error.message);
    }
}

export async function outLocalRoomChat(roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${roomId}/leave`, method: 'POST' });
        if (response.isFailed) {
            console.log('OUT LOCAL ROOM CHAT ERROR: ', response);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}outLocalRoomChat`, error.message);
    }
}

export async function sendFriendRequest(userId: string, friendId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/friend`,
            method: 'POST',
            body: {
                userId: friendId
            }
        });
        if (response.isFailed) {
            console.log('SEND FRIEND REQUEST ERROR: ', response);
            return null;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}sendFriendRequest`, error.message);
    }
}

export async function cancelRequestAddFriend(userId: string, friendId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/sent-friend-request/${friendId}`,
            method: 'DELETE'
        });
        if (response.isFailed) {
            console.log('CANCEL SEND FRIEND REQUEST ERROR: ', response);
            return null;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}cancelRequestAddFriend`, error.message);
    }
}