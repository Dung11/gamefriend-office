import { fetchAPI } from "./BaseServices";
import DeviceInfo from 'react-native-device-info';
import axios from "axios";
import Environments from "../config/Environments";

const ERROR_PREFIX = 'Api - GeneralService - ';

export async function checkAppUpdate() {
    let buildNumber = DeviceInfo.getVersion();
    let systemName = DeviceInfo.getSystemName();
    let response: any = await fetchAPI({
        url: '/version',
        body: {
            os: systemName.toLowerCase(),
            version: buildNumber,
        },
        method: 'POST',
    });
    return process.env.NODE_ENV === 'production' ? response?.data?.isValid : true;
}

export async function setNotiAll(id: string, status: 'mute' | 'unmute') {
    try {
        let response: any = await fetchAPI({
            url: `/user/${id}/notification-all/${status}`,
            method: 'POST'
        });
        if (response.isFailed) {
            console.log('SET NOTI ALL ERROR: ', response.error);
            return false;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}setNotiAll: `, error)
    }
}

export async function setNotiRecommend(id: string, status: 'mute' | 'unmute') {
    try {
        let response: any = await fetchAPI({
            url: `/user/${id}/notification-recommend/${status}`,
            method: 'POST'
        });
        if (response.isFailed) {
            console.log('SET NOTI RECOMMEND ERROR: ', response.error);
            return false;
        }
        return !response.isFailed;
    } catch (error) {
        console.log(`${ERROR_PREFIX}setNotiRecommend: `, error)
    }
}

export async function getGameRule() {
    try {
        let response: any = await fetchAPI({ url: `/item-rule` });
        if (response.isFailed) {
            console.log('GET GAME RULE ERROR: ', response.error);
            return null;
        }
        return response.data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getGameRule: `, error)
    }
}

export async function getAddrFromLatLng(lat: number, lng: number) {
    const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&result_type=street_address&key=${Environments.GOOGLE_API_KEY}&language=ko`);
    return response.data;
}

