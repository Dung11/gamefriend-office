import { fetchAPI } from "./BaseServices";

const ERROR_PREFIX = 'Api - RecommendedServices - ';

export async function getExtendOption() {
    try {
        const response: any = await fetchAPI({ url: '/recommend-option' });
        const data = response.data;
        if (response.isFailed) {
            console.log('GET EXTEND OPTION ERROR: ', response.error);
            return null;
        }
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getExtendOption`, error.message);
    }
}

export async function getRecommendationList(userId: string, params: any = null) {
    try {
        let paramStr: string;
        paramStr = '';
        for (const key in params) {
            if (Object.prototype.hasOwnProperty.call(params, key)) {
                if (paramStr == '') {
                    paramStr = '?';
                } else {
                    paramStr += '&';
                }
                paramStr += `${key}=${params[key]}`;
            }
        }
        const response: any = await fetchAPI({ url: `/user/${userId}/recommendation${paramStr}` });
        if (response.isFailed) {
            console.log('GET RECOMMENDDATION LIST ERROR: ', response.error);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRecommendationList`, error.message);
    }
}

export async function updateFilterRecommendationList(userId: string, body: any = null) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/recommendation`,
            method: 'PUT',
            body
        });
        if (response.isFailed) {
            console.log('UPDATE FILTER RECOMMENDDATION LIST ERROR: ', response.error);
        }
        console.log(`response`, response)
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}updateFilterRecommendationList`, error.message);
    }
}

export async function getRecommendationListPayload(payload: any) {
    try {
        let params = payload.data.params;
        let paramStr: string;
        paramStr = '';
        for (const key in params) {
            if (Object.prototype.hasOwnProperty.call(params, key)) {
                if (paramStr == '') {
                    paramStr = '?';
                } else {
                    paramStr += '&';
                }
                paramStr += `${key}=${params[key]}`;
            }
        }
        const response: any = await fetchAPI({ url: `/user/${payload.data.userId}/recommendation${paramStr}` });
        console.log('response: ', response)
        const data = response.data;
        if (response.isFailed) {
            console.log('GET RECOMMENDATION LIST ERROR: ', response.error);
            return null;
        }
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRecommendationList`, error.message);
    }
}

export async function refreshRecommendationList(userId: string) {
    try {

        const response: any = await fetchAPI({ url: `/user/${userId}/recommendation`, method: 'PUT' });
        if (response.isFailed) {
            console.log('REFRESH RECOMMENDATION LIST ERROR: ', response.error);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}refreshRecommendationList`, error.message);
    }
}

export async function sendFriendRequest(userId: string, friendId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/friend`, method: 'POST', body: {
                userId: friendId
            }
        });
        if (response.isFailed) {
            console.log('REFRESH RECOMMENDATION LIST ERROR: ', response.error);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}sendFriendRequest`, error.message);
    }
}

export async function getMatchingPercentage(userId: string, recommendationId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/recommendation/${recommendationId}`
        });
        if (response.isFailed) {
            console.log('GET MATHCING PERCENTAGE ERROR: ', response.error);
            return null;
        }
        const data = response.data;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getMatchingPercentage`, error.message);
    }
}

export async function getUserIndexes(id: string) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/index`
        });
        if (response.isFailed) {
            console.log('GET USER INDEX ERROR: ', response.error);
            return null;
        }
        const data = response.data.indexes;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getUserIndexes`, error.message);
    }
}

export async function getTendencyOption() {
    try {
        const response: any = await fetchAPI({
            url: '/tendency-option'
        });
        if (response.isFailed) {
            console.log('GET TENDENCY OPTION ERROR: ', response.error);
            return null;
        }
        const data = response.data.options;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getTendencyOption`, error.message);
    }
}

export async function getRecommendFilter(id: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/recommend-filter`
        });
        if (response.isFailed) {
            console.log('GET RECOMMEND FILTER ERROR: ', response.error);
            return null;
        }
        const data = response.data.recommendFilter;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRecommendFilter`, error.message);
    }
}

export async function updateRecommendFilter(id: any, params: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${id}/recommend-filter`,
            method: 'PUT',
            body: params
        });
        if (response.isFailed) {
            console.log('GET RECOMMEND FILTER ERROR: ', response.error);
            return null;
        }
        const data = response.data.recommendFilter;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRecommendFilter`, error.message);
    }
}