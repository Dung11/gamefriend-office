import { apiXMLRequest, fetchAPI } from "./BaseServices";

const ERROR_PREFIX = 'Api - ChatService - ';

export async function getRoomList(page: number, limit: number, search?: any) {
    try {
        const response: any = await fetchAPI({ url: `/room${search ? `?${search}&` : '?'}page=${page}&limit=${limit}` });
        if (response.isFailed) {
            console.log('GET LIST ROOM ERROR', response);
            return null;
        }
        const data = response.data;
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRoomListLivetalk`, error.message);
    }
}

export async function getRoomInfo(payLoad: any) {
    try {
        const response: any = await fetchAPI({ url: `/user/${payLoad.userId}/room/${payLoad.roomId}` });
        if (response.isFailed) {
            console.log('GET INFO ROOM ERROR', response);
        }
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getRoomInformation`, error.message);
    }
}

export async function createRoom(userId: string, payLoad: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/room`,
            body: payLoad,
            method: 'POST'
        });

        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}postCreateRoom`, error.message);
    }
}

export async function joinRoom(roomId: string, password: string) {
    try {
        const response: any = await fetchAPI({
            url: `/room/${roomId}/join`,
            body: { password: password },
            method: 'POST'
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}joinRoom`, error.message);
    }
}

export async function getInvitableUsers(userId: string, roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/room/${roomId}/invitator` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getInvitableUsers`, error.message);
    }
}

export async function getMemberInRoom(userId: string, roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/room/${roomId}/member` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getMemberInRoom`, error.message);
    }
}

export async function addUserRoom(userId: string, roomId: string, userIds: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/room/${roomId}/member`,
            body: { userIds },
            method: 'POST'
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}addUserRoom`, error.message);
    }
}

export async function kickUserRoom(userId: string, roomId: string, userIds: any) {
    try {
        const response: any = await fetchAPI({
            url: `/user/${userId}/room/${roomId}/member/${userIds}`,
            method: 'DELETE'
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}kickUserRoom`, error.message);
    }
}

export async function getProfileMember(userId: string, roomId: string, memberId: string) {
    try {
        const response: any = await fetchAPI({ url: `/user/${userId}/room/${roomId}/member/${memberId}` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getProfileMember`, error.message);
    }
}

export async function getNeighborRooms(isWaiting: boolean, page: number, limit: number) {
    try {
        const response: any = await fetchAPI({ url: `/local-room?needWaiting=${isWaiting}&page=${page}&limit=${limit}` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getNeighborRooms`, error.message);
    }
}

export async function createLocalRoom(payload: any) {
    try {
        const response: any = await fetchAPI({
            url: `/local-room`,
            body: { ...payload },
            method: 'POST'
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}createLocalRoom`, error.message);
    }
}

export async function joinLocalRoom(roomId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/local-room/${roomId}/join`,
            method: 'POST'
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}joinLocalRoom`, error.message);
    }
}

export async function leaveLocalRoom(roomId: string) {
    try {
        const response: any = await fetchAPI({
            url: `/local-room/${roomId}/leave`,
            method: 'POST'
        });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}leaveLocalRoom`, error.message);
    }
}

export async function getLocalRoomInfo(roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${roomId}` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getLocalRoomInfo`, error.message);
    }
}

export async function getLocalRoomChatMessages(roomId: string) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${roomId}/chat` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getLocalRoomChatMessages`, error.message);
    }
}

export async function getLocalRoomMember(data: any) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${data.roomId}/member` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getLocalRoomMember`, error.message);
    }
}

export async function getLocalProfileMember(roomId: string, memberId: string) {
    try {
        const response: any = await fetchAPI({ url: `/local-room/${roomId}/member/${memberId}` });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getLocalProfileMember`, error.message);
    }
}

export async function getLocalRoomStatus() {
    try {
        const response: any = await fetchAPI({ url: '/local-room-status' });
        return response;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getLocalRoomStatus`, error.message);
    }
}