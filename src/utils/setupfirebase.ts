import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth'
import Environments from '../config/Environments';

const PREFIX = 'SETUP_FIREBASE ';

const firebaseConfig = {
    apiKey: "AIzaSyDIq6HXti4oN6ElS2GTD2UAcVKNxt6owcg",
    authDomain: "game-friend-47cce.firebaseapp.com",
    projectId: "game-friend-47cce",
    storageBucket: "game-friend-47cce.appspot.com",
    messagingSenderId: "661684118621",
    appId: "1:661684118621:web:ab1b41bc9e379d249899b4",
    measurementId: "G-TZPMWPPXXJ"
};
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}
export default () => {
    return { firebase, auth }
}

export async function saveTopics(topic: string) {
    try {
        const topicDataSto = await AsyncStorage.getItem(Environments.TOPICS_DATA);
        let topicData: string[] = [];
        if (topicDataSto) {
            topicData = JSON.parse(topicDataSto);
        }
        let topicIndex = topicData.findIndex((d) => d === topic);
        topicIndex < 0 && topicData.push(topic);

        await messaging().subscribeToTopic(topic);

        await AsyncStorage.setItem(Environments.TOPICS_DATA, JSON.stringify(topicData));
        console.log(`${PREFIX} saveTopics subscribed from ${topic} topic!`);
        return true;
    } catch (error) {
        console.log(`${PREFIX}:${error}`);
        return false;
    }
}

export async function removeTopics() {
    try {
        const topicDataSto = await AsyncStorage.getItem(Environments.TOPICS_DATA);
        let topicData: string[] = [];
        if (topicDataSto) {
            topicData = JSON.parse(topicDataSto);
        }

        if (topicData.length === 0) return true;

        let promises: Promise<void>[] = [];
        for (let i = 0; i < topicData.length; i++) {
            const topic = topicData[i];
            promises.push(
                messaging()
                    .unsubscribeFromTopic(topic)
                    .then(() => console.log(`${PREFIX} removeTopics Unsubscribed from ${topic} topic!`))
            );
        }
        await Promise.all(promises);
        await AsyncStorage.removeItem(Environments.TOPICS_DATA);
        return true;

    } catch (error) {
        console.log(`${PREFIX}:${error}`);
        return false;
    }
}