import { StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';
import Environments from '../config/Environments';

const styles = StyleSheet.create({
    text1Style: {
        fontSize: 30,
        fontFamily: Environments.DefaultFont.Regular
    }
});

interface ToastConfig {
    type: 'success' | 'error' | 'info',
    text1?: string,
    text2?: string,
    onShow?: () => void,
    onHide?: () => void,
    position: 'top' | 'bottom'
}

const privateConfig = {
    visibilityTime: 1000,
    autoHide: true,
    topOffset: 30,
    bottomOffset: 40,
    text1Style: styles.text1Style,
    text2Style: styles.text1Style,
}

function showToast(config: ToastConfig) {
    Toast.show({
        ...privateConfig,
        ...config,
        onPress: () => hideToast(),
    });
}

function hideToast() {
    Toast.hide();
}


export default {
    showToast,
    hideToast
}