import AsyncStorage from "@react-native-community/async-storage";
import { GoogleSignin } from "@react-native-community/google-signin";
import KakaoLogins from "@react-native-seoul/kakao-login";
import { NavigationProp } from "@react-navigation/native";
import { LoginManager } from "react-native-fbsdk";
import { RootStackParamList } from "../@types";
import { logout } from "../api/AuthorServices";
import Environments from "../config/Environments";
import { disconnect } from "../socketio/socketController";
import { keyValueStorage } from "../storage/keyValueStorage";


export async function setAuthenticatedUser(info: any) {
    let data = { ...info, ...info.userInfo };
    delete data['userInfo'];
    await AsyncStorage.setItem(Environments.AUTHENTICATED_USER, JSON.stringify(data));
}

export async function removeAuthenticatedUser() {
    await AsyncStorage.removeItem(Environments.AUTHENTICATED_USER);
}

export async function getAuthenticatedUser() {
    try {
        const user = await AsyncStorage.getItem(Environments.AUTHENTICATED_USER);
        if (user) {
            return JSON.parse(user);
        }
        return null;
    } catch (error) {
        await AsyncStorage.removeItem(Environments.AUTHENTICATED_USER);
        return null;
    }
}

export async function setAccessToken(token: string) {
    await AsyncStorage.setItem(Environments.ACCESSTOKEN, token);
}

export async function removeAccessToken() {
    await AsyncStorage.removeItem(Environments.ACCESSTOKEN);
}

export async function getAccessToken() {
    const token = await AsyncStorage.getItem(Environments.ACCESSTOKEN);
    return token;
}

export async function setRefreshData(data: any) {
    await AsyncStorage.setItem(Environments.REFRESH_DATA, JSON.stringify(data));
}

export async function removeRefreshData() {
    await AsyncStorage.removeItem(Environments.REFRESH_DATA);
}

export async function getRefreshData() {
    const data = await AsyncStorage.getItem(Environments.REFRESH_DATA);
    if (data) {
        return JSON.parse(data);
    }
    return null;
}

export async function onLogout(navigation: NavigationProp<RootStackParamList, any>) {
    try {
        const result = await logout()
        if (result.status === 200) {

            await keyValueStorage.delete("access-token");
            await keyValueStorage.delete("userID");
            await keyValueStorage.delete("userName");
            await keyValueStorage.delete("paramsMatch");
            await keyValueStorage.delete("showModalMatch");
            await keyValueStorage.delete("messageIntroduction");
            await keyValueStorage.delete("isFirstlogin");
            KakaoLogins.logout()
            LoginManager.logOut();
            GoogleSignin.configure({});
            GoogleSignin.signOut();

            disconnect();
            await removeRefreshData();
            navigation.reset({
                index: 0,
                routes: [{ name: 'Author' }],
            });
        }
    } catch (error) {
        console.log("error", error.response)
    }
}