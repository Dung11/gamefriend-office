function validateEmail(email: string) {
    let re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return re.test(email);
}
function validatePhone(phone: string) {
    let re = /^\d{11}$/;
    return re.test(phone)
}
function validatePassword(passw: string) {
    let re = /^(?=.*\d)(?=.*[a-z]).{6,20}$/;
    return re.test(passw)
}

function timeAgoHandle(time: any) {
    try {
        if (time.monthAgo > 0) {
            return time.monthAgo + '달 전';
        } else if (time.monthAgo == 0 && time.dayAgo > 0) {
            return time.dayAgo + '일 전';
        } else if (time.monthAgo == 0 && time.dayAgo == 0 && time.hourAgo > 0) {
            return time.hourAgo + '시간 전';
        } else if (time.monthAgo == 0 && time.dayAgo == 0 && time.hourAgo == 0 && time.minuteAgo > 0) {
            return time.minuteAgo + '분 전';
        } else {
            return '방금전';
        }

    } catch (e) {
        return '';
    }
}

export default {
    validateEmail,
    validatePassword,
    validatePhone,
    timeAgoHandle
}