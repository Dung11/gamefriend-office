
import { Platform } from 'react-native';
import { PERMISSIONS, request, RESULTS } from 'react-native-permissions';

const ERROR_PREFIX = 'Utils - Common ';

export function checkReadStoragePermission() {
    try {
        return new Promise(async (resolve, reject) => {
            const AccessPermission = Platform.select({
                ios: PERMISSIONS.IOS.PHOTO_LIBRARY
                , android: PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE
            });
            if (!AccessPermission) {
                resolve(false);
                return;
            }
            request(AccessPermission).then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        resolve(false);
                        break;
                    case RESULTS.DENIED:
                        resolve(false);
                        break;
                    case RESULTS.BLOCKED:
                        resolve(false);
                        break;
                    case RESULTS.GRANTED:
                        resolve(true);
                        break
                }
            });
        });
    } catch (error) {
        console.log(`${ERROR_PREFIX} - ${error.message}`);
    }
}