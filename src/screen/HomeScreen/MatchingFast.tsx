
import React from 'react';
import { StyleSheet, View, StatusBar, Image } from 'react-native';
import { Icons } from '../../assets';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ModalSelectGender from '../../components/Modal/ModalSelectGender';
import IntroducingTextInputScreen from './MatchingLoading/IntroducingTextInputScreen';
import ButtonMatch from './component/ButtonMatch';
import ModalTipsMatching from '../../components/Modal/ModalTipsMatching';
import CustomLabelAge from '../../components/CustomLabelAge/CustomLabelAge';
import _Text from '../../components/_Text';

const GAP_NUMBER = 4;

interface Props {
    showModelGender: boolean,
    showModelTipsFast: boolean,
    showIntroduction: boolean,
    disabaleMatch: boolean,
    listAge: any,
    nameGame: string,
    nameGender: string,
    valueIntroduction: string,
    isRequirChooseGame: boolean,
    onChangeIntroduction: (value: string) => void
    onShowModal: () => void,
    onSellectGender: (name: string, key: string) => void,
    onShowTips: () => void,
    onHideTips: () => void,
    multiSliderValuesChange: (data: any) => void,
    onAddGame: () => void,
    onDeleteGame: () => void,
    onMatching: () => void,
    onSubmitTwoPoin: () => void,
    onSubmitOnePoin: () => void,
    onBackIntroduction: () => void,
    onBackdropPress: () => void,
}
export default function MatchingFast({
    showModelGender,
    showModelTipsFast,
    showIntroduction,
    disabaleMatch,
    listAge,
    nameGame,
    nameGender,
    valueIntroduction,
    isRequirChooseGame,
    onChangeIntroduction,
    onShowModal,
    onSellectGender,
    onShowTips,
    onHideTips,
    multiSliderValuesChange,
    onAddGame,
    onDeleteGame,
    onMatching,
    onSubmitTwoPoin,
    onSubmitOnePoin,
    onBackIntroduction,
    onBackdropPress,
}: Props) {

    return (
        <>
            <StatusBar barStyle="light-content" />
            <ModalSelectGender onBackdropPress={onBackdropPress} onSellect={onSellectGender} visible={showModelGender} />
            <ModalTipsMatching typeFast onBackdropPress={onBackdropPress} onClose={onHideTips} visible={showModelTipsFast} />

            <IntroducingTextInputScreen
                value={valueIntroduction}
                onChange={onChangeIntroduction}
                visible={showIntroduction}
                onExit={onBackIntroduction}
                onBack={onBackIntroduction}
                onSubmitOnePoin={onSubmitOnePoin}
                onSubmitTwoPoin={onSubmitTwoPoin} />
            <View style={styles.content}>
                <View style={styles.wrapTips}>
                    <View>
                        <_Text style={{ color: colors.mainColor }}>매칭 조건</_Text>
                    </View>
                    <TouchableOpacity onPress={onShowTips}>
                        <Image source={Icons.ic_tips} />
                    </TouchableOpacity>
                </View>

                <View style={styles.wrapGender}>
                    <View>
                        <_Text>성별</_Text>
                    </View>
                    <View style={styles.rowCenter}>
                        <_Text>{" "}</_Text>
                        <TouchableOpacity onPress={onShowModal}>
                            <_Text style={{ color: colors.mainColor }}>{nameGender}</_Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ width: '100%', paddingHorizontal: 15 }}>
                    <View style={{ width: '100%', borderBottomColor: '#C6C6C8', borderBottomWidth: 0.5 }} />
                </View>
                <View style={styles.wrapAge}>
                    <View style={{ width: '100%' }}>
                        <_Text style={{ marginBottom: 40 }}>나이</_Text>
                    </View>
                    <MultiSlider
                        selectedStyle={{ backgroundColor: colors.mainColor }}
                        markerStyle={styles.market}
                        onValuesChange={multiSliderValuesChange}
                        values={[listAge[0], listAge[1]]}
                        minMarkerOverlapDistance={
                            (GAP_NUMBER * (screen.widthscreen - 40)) / (50 - 15)
                        }
                        allowOverlap={false}
                        min={15}
                        max={50}
                        enableLabel
                        sliderLength={screen.widthscreen - 40}
                        customLabel={CustomLabelAge}
                    />
                </View>
                <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 15 }}>
                    <View style={{ width: '100%', borderBottomColor: '#C6C6C8', borderBottomWidth: 0.5 }} />
                </View>
                <View style={styles.wrapChooseGame}>
                    <_Text>게임 종류</_Text>
                    <View style={{ paddingHorizontal: 20 }}>
                        {nameGame !== '' && <View style={styles.listItem}>
                            <_Text style={styles.nameListItem}>{nameGame}</_Text>
                            <TouchableOpacity style={styles.deleteGame} onPress={onDeleteGame}>
                                <Image source={Icons.ic_closed} />
                            </TouchableOpacity>
                        </View>}
                    </View>
                    <TouchableOpacity style={styles.addGame} onPress={onAddGame}>
                        <Image style={{ tintColor: colors.mainColor }} source={Icons.ic_search} />
                    </TouchableOpacity>
                </View>
            </View>
            {isRequirChooseGame && <_Text style={styles.errorText}>게임 종류를 선택해주세요!</_Text>}
            <View style={styles.wrapBtn}>
                <ButtonMatch disabled={disabaleMatch}
                    onPress={onMatching}
                    title="지금 바로 매칭하기"
                    backgroundColor={disabaleMatch ? colors.grayLight : colors.mainColor}
                    iconRight poin={1}
                    style={{ marginBottom: 10 }} />
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    content: {
        paddingBottom: 10,
        width: '100%',
        backgroundColor: colors.white,
        marginBottom: 10
    },
    wrapTips: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#ECF1F9'
    },
    wrapGender: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 10
    },
    wrapAge: {
        width: '100%',
        paddingHorizontal: 10,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    wrapChooseGame: {
        paddingHorizontal: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    addGame: {
        paddingLeft: 15,
        paddingVertical: 10,
    },
    deleteGame: {
        paddingVertical: 3,
        paddingHorizontal: 5,
    },
    listItem: {
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 1,
        borderColor: colors.mainColor,
    },
    nameListItem: {
        color: colors.mainColor,
        marginRight: 10,
    },
    wrapBtn: {
        width: screen.widthscreen,
        paddingHorizontal: 20,
    },
    errorText: {
        color: colors.red,
        marginBottom: 20,
    },
    market: {
        backgroundColor: colors.white,
        width: 30,
        height: 30,
        elevation: 10,
        borderRadius: 30,
    },
    rowCenter: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});

