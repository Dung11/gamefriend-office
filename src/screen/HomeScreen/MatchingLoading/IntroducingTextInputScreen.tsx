
import React from 'react';
import { StyleSheet, View, Image, Modal } from 'react-native';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { colors } from '../../../config/Colors';
import { Icons, Images } from '../../../assets';
import { TouchableOpacity } from 'react-native';
import ButtonMatch from '../component/ButtonMatch';
import InpuTextArea from '../../../components/Input/InputTextArea';
import DeviceInfo from 'react-native-device-info';
import _Text from '../../../components/_Text';

interface Props {
    visible: boolean,
    onBack?: () => void,
    onSubmitOnePoin?: () => void,
    onSubmitTwoPoin?: () => void,
    onChange?: (value: string) => void,
    value?: any,
    onExit?: () => void
}
export default function IntroducingTextInputScreen({
    visible,
    onBack,
    onSubmitOnePoin,
    onSubmitTwoPoin,
    onExit,
    onChange,
    value
}: Props) {

    const submitOnePoint = () => {
        onExit && onExit();
        onSubmitOnePoin && onSubmitOnePoin();
    }

    const submitTwoPoint = () => {
        onExit && onExit();
        onSubmitTwoPoin && onSubmitTwoPoin();
    }

    return (
        <Modal visible={visible} transparent>
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={onBack}>
                        <Image source={Icons.ic_back} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={onExit}>
                        <_Text style={styles.exit}>취소</_Text>
                    </TouchableOpacity>
                </View>
                <ContainerMain styles={styles.content}>
                    <View style={styles.wrapInput}>
                        <InpuTextArea
                            label="매칭 상대방에게 나에 대해서 알려주세요 :)"
                            value={value}
                            onChangeText={onChange}
                            placeholder="나에 대한 간단한 소개나 평소 게임 스타일 등 자유롭게 이야기해 보세요!" />
                    </View>
                    <ButtonMatch onPress={submitOnePoint} title="지금 바로 매칭하기" backgroundColor={colors.mainColor} iconRight poin={1} style={{ marginBottom: 10 }} />
                    <ButtonMatch onPress={submitTwoPoint} title="더 빠른 매칭하기" backgroundColor={colors.pink} iconLeft iconRight poin={2} />
                </ContainerMain>
            </View>

        </Modal>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        justifyContent: 'center',
        paddingHorizontal: 16,
        paddingVertical: 40,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        alignItems: 'center',
        paddingBottom: 10,
        paddingRight: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    label: {
        color: colors.grayLight
    },
    wrapInput: {
        width: '100%',
        paddingHorizontal: 10,
        marginVertical: 20,
    },
    exit: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: 18
    }
});

