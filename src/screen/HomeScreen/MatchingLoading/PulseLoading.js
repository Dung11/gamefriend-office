import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, TouchableOpacity, Animated, Easing, LogBox } from 'react-native';
import Pulse from './Pulse';
import { screen } from '../../../config/Layout';
import { colors } from '../../../config/Colors';


export default class PulseLoader extends React.Component {
	constructor(props) {
		super(props);
	
		this.state = {
			circles: []
		};

		this.counter = 1;
		this.setInterval = null;
		this.anim = new Animated.Value(1);
	}

	componentDidMount() {
		this.setCircleInterval();
		LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
	}
	componentWillUnmount(){
		clearInterval(this.setInterval)
	}
	setCircleInterval() {
		this.setInterval = setInterval(this.addCircle.bind(this), this.props.interval);
		this.addCircle();
	}

	addCircle() {
		this.setState({ circles: [...this.state.circles, this.counter] });
		this.counter++;
	}

	onPressIn() {
		Animated.timing(this.anim, {
			toValue: this.props.pressInValue,
			duration: this.props.pressDuration,
			easing: this.props.pressInEasing,
		}).start(() => clearInterval(this.setInterval));
	}

	onPressOut() {
		Animated.timing(this.anim, {
			toValue: 1,
			duration: this.props.pressDuration,
			easing: this.props.pressOutEasing,
		}).start(this.setCircleInterval.bind(this));
	}

	render() {
		const { size, avatarBackgroundColor, interval } = this.props;

		return (
			<View style={{
				// width: screen.widthscreen,
				// height: screen.heightscreen/4,
				justifyContent: 'center',
				alignItems: 'center',
				backgroundColor: 'gray'
			}}>
				{this.state.circles.map((circle) => (
					<Pulse
						key={circle}
						{...this.props}
					/>
				))}

				<View
					activeOpacity={1}
					style={{
						position: 'absolute',
						bottom: 65,
						height: 70,
						width: 70,
						borderRadius: 100,
						backgroundColor: colors.mainColor,
					}}/>
			</View>
		);
	}	
}

PulseLoader.propTypes = {
  interval: PropTypes.number,
  size: PropTypes.number,
  pulseMaxSize: PropTypes.number,
  avatar: PropTypes.string.isRequired,
  avatarBackgroundColor: PropTypes.string,
  pressInValue: PropTypes.number,
  pressDuration: PropTypes.number,
  borderColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  getStyle: PropTypes.func,
};

PulseLoader.defaultProps = {
  interval: 2000,
  size: 100,
  pulseMaxSize: 200,
  avatar: undefined,
  avatarBackgroundColor: 'white',
  pressInValue: 0.8,
  pressDuration: 150,
  pressInEasing: Easing.in,
  pressOutEasing: Easing.in,
  borderColor: colors.mainColor,
  backgroundColor: '#6AA1F5',
  getStyle: undefined,
};