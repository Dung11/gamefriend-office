
import React from 'react';
import { StyleSheet, View, StatusBar, Image, TouchableOpacity } from 'react-native';
import { Icons } from '../../assets';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import ModalSelectGender from '../../components/Modal/ModalSelectGender';
import IntroducingTextInputScreen from './MatchingLoading/IntroducingTextInputScreen';
import ButtonMatch from './component/ButtonMatch';
import ModalChooseGameLevel from './component/ModalChooseGameLevel';
import ModalTipsMatching from '../../components/Modal/ModalTipsMatching';
import CustomLabel from '../../components/CustomLabel/CustomLabel';
import CustomLabelAge from '../../components/CustomLabelAge/CustomLabelAge';
import _Text from '../../components/_Text';

const GAP_NUMBER = 4;

interface Props {
    showModelGender: boolean,
    showModelTipsFast: boolean,
    showIntroduction: boolean,
    showModelGameLevel: boolean,
    showModalDesiredGameLevel: boolean,
    disabaleMatch: boolean,
    listAge: any,
    nameGame: string,
    nameGender: string,
    gameLevel: string,
    desiredGameLevel: string,
    isRequirGameLevel: boolean,
    isRequirChooseGame: boolean,
    onShowModal: () => void,
    onSellectGender: (name: string, key: string) => void,
    onShowTips: () => void,
    onHideTips: () => void,
    multiSliderValuesChange: (data: any) => void,
    onAddGame: () => void,
    onDeleteGame: () => void,
    onMatching: () => void,
    onSubmitTwoPoin: () => void,
    onSubmitOnePoin: () => void,
    onBackIntroduction: () => void
    onFinishChooseGamelevel: (value: string) => void,
    onFinishChooseDesiredGamelevel: (value: string) => void,
    onChooseGameLevel: () => void,
    onDeleteChooseGameLevel: () => void,
    onDeleteChooseDesiredGameLevel: () => void,
    onChooseDesiredGameLevel: () => void,
    onBackdropPress: () => void
}
export default function MatchingDetail({
    showModelGender,
    showModelTipsFast,
    showIntroduction,
    showModelGameLevel,
    showModalDesiredGameLevel,
    disabaleMatch,
    listAge,
    nameGame,
    nameGender,
    gameLevel,
    desiredGameLevel,
    isRequirGameLevel,
    isRequirChooseGame,
    onShowModal,
    onSellectGender,
    onShowTips,
    onHideTips,
    multiSliderValuesChange,
    onAddGame,
    onDeleteGame,
    onMatching,
    onSubmitTwoPoin,
    onSubmitOnePoin,
    onBackIntroduction,
    onChooseGameLevel,
    onFinishChooseGamelevel,
    onDeleteChooseGameLevel,
    onDeleteChooseDesiredGameLevel,
    onChooseDesiredGameLevel,
    onFinishChooseDesiredGamelevel,
    onBackdropPress,
}: Props) {

    return (
        <>
            <StatusBar barStyle="light-content" />
            <ModalTipsMatching typeDetail onBackdropPress={onBackdropPress} onClose={onHideTips} visible={showModelTipsFast} />
            <ModalSelectGender onBackdropPress={onBackdropPress} onSellect={onSellectGender} visible={showModelGender} />
            <ModalChooseGameLevel onBackdropPress={onBackdropPress} onSellect={onFinishChooseGamelevel} visible={showModelGameLevel} />
            <ModalChooseGameLevel onBackdropPress={onBackdropPress} onSellect={onFinishChooseDesiredGamelevel} visible={showModalDesiredGameLevel} />

            <IntroducingTextInputScreen
                visible={showIntroduction}
                onExit={onBackIntroduction}
                onBack={onBackIntroduction}
                onSubmitOnePoin={onSubmitOnePoin}
                onSubmitTwoPoin={onSubmitTwoPoin} />
            <View style={styles.content}>
                <View style={styles.wrapTips}>
                    <View>
                        <_Text style={{ color: colors.mainColor }}>매칭 조건</_Text>
                    </View>
                    <TouchableOpacity onPress={onShowTips}>
                        <Image source={Icons.ic_tips} />
                    </TouchableOpacity>
                </View>

                <View style={styles.wrapGender}>
                    <View>
                        <_Text>성별</_Text>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <_Text>{" "}</_Text>
                        <TouchableOpacity onPress={onShowModal}>
                            <_Text style={{ color: colors.mainColor }}>{nameGender}</_Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={{ width: '100%', paddingHorizontal: 15 }}>
                    <View style={{ width: '100%', borderBottomColor: '#C6C6C8', borderBottomWidth: 0.5 }} />
                </View>
                <View style={styles.wrapAge}>
                    <View style={{ width: '100%' }}>
                        <_Text style={{ marginBottom: 40, textAlign: 'left' }}>나이</_Text>
                    </View>
                    <MultiSlider
                        selectedStyle={{ backgroundColor: colors.mainColor }}
                        markerStyle={styles.market}
                        onValuesChange={multiSliderValuesChange}
                        values={[listAge[0], listAge[1]]}
                        minMarkerOverlapDistance={
                            (GAP_NUMBER * (screen.widthscreen - 40)) / (50 - 15)
                        }
                        allowOverlap={false}
                        min={15}
                        max={50}
                        enableLabel
                        sliderLength={screen.widthscreen - 40}
                        customLabel={CustomLabelAge}
                    />
                </View>
                <View style={{ width: '100%', paddingHorizontal: 15, paddingVertical: 15 }}>
                    <View style={{ width: '100%', borderBottomColor: '#C6C6C8', borderBottomWidth: 0.5 }} />
                </View>
                <View style={styles.wrapChooseGame}>
                    <_Text>게임 종류</_Text>
                    <View style={{ paddingHorizontal: 20, alignItems: 'flex-start' }}>
                        {nameGame !== '' && <View style={styles.listItem}>
                            <_Text style={styles.nameListItem}>{nameGame}</_Text>
                            <TouchableOpacity style={styles.deleteGame} onPress={onDeleteGame}>
                                <Image source={Icons.ic_closed} />
                            </TouchableOpacity>
                        </View>}
                    </View>
                    <TouchableOpacity style={styles.addGame} onPress={onAddGame}>
                        <Image style={{ tintColor: colors.mainColor }} source={Icons.ic_search} />
                    </TouchableOpacity>
                </View>
                <View style={[styles.wrapChooseLevelGame, isRequirGameLevel && { backgroundColor: colors.grayLight }]}>
                    <_Text>나의 게임 티어 </_Text>
                    <View style={{ paddingHorizontal: 20, alignItems: 'flex-start' }}>
                        {gameLevel !== '' && <View style={styles.listItem}>
                            <_Text style={styles.nameListItem}>{gameLevel}</_Text>
                            <TouchableOpacity style={styles.deleteGame} onPress={onDeleteChooseGameLevel}>
                                <Image source={Icons.ic_closed} />
                            </TouchableOpacity>
                        </View>}
                    </View>
                    <TouchableOpacity disabled={isRequirGameLevel} style={styles.select} onPress={onChooseGameLevel}>
                        <Image style={{ tintColor: colors.mainColor }} source={Icons.ic_select} />
                    </TouchableOpacity>
                </View>
                <View style={[styles.wrapChooseDesiredLevelGame, isRequirGameLevel && { backgroundColor: colors.grayLight }]}>
                    <_Text>원하는 게임 티어</_Text>
                    <View style={{ paddingHorizontal: 20, alignItems: 'flex-start' }}>
                        {desiredGameLevel !== '' && <View style={styles.listItem}>
                            <_Text style={styles.nameListItem}>{desiredGameLevel}</_Text>
                            <TouchableOpacity style={styles.deleteGame} onPress={onDeleteChooseDesiredGameLevel}>
                                <Image source={Icons.ic_closed} />
                            </TouchableOpacity>
                        </View>}
                    </View>
                    <TouchableOpacity disabled={isRequirGameLevel} style={styles.select} onPress={onChooseDesiredGameLevel}>
                        <Image style={{ tintColor: colors.mainColor }} source={Icons.ic_select} />
                    </TouchableOpacity>
                </View>
            </View>
            {isRequirChooseGame && <_Text style={styles.errorText}>게임 종류를 선택해주세요!</_Text>}
            <_Text style={styles.warningText}>세부 매칭은 대기시간이 길어질 수 있습니다.</_Text>
            <View style={styles.wrapBtn}>
                <ButtonMatch disabled={disabaleMatch}
                    onPress={onMatching}
                    title="지금 바로 매칭하기"
                    backgroundColor={disabaleMatch ? colors.grayLight : colors.mainColor}
                    iconRight poin={1}
                    style={{ marginBottom: 10 }} />
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    content: {
        width: '100%',
        backgroundColor: colors.white,
        marginBottom: 10,
    },
    wrapTips: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: '#ECF1F9',
    },
    wrapGender: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    btnMatch: {
        marginTop: 30,
        paddingTop: 15,
        paddingBottom: 10,
        width: screen.widthscreen / 1.1,
        backgroundColor: colors.mainColor,
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 7,
    },
    wrapAge: {
        width: '100%',
        paddingHorizontal: 10,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    wrapChooseLevelGame: {
        width: '100%',
        paddingHorizontal: 10,
        paddingTop: 10,
        marginTop: 15,
        borderTopColor: colors.grayLight,
        borderTopWidth: 0.5,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center',
        paddingBottom: 10,
    },
    wrapChooseDesiredLevelGame: {
        width: '100%',
        paddingHorizontal: 10,
        paddingTop: 10,
        borderTopColor: colors.grayLight,
        borderTopWidth: 0.5,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center',
        paddingBottom: 10,
    },
    wrapChooseGame: {
        paddingHorizontal: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    deleteGame: {
        paddingVertical: 3,
        paddingHorizontal: 5,
    },
    listItem: {
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 1,
        borderColor: colors.mainColor,
    },
    nameListItem: {
        color: colors.mainColor,
        marginRight: 10,
    },
    wrapBtn: {
        width: screen.widthscreen,
        paddingHorizontal: 20
    },
    warningText: {
        color: colors.warning,
        marginBottom: 20,
        marginHorizontal: 5,
    },
    select: {
        height: 25,
        alignItems: 'flex-end',
        justifyContent: 'center',
        width: 50,
    },
    addGame: {
        paddingLeft: 15,
        paddingVertical: 10,
    },
    errorText: {
        color: colors.red,
        marginBottom: 20,
    },
    market: {
        backgroundColor: colors.white,
        width: 30,
        height: 30,
        elevation: 10,
        borderRadius: 30,
    },
});

