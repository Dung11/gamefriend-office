import React from 'react';
import { TouchableOpacity, ViewStyle, Dimensions, StyleSheet, Image } from 'react-native';
import { Icons, Images } from '../../../assets';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';

interface Props {
	title: string;
	backgroundColor?: string;
	onPress?: () => any;
	style?: ViewStyle;
	titleBlack?: boolean;
	iconRight?: boolean;
	iconLeft?: boolean;
	urlIcon?: any;
	poin?: number;
	disabled?: boolean,
}

export default ({
	title,
	onPress,
	style,
	backgroundColor,
	iconRight,
	iconLeft,
	poin,
	disabled,
}: Props) => {
	const containerStyles: ViewStyle = {
		...styles.wrapper,
		...style,
		...{
			backgroundColor: backgroundColor,
		},
	};
	return (
		<TouchableOpacity disabled={disabled} style={containerStyles} onPress={onPress}>
			{ iconLeft && <Image style={styles.iconLeft} source={Images.confirm_vector} />}
			<_Text style={styles.title}>{title}</_Text>
			{ iconRight && <Image style={styles.iconRight} source={Icons.ic_game} />}
			{ poin && <_Text style={styles.poin}>{poin}</_Text>}
		</TouchableOpacity>
	)
};

const screen = Dimensions.get("window");
const styles = StyleSheet.create({
	wrapper: {
		borderRadius: 7,
		paddingVertical: 5,
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%',
		marginVertical: 5,
		flexDirection: 'row',
	},
	title: {
		marginHorizontal: 10,
		fontSize: screen.width / 20,
		color: colors.white,
		fontWeight: "900",
		textAlign: 'left',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 24,
	},
	wrapTitle: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	iconLeft: {
		width: 20,
		height: 20,
		marginLeft: 20
	},
	iconRight: {
		marginTop: 10
	},
	poin: {
		color: colors.white,
	},
});
