import React from "react";
import { View, TouchableOpacity, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import { screen } from "../../../config/Layout";
import { AppState } from "../../../redux/store";
import Modal from 'react-native-modal';
import { renderItem } from "../../../@types";
import { colors } from "../../../config/Colors";
import _Text from "../../../components/_Text";

interface Props {
    visible: boolean,
    onSellect: (name: string) => void;
    dataTierGame?: any;
    onBackdropPress: () => void;
}

const ModalChooseGameLevel = (props: Props) => {
    const _renderItem = ({ item, index }: renderItem) => {
        return (
            <TouchableOpacity key={index} style={[styles.WrapperItem, index === 0 && { borderTopColor: colors.white }]} onPress={() => { props.onSellect(item) }}>
                <_Text style={styles.Text}>{item}</_Text>
            </TouchableOpacity>
        );
    }
    return (
        <Modal isVisible={props.visible} onBackdropPress={props.onBackdropPress} propagateSwipe>
            <View style={styles.Wrapper}>
                <View style={styles.Content}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={props?.dataTierGame?.data?.game?.tiers}
                        renderItem={_renderItem} />
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    Wrapper: {
        justifyContent: "center",
        alignItems: "center",
    },
    Content: {
        backgroundColor: "white",
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10,
    },
    WrapperItem: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        borderTopWidth: 0.5,
        borderTopColor: '#e3f0ee',
        width: '100%',
        flexDirection: 'row',
        height: screen.heightscreen / 16,
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10,
    },
    Text: {
        fontWeight: '900',
    },
});

const mapStateToProps = (state: AppState) => ({
    dataTierGame: state.TireGameReducer.tierGame
});

export default connect(mapStateToProps, null)(ModalChooseGameLevel);
