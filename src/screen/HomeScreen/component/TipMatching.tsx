import React, { useEffect, useState } from "react";
import { View, TouchableOpacity, StyleSheet, FlatList, Image, Alert } from "react-native";
import { connect } from "react-redux";
import { screen } from "../../../config/Layout";
import { AppState } from "../../../redux/store";
import Modal from 'react-native-modal';
import { renderItem } from "../../../@types";
import { colors } from "../../../config/Colors";
import _Text from "../../../components/_Text";
import { Icons } from "../../../assets";
import Carousel from "react-native-snap-carousel";
import { getTip } from "../../../api/ChatServices";

interface Props {

}

export const TipMatch = () => {

    const [listText, setListText] = useState([])

    const getTipMatch = async () => {
        try {
            let result = await getTip()
            if (result) {
                setListText(result.matching)
            } else {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
            console.log(error)
        }
    }

    useEffect(()=>{
        getTipMatch()
    },[])

    const _renderItem = ({ item, index }: renderItem) => {
        return (
            <View key={index}>
                <_Text>{item}</_Text>
            </View>
        );
    }
    return (
        <View style={styles.wrapTip}>
            <View style={styles.wrapLableTip}>
                <View style={styles.row}>
                    <Image source={Icons.ic_game_controller} />
                    <_Text style={styles.label}>{" "}겜친</_Text>
                </View>
                <_Text style={styles.label}>지금</_Text>
            </View>
            <_Text style={styles.tip}>팁:</_Text>
            <Carousel
                // ref={c => carouselRef = c}
                firstItem={1}
                data={listText}
                renderItem={_renderItem}
                sliderWidth={screen.widthscreen-40}
                itemWidth={screen.widthscreen-40}
                loop={true}
                lockScrollTimeoutDuration={1000}
                autoplay
            />

        </View>
    );
};

const styles = StyleSheet.create({
    wrapTip: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 7,
    },
    wrapLableTip: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
    tip: {
        fontWeight: '900',
    },
    label: {
        color: colors.grayLight,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});




