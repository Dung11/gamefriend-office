import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { RootStackParamList } from "../../../@types";
import { Images } from "../../../assets";
import _Text from "../../../components/_Text";
import { colors } from "../../../config/Colors";
import { screen } from "../../../config/Layout";

type Params = {
    params: {
        roomId: any,
    },
}
interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default ({
    navigation,
    route
}: Props) => {
    const [roomId, setRoomId] = useState()

    useEffect(() => {
        setRoomId(route.params.roomId)
    }, [])
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Image source={Images.match_done} />
                <View style={styles.info}>
                    <_Text style={styles.title}>매칭완료</_Text>
                </View>

                <View style={styles.wrapBtn}>
                    <TouchableOpacity style={[styles.btn, { backgroundColor: colors.backgroundApp }]} onPress={() => {
                        navigation.replace("Main")
                    }}>
                        <_Text style={[styles.titleBtn, { color: colors.black }]}>취소</_Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => {
                        navigation.navigate("ChatMatch", { screen: 'ChatDetailMatch', params: { roomId } })
                    }}>
                        <_Text style={styles.titleBtn}>지금 바로 대화하기</_Text>
                    </TouchableOpacity>
                </View>

            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: { flex: 1, paddingHorizontal: 16, alignItems: 'center', justifyContent: "center" },
    content: { backgroundColor: colors.white, width: screen.widthscreen - 20, alignItems: 'center', paddingVertical: 20, borderRadius: 10 },
    info: {
        marginVertical: 10
    },
    title: {
        fontWeight: '900',
        fontSize: 20,
        marginVertical: 10
    },
    txt: {
        fontSize: 14,
        color: colors.gray,
        marginVertical: 10
    },
    btn: {
        width: '45%',
        backgroundColor: colors.mainColor,
        paddingVertical: 20,
        borderRadius: 10,
        marginHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleBtn: {
        color: colors.white
    },
    wrapBtn: {
        marginTop: 10,
        flexDirection: 'row',
        width: screen.widthscreen - 30,
        justifyContent: "center",
    }
});
