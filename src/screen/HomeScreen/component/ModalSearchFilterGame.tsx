import React, { useEffect, useState } from 'react';
import { TextInput, StyleSheet, View, Platform, Image, TouchableOpacity, FlatList, StatusBar, Alert } from "react-native";
import { Icons } from '../../../assets';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import Modal from 'react-native-modal';
import DeviceInfo from 'react-native-device-info';
import _Text from '../../../components/_Text';
import { getListGame } from '../../../api/GameServices';

interface Props {
    visible: boolean,
    onFinish: (data: any) => void,
    onBack: () => void
}
export const ModalSearchFilterGame = ({
    visible, onBack, onFinish
}: Props) => {
    const [isOK, setIsOk] = useState(false);
    const [fullLisDataPC, setFullListDataPC] = useState<any[]>([]);
    const [fullLisDataMB, setFullListDataMB] = useState<any[]>([]);
    const [fullLisDataST, setFullListDataST] = useState<any[]>([]);

    const [showItemPC, setShowItemPC] = useState(false);
    const [showItemMB, setShowItemMB] = useState(false);
    const [showItemST, setShowItemST] = useState(false);
    const [lisDataPC, setListDataPC] = useState<any[]>([]);
    const [lisDataMB, setListDataMB] = useState<any[]>([]);
    const [lisDataST, setListDataST] = useState<any[]>([]);
    const [search, setSearch] = useState('');
    const [dataResult, setDataResult] = useState<any>();
    const [data, setData] = useState<any[]>([]);
    // const callbackFavorite = route?.params?.resultFavoriteGame;
    // const callback = route?.params?.result;
    const numberOfChoice = 1;
    const onDropDowmPC = () => {
        setShowItemPC(!showItemPC)
    }

    const onDropDowmMB = () => {
        setShowItemMB(!showItemMB)
    }

    const onDropDowmST = () => {
        setShowItemST(!showItemST)
    }

    const onChangeSearch = (value: any) => {
        setSearch(value)
        setIsOk(true)
    }

    const checkIsExisted = (_id: number) => {
        let index = data.findIndex((d) => d.id === _id);
        if (index != -1) return true;
        return false;
    }


    const onClickPC = (id: any, name: string, index: number) => {
        const selectedItem = lisDataPC[index];
        let tempData = [...lisDataPC];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };
        setListDataPC(tempData);
        if (numberOfChoice === 1) {
            setDataResult({ name, id });
        } else {
            let tmpData = [...data];
            let checkExisted = checkIsExisted(id);
            if (!checkExisted) {
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `${numberOfChoice}개까지만 선택 가능합니다`);
                } else {
                    tmpData.push({ name, id });
                    setData(tmpData);
                }
            } else {
                tmpData = tmpData.filter((d) => d.id !== id);
                setData(tmpData);
            }
        }

    }

    const onClickMB = (id: any, name: string, index: number) => {
        const selectedItem = lisDataMB[index];
        let tempData = [...lisDataMB];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };
        setListDataMB(tempData);
        if (numberOfChoice === 1) {
            setDataResult({ name, id });
        } else {
            let tmpData = [...data];
            let checkExisted = checkIsExisted(id);
            if (!checkExisted) {
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `${numberOfChoice}개까지만 선택 가능합니다`);
                } else {
                    tmpData.push({ name, id });
                    setData(tmpData);
                }
            } else {
                tmpData = tmpData.filter((d) => d.id !== id);
                setData(tmpData);
            }
        }

    }

    const onClickST = (id: any, name: string, index: number) => {
        const selectedItem = lisDataST[index];
        let tempData = [...lisDataST];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };
        setListDataST(tempData);
        if (numberOfChoice === 1) {
            setDataResult({ name, id });
        } else {
            let tmpData = [...data];
            let checkExisted = checkIsExisted(id);
            if (!checkExisted) {
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `${numberOfChoice}개까지만 선택 가능합니다`);
                } else {
                    tmpData.push({ name, id });
                    setData(tmpData);
                }
            } else {
                tmpData = tmpData.filter((d) => d.id !== id);
                setData(tmpData);
            }
        }

    }

    const getGamePC = async () => {
        try {
            const response = await getListGame('pc')
            setListDataPC(response?.data.games);
            setFullListDataPC(response?.data.games);
        } catch (error) {
            console.log("error >>>", error)
        }
    }

    const getGameMB = async () => {
        try {
            const response = await getListGame('mobile')
            setListDataMB(response?.data.games);
            setFullListDataMB(response?.data.games);
        } catch (error) {
            console.log("error >>>", error)
        }
    }

    const getGameST = async () => {
        try {
            const response = await getListGame('steam')
            setListDataST(response?.data.games);
            setFullListDataST(response?.data.games);
        } catch (error) {
            console.log("error >>>", error)
        }
    }

    useEffect(() => {
        getGamePC()
        getGameMB()
        getGameST()
    }, [])

    const renderItemPC = ({ item, index }: any) => {
        if (numberOfChoice === 1) {
            return (
                <TouchableOpacity key={index}
                    style={[styles.listItem, item.id === dataResult?.id && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                    onPress={() => onClickPC(item.id, item.name, index)}>
                    <_Text numberOfLines={2} style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>{item.name}</_Text>
                </TouchableOpacity>

            )
        } else {
            return (
                <TouchableOpacity key={index}
                    style={[styles.listItem, checkIsExisted(item.id) === true && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                    onPress={() => onClickPC(item.id, item.name, index)}>
                    <_Text numberOfLines={2} style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>{item.name}</_Text>
                </TouchableOpacity>

            )
        }
    }
    const renderItemMB = ({ item, index }: any) => {
        if (numberOfChoice === 1) {
            return (
                <TouchableOpacity key={index}
                    style={[styles.listItem, item.id === dataResult?.id && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                    onPress={() => onClickMB(item.id, item.name, index)}>
                    <_Text numberOfLines={2} style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>{item.name}</_Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity key={index}
                    style={[styles.listItem, checkIsExisted(item.id) === true && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                    onPress={() => onClickMB(item.id, item.name, index)}>
                    <_Text numberOfLines={2} style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>{item.name}</_Text>
                </TouchableOpacity>

            )
        }
    }
    const renderItemST = ({ item, index }: any) => {
        if (numberOfChoice === 1) {
            return (
                <TouchableOpacity key={index}
                    style={[styles.listItem, item.id === dataResult?.id && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                    onPress={() => onClickST(item.id, item.name, index)}>
                    <_Text numberOfLines={2} style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>
                        {item.name}
                    </_Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity key={index}
                    style={[styles.listItem, checkIsExisted(item.id) === true && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                    onPress={() => onClickST(item.id, item.name, index)}>
                    <_Text numberOfLines={2} style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>{item.name}</_Text>
                </TouchableOpacity>

            )
        }
    }

    return (
        <Modal isVisible={visible} deviceHeight={screen.heightscreen} coverScreen={false} propagateSwipe deviceWidth={screen.widthscreen}>
            <ContainerMain styles={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={onBack}>
                        <Image source={Icons.ic_back} />
                    </TouchableOpacity>
                    {/* {isOK && */}
                    <TouchableOpacity onPress={() => { onFinish(dataResult) }}>
                        <_Text style={styles.ok}>완료</_Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.wrapInput}>
                    <Image source={Icons.ic_search} />
                    <TextInput style={styles.textInput} value={search} onChangeText={(value: any) => { onChangeSearch(value) }} />
                    <Image source={Icons.ic_microphone} />
                </View>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmPC} style={[styles.wrapDropDowm, { backgroundColor: showItemPC ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>PC게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemPC ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_raphael_pc} />
                    </TouchableOpacity>
                    {showItemPC &&
                        <View style={{ height: screen.heightscreen / 5.5 }}>
                            <FlatList
                                contentContainerStyle={styles.flatList}
                                data={lisDataPC}
                                numColumns={3}
                                renderItem={renderItemPC}
                                keyExtractor={item => item.id}
                            />
                        </View>
                    }
                </View>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmMB} style={[styles.wrapDropDowm, { backgroundColor: showItemMB ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>Mobile게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemMB ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_mobile_vibration} />
                    </TouchableOpacity>
                    {showItemMB &&
                        <View style={{ height: screen.heightscreen / 6.5 }}>
                            <FlatList
                                contentContainerStyle={styles.flatList}
                                data={lisDataMB}
                                numColumns={3}
                                renderItem={renderItemMB}
                                keyExtractor={item => item.id}
                            />
                        </View>
                    }
                </View>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmST} style={[styles.wrapDropDowm, { backgroundColor: showItemST ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>Steam게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemST ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_cib_steam} />
                    </TouchableOpacity>
                    {showItemST &&
                        <View style={{ height: screen.heightscreen / 6.5, alignItems: 'center' }}>
                            <FlatList
                                contentContainerStyle={styles.flatList}
                                data={lisDataST}
                                numColumns={3}
                                renderItem={renderItemST}
                                keyExtractor={item => item.id}
                            />
                        </View>
                    }
                </View>
            </ContainerMain>
        </Modal>
    );
}


const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: screen.heightscreen / 12,
        backgroundColor: colors.mainColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
        marginBottom: 10,
    },
    container: {
        alignItems: 'center',
        flex: 1,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    ok: {
        color: colors.white,
        fontSize: 17,
        fontWeight: '400',
    },
    wrapInput: {
        width: '95%',
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingVertical: Platform.OS === 'ios' ? 15 : 0,
        flexDirection: 'row',
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: screen.widthscreen / 30,
        width: '90%',
    },
    iconLeft: {
        resizeMode: 'contain',
        height: 20,
        width: 20,
    },
    listItem: {
        borderColor: '#EDEDED',
        backgroundColor: '#EDEDED',
        height: screen.heightscreen / 23,
        width: screen.widthscreen / 4,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 30,
        borderWidth: 1,
    },
    nameListItem: {
        color: '#65676B',
        textAlign: 'center',
        fontSize: 12,
    },
    wrapItem: {
        paddingHorizontal: 3,
        paddingVertical: 3,
        backgroundColor: colors.white,
        borderRadius: 10,
        marginVertical: 10,
    },
    wrapDropDowm: {
        paddingHorizontal: 15,
        justifyContent: "flex-end",
        borderRadius: 10,
        backgroundColor: 'white',
        width: screen.widthscreen - 32,
        height: screen.heightscreen / 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    labelDropDowm: {
        color: colors.mainColor,
        fontSize: 16,
    },
    flatList: {
        alignItems: 'center',
        marginTop: 10,
    },
});
