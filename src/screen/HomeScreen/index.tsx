
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, ScrollView, Alert, DeviceEventEmitter, Image } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import DeviceInfo from 'react-native-device-info';
import messaging from '@react-native-firebase/messaging';
import { useNavigation } from '@react-navigation/native';

import { Images } from '../../assets';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';
import { ContainerMain } from '../../components/Container/ContainerMain';
import MainHeader from '../../components/MainHeader/MainHeader';
import MatchingFast from './MatchingFast';
import MatchingNeighborhood from './MatchingNeighborhood';
import MatchingDetail from './MatchingDetail';
import { addHandler, removeHandler, sendIntroMatch } from '../../socketio/socketController';
import { keyValueStorage } from '../../storage/keyValueStorage';
import store from '../../redux/store';
import { tierGameRequest } from '../../redux/tiergame/actions';
import _Text from '../../components/_Text';
import { saveTopics } from '../../utils/setupfirebase';
import { joinLocalRoom } from '../../api/LivetalkServices';
import { getMatching, getStatusMatching, stopMatching } from '../../api/MatchingServices';


const data = [
    {
        name: "실시간 맞춤 매칭",
        uri: Images.matching_custom_rt,
    },
    {
        name: "실시간 빠른 매칭",
        uri: Images.matching_realtime,
    },
    {
        name: "실시간 동네 매칭",
        uri: Images.matching_neighborhood,
    },
];

export default function HomeScreen() {

    let carouselRef: any;
    let currentMatchId: any;
    const listAge = [22, 35];

    const navigation = useNavigation();
    const [showModelGender, setShowModalGender] = useState(false);
    const [showModelTipsFast, setShowModelTipsFast] = useState(false);
    const [showModalDesiredGameLevel, setShowModalDesiredGameLevel] = useState(false);
    const [showModelGameLevel, setShowModalGameLevel] = useState(false);

    const [colorStatusDetail, setColorStatusDetail] = useState('#8E8E92');
    const [colorStatusFast, setColorStatusFast] = useState('#8E8E92');
    const [colorStatusNeighborhood, setColorStatusNeighborhood] = useState('#8E8E92');
    const [nameStatusDetail, setNameStatusDetail] = useState('');
    const [nameStatusFast, setNameStatusFast] = useState('');
    const [nameStatusNeighborhood, setNameStatusNeighborhood] = useState('');
    const [switchItem, setSwitchItem] = useState(1);
    const [distance, setDistance] = useState([10]);
    const [ageYoung, setAgeYoung] = useState(15);
    const [ageOld, setAgeOld] = useState(30);
    const [gender, setGender] = useState('any');
    const [gameId, setGameId] = useState('');
    const [nameGame, setNameGame] = useState('');
    const [nameGender, setNameGender] = useState('상관없음');
    const [gameLevel, setGameLevel] = useState('')
    const [desiredGameLevel, setDesiredGameLevel] = useState('');
    const [isRequirGameLevel, setIsRequirGameLevel] = useState(true);
    const [isRequirChooseGame, setIsRequirChooseGame] = useState(false);
    const [historyChooseGame, setHistoryChooseGame] = useState<any>();
    /// matching
    const [showIntroduction, setShowIntroduction] = useState(false);
    const [disabaleMatch, setDisableMatch] = useState(false);
    const [matchId, setMatchId] = useState('');
    const [valueIntroduction, setValueIntroduction] = useState('');

    const onShowModal = () => {
        setShowModalGender(true);
    }

    const onSellectGender = (name: string, key: string) => {
        setNameGender(name);
        setGender(key);
        setShowModalGender(false);
    }

    /// handle show popup tip
    const onShowTips = () => {
        setShowModelTipsFast(true);
    }

    const onHideTips = () => {
        setShowModelTipsFast(false);
    }
    const onBackdropPress = () => {
        setShowModalGameLevel(false);
        setShowModalDesiredGameLevel(false);
        setShowModelTipsFast(false);
        setShowModalGender(false);
    }

    /// handle choose Game
    const multiSliderValuesChange = (data: any) => {
        for (let index = 0; index < data.length; index++) {
            if (index === 0) {
                setAgeYoung(data[index]);
            }
            if (index === 1) {
                setAgeOld(data[index]);
            }
        }
    }
    const multiSliderValuesChangeDistance = (data: any) => {
        setDistance(data);
    }
    /// handle choose game
    const onAddGame = () => {
        navigation.navigate('SearchFavouriteGame', {
            resultFavoriteGame,
            numberOfChoice: 1,
            historyChooseGame: historyChooseGame ? [historyChooseGame] : [],
            isTier: false,
        });
    };

    const onAddGameDetail = () => {
        navigation.navigate('SearchFavouriteGame', {
            resultFavoriteGame,
            numberOfChoice: 1,
            historyChooseGame: historyChooseGame ? [historyChooseGame] : [],
            isTier: true,
        });
    };

    const onDeleteGame = () => {
        setHistoryChooseGame(null);
        setNameGame('');
        setGameId('');
        setIsRequirGameLevel(true);
    }
    const resultFavoriteGame = (data: any) => {
        setHistoryChooseGame(data);
        if (data) {
            setNameGame(data?.name);
            setGameId(data?.id);
            setIsRequirGameLevel(false);
            setIsRequirChooseGame(false);
            store.dispatch(tierGameRequest(data.id));
        }
    }

    /// handle choose game level
    const onChooseGameLevel = () => {
        setShowModalGameLevel(true);
    }
    const onDeleteChooseGameLevel = () => {
        setGameLevel('');
    }
    const onFinishChooseGamelevel = (level: string) => {
        setGameLevel(level);
        setShowModalGameLevel(false);
    }

    const onDeleteChooseDesiredGameLevel = () => {
        setDesiredGameLevel('');
    }
    const onChooseDesiredGameLevel = () => {
        setShowModalDesiredGameLevel(true);
    }
    const onFinishChooseDesiredGamelevel = (level: string) => {
        setDesiredGameLevel(level);
        setShowModalDesiredGameLevel(false);
    }
    const renderItem = ({ item, index }: any) => {
        return (
            <View style={styles.wrapItemSlider} key={index}>
                <Image source={item.uri} />
                <_Text style={styles.nameItemSlider}>{item.name}</_Text>
            </View>
        )
    }

    const onChangeIntroduction = (value: string) => {
        setValueIntroduction(value);
    }

    const getStatusMatch = async () => {
        try {
            const res = await getStatusMatching();
            if (res.status === 200) {
                setColorStatusDetail(res.data.detailMatchingStatus.color);
                setNameStatusDetail(res.data.detailMatchingStatus.text);
                setColorStatusFast(res.data.quickMatchingStatus.color);
                setNameStatusFast(res.data.quickMatchingStatus.text);
                setColorStatusNeighborhood(res.data.locationMatchingStatus.color);
                setNameStatusNeighborhood(res.data.locationMatchingStatus.text);
            }
        } catch (error) {
            console.log(error);
        }
    }

    const onBeforeSnapToItem = (event: number) => {
        setSwitchItem(event);
        getStatusMatch();
        clearDataMatch()
    }

    const clearDataMatch = () => {
        setIsRequirGameLevel(true);
        setIsRequirChooseGame(false);
        setNameGame('');
        setGameId('');
        setGameLevel('');
        setDesiredGameLevel('');
        setHistoryChooseGame(null);
        setValueIntroduction('')
    }

    // handel matching
    const onMatching = async () => {
        if ((gameId !== '' && switchItem !== 0) ||
            ((gameLevel !== '' && desiredGameLevel !== '') && switchItem === 0)) {
            setIsRequirChooseGame(false);
            setShowIntroduction(true);
        } else if ((gameLevel === '' || desiredGameLevel === '') && switchItem === 0) {
            setIsRequirChooseGame(true);
        }
        else {
            setIsRequirChooseGame(true);
        }
    }

    const onConfirmOnePointFast = () => {
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'match',
                onSubmit: async () => onSubmitOnePoinFast(),
            },
        });
    }

    const onConfirmTwoPointFast = (optionalParams = {}) => {
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                ...optionalParams,
                type: 'priorMatch',
                onSubmit: async () => onSubmitTwoPoinFast(),
            },
        });
    }

    const onSubmitOnePoinFast = async () => {
        await keyValueStorage.save('messageIntroduction', valueIntroduction);
        let params: any = {
            type: 'quick',
            gender: gender,
            gameId: gameId,
            age: `${ageYoung}-${ageOld}`,
            isPrior: false,
        }

        try {
            const result = await getMatching(params);
            if (result.status === 200) {
                currentMatchId = result.data.matchId;
                // if (!disabaleMatch) {
                // }
                setMatchId(currentMatchId);
                setShowIntroduction(false);
            }
            else if (result.status === 402) {
                navigation.navigate('Modal', { screen: "AlertNoItem", params: data });
                setShowIntroduction(false);
            }
            else {
                Alert.alert('오류27', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error);
            Alert.alert(error);
        }
    }

    // handel submit Fast
    const onSubmitTwoPoinFast = async () => {
        await keyValueStorage.save('messageIntroduction', valueIntroduction);
        let params: any = {
            type: 'quick',
            gender: gender,
            gameId: gameId,
            age: `${ageYoung}-${ageOld}`,
            isPrior: true,
        }
        try {
            const result = await getMatching(params);
            if (result.status === 200) {
                currentMatchId = result.data.matchId;
                // if (!disabaleMatch) {
                // }
                setMatchId(currentMatchId);
                setShowIntroduction(false);
            }
            else if (result.status === 402) {
                navigation.navigate('Modal', { screen: "AlertNoItem", params: data });
                setShowIntroduction(false);
            }
            else {
                Alert.alert('오류28', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error.response);
            Alert.alert(error.response);
        }
    }

    // handel submit detail
    const onConfirmOnePointDetail = () => {
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'match',
                onSubmit: async () => onSubmitOnePoinDetail(),
            },
        });
    }

    const onConfirmTwoPointDetail = () => {
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'priorMatch',
                onSubmit: async () => onSubmitTwoPoinDetail(),
            },
        });
    }

    const onSubmitOnePoinDetail = async () => {
        await keyValueStorage.save('messageIntroduction', valueIntroduction);
        let params: any = {
            type: 'detail',
            gender: gender,
            gameId: gameId,
            age: `${ageYoung}-${ageOld}`,
            isPrior: false,
            tier: gameLevel,
            requestTier: desiredGameLevel,
        }

        try {
            const result = await getMatching(params);
            if (result.status === 200) {
                currentMatchId = result.data.matchId;
                // if (!disabaleMatch) {
                // }
                setMatchId(currentMatchId);
                setShowIntroduction(false);
            }
            else if (result.status === 402) {
                setShowIntroduction(false);
                navigation.navigate('Modal', { screen: "AlertNoItem", params: data });
            }
            else {
                Alert.alert('오류29', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error.response);
            Alert.alert(error.response);
        }
    }

    const onSubmitTwoPoinDetail = async (optionalParams = {}) => {
        await keyValueStorage.save('messageIntroduction', valueIntroduction);
        let params: any = {
            type: 'detail',
            gender: gender,
            gameId: gameId,
            age: `${ageYoung}-${ageOld}`,
            isPrior: true,
            tier: gameLevel,
            requestTier: desiredGameLevel,
        }
        try {
            const result = await getMatching(params)
            if (result.status === 200) {
                currentMatchId = result.data.matchId;
                // if (!disabaleMatch) {
                // }
                setMatchId(currentMatchId);
                setShowIntroduction(false);
            }
            else if (result.status === 402) {
                navigation.navigate('Modal', { screen: "AlertNoItem", params: data });
                setShowIntroduction(false);
            }
            else {
                Alert.alert('오류30', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error.response);
            Alert.alert(error.response);
        }
    }

    // handel submit Neighthood
    const onConfirmOnePointNeighborhood = () => {
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'match',
                onSubmit: async () => onSubmitOnePoinNeighborhood(),
            },
        });
    }

    const onConfirmTwoPointNeighborhood = () => {
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'priorMatch',
                onSubmit: async () => onSubmitTwoPoinNeighborhood(),
            },
        });
    }

    const onSubmitOnePoinNeighborhood = async () => {
        await keyValueStorage.save('messageIntroduction', valueIntroduction);

        let params: any = {
            type: 'location',
            gender: gender,
            gameId: gameId,
            age: `${ageYoung}-${ageOld}`,
            isPrior: false,
            distance: `0-${distance}`,
        }
        try {
            const result = await getMatching(params);
            if (result.status === 200) {
                currentMatchId = result.data.matchId;
                // if (!disabaleMatch) {
                // }
                setMatchId(currentMatchId);
                setShowIntroduction(false);
            }
            else if (result.status === 402) {
                navigation.navigate('Modal', { screen: "AlertNoItem", params: data });
                setShowIntroduction(false);
            }
            else {
                Alert.alert('오류31', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error.response);
            Alert.alert(error.response);
        }
    }

    const onSubmitTwoPoinNeighborhood = async (optionalParams = {}) => {
        await keyValueStorage.save('messageIntroduction', valueIntroduction)
        let params: any = {
            type: 'location',
            gender: gender,
            gameId: gameId,
            age: `${ageYoung}-${ageOld}`,
            isPrior: true,
            distance: `0-${distance}`,
        }
        try {
            const result = await getMatching(params);
            if (result.status === 200) {
                currentMatchId = result.data.matchId;
                // if (!disabaleMatch) {
                // }
                setMatchId(currentMatchId);
                setShowIntroduction(false);
            }
            else if (result.status === 402) {
                navigation.navigate('Modal', { screen: "AlertNoItem", params: data });
                setShowIntroduction(false);
            }
            else {
                Alert.alert('오류32', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error.response);
            Alert.alert(error.response);
        }
    }

    const onExitMatch = async () => {
        try {
            const params = {
                matchId: matchId,
            };
            const result = await stopMatching(params);
            if (result.status === 200) {
                setValueIntroduction('')
                setDisableMatch(false);
                setMatchId(currentMatchId);
                // removeHandler('match-status')
                DeviceEventEmitter.emit("showWattingMatchTwoPoint", { visible: false });
                DeviceEventEmitter.emit("showWattingMatch", { visible: false });
            }
        } catch (error) {
            console.log("Stop matching error:>", error.response);
        }
    }

    const onBackWaitngMatch = () => {
        setDisableMatch(true);
    }

    const onChangeMatchStatus = (data: any) => {
        setDisableMatch(data.disable);
    }

    const onBackIntroduction = () => {
        setValueIntroduction('')
        setShowIntroduction(false);
    }

    const acctionMatchTwoPoint = (optionalParams: any) => {
        if (switchItem === 0) {
            onSubmitTwoPoinDetail({ ...optionalParams });
        }
        if (switchItem === 1) {
            onConfirmTwoPointFast({ ...optionalParams });
            // onSubmitTwoPoinFast();
        }
        if (switchItem === 2) {
            onSubmitTwoPoinNeighborhood({ ...optionalParams });
        }
    }

    const onEnterWaitingLocalRoom = async (data: any) => {
        // console.log('Room id on popup component: ', data.roomId);
        var result = await joinLocalRoom(data.roomId);

        if (result.status == 200 || result.status == 409) {
            // console.log('success, ', data.roomId);
            const params: any = {
                roomId: data.roomId,
                roomType: 'livetalk-local'
            }

            if (data.isFull) {
                navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
            } else {
                navigation.navigate('WattingJoinRoom', { screen: 'WaitingLocalChat', params });
            }
        } else {
        }
    }

    const getFCMToken = async () => {
        try {
            const token = await messaging().getToken();
            if (token) {
                // console.log('FCM token: ', token);

                // *** Use this for local notification ***
                // PushNotification.createChannel(
                //     {
                //       channelId: "channel-id", // (required)
                //       channelName: "My channel", // (required)
                //       channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
                //       playSound: false, // (optional) default: true
                //       soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
                //       importance: 4, // (optional) default: 4. Int value of the Android notification importance
                //       vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
                //     },
                //     (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
                //   );

                return token;
            }
        } catch (error) {
            console.log(error);
        }
    }

    const subscribeToNotiTopics = async () => {
        const userId = await keyValueStorage.get('userID');
        await saveTopics('gamefriend');
        await saveTopics(`gamefriend_${userId}`);
    }

    useEffect(() => {
        console.log('PROCESS ENV: ', process.env);
        getFCMToken();
        subscribeToNotiTopics();

        const unsubscribe = messaging().onMessage(async (remoteMessage: any) => {
            let dataMatchDone = JSON.parse(remoteMessage.data.data);
            if (remoteMessage.data.type === 'match-done') {
                DeviceEventEmitter.emit("showWattingMatch", { visible: false });
                DeviceEventEmitter.emit("showWattingMatchTwoPoint", { visible: false });
                navigation.navigate('Modal', { screen: "AlertMatchingSuccess", params: dataMatchDone });
                sendIntroMatch(dataMatchDone.roomId);
                setDisableMatch(false);
                setMatchId(currentMatchId);
                clearDataMatch()
            }

            console.log('remote message: ', JSON.stringify(remoteMessage));

            //     await bulletinValueStorage.save('isViewNotify', 'false')
            // console.log("dzooo dayy hien local push notification")

            // *** Use this for local notification ***
            // PushNotification.localNotification({
            //     id: 0,
            //     channelId: "channel-id",
            //     title: "My Notification Title",
            //     message: "My Notification Message",
            //     vibrate: true,
            //     playSound: false,
            //     vibration: 300,
            //     priority: "max",
            //     visibility: "public",
            //     bigText: "My big text that will be shown when notification is expanded", 
            //     subText: "This is a subText",
            // });
        });
        return unsubscribe;
    }, []);

    /// delete data search live talk
    const removeKeySeacrh = async () => {
        await keyValueStorage.delete("datasearchlivetalk");
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {

            removeKeySeacrh()
        });
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {

        removeKeySeacrh()
        getStatusMatch();

        addHandler('match-done', (data: any) => {
            // removeHandler('match-done')
            DeviceEventEmitter.emit("showWattingMatch", { visible: false });
            DeviceEventEmitter.emit("showWattingMatchTwoPoint", { visible: false });

            navigation.navigate('Modal', { screen: "AlertMatchingSuccess", params: data });
            sendIntroMatch(data.roomId);
            setDisableMatch(false);
            setMatchId(currentMatchId);
            clearDataMatch()
        });

        addHandler('match-status', (data: any) => {
            setColorStatusDetail(data.detailMatchingStatus.color);
            setNameStatusDetail(data.detailMatchingStatus.text);

            setColorStatusFast(data.quickMatchingStatus.color);
            setNameStatusFast(data.quickMatchingStatus.text);

            setColorStatusNeighborhood(data.locationMatchingStatus.color);
            setNameStatusNeighborhood(data.locationMatchingStatus.text);
        })
        return () => {
            removeHandler('match-done');
            removeHandler('match-status');
        }
    }, []);

    useEffect(() => {
        DeviceEventEmitter.addListener('exitMatch', onExitMatch);
        DeviceEventEmitter.addListener('submitMatchTwoPoint', acctionMatchTwoPoint);
        DeviceEventEmitter.addListener('backWatingMatch', onBackWaitngMatch);
        DeviceEventEmitter.addListener('enterWaitingLocalRoom', onEnterWaitingLocalRoom);
        DeviceEventEmitter.addListener('changeMatchStatus', onChangeMatchStatus);
        return () => {
            DeviceEventEmitter.removeListener('submitMatchTwoPoint', acctionMatchTwoPoint);
            DeviceEventEmitter.removeListener('exitMatch', onExitMatch);
            DeviceEventEmitter.removeListener('backWatingMatch', onBackWaitngMatch);
            DeviceEventEmitter.removeListener('enterWaitingLocalRoom', onEnterWaitingLocalRoom);
            DeviceEventEmitter.removeListener('changeMatchStatus', onChangeMatchStatus);
        }
    }, [onExitMatch, acctionMatchTwoPoint, onBackWaitngMatch, onEnterWaitingLocalRoom, onChangeMatchStatus]);

    let submitOnePoint = onConfirmOnePointDetail;
    let submitTwoPoint = onConfirmTwoPointDetail;
    let statusBackground = colorStatusDetail;
    let statusName = nameStatusDetail;
    if (switchItem === 1) {
        statusBackground = colorStatusFast;
        statusName = nameStatusFast;
        submitOnePoint = onConfirmOnePointFast;
        submitTwoPoint = onConfirmTwoPointFast;
    } else if (switchItem === 2) {
        statusBackground = colorStatusNeighborhood;
        statusName = nameStatusNeighborhood;
        submitOnePoint = onConfirmOnePointNeighborhood;
        submitTwoPoint = onConfirmTwoPointNeighborhood;
    }

    const matchingParams = {
        showModelGender,
        showModelTipsFast,
        showIntroduction,
        disabaleMatch,
        listAge,
        nameGame,
        nameGender,
        isRequirChooseGame,
        onShowModal,
        onSellectGender,
        onShowTips,
        onHideTips,
        multiSliderValuesChange,
        onDeleteGame,
        onMatching,
        onBackIntroduction,
        onBackdropPress,
        onAddGame: switchItem === 0 ? onAddGameDetail : onAddGame,
        // 0
        showModelGameLevel,
        showModalDesiredGameLevel,
        gameLevel,
        desiredGameLevel,
        isRequirGameLevel,
        onSubmitTwoPoin: submitTwoPoint,
        onSubmitOnePoin: submitOnePoint,
        onChooseGameLevel,
        onDeleteChooseGameLevel,
        onFinishChooseGamelevel,
        onDeleteChooseDesiredGameLevel,
        onChooseDesiredGameLevel,
        onFinishChooseDesiredGamelevel,
        // 1
        valueIntroduction,
        onChangeIntroduction,
        // 2
        distance,
        multiSliderValuesChangeDistance,
    };

    return (
        <ContainerMain>
            <MainHeader />
            <ScrollView style={styles.container}>
                <View style={styles.sectionContainer}>
                    <View style={styles.rowCenter}>
                        <_Text style={styles.statusItemSlider}>매칭상태</_Text>
                        <View style={[styles.statusBackground, { backgroundColor: statusBackground }]} />
                        <_Text style={styles.statusItemSlider}>{statusName}</_Text>
                    </View>
                    <Carousel
                        onSnapToItem={onBeforeSnapToItem}
                        ref={c => carouselRef = c}
                        firstItem={1}
                        data={data}
                        renderItem={renderItem}
                        sliderWidth={screen.widthscreen}
                        itemWidth={screen.widthscreen / 2.4}
                        activeSlideOffset={20}
                        inactiveSlideScale={0.8}
                        inactiveSlideOpacity={0.4}
                        loop={true}
                        loopClonesPerSide={7}
                        onLayout={(e) => { }}
                        lockScrollTimeoutDuration={1000}
                        lockScrollWhileSnapping={true}
                        directionalLockEnabled={disabaleMatch}
                        scrollEnabled={!disabaleMatch}
                    />
                    {switchItem === 0 && <MatchingDetail {...matchingParams} />}
                    {switchItem === 1 && <MatchingFast {...matchingParams} />}
                    {switchItem === 2 && <MatchingNeighborhood {...matchingParams} />}
                </View>
            </ScrollView>
        </ContainerMain>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        alignItems: 'center',
        paddingBottom: 10,
        paddingRight: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    sectionContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 50
    },
    wrapItemSlider: {
        alignItems: 'center',
        paddingTop: 20
    },
    statusItemSlider: {
        marginHorizontal: 10,
    },
    nameItemSlider: {
        fontWeight: '800',
        fontSize: 14,
        color: '#1C1E21'
    },
    rowCenter: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    statusBackground: {
        height: 10,
        width: 10,
        borderRadius: 10,
    },
});

