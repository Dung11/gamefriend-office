import { StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";
import { screen } from "../../../config/Layout";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 30,
        backgroundColor: colors.white,
    },
    chartContainer: {
        paddingTop: 30,
        justifyContent: 'center',
        alignContent: 'center',
        marginHorizontal: 10,
    },
    chart: {
        height: screen.heightscreen / 2,
    },
    tagContainer: {
        backgroundColor: '#EEF5FF',
        borderColor: colors.blue,
        borderWidth: 1,
        width: screen.widthscreen / 2, marginLeft: -10,
        borderTopRightRadius: 12,
        borderBottomRightRadius: 12
    },
    tagText: {
        color: colors.blue,
        fontWeight: '400',
        fontSize: 14,
        textAlign: 'center',
        paddingVertical: 10
    },
    centerRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    setupText: {
        fontWeight: '400',
        fontSize: 12,
        lineHeight: 14,
        color: colors.blue
    }
});

export default styles;