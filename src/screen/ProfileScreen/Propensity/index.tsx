import React, { Component } from 'react'
import { Image, processColor, ScrollView, StatusBar, TouchableOpacity, View } from 'react-native'
import { ContainerMain } from '../../../components/Container/ContainerMain';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import styles from './styles';
import { RadarChart } from 'react-native-charts-wrapper';
import update from 'immutability-helper';
import { NavigationProp } from '@react-navigation/native';
import { RootStackParamList } from '../../../@types';
import _CloseHeader from '../../../components/CloseHeader/_CloseHeader';
import { Icons } from '../../../assets';
import { connect } from 'react-redux';
import { getPersonalTendencyRequest } from '../../../redux/personal-tendency/actions';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import store, { AppState } from '../../../redux/store';
import _Text from '../../../components/_Text';
import Environments from '../../../config/Environments';

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    tendency: any
}

export class Propensity extends Component<Props> {
    
    state = {
        isLoading: true,
        isLoadingInScreen: false,
        data: {},
        xAxis: {
            valueFormatter: ['게임스타일', '소통', '멘탈', '실력', '희생정신'],
            textSize: 14,
            fontWeight: '800',
            textColor: processColor(colors.blue),
            fontFamily: Environments.DefaultFont.Bold,
            drawValues: false
        },
        legend: {
            enabled: false,
        },
        yAxis: {
            labelCount: 6,
            labelCountForce: true,
            drawLabels: false,
            axisMaximum: 5,
            axisMinimum: 0
        },
        friendProfile: null,
        userIndex: [],
        hashtag: '',
        tendency: [],
    }

    async componentDidMount() {
        let userId = await keyValueStorage.get("userID");
        store.dispatch(getPersonalTendencyRequest({ id: userId, callback: this.onDone }))
    }

    onDone = (response: any) => {
        this.setState({
            tendency: response
        }, async () => {
            this.loadChartData();
        })
    }

    loadChartData = async () => {
        let tendency: any = this.state.tendency;
        let hashtag = '';
        if (tendency) {
            for (let i = 0; i < tendency.length; i++) {
                let curTendency = tendency[i];
                hashtag += `${curTendency.tag} `
            }
        }

        let types: any[] = ['게임스타일', '소통', '멘탈', '실력', '희생정신'];
        let userIndex: any[] = [{ value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }];
        if (tendency.length > 0) {
            types = [];
            userIndex = [];
        }
        for (let i = 0; i < tendency.length; i++) {
            let curTendency = tendency[i];
            userIndex.push({
                value: curTendency.level
            });
            types.push(curTendency.type);
        }

        this.setState({ userIndex, isLoading: false, tendency, hashtag }, () => {
            console.log('valueeee: ', userIndex);
            this.setState(
                update(this.state, {
                    data: {
                        $set: {
                            dataSets: [
                                {
                                    values: this.state.userIndex,
                                    label: '내 정보',
                                    config: {
                                        color: processColor(colors.blue),
                                        drawFilled: true,
                                        fillColor: processColor('#4387EF'),
                                        fillAlpha: 100,
                                        lineWidth: 3,
                                        drawValues: false
                                    }
                                },
                            ],
                        }
                    },
                    xAxis: {
                        $set: {
                            valueFormatter: types,
                            textSize: 14,
                            fontWeight: '800',
                            fontFamily: Environments.DefaultFont.Bold,
                            textColor: processColor(colors.blue),
                            drawValues: true
                        },
                    },
                    yAxis: {
                        $set: {
                            labelCount: 6,
                            labelCountForce: true,
                            drawLabels: false,
                            axisMaximum: 5,
                            axisMinimum: 0
                        }
                    },
                })
            );
        });
    }

    render() {
        let { isLoading, hashtag, tendency } = this.state;
        return (
            <ContainerMain>
                <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                <LoadingIndicator visible={isLoading} />
                <_CloseHeader {...this.props} title={'나의 성향'} selectedTitle={''} />
                <ScrollView style={styles.container}>
                    <View style={styles.chartContainer}>
                        {
                            hashtag.length > 0 &&
                            <View style={styles.tagContainer}>
                                <_Text style={styles.tagText}>{hashtag}</_Text>
                            </View>
                        }
                        <RadarChart
                            style={styles.chart}
                            data={this.state.data}
                            xAxis={this.state.xAxis as any}
                            yAxis={this.state.yAxis}
                            chartDescription={{ text: '' }}
                            legend={this.state.legend}
                            drawWeb={true}

                            webLineWidth={1}
                            webLineWidthInner={2}
                            webAlpha={255}
                            webColor={processColor("#ffffff")}
                            webColorInner={processColor("#bababa")}

                            skipWebLineCount={1}
                            touchEnabled={false}
                        />
                        <View style={{ flexDirection: 'row', alignContent: 'center', marginLeft: 25 }}>
                            <View style={{
                                justifyContent: 'center',
                                alignContent: 'center',
                            }}>
                                <View style={{
                                    backgroundColor: '#7AAAF4',
                                    alignItems: 'center',
                                    width: screen.widthscreen / 7,
                                    borderRadius: 6,
                                    height: 7
                                }}>
                                </View>
                            </View>
                            <View style={{
                                justifyContent: 'center',
                                alignContent: 'center',
                            }}>
                                <_Text style={{ justifyContent: 'center', alignContent: 'center', paddingLeft: 10, fontSize: 14, fontWeight: '800', color: '#7AAAF4' }}>내 정보</_Text>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row', marginTop: 30 }}>
                            <TouchableOpacity style={styles.centerRow}
                                onPress={() => {
                                    this.props.navigation.navigate('SetPropensity', {
                                        tendency
                                    });
                                }}>
                                <_Text style={styles.setupText}>내 성향 다시 설정하기  </_Text>
                                <Image source={Icons.ic_round_right} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </ContainerMain>
        )
    }
}

const mapStateToProps = (state: AppState): Partial<Props> => ({
    tendency: state.PersonalTendencyReducer.tendency,
});

export default connect(mapStateToProps, null)(Propensity);
