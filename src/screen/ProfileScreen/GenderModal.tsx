import React from "react";
import {
    View,
    TouchableOpacity,
    Image,
    StyleSheet
} from "react-native";
import { Icons } from "../../assets";
import Modal from 'react-native-modal';
import { NavigationProp } from "@react-navigation/native";
import { RootStackParamList } from "../../@types";
import { colors } from "../../config/Colors";
import _Text from "../../components/_Text";

interface Props {
    visible?: boolean,
    onClose: () => void,
    navigation: NavigationProp<RootStackParamList, any>,
    onSubmit: (name: string, value: string) => void,
    selectedValue?: string
}

const _GenderModal = (props: Props) => {

    const onSubmit = (name: string, value: string) => {
        props.onSubmit(name, value);
    }

    return (
        <Modal isVisible={props.visible} onBackdropPress={props.onClose}>
            <View style={styles.Content}>
                <TouchableOpacity style={styles.WrapperItem}
                    onPress={() => { onSubmit("상관없음", "any") }}>
                    <_Text style={[styles.txt,
                    { color: props.selectedValue === 'any' ? colors.blue : colors.black }]}>
                        상관없음
                            </_Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.WrapperItem, { paddingVertical: 15 }]}
                    onPress={() => { onSubmit("남성", "male") }}>
                    <Image style={styles.icon} source={Icons.ic_male} />
                    <_Text style={[styles.txt, { color: props.selectedValue === 'male' ? colors.blue : colors.black }]}>남성</_Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.WrapperItem, { borderBottomColor: 'white' }]}
                    onPress={() => { onSubmit("여성", "female") }}>
                    <Image style={styles.icon} source={Icons.ic_female} />
                    <_Text style={[styles.txt, { color: props.selectedValue === 'female' ? colors.blue : colors.black }]}>여성</_Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

export default _GenderModal;

const styles = StyleSheet.create({
    Wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        paddingHorizontal: 16
    },
    Content: {
        backgroundColor: "white",
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10
    },
    WrapperItem: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e3f0ee',
        width: '100%',
        flexDirection: 'row',
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10
    },
    txt: {
        fontWeight: '900'
    }
});