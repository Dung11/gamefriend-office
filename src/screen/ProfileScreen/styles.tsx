import { StyleSheet } from "react-native";
import { colors } from "../../config/Colors";
import { screen } from "../../config/Layout";

const styles = StyleSheet.create({
    body: {
        backgroundColor: colors.white,
    },
    sectionHeader: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionContainer: {
        marginTop: 80,
        paddingHorizontal: 24,
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionTitle: {
        fontSize: 30,
        lineHeight: 36,
        fontWeight: '600',
        color: colors.white,
    },
    username: {
        fontSize: 30,
        fontWeight: '600',
        lineHeight: 36
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: colors.black,
    },
    blueSubSectionHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        width: screen.widthscreen,
        backgroundColor: colors.blue,
        minHeight: 200
    },
    btnEdit: {
        backgroundColor: colors.blue,
        flexDirection: 'row',
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 20
    },
    editText: {
        fontWeight: '900',
        fontSize: 12,
        lineHeight: 14,
        color: colors.white
    },
    btnSetup: {
        flexDirection: 'row',
        borderWidth: 0.5,
        borderColor: '#8E8E92',
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 20,
    },
    setupText: {
        fontWeight: '900',
        fontSize: 12,
        lineHeight: 14,
        marginRight: 13,
        color: colors.blue,
    },
    itemContainer: {
        borderBottomColor: '#EEEEEF',
        borderBottomWidth: 1,
        marginHorizontal: 17,
        minHeight: 76,
        justifyContent: 'center',
    },
    itemHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },
    itemContent: {
        paddingVertical: 12
    },
    centerRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    seeMoreText: {
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 17,
        color: colors.blue
    },
    icon: {
        marginRight: 8
    },
    itemTitle: {
        flex: 8,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemIcon: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    flexRowEnd: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    avatarBackground: {
        position: 'absolute',
        top: 120,
        zIndex: 1,
    },
    avatarContainer: {
        width: screen.widthscreen / 2.7,
        height: screen.widthscreen / 2.7,
        backgroundColor: colors.white,
        borderRadius: screen.widthscreen,
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        width: screen.widthscreen / 3,
        height: screen.widthscreen / 3,
        borderRadius: screen.widthscreen / 3 / 2,
    },
});

export default styles;