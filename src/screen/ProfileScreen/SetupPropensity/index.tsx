import { StackNavigationProp } from '@react-navigation/stack';
import { Alert, StatusBar, View } from 'react-native'
import { RootStackParamList } from '../../../@types';
import _CloseHeader from '../../../components/CloseHeader/_CloseHeader';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { colors } from '../../../config/Colors';
import React, { useEffect, useState } from 'react';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { ScrollView } from 'react-native-gesture-handler';
import styles from './styles';
import _GenderModal from '../GenderModal';
import { getTendencyOption } from '../../../api/RecommendedServices';
import { saveUserTendency } from '../../../api/UserServices';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import { RouteProp } from '@react-navigation/native';
import store from '../../../redux/store';
import { personalTendencyRequest } from '../../../redux/personal-tendency/actions';
import _Text from '../../../components/_Text';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { screen } from '../../../config/Layout';

type Params = {
    params: {
        tendency: any
    }
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
    route: RouteProp<Params, 'params'>
}

const SetupPropensity = (props: Props) => {

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isLoadingInView, setIsLoadingInView] = useState<boolean>(false);
    const [tendencyOptions, setTendencyOptions] = useState<any[]>([]);
    const [userTendency, setUserTendency] = useState<any>();

    const getData = () => {
        const { route }: any = props;
        let { tendency } = route.params;
        getTendencyOption().then((response: any) => {
            setTendencyOptions(response);
            if (tendency.length === 0) {
                let userTendencyTmp: any[] = [];
                for (let i = 0; i < response.length; i++) {
                    let curTendency = response[i];
                    userTendencyTmp.push({
                        type: curTendency.type,
                        level: 1,
                        tag: curTendency.answers[0].tag
                    });
                }
                setUserTendency(userTendencyTmp);
            } else {
                setUserTendency(tendency);
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message)
        }).finally(() => {
            setIsLoading(false);
        })
    }

    const onSubmit = async () => {
        setIsLoadingInView(true);
        let userId: any = await keyValueStorage.get("userID");
        console.log('userTendency: ', userTendency)
        let response = await saveUserTendency(userId, { tendency: userTendency });
        if (!response) {
            Alert.alert('오류62', '잠시 후 다시 시도해주세요');
            setIsLoading(false);
            return;
        }
        store.dispatch(personalTendencyRequest({ id: userId, callback: onTendencyDone }))
    }

    const onTendencyDone = (response: any) => {
        props.navigation.navigate('Profile');
        setIsLoadingInView(false);
    }

    const onSliderChange = (type: string, value: any, tag: any) => {
        let userTendencyTmp = [...userTendency];
        let selectedIndex = userTendencyTmp.findIndex((d: any) => d.type === type);
        if (selectedIndex >= 0) {
            userTendencyTmp[selectedIndex].level = value;
            userTendencyTmp[selectedIndex].type = type;
            userTendencyTmp[selectedIndex].tag = tag;
            setUserTendency(userTendencyTmp);
        }
    }

    const getSelectedChoice = (type: string) => {
        let userTendencyTmp = [...userTendency];
        let selectedIndex = userTendencyTmp.findIndex((d: any) => d.type === type);
        if (selectedIndex >= 0) {
            return userTendencyTmp[selectedIndex].level;
        }
        return 1;
    }

    useEffect(() => {
        getData();
    }, []);

    if (isLoading) {
        return (
            <ContainerMain>
                <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                <LoadingIndicator visible={isLoading} />
                <_CloseHeader {...props} title={'프로필 수정'} selectedTitle={'완료'} />
            </ContainerMain>
        )
    }

    return (
        <ContainerMain>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <LoadingIndicator visible={isLoadingInView} />
            <_CloseHeader {...props} title={'프로필 수정'} selectedTitle={'완료'} onSubmit={onSubmit} />
            <ScrollView contentContainerStyle={{ marginTop: 20 }}>
                {
                    tendencyOptions.map((item: any, index: number) => {
                        return (
                            <View key={index.toString()} style={styles.itemContainer}>
                                <_Text style={styles.questionTitle}>{item.question}</_Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>
                                        <_Text style={styles.note}>{item.start}</_Text>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                        <_Text style={styles.note}>
                                            {item.end}
                                        </_Text>
                                    </View>
                                </View>
                                <MultiSlider
                                    selectedStyle={{ backgroundColor: colors.mainColor }}
                                    markerStyle={styles.market}
                                    onValuesChange={(value) => onSliderChange(item.type, value[0], item.answers[value[0] - 1].tag)}
                                    values={[getSelectedChoice(item.type)]}
                                    min={1}
                                    max={item.answers.length}
                                    enableLabel={false}
                                    snapped={true}
                                    sliderLength={screen.widthscreen - 40}
                                />
                            </View>
                        )
                    })
                }
            </ScrollView>
        </ContainerMain>
    );

};

export default SetupPropensity