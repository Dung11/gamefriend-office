import { StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";

const styles = StyleSheet.create({
    itemContainer: {
        borderBottomColor: '#EEEEEF',
        borderBottomWidth: 1,
        paddingHorizontal: 20,
        minHeight: 86,
        justifyContent: 'center',
        backgroundColor: colors.white,
        marginBottom: 2,
    },
    questionTitle: {
        fontWeight: '500',
        fontSize: 16,
        paddingVertical: 14
    },
    note: {
        color: '#8E8E93',
        fontWeight: '400',
        fontSize: 13,
        lineHeight: 18
    },
    market: {
        backgroundColor: colors.white,
        width: 26,
        height: 26,
        elevation: 10,
        borderRadius: 26
    },
});

export default styles;