import React, { useEffect, useState } from 'react';
import { NavigationProp } from "@react-navigation/native";
import { RootStackParamList } from "../../../@types";
import Modal from 'react-native-modal';
import { ContainerMain } from "../../../components/Container/ContainerMain";
import { Alert, Image, StatusBar, StyleSheet, TouchableOpacity, View } from "react-native";
import { Icons } from '../../../assets';
import { screen } from '../../../config/Layout';
import { colors } from '../../../config/Colors';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import DeviceInfo from 'react-native-device-info';
import _Text from '../../../components/_Text';
import { getPersonality } from '../../../api/AuthorServices';

interface Props {
    visible: boolean,
    onSubmit: (data: any) => void,
    onClose: () => void,
    navigation: NavigationProp<RootStackParamList, any>,
    choices: []
}

const _ChoosePersonalitiesModal = (props: Props) => {
    const { onClose } = props;
    const [isLoading, setIsLoading] = useState(true);
    const [visible, setVisible] = useState(props.visible);
    const [listPersonality, setListPersonality] = useState<any[]>([]);
    const [selectedChoices, setSelectedChoice] = useState<any[]>(props.choices);

    const getListPersonality = async () => {
        try {
            const result = await getPersonality()
            if (result.status === 200) {
                const dataResult = result.data.personalities;
                const tempData = dataResult.map((nitem: any, nindex: any) => {
                    return {
                        name: nitem,
                        id: nindex
                    }
                })
                setListPersonality(tempData);
            }
        } catch (error) {
            Alert.alert('오류', '현재 시스템 점검 중입니다. 잠시 후 다시 시도해주세요')
        } finally {
            setIsLoading(false);
        }
    }

    const onChoose = (name: string, index: number) => {
        let selectedChoicesTmp = [...selectedChoices];
        let selectedIndex = selectedChoicesTmp?.findIndex((d: any) => d === name);
        if (selectedIndex >= 0 && selectedChoicesTmp.length === 1) {
            return;
        }
        if (selectedIndex >= 0) {
            selectedChoicesTmp = selectedChoicesTmp.filter((d: any) => d !== name);
        } else {
            selectedChoicesTmp.push(name)
        }
        setSelectedChoice(selectedChoicesTmp);
    }

    const checkExisted = (name: string) => {
        let selectedIndex = selectedChoices?.findIndex((d: any) => d === name);
        if (selectedIndex >= 0) {
            return true;
        }
        return false;
    }

    useEffect(() => {
        getListPersonality()
    }, []);

    useEffect(() => {
        setSelectedChoice(props.choices);
        setVisible(props.visible);
    }, [props.choices, props.visible]);

    return (
        <Modal isVisible={visible} style={styles.background}
            onBackdropPress={onClose}>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <LoadingIndicator visible={isLoading} />
            <ContainerMain styles={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={onClose}>
                        <Image source={Icons.ic_back} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { props.onSubmit(selectedChoices) }}>
                        <_Text style={styles.ok}>완료</_Text>
                    </TouchableOpacity>
                </View>
                <_Text style={styles.title}>성격을 선택해주세요(최대 4가지 선택 가능)</_Text>
                <View style={styles.wrapItem}>
                    <_Text style={styles.titleItem}>추천친구에 반영됩니다</_Text>
                    <View style={styles.sectionItem}>
                        <View style={{ flexWrap: "wrap", flexDirection: 'row', alignItems: 'center' }}>
                            {
                                listPersonality.map((item: any, index: any) =>
                                (
                                    <TouchableOpacity key={index}
                                        style={[styles.listItem, checkExisted(item.name) === true && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                                        onPress={() => onChoose(item.name, index)}>
                                        <_Text style={[styles.nameListItem, { color: checkExisted(item.name) === true ? colors.mainColor : '#65676B' }]}>
                                            {item.name}
                                        </_Text>
                                    </TouchableOpacity>
                                ))
                            }
                        </View>
                    </View>
                </View>
            </ContainerMain>
        </Modal>
    )
}

export default _ChoosePersonalitiesModal;

const styles = StyleSheet.create({
    background: {
        margin: 0,
        padding: 0,
    },
    header: {
        width: '100%',
        height: 60,
        backgroundColor: colors.mainColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        marginBottom: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20
    },
    container: {
        alignItems: 'center',
        flex: 1
    },
    ok: {
        color: colors.white,
        fontSize: 17,
        fontWeight: '400'
    },
    title: {
        marginTop: 80,
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 18,
        fontWeight: '800',
        paddingHorizontal: 50,
        textAlign: 'center'
    },
    titleItem: {
        marginBottom: 10
    },
    wrapItem: {
        borderRadius: 10,
        paddingVertical: 10,
        backgroundColor: colors.white,
        alignItems: 'center',
        width: screen.widthscreen - 40,
    },
    sectionItem: {
        marginHorizontal: 10
    },
    listItem: {
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 1,
        borderColor: '#EDEDED',
        backgroundColor: '#EDEDED',
    },
    nameListItem: {
        color: colors.mainColor
    }
});