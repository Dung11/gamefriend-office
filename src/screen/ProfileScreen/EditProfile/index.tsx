import { StackNavigationProp } from '@react-navigation/stack';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, FlatList, Image, Platform, StatusBar, Switch, TouchableOpacity, View } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { RootStackParamList } from '../../../@types';
import { checkNickname } from '../../../api/AuthorServices';
import { getCharacterByType } from '../../../api/GameServices';
import { uploadImages } from '../../../api/ProfileServices';
import { getUserPicture, hideReview, personalProfileById, updatePersonalProfile } from '../../../api/UserServices';
import { Icons } from '../../../assets';
import _CloseHeader from '../../../components/CloseHeader/_CloseHeader';
import _CommentContainer from '../../../components/CommentContainer';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import _ModalShowImage from '../../../components/Modal/_ModalShowImage';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';
import Environments from '../../../config/Environments';
import { screen } from '../../../config/Layout';
import { personalProfileRequest } from '../../../redux/personal-profile/actions';
import store from '../../../redux/store';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import ChooseAddressModal from './ChooseAddressModal';
import _ChoosePersonalitiesModal from './ChoosePersonalitiesModal';
import { _ChoosingGameModal } from './ChoosingGameModal';
import EditAvatarView from './EditAvatarView';
import _ModifyingImageModal from './ModifyingImageModal';
import styles from './styles';
const RNFS = require('react-native-fs');

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
}

const TIME_DATA = [
    {
        id: 1,
        numbertime: '6~12',
        nametime: '아침',
        isCheck: false
    },
    {
        id: 2,
        numbertime: '12~18',
        nametime: '오후',
        isCheck: false
    },
    {
        id: 3,
        numbertime: '18~24',
        nametime: '저녁',
        isCheck: false
    },
    {
        id: 4,
        numbertime: '00~06',
        nametime: '새벽',
        isCheck: false
    },
];

export class EditProfile extends Component<Props> {

    checkWaiting: any;

    state = {
        isLoading: true,
        dataMental: null,
        dataPersonality: null,
        dataTeamwork: null,
        listData: TIME_DATA,
        referral: true,
        profileData: null,
        userId: null,
        isPersonalityVisible: false,
        isSearchGameVisible: false,
        isChangeAddressVisible: false,
        numberOfChoice: 1,
        bannedGames: [],
        gameType: '',
        selecetedChoices: [],
        selectedImage: '',
        isSubmit: false,
        isValid: true,
        isLoadingCheck: false,
        reviewIds: [],
        isLoadingInScreen: false,
        visibleShowImg: false,
        showingImage: false,
        currentUserName: ''
    }

    async componentDidMount() {
        await this.loadData();
    }

    loadData = async () => {
        let userId: any = await keyValueStorage.get('userID');
        let [teamworkRes, mentalRes, personalityRes, profileRes] = await Promise.all([
            getCharacterByType('teamwork'),
            getCharacterByType('mental'),
            getCharacterByType('personality'),
            personalProfileById(userId)
        ]);
        this.setState({
            dataMental: mentalRes,
            dataPersonality: personalityRes,
            dataTeamwork: teamworkRes,
            profileData: profileRes,
            isLoading: false,
            userId,
            currentUserName: profileRes?.userName
        })
    }

    onChangeUsername = (value: string) => {
        let { profileData, currentUserName }: any = this.state;
        profileData.userName = value;

        if (this.checkWaiting) {
            clearTimeout(this.checkWaiting);
        }

        if (value === currentUserName) {
            this.setState({
                isSubmit: true,
                isValid: true,
                isLoadingCheck: false
            });
            return;
        }

        this.setState({
            profileData,
            isLoadingCheck: true,
            isValid: false
        }, () => {
            this.checkWaiting = setTimeout(() => {
                this.onSubmitEdit(value)
            }, 500);
        });
    }

    onChangeBio = (value: string) => {
        let { profileData }: any = this.state;
        profileData.bio = value;
        this.setState({ profileData, isLoading: false });
    }

    onChangeCharacter(type: 'teamwork' | 'mental' | 'personality', name: string) {
        let { profileData }: any = this.state;
        let character: any = profileData.character;
        if (type === 'teamwork') {
            character.teamwork = name;
        } else if (type === 'mental') {
            character.mental = name;
        } else {
            character.personality = name;
        }
        profileData.character = character;
        this.setState({ profileData });
    }

    onChoosePlaytime = (value: string) => {
        let { profileData }: any = this.state;
        let itemIndex = profileData?.playTime.findIndex((d: any) => d === value);
        if (itemIndex >= 0) {
            let playTime = profileData?.playTime.filter((d: any) => d !== value);
            profileData.playTime = playTime
        } else {
            profileData?.playTime.push(value);
        }
        this.setState({ profileData });
    }

    onProcessData = async (userId: any) => {
        try {
            let { profileData, reviewIds }: any = this.state;
            let localProfilePicture: any = null;
            let localPictures: any[] = [];
            let gameIds: any[] = [];
            let arrayHideReviewPromise = [];

            for (let i = 0; i < reviewIds.length; i++) {
                let reviewId = reviewIds[i];
                arrayHideReviewPromise.push(
                    new Promise((resolve, reject) => {
                        hideReview(userId, reviewId).then((response: any) => {
                            if (response) {
                                resolve(true);
                            } else {
                                reject(false);
                            }
                        }).catch((error: Error) => {
                            reject(false);
                        })
                    })
                )
            }
            //GameIds
            for (let i = 0; i < profileData.games.length; i++) {
                let curGame = profileData.games[i];
                gameIds.push(curGame.id);
            }
            profileData.gameIds = gameIds;
            //End GameIds

            //FavoriteGameIds
            profileData.favoriteGameId = profileData.favoriteGame?.id;
            //End FavoriteGameIds

            //ProfilePicture
            if (!profileData?.profilePicture.startsWith('http')) {
                localProfilePicture = profileData?.profilePicture;
                if (Platform.OS === 'ios') {
                    let photoPATH = `assets-library://asset/asset.JPG?id=${localProfilePicture.substring(5)}&ext=JPG`;
                    const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                    localProfilePicture = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
                }
            }
            //End ProfilePicture

            //Pictures
            let filterPictures = profileData?.pictures?.filter((url: string) => url !== profileData?.profilePicture);

            for (let i = 0; i < filterPictures.length; i++) {
                let curPicture = filterPictures[i];
                if (!curPicture.startsWith('http')) {
                    if (Platform.OS === 'ios') {
                        let photoPATH = `assets-library://asset/asset.JPG?id=${curPicture.substring(5)}&ext=JPG`;
                        const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                        curPicture = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
                    }
                    localPictures.push(curPicture);
                }
            }
            let oldPictures = filterPictures.filter((url: string) => url.startsWith('http') === true);
            //End Pictures
            let [uploadProfileRes, uploadPicturesRes]: any = await Promise.all([
                this.uploadPic(localProfilePicture),
                this.upLoadMulPic(localPictures),
                ...arrayHideReviewPromise
            ]);
            uploadProfileRes != '' && (profileData.profilePicture = uploadProfileRes);

            profileData.pictures = [
                ...oldPictures,
                ...uploadPicturesRes,
                profileData.profilePicture];

            this.setState({ profileData });
        } catch (error) {
            Alert.alert('오류', error.message);
            this.setState({ isLoading: false });
        }
    }

    uploadPic = (uri: string) => {
        return new Promise((resolve, reject) => {
            if (!uri) {
                resolve('');
                return;
            }
            let formdata = new FormData();
            const ext = this.getImageExstension(uri);
            const filename = `${Date.now()}.${ext}`;
            let file = {
                uri,
                type: 'image/jpeg',
                name: filename
            };
            formdata.append('photos', file);
            uploadImages(formdata).then((response: any) => {
                let url = '';
                if (response && response.data?.locations) {
                    let data = response.data;
                    url = data.locations.length > 0 ?
                        data.locations[0] : '';
                }
                resolve(url);
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                console.log('오류', error.message);
                reject('');
            })
        })
    }

    upLoadMulPic = (uriArr: string[]) => {
        return new Promise((resolve, reject) => {
            let formdata = new FormData();
            if (uriArr.length === 0) {
                resolve([]);
                return;
            }
            for (let i = 0; i < uriArr.length; i++) {
                let uri = uriArr[i];
                const ext = this.getImageExstension(uri);
                const filename = `${Date.now()}.${ext}`;
                let file = {
                    uri,
                    type: 'image/jpeg',
                    name: filename
                };
                formdata.append('photos', file);
            }
            uploadImages(formdata).then((response: any) => {
                if (response && response.data?.locations) {
                    let data = response.data;
                    resolve(data.locations)
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                reject([]);
            });
        });
    }

    getImageExstension = (uri: string) => {
        if (uri) {
            const splitNameArr = uri.split('/');
            const filename = splitNameArr[splitNameArr.length - 1];
            const splitFilenameArr = filename.split('.');
            return splitFilenameArr[splitFilenameArr.length - 1];
        }
        return '';
    }

    onSubmit = async () => {
        try {
            this.setState({ isLoading: true });
            let userId: any = await keyValueStorage.get('userID');
            await this.onProcessData(userId);
            let { profileData }: any = this.state;
            let resquestData = Object.assign({}, profileData);

            delete resquestData.games;
            delete resquestData.favoriteGame;
            delete resquestData.id;
            let response = await updatePersonalProfile(userId, resquestData);
            if (!response) {
                Alert.alert('오류60', '잠시 후 다시 시도해주세요');
                this.setState({ isLoading: false });
                return;
            }
            store.dispatch(personalProfileRequest({ id: userId, callback: this.onDone }));
        } catch (error) {
            this.setState({ isLoading: false });
        }
    }

    onPersonalityModalSubmit = (data: any) => {
        let { profileData }: any = this.state;
        profileData.personalities = data;
        this.setState({ profileData, isPersonalityVisible: false });
    }

    onSearchGameModalSubmit = (data: any, type: 'favorite-game' | 'game' | 'game-filter') => {
        let { profileData }: any = this.state;
        if (type === 'favorite-game') {
            profileData.favoriteGame = data[0];
        } else {
            profileData.games = data;
        }
        this.setState({ profileData, isSearchGameVisible: false });
    }

    onDone = () => {
        let { profileData }: any = this.state;
        keyValueStorage.save('userName', profileData.userName).then((res: any) => {
            this.props.navigation.goBack();
            this.setState({
                isLoading: false
            })
        })
    }

    onClosePersonality = () => {
        this.setState({
            isPersonalityVisible: false
        })
    }

    onShowChoosingGame = (type: 'favorite-game' | 'game') => {
        let { profileData }: any = this.state;
        if (type === 'favorite-game') {
            this.setState({
                numberOfChoice: 1,
                bannedGames: profileData.games,
                gameType: type,
                selecetedChoices: [profileData?.favoriteGame]
            }, () => {
                this.setState({
                    isSearchGameVisible: true,
                });
            });
        } else {
            this.setState({
                numberOfChoice: 3,
                bannedGames: [profileData.favoriteGame],
                gameType: type,
                selecetedChoices: profileData?.games
            }, () => {
                setTimeout(() => {
                    this.setState({
                        isSearchGameVisible: true,
                    });
                }, 1000);
            });
        }
    }

    onUpdateImages = (pictures: any, profilePicture: any) => {
        let { profileData }: any = this.state;
        profileData.profilePicture = profilePicture;
        profileData.pictures = pictures;
        this.setState({ profileData });
    }

    onRemove = (reviewId: string) => {
        let { reviewIds }: any = this.state;
        reviewIds.push(reviewId);
        this.setState({ reviewIds });
    }

    renderItemMental = ({ item, index }: any) => {
        let { profileData }: any = this.state;
        return (
            <TouchableOpacity
                key={index}
                style={[styles.listItem, profileData?.character?.mental === item.name && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                onPress={() => { this.onChangeCharacter('mental', item.name) }}>
                <_Text style={[styles.nameListItem, { color: profileData?.character?.mental === item.name ? colors.mainColor : '#65676B' }]}>
                    {item.name}
                </_Text>
            </TouchableOpacity>
        )
    }

    renderItemPersonality = ({ item, index }: any) => {
        let { profileData }: any = this.state;
        return (
            <TouchableOpacity
                key={index}
                style={[styles.listItem, profileData?.character?.personality === item.name && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                onPress={() => { this.onChangeCharacter('personality', item.name) }}>
                <_Text style={[styles.nameListItem, { color: profileData?.character?.personality === item.name ? colors.mainColor : '#65676B' }]}>
                    {item.name}
                </_Text>
            </TouchableOpacity>
        )
    }

    renderItemTeamwork = ({ item, index }: any) => {
        let { profileData }: any = this.state;
        return (
            <TouchableOpacity
                key={index}
                style={[styles.listItem, profileData?.character?.teamwork === item.name && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                onPress={() => { this.onChangeCharacter('teamwork', item.name) }}>
                <_Text
                    style={[styles.nameListItem, { color: profileData?.character?.teamwork === item.name ? colors.mainColor : '#65676B' }]}>
                    {item.name}
                </_Text>
            </TouchableOpacity>
        )
    }

    renderUserPersonality = ({ item, index }: any) => {
        return (
            <View
                key={index}
                style={[styles.listItem, { borderColor: colors.mainColor, backgroundColor: colors.white }]}>
                <_Text
                    style={[styles.nameListItem, { color: colors.mainColor }]}>
                    {item}
                </_Text>
            </View>
        )
    }

    renderItem = ({ item, index }: any) => {
        let { profileData }: any = this.state;
        let itemIndex = profileData?.playTime.findIndex((d: any) => d === item.nametime);
        return (
            <TouchableOpacity style={styles.listTimeItem} key={index}
                onPress={() => this.onChoosePlaytime(item.nametime)}>
                <View style={{
                    borderTopColor: itemIndex >= 0 ? colors.blue : colors.grayLight,
                    borderTopWidth: 7,
                    alignItems: 'center',
                    width: screen.widthscreen / 7
                }}>
                    <_Text style={styles.nameListTimeItem}>{item.numbertime}</_Text>
                    <_Text style={styles.nameListTimeItem}>{item.nametime}</_Text>
                </View>
            </TouchableOpacity>
        )
    }

    toggleSwitch = () => {
        let { profileData }: any = this.state;
        if (profileData.recommendationStatus === 'active') {
            profileData.recommendationStatus = 'inactive';
        } else {
            profileData.recommendationStatus = 'active';
        }
        this.setState({ profileData });
    }

    onSubmitChangeAddrModal = (data: any) => {
        let { profileData }: any = this.state;
        profileData.address = data.address;
        profileData.location = data.location;
        this.setState({ profileData, isChangeAddressVisible: false });
    }

    onSubmitEdit = (value: string) => {
        let nickName = value;
        if (nickName.length >= 2 && nickName.length <= 8) {
            checkNickname(nickName).then((response: any) => {
                if (response.status === 200) {
                    if (response.data.isExist === true) {
                        this.setState({
                            isSubmit: true,
                            isValid: false,
                            isLoadingCheck: false
                        });
                    } else {
                        this.setState({
                            isSubmit: true,
                            isValid: true,
                            isLoadingCheck: false
                        });
                    }
                } else {
                    Alert.alert('오류61', '잠시 후 다시 시도해주세요');
                    this.setState({ isLoadingCheck: false });
                }
            });
        } else {
            this.setState({
                isSubmit: false,
                isValid: false,
                isLoadingCheck: false
            });
        }
    }

    onClickReviewerAvatar = async (friendId: string) => {
        this.setState({ isLoadingInScreen: true });
        let pictures = await getUserPicture(friendId);
        if (pictures.length === 0) {
            this.setState({ isLoadingInScreen: false });
            return;
        }
        this.setState({
            showingImage: pictures,
            visibleShowImg: true,
            isLoadingInScreen: false
        });
    }

    onCloseModalShowImage = () => {
        this.setState({ visibleShowImg: false })
    }

    render() {
        let { isLoading,
            dataMental,
            dataPersonality,
            dataTeamwork,
            listData,
            profileData,
            userId,
            isChangeAddressVisible,
            isPersonalityVisible,
            isSearchGameVisible,
            gameType,
            isValid,
            isSubmit,
            isLoadingCheck,
            visibleShowImg,
            showingImage
        }: any = this.state;
        return (
            <ContainerMain>
                <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                <LoadingIndicator visible={isLoading} />
                <_ChoosePersonalitiesModal {...this.props}
                    visible={isPersonalityVisible}
                    onClose={this.onClosePersonality}
                    onSubmit={this.onPersonalityModalSubmit}
                    choices={profileData?.personalities} />
                {
                    (visibleShowImg) &&
                    <_ModalShowImage
                        visible={visibleShowImg}
                        onClose={this.onCloseModalShowImage}
                        banner={showingImage} />
                }
                {
                    isSearchGameVisible &&
                    <_ChoosingGameModal
                        visible={isSearchGameVisible}
                        {...this.props}
                        onClose={() => { this.setState({ isSearchGameVisible: false }) }}
                        onSubmit={this.onSearchGameModalSubmit}
                        numberOfChoice={this.state.numberOfChoice}
                        bannedGames={this.state.bannedGames}
                        choices={this.state.selecetedChoices}
                        type={gameType}
                        isRequired={true}
                    />
                }
                <ChooseAddressModal
                    {...this.props}
                    visible={isChangeAddressVisible}
                    address={profileData?.address}
                    lat={''}
                    lng={''}
                    onClose={() => { this.setState({ isChangeAddressVisible: false }) }}
                    onSubmit={this.onSubmitChangeAddrModal}
                />
                <_CloseHeader
                    {...this.props}
                    title={'프로필 수정'}
                    selectedTitle={'완료'}
                    onSubmit={this.onSubmit}
                    canSubmit={isValid} />
                <ScrollView contentContainerStyle={{ marginTop: 20 }}>
                    <EditAvatarView
                        {...this.props}
                        pictures={profileData?.pictures}
                        profilePicture={profileData?.profilePicture}
                        onUpdateData={this.onUpdateImages}
                    />
                    <View style={{ backgroundColor: colors.white, borderTopLeftRadius: 16, borderTopRightRadius: 16, marginTop: 22, padding: 16 }}>
                        <View style={[styles.itemContainer]}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: .4, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image style={styles.icon} source={Icons.ic_blue_account} />
                                    <_Text style={styles.title}>닉네임</_Text>
                                </View>
                                <View style={{ flex: .6, flexDirection: 'row' }}>
                                    <TextInput
                                        style={{
                                            color: '#65676B',
                                            fontWeight: '800',
                                            fontSize: 17,
                                            lineHeight: 24
                                        }}
                                        placeholder={'닉네임'}
                                        value={profileData?.userName}
                                        onChangeText={this.onChangeUsername} />
                                    {isLoadingCheck && <ActivityIndicator style={{ marginLeft: 'auto' }} color={colors.mainColor} />}
                                </View>
                            </View>
                            <View style={styles.centerRow}>
                                {
                                    (isSubmit === true && isValid === true) &&
                                    <View style={styles.textContainer}>
                                        <Image source={Icons.ic_sign_success} />
                                        <_Text style={styles.successText}>사용가능한 닉네임입니다</_Text>
                                    </View>
                                }
                                {
                                    (isSubmit === true && isValid === false) &&
                                    <View style={styles.textContainer}>
                                        <Image source={Icons.ic_sign_error} />
                                        <_Text style={styles.errorText}>해당 닉네임은 이미 사용중입니다!</_Text>
                                    </View>
                                }
                                {
                                    (isSubmit === false && profileData?.userName?.length > 8) &&
                                    <View style={styles.textContainer}>
                                        <Image source={Icons.ic_sign_error} />
                                        <_Text style={styles.errorText}>닉네임은 8자 미만입니다.</_Text>
                                    </View>
                                }
                                {
                                    (isSubmit === false && profileData?.userName?.length < 2) &&
                                    <View style={styles.textContainer}>
                                        <Image source={Icons.ic_sign_error} />
                                        <_Text style={styles.errorText}>별명에 2 자 이상이 포함되어 있습니다.</_Text>
                                    </View>
                                }
                            </View>
                        </View>
                        <View style={styles.itemContainer}>
                            <View style={{ flexDirection: 'row', marginBottom: 8 }}>
                                <Image style={styles.icon} source={Icons.ic_blue_star} />
                                <_Text style={styles.title}>좋아하는 게임</_Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginHorizontal: 8, flexWrap: 'wrap' }}>
                                {
                                    profileData && [profileData?.favoriteGame, ...profileData?.games].map((item: any, index: number) => {
                                        return (
                                            <View style={styles.gameTitleContainer}>
                                                <_Text style={styles.gameTitle}>{item.name}</_Text>
                                            </View>
                                        )
                                    })
                                }
                            </View>
                            <TouchableOpacity style={{ justifyContent: 'flex-end', flexDirection: 'row' }}
                                onPress={() => { this.onShowChoosingGame('favorite-game') }}>
                                <View style={styles.centerRow}>
                                    <_Text style={styles.seeMoreText}>가장 좋아하는 게임 수정</_Text>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'flex-end', flexDirection: 'row', marginTop: 10 }}
                                onPress={() => { this.onShowChoosingGame('game') }}>
                                <View style={styles.centerRow}>
                                    <_Text style={styles.seeMoreText}>좋아하는 게임 수정</_Text>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.itemContainer, styles.centerRow]}>
                            <View style={{ flex: .7 }}>
                                <View style={{ flexDirection: 'row', marginBottom: 8 }}>
                                    <Image style={styles.icon} source={Icons.ic_crosshair} />
                                    <_Text style={styles.title}>위치</_Text>
                                </View>
                                <_Text style={styles.hashtagTxt}>#{profileData?.address}</_Text>
                            </View>
                            <TouchableOpacity style={{ flex: .3, justifyContent: 'center', alignItems: 'center' }}
                                onPress={() => { this.setState({ isChangeAddressVisible: true }) }}>
                                <Image source={Icons.ic_location} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.itemContainer}>
                            <_Text style={styles.subTitle}>나의 멘탈은</_Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <FlatList
                                    data={dataMental}
                                    numColumns={3}
                                    renderItem={this.renderItemMental}
                                    keyExtractor={index => index.toString()}
                                />
                            </View>
                            <_Text style={styles.subTitle}>게임친구에게 바라는 것은</_Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <FlatList
                                    data={dataPersonality}
                                    numColumns={3}
                                    renderItem={this.renderItemPersonality}
                                    keyExtractor={index => index.toString()}
                                />
                            </View>
                            <_Text style={styles.subTitle}>나의 게임 스타일은</_Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <FlatList
                                    data={dataTeamwork}
                                    numColumns={3}
                                    renderItem={this.renderItemTeamwork}
                                    keyExtractor={index => index.toString()}
                                />
                            </View>
                            <_Text style={styles.subTitle}>나의 성격은</_Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <FlatList
                                    data={profileData?.personalities}
                                    numColumns={3}
                                    renderItem={this.renderUserPersonality}
                                    keyExtractor={index => index.toString()}
                                />
                            </View>
                            <TouchableOpacity style={{ justifyContent: 'flex-end', flexDirection: 'row', marginTop: 5 }}
                                onPress={() => { this.setState({ isPersonalityVisible: true }) }}>
                                <View style={styles.centerRow}>
                                    <_Text style={styles.seeMoreText}>나의 성격  설정하기 </_Text>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.itemContainer}>
                            <View style={styles.rowWrap}>
                                <FlatList
                                    scrollEnabled={false}
                                    contentContainerStyle={{ alignItems: 'center' }}
                                    data={listData}
                                    numColumns={4}
                                    renderItem={this.renderItem}
                                    keyExtractor={index => index.toString()}
                                />
                            </View>
                        </View>
                        <View style={styles.itemContainer}>
                            <_Text style={styles.subTitle}>인사말</_Text>
                            <TextInput
                                multiline={true}
                                numberOfLines={6}
                                placeholder={'자기소개를 입력해주세요'}
                                textAlignVertical={'top'}
                                value={profileData?.bio}
                                onChangeText={this.onChangeBio}
                                style={{ fontFamily: Environments.DefaultFont.Regular }}
                            />
                        </View>
                        <View style={[styles.itemContainer, styles.centerRow]}>
                            <_Text style={styles.subTitle}>내 프로필을 다른 친구에게 추천해주기</_Text>
                            <Switch style={{ marginLeft: 'auto' }}
                                trackColor={{ false: '#f0f0f0', true: colors.blue }}
                                thumbColor={profileData?.recommendationStatus === 'active' ? '#F7FAFC' : '#F7FAFC'}
                                ios_backgroundColor="#dddddd"
                                onValueChange={() => { this.toggleSwitch() }}
                                value={profileData?.recommendationStatus === 'active' ? true : false}
                            />
                        </View>
                        <View style={styles.itemContainer}>
                            <_Text style={styles.subTitle}>내 매칭 후기</_Text>
                            {userId
                                && <_CommentContainer
                                    friendId={userId}
                                    {...this.props}
                                    canRemove={true}
                                    onRemove={this.onRemove}
                                    onClickAvatar={this.onClickReviewerAvatar}
                                />}
                        </View>
                    </View>
                </ScrollView>
            </ContainerMain>
        )
    }
}


export default EditProfile;
