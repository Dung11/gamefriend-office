import { StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";
import { screen } from "../../../config/Layout";

const styles = StyleSheet.create({
    centerRow: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    itemContainer: {
        paddingVertical: 10,
        borderBottomColor: '#EEEEEF',
        borderBottomWidth: 1,
    },
    title: {
        fontWeight: '500',
        fontSize: 16,
        lineHeight: 23,
        color: '#1C1E21'
    },
    icon: {
        marginHorizontal: 8
    },
    gameTitleContainer: {
        borderRadius: 30,
        borderWidth: 1,
        borderColor: colors.blue,
        marginRight: 7,
        marginVertical: 5
    },
    gameTitle: {
        color: colors.blue,
        fontWeight: '400',
        fontSize: 12,
        lineHeight: 22,
        paddingVertical: 2,
        paddingHorizontal: 8,
    },
    seeMoreText: {
        color: colors.blue,
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 17
    },
    hashtagTxt: {
        fontSize: 12,
        lineHeight: 22,
        marginHorizontal: 8
    },
    subTitle: {
        color: colors.blue,
        fontSize: 14,
        fontWeight: '900',
        marginHorizontal: 8,
        marginVertical: 16
    },
    listItem: {
        borderColor: '#EDEDED',
        backgroundColor: '#EDEDED',
        height: screen.heightscreen / 23,
        width: screen.widthscreen / 4,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 30,
        borderWidth: 1
    },
    nameListItem: {
        color: '#65676B',
        textAlign: 'center',
        fontSize: 12
    },
    rowWrap: {
        flexDirection: 'row',
        marginTop: 10,
        flexWrap: 'wrap'
    },
    nameListTimeItem: {
        fontWeight: '400',
        fontSize: 12,
        lineHeight: 14,
        paddingTop: 5
    },
    listTimeItem: {
        marginHorizontal: 2,
        marginVertical: 5,
        paddingHorizontal: 20,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: colors.mainColor
    },
    textContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 20
    },
    successText: {
        color: '#00C637',
        fontWeight: '400',
        fontSize: 10,
        paddingLeft: 10,
    },
    errorText: {
        color: '#E35C5C',
        fontWeight: '400',
        fontSize: 10,
        paddingLeft: 10,
    }
});

export default styles;