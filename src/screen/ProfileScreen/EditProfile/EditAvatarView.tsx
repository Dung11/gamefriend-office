import React, { useEffect, useState } from "react";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../../../@types";
import { Dimensions, ScrollView, View } from "react-native";
import AvatarItem from "./AvatarItem";

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
    pictures: any[]
    profilePicture: any,
    onUpdateData: (pictures: any[], profilePicture: any) => void
}

const EditAvatarView = (props: Props) => {

    const [pictures, setPictures] = useState<any[]>(props.pictures);
    const [profilePicture, setProfilePicture] = useState<any>(props.profilePicture);

    const onSubmitModal = (pictures: any, profilePicture: any) => {
        setPictures(pictures);
        setProfilePicture(profilePicture);
        props.onUpdateData(pictures, profilePicture);
    }

    const onChangeProfileImage = (data: any) => {
        if (data){
            setProfilePicture(data);
            props.onUpdateData(pictures, data);
        }
    }

    useEffect(() => {
        setPictures(props.pictures);
        setProfilePicture(props.profilePicture);

    }, [props.pictures, props.profilePicture])

    return (
        <ScrollView nestedScrollEnabled={true}
            indicatorStyle="white"
            horizontal
            style={{ marginTop: 20 }}
            showsHorizontalScrollIndicator={false}>
            <View style={{ width: Dimensions.get('screen').width * 33 / 100 }}></View>
            {
                [0, 1, 2, 3, 4].map((item: number, index: number) => {
                    return (
                        <AvatarItem
                            {...props}
                            pictures={pictures}
                            profilePicture={profilePicture}
                            onSubmit={onSubmitModal}
                            onSubmitProfileImage={onChangeProfileImage}
                            imageUrl={typeof pictures !== 'undefined' ?
                                (typeof pictures[index] !== 'undefined' ? pictures[index] : null)
                                : null}
                        />
                    );
                })
            }
        </ScrollView>
    );
}

export default EditAvatarView;