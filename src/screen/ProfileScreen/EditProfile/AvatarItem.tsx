import React, { useEffect, useState } from 'react';
import { StackNavigationProp } from "@react-navigation/stack";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { RootStackParamList } from "../../../@types";
import { Icons, Images } from "../../../assets";
import _ModifyingImageModal from './ModifyingImageModal';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
    pictures: any[]
    profilePicture: any,
    onSubmit: (pictures: any, profilePicture: any) => void,
    onSubmitProfileImage: (profilePicture: any) => void,
    imageUrl: any
}

const AvatarItem = (props: Props) => {

    const [isModifyingImageVisible, setIsModifyingImageVisible] = useState<boolean>(false);
    const [selectedImage, setSelectedImage] = useState<any>(props.profilePicture);
    const [pictures, setPictures] = useState<any>(props.pictures);
    const [imageUrl, setImageUrl] =  useState<any>(props.imageUrl);

    useEffect(() => {
        setSelectedImage(props.profilePicture);
    }, [props.profilePicture]);

    const onClick = () => {
        setSelectedImage(imageUrl)
        setIsModifyingImageVisible(true);
        props.onSubmitProfileImage(imageUrl)
    }

    const onModalSubmit = (pictures: any, profilePicture: any) => {
        setIsModifyingImageVisible(false);
        props.onSubmit(pictures, profilePicture);
    }

    useEffect(() => {
        setImageUrl(props.imageUrl);
        setSelectedImage(props.profilePicture);
        setPictures(props.pictures);
    }, [props.profilePicture, props.pictures, props.imageUrl])

    return (
        <TouchableOpacity onPress={onClick}>
            <_ModifyingImageModal
                {...props}
                visible={isModifyingImageVisible}
                onClose={() => { setIsModifyingImageVisible(false) }}
                onSubmit={onModalSubmit}
                pictures={pictures}
                profilePicture={selectedImage}
                selectedImage={selectedImage}
            />
            <Image style={styles.avatar} source={props.imageUrl ? { uri: props.imageUrl } : Images.avatar_default} />
            {
                props.profilePicture === props.imageUrl &&
                <>
                    <View style={styles.overlay} />
                    <View style={styles.wrappedContainer}>
                        <View style={styles.wrappedArea}>
                            <Image style={{ marginTop: 8, transform: [{ rotate: '180deg' }] }}
                                source={Icons.ic_check_circle_fill} />
                        </View>
                    </View>
                </>
            }
        </TouchableOpacity>
    )
}

export default AvatarItem;

const styles = StyleSheet.create({
    avatar: {
        marginHorizontal: 10,
        width: 72,
        height: 72,
        borderRadius: 50,
    },
    wrappedContainer: {
        height: 25,
        overflow: 'hidden',
        position: 'absolute',
        left: 10,
        top: 48,
        transform: [
            { rotate: '180deg' }
        ]
    },
    wrappedArea: {
        backgroundColor: '#FBFBFB',
        opacity: .7,
        minHeight: 72,
        width: 72,
        borderRadius: 100,
        justifyContent: 'center',
        flexDirection: 'row',
        paddingBottom: 100
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        marginHorizontal: 10,
        width: 72,
        height: 72,
        borderRadius: 50,
        backgroundColor: 'rgba(47,47,47,0.6)',
    }
});
