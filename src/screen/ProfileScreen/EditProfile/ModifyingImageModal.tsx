import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, View, TouchableOpacity, Alert } from 'react-native';
import Modal from 'react-native-modal';
import { Images } from '../../../assets';
import _ChooseImageModal from '../../../components/ChooseImageModal/_ChooseImageModal';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';

interface Props {
    visible: boolean,
    onSubmit: (pictures: any, profileImage: any) => void,
    onClose: () => void,
    pictures: any[],
    profilePicture: any,
    selectedImage: any,
}

const _ModifyingImageModal = (props: Props) => {

    const [visible, setVisible] = useState<boolean>(props.visible);
    const [imageModalVisible, setImageModalVisible] = useState<boolean>(false);
    const [pictures, setPictures] = useState<any[]>(props.pictures);
    const [profilePicture, setProfilePicture] = useState<any>(props.profilePicture);
    const [selectedImage, setSelectedImage] = useState<any>(props.selectedImage);

    const onChangeImage = () => {
        setImageModalVisible(true);
    }

    const onDelImage = () => {
        if (pictures.length === 1) {
            Alert.alert('오류', '사진을 1 개 이상 선택하세요.');
            return;
        }
        let filterPictures = pictures.filter((d: any) => d !== props.selectedImage);
        setPictures(filterPictures);
        if (profilePicture === props.selectedImage) {
            setProfilePicture(filterPictures[0])
        }
        setImageModalVisible(false);
        props.onSubmit(filterPictures, filterPictures[0]);
        props.onClose();
    }

    const onCloseModal = () => {
        setImageModalVisible(false);
    }

    const onChangeModal = (data: any) => {
        let picturesTmp = [...pictures];
        let changedIndex = picturesTmp.findIndex((d: any) => d === props.selectedImage);
        if (changedIndex >= 0) {
            picturesTmp[changedIndex] = data[0];
            setProfilePicture(data[0]);
            setPictures(picturesTmp);
            props.onSubmit(picturesTmp, data[0]);
        } else {
            picturesTmp.push(data[0]);
            setPictures(picturesTmp);
            setProfilePicture(data[0]);
            props.onSubmit(picturesTmp, data[0]);
        }
        setImageModalVisible(false);
        props.onClose();
    }

    useEffect(() => {
        setVisible(props.visible)
        setPictures(props.pictures);
        setProfilePicture(props.profilePicture);
        setSelectedImage(props.selectedImage);
    }, [props.visible])

    return (
        <Modal isVisible={visible} onBackdropPress={props.onClose}>
            <_ChooseImageModal
                visible={imageModalVisible}
                title={'최근에'}
                doneButtonText={'완료'}
                onClose={onCloseModal}
                onChange={onChangeModal}
                numberOfImages={1}
            />
            <View style={styles.wrapper}>
                <Image source={Images.modyfying_image} />
                <View style={styles.content}>
                    <_Text style={styles.title}>사진 변경</_Text>
                    <_Text style={styles.subTitle}>해당 사진을 변경 혹은 삭제 하시겠습니까?</_Text>
                    <View style={styles.buttonGroupContainer}>
                        <TouchableOpacity style={[styles.btnChange, { backgroundColor: '#EDF1F7' }]} onPress={onChangeImage}>
                            <_Text style={[styles.txt, { color: '#6D6F72', }]}>이미지 변경</_Text>
                        </TouchableOpacity>
                        {
                            selectedImage === null ||
                            <TouchableOpacity style={[styles.btnDel, { backgroundColor: '#4F91F3' }]} onPress={onDelImage}>
                                <_Text style={[styles.txt, { color: colors.white }]}>이 이미지 삭제</_Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        </Modal >
    )
}

export default _ModifyingImageModal;

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 24,
        paddingTop: 50,
        backgroundColor: "white",
        borderRadius: 20,
    },
    content: {
        width: '100%',
        paddingVertical: 10,
    },
    wrapperItem: {
        paddingTop: 50,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        width: '100%',
    },
    title: {
        fontWeight: '900',
        fontSize: 20,
        lineHeight: 26,
        marginTop: 25,
    },
    subTitle: {
        color: '#757575',
        fontSize: 14,
        lineHeight: 21,
        marginTop: 36,
    },
    buttonGroupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 48,
        marginBottom: 26
    },
    btnChange: {
        flex: 1,
        marginRight: 10,
        borderRadius: 10
    },
    btnDel: {
        flex: 1,
        marginLeft: 10,
        borderRadius: 10
    },
    txt: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 21,
        textAlign: 'center',
        paddingVertical: 10
    },
});