import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react'
import { Alert, Image, ScrollView, StatusBar, View } from 'react-native'
import { RootStackParamList } from '../../../@types';
import { Icons } from '../../../assets';
import _CloseHeader from '../../../components/CloseHeader/_CloseHeader';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import styles from './styles';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import CustomDistanceLabel from '../../../components/CustomDistanceLabel/CustomDistanceLabel';
import CustomLabelAge from '../../../components/CustomLabelAge/CustomLabelAge';
import _GenderModal from '../GenderModal';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { getRecommendFilter, updateRecommendFilter } from '../../../api/RecommendedServices';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import _Text from '../../../components/_Text';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
}

const GAP_NUMBER = 4;

const SuggestedFriendSetting = (props: Props) => {

    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [listAge, setListAge] = useState([22, 35]);
    const [distance, setDistance] = useState(50);
    const [genderVisible, setGenderVisible] = useState(false);
    const [recommendFilter, setRecommendFilter] = useState<any>();
    const [selectSex, setSelectSex] = useState<string>('상관없음');

    const onChangeRangeAge = (data: any) => {
        let recommendFilterTmp = { ...recommendFilter };
        let changedGte = data[0];
        let changedLte = data[1];
        recommendFilterTmp.age.gte = changedGte;
        recommendFilterTmp.age.lte = changedLte;
        setRecommendFilter(recommendFilterTmp);
    }

    const onChangeDistance = (data: any) => {
        let recommendFilterTmp = { ...recommendFilter };
        recommendFilterTmp.distance = {
            gte: 0,
            lte: data[0]
        }
        setRecommendFilter(recommendFilterTmp);
    }


    const onCloseGender = () => {
        setGenderVisible(false);
    }

    const onShowGender = () => {
        setGenderVisible(true);
    }


    const getData = () => {
        keyValueStorage.get('userID').then((userId: any) => {
            getRecommendFilter(userId).then((response: any) => {
                if (response && Object.keys(response).length !== 0) {
                    setRecommendFilter(response);
                    let sex = response.gender;
                    if (sex === 'any') {
                        setSelectSex('상관없음');
                    } else if (sex === 'male') {
                        setSelectSex('남성');
                    } else {
                        setSelectSex('여성');
                    }
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
            }).finally(() => {
                setIsLoading(false);
            })
        })
    }

    const onChangeGender = (name: string, value: string) => {
        let recommendFilterTmp = recommendFilter;
        recommendFilterTmp.gender = value
        setRecommendFilter(recommendFilterTmp);
        onCloseGender();
        setSelectSex(name);
    }

    const onSubmit = async () => {
        setIsLoading(true);
        let recommendFilterTmp: any = recommendFilter;

        if (!recommendFilter.hasOwnProperty('gender')) {
            recommendFilterTmp.gender = 'any';
        }

        if (!recommendFilter.hasOwnProperty('age')) {
            recommendFilterTmp.age = {
                gte: 22,
                lte: 35
            };
        }

        if (!recommendFilter.hasOwnProperty('distance')) {
            recommendFilterTmp.distance = {
                gte: 0,
                lte: 50
            };
        }

        let userId = await keyValueStorage.get('userID');
        let response: any = await updateRecommendFilter(userId, recommendFilterTmp);
        if (response) {
            props.navigation.goBack();
        } else {
            Alert.alert('오류63', '잠시 후 다시 시도해주세요');
        }
        setIsLoading(false);
    }

    useEffect(() => {
        setRecommendFilter({
            age: {
                gte: 22,
                lte: 35
            },
            distance: {
                gte: 0,
                lte: 50
            }
        });
        getData();
    }, [])

    return (
        <ContainerMain>
            <_CloseHeader {...props} title={'추천 친구 설정'} selectedTitle={'완료'} onSubmit={onSubmit} />
            <_GenderModal
                onClose={onCloseGender}
                visible={genderVisible}
                {...props}
                onSubmit={onChangeGender}
                selectedValue={recommendFilter?.gender} />
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <LoadingIndicator visible={isLoading} />
            <ScrollView contentContainerStyle={{ marginTop: 20 }}>
                <View style={styles.sliderContainer}>
                    <View style={styles.genderContainer}>
                        <View style={{ flex: .5 }}>
                            <_Text style={styles.title}>성별</_Text>
                        </View>
                        <View style={{ flex: .5 }}>
                            <TouchableOpacity style={styles.btnChooseGender} onPress={onShowGender}>
                                <_Text style={styles.selectedGenderText}>{selectSex}</_Text>
                                <Image source={Icons.ic_select} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.rangeAgeContainer}>
                        <_Text style={styles.title}>나이</_Text>
                        <MultiSlider
                            selectedStyle={{ backgroundColor: colors.mainColor }}
                            markerStyle={styles.market}
                            values={[recommendFilter?.age?.gte || listAge[0], recommendFilter?.age?.lte || listAge[1]]}
                            minMarkerOverlapDistance={
                                (GAP_NUMBER * (screen.widthscreen - 30)) / (50 - 15)
                            }
                            allowOverlap={false}
                            onValuesChange={onChangeRangeAge}
                            min={15}
                            max={50}
                            enableLabel
                            sliderLength={screen.widthscreen - 40}
                            customLabel={CustomLabelAge}
                        />
                    </View>
                    <View style={styles.rangeAgeContainer}>
                        <_Text style={styles.title}>거리</_Text>
                        <MultiSlider
                            selectedStyle={{ backgroundColor: colors.mainColor }}
                            markerStyle={styles.market}
                            onValuesChange={onChangeDistance}
                            values={[recommendFilter?.distance?.lte || distance]}
                            min={0}
                            max={401}
                            enableLabel
                            sliderLength={screen.widthscreen - 40}
                            customLabel={CustomDistanceLabel}
                        />
                    </View>
                </View>
                <View style={{ marginHorizontal: 8, backgroundColor: colors.white, borderRadius: 13, marginVertical: 12 }}>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 16, paddingVertical: 8 }}>
                        <View style={{ flexDirection: 'row', flex: 0.8 }}>
                            <Image style={{ marginRight: 10 }} source={Icons.ic_game_controller} />
                            <_Text style={styles.textTip}>팁</_Text>
                        </View>
                        {/* <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', flex: .2 }}>
                            <_Text style={styles.textTip}>now</_Text>
                        </View> */}
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 16, paddingVertical: 8 }}>
                        <_Text style={{ fontSize: 15, fontWeight: '900', lineHeight: 20 }}>
                            친구 설정은 추천 친구를 위해 사용됩니다.
                            매칭에서는 사용되지 않아요!
                        </_Text>
                    </View>
                </View>
            </ScrollView>
        </ContainerMain>
    )

}

export default SuggestedFriendSetting;
