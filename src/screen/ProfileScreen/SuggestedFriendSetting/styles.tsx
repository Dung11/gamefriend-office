import { StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";

const styles = StyleSheet.create({
    sliderContainer: {
        paddingHorizontal: 16,
        backgroundColor: colors.white
    },
    title: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 20,
        paddingBottom: 40
    },
    selectedGenderText: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 20,
        color: colors.blue,
        marginRight: 10
    },
    genderContainer: {
        flexDirection: 'row',
        paddingVertical: 12,
        borderBottomColor: '#EEEEEF',
        borderBottomWidth: 1
    },
    rangeAgeContainer: {
        borderBottomColor: '#EEEEEF',
        borderBottomWidth: 1,
        paddingVertical: 12,
        justifyContent: 'center',
    },
    market: {
        backgroundColor: colors.white,
        width: 30,
        height: 30,
        elevation: 10,
        borderRadius: 30
    },
    btnChooseGender: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    textTip: {
        fontWeight: '400',
        fontSize: 13,
        lineHeight: 18,
        color: '#C4C4C6'
    }
});

export default styles;