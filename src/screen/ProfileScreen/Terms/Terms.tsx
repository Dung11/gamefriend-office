import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp, StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { ItemPolicy } from './ItemPolicy';
import { RouteProp } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { Image, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { colors } from '../../../config/Colors';
import { Icons } from '../../../assets';
import DeviceInfo from 'react-native-device-info';
import _Text from '../../../components/_Text';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}
const Terms = (props: Props) => {

    const [isLoading, setIsLoading] = useState<boolean>(false);

    return (
        <ContainerMain>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <LoadingIndicator visible={isLoading} />
            <View style={styles.header}>
                <View style={styles.titleContainer}>
                    <_Text style={styles.headerTitle}>이용 약관</_Text>
                </View>
            </View>
            <ScrollView style={styles.termCondition}>
                <ItemPolicy type="privacy" />
                <ItemPolicy type="service" />
                <ItemPolicy type="board-service" />
                <View style={styles.emptySpace} />
            </ScrollView>
            <View style={styles.backButton} >
                <TouchableOpacity onPress={() => { props.navigation.goBack() }}>
                    <Image style={styles.icon} source={Icons.ic_back} />
                </TouchableOpacity>
            </View>
        </ContainerMain>
    );
}

export default Terms;

const styles = StyleSheet.create({
    header: {
        backgroundColor: colors.mainColor,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    headerTitle: {
        fontSize: 16,
        fontWeight: '900',
        lineHeight: 23,
        color: colors.white
    },
    icon: {
        resizeMode: 'contain'
    },
    termCondition: {
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 20,
    },
    backButton: {
        padding: 20,
        position: 'absolute',
        left: 20,
        bottom: 20,
    },
    emptySpace: {
        height: 100,
    },
});