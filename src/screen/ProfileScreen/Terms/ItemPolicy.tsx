import React, { useEffect, useState } from 'react';
import { StyleSheet, View, ViewStyle, Switch, ScrollView, Dimensions } from "react-native";
import { getPrivacyPolicy, getServicePolicy, getBoardServicePolicy } from '../../../api/UserServices';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';

interface Props {
    styles?: ViewStyle;
    type: 'privacy' | 'service' | 'board-service';
}

export const ItemPolicy = (props: Props) => {

    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');

    useEffect(() => {
        if (props.type == 'privacy') {
            getPrivacyPolicy().then((response: any) => {
                if (response?.data) {
                    setTitle(response.data.title);
                    setContent(response.data.content);
                }
            });
        } else if (props.type == 'service') {
            getServicePolicy().then((response: any) => {
                if (response?.data) {
                    setTitle(response.data.title);
                    setContent(response.data.content);
                }
            });
        } else if (props.type == 'board-service') {
            getBoardServicePolicy().then((response: any) => {
                if (response?.data) {
                    setTitle(response.data.title);
                    setContent(response.data.content);
                }
            });
        }
        return () => { }
    }, [])

    return (
        <View style={[styles.container, props.styles]}>
            <ScrollView style={styles.contentContainer} showsVerticalScrollIndicator={false}>
                <_Text style={styles.title}>{title}</_Text>
                <_Text style={styles.textContent}>{content}</_Text>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingVertical: 10,
        width: '100%',
        borderRadius: 13,
        height: Dimensions.get('screen').height / 2.5,
        marginBottom: 20,
    },
    contentContainer: {
        paddingHorizontal: 20,
    },
    title: {
        color: colors.mainColor,
        fontSize: 16,
        fontWeight: '800',
        lineHeight: 23,
        textAlign: 'center',
        marginBottom: 16
    },
    textContent: {
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 20
    },
})
