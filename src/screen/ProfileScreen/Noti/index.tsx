import { NavigationProp, RouteProp } from "@react-navigation/native"
import React, { useEffect, useState } from "react"
import { Alert, StatusBar, StyleSheet, Switch, View } from "react-native"
import { RootStackParamList } from "../../../@types"
import { setNotiAll, setNotiRecommend } from "../../../api/GeneralServices"
import BackHeader from "../../../components/BackHeader/BackHeader"
import { ContainerMain } from "../../../components/Container/ContainerMain"
import LoadingIndicator from "../../../components/LoadingIndicator/LoadingIndicator"
import _Text from "../../../components/_Text"
import { colors } from "../../../config/Colors"
import { personalProfileRequest } from "../../../redux/personal-profile/actions"
import store from "../../../redux/store"
import { keyValueStorage } from "../../../storage/keyValueStorage"

type Params = {
    params: {
        notiAll: boolean,
        notiRecommend: boolean
    }
}

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    route: RouteProp<Params, 'params'>
}

const Noti = (props: Props) => {

    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [notiAllAlarm, setNotiAllAlarm] = useState<boolean>(false);
    const [notiRecommendAlarm, setNotiRecommendAlarm] = useState<boolean>(false);

    useEffect(() => {
        const { route }: any = props;
        let { notiAll, notiRecommend } = route.params;
        setNotiAllAlarm(notiAll);
        setNotiRecommendAlarm(notiRecommend);
    }, []);

    const onDone = (response: any) => {
        setIsLoading(false);
    }

    const  toggleSwitch = async (type: string) => {
        try {
            setIsLoading(true)
            let userId: any = await keyValueStorage.get("userID");
            if (type === 'notiAlarm') {
                let isSuccess = await setNotiAll(userId, notiAllAlarm ? 'unmute': 'mute');
                if (isSuccess) {
                    setNotiAllAlarm(!notiAllAlarm);
                    store.dispatch(personalProfileRequest({
                        id: userId
                        , callback: onDone
                    }));
                } else {
                    Alert.alert('오류', '잠시 후 다시 시도해주세요');
                }
            } else if (type === 'notiRecommend') {
                let isSuccess = await setNotiRecommend(userId, notiRecommendAlarm ? 'unmute': 'mute');
                if (isSuccess) {
                    setNotiRecommendAlarm(!notiRecommendAlarm);
                    store.dispatch(personalProfileRequest({
                        id: userId
                        , callback: onDone
                    }));
                } else {
                    Alert.alert('오류', '잠시 후 다시 시도해주세요');
                }
            }
        } catch (error) {
            Alert.alert('오류', error);
        } finally {
            setIsLoading(false);
        }
    }

    return (
        <ContainerMain>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <LoadingIndicator visible={isLoading} />
            <BackHeader {...props} />
            <View style={styles.listItemContainer}>
                <_Text style={styles.listItemLabel}>전체 알림</_Text>
                <Switch
                    style={{ position: 'absolute', right: 24 }}
                    trackColor={{ false: '#f0f0f0', true: colors.blue }}
                    thumbColor={notiAllAlarm ? '#F7FAFC' : '#F7FAFC'}
                    ios_backgroundColor="#dddddd"
                    onValueChange={() => { toggleSwitch('notiAlarm') }}
                    value={!notiAllAlarm}
                />
            </View>
            <View style={styles.listItemContainer}>
                <_Text style={styles.listItemLabel}>추천 친구 알림</_Text>
                <Switch
                    style={{ position: 'absolute', right: 24 }}
                    trackColor={{ false: '#f0f0f0', true: colors.blue }}
                    thumbColor={notiRecommendAlarm ? '#F7FAFC' : '#F7FAFC'}
                    ios_backgroundColor="#dddddd"
                    onValueChange={() => { toggleSwitch('notiRecommend') }}
                    value={!notiRecommendAlarm}
                />
            </View>
        </ContainerMain>
    )
}

export default Noti;

const styles = StyleSheet.create({
    listItemContainer: {
        width: '100%',
        backgroundColor: '#F7FAFC',
        paddingVertical: 18,
        paddingHorizontal: 21,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    listItemLabel: {
        fontSize: 14,
        lineHeight: 23.17,
        fontWeight: '500',
        marginLeft: 15,
    },
});