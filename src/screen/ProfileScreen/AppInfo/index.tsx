import React, { useEffect, useState } from 'react'
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../../../@types";
import { ContainerMain } from '../../../components/Container/ContainerMain';
import BackHeader from '../../../components/BackHeader/BackHeader';
import { Image, StyleSheet, View, TouchableOpacity, Linking } from 'react-native';
import { Icons } from '../../../assets';
import { colors } from '../../../config/Colors';
import _Text from '../../../components/_Text';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
}

const AppInfo = (props: Props) => {

    const onSendMail = () => {
        Linking.openURL('mailto:gamefriend@gmail.com')
    }

    return (
        <ContainerMain>
            <BackHeader {...props} />
            <View style={{ backgroundColor: colors.white, paddingHorizontal: 16, marginTop: 12, paddingVertical: 10 }}>
                <_Text style={styles.title}>문의하기(공휴일 제외 오전 10시부터 7시까지 운영)</_Text>
                <View style={{ justifyContent: 'flex-end', flexDirection: 'row', marginTop: 30 }}>
                    <TouchableOpacity style={styles.centerRow} onPress={onSendMail}>
                        <_Text style={styles.emailText}>gamefriend.official@gmail.com</_Text>
                    </TouchableOpacity>
                </View>
            </View>
            <TouchableOpacity style={{ backgroundColor: colors.white, paddingHorizontal: 16, marginTop: 12, paddingVertical: 10 }}
                onPress={() => { props.navigation.navigate('AppInfoMenu') }}>
                <View style={styles.centerRow}>
                    <View style={[{ flex: .8, flexDirection: 'row', alignItems: 'center', }]}>
                        <_Text style={styles.title}>겜친, 무엇이 더필요한가요?</_Text>
                    </View>
                    <View style={{ flex: .2, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Image source={Icons.ic_gray_round_right} />
                    </View>
                </View>
            </TouchableOpacity>
        </ContainerMain>
    )
}

export default AppInfo;

const styles = StyleSheet.create({
    title: {
        fontWeight: '500',
        fontSize: 16,
        color: '#1C1E21',
        lineHeight: 23
    },
    emailText: {
        color: colors.blue,
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 22
    },
    centerRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
});