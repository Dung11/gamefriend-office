
import { GoogleSignin } from '@react-native-community/google-signin';
import KakaoLogins from '@react-native-seoul/kakao-login';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react';
import { View, StatusBar, Image, Alert, DeviceEventEmitter } from 'react-native';
import { LoginManager } from 'react-native-fbsdk';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { RootStackParamList } from '../../@types';
import { withdrawUser, updateGameFilter } from '../../api/UserServices';
import { Icons, Images } from '../../assets';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';
import { personalProfileRequest } from '../../redux/personal-profile/actions';
import { personalTendencyRequest } from '../../redux/personal-tendency/actions';
import store, { AppState } from '../../redux/store';
import { disconnect } from '../../socketio/socketController';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { removeRefreshData } from '../../utils/UserUtils';
import { _ChoosingGameModal } from './EditProfile/ChoosingGameModal';
import styles from './styles';
import _Text from '../../components/_Text';
import _ModalShowImage from '../../components/Modal/_ModalShowImage';
import { removeTopics } from '../../utils/setupfirebase';
import _ModalLogout from '../../components/Modal/_ModalLogout';
import { logout } from '../../api/AuthorServices';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
    profile: any,
    tendency: any,
}

const ProfileScreen = ({ navigation, profile, tendency }: Props) => {

    const [isProfileLoading, setIsProfileLoading] = useState<boolean>(true);
    const [isTendencyLoading, setIsTendencyLoading] = useState<boolean>(true);
    const [isSearchGameVisible, setIsSearchGameVisible] = useState<boolean>(false);
    const [profileData, setProfileData] = useState<any>(profile);
    const [tendencyData, setTendencyData] = useState<any>(tendency);
    const [hashtag, setHashtag] = useState<string>('');
    const [gameFilterTag, setGameFilterTag] = useState<string>('');
    const [showingImage, setShowingImage] = useState<string[]>([]);
    const [visibleShowImg, setvisibleShowImg] = useState<boolean>(false);

    const [content, setContent] = useState<string>('');
    const [title, setTitle] = useState<string>('');
    const [logoutConfirmVisible, setLogoutConfirmVisible] = useState<boolean>(false);
    const [status, setStatus] = useState<'logout' | 'withdrawl'>('logout')

    const onDone = (response: any) => {
        setIsProfileLoading(false);
        setProfileData(response);
    }

    const onTendencyDone = (response: any) => {
        setIsTendencyLoading(false);
        setTendencyData(response);
        if (response) {
            let hashTagTmp = '';
            for (let i = 0; i < response.length; i++) {
                let curTendency = response[i];
                hashTagTmp += `${curTendency.tag} `
            }
            setHashtag(hashTagTmp);
        }
    }

    const getPersonProfileData = () => {
        keyValueStorage.get("userID").then((userId: any) => {
            store.dispatch(personalProfileRequest({ id: userId, callback: onDone }));
            store.dispatch(personalTendencyRequest({ id: userId, callback: onTendencyDone }));
        });
    }

    const onWithdrawUser = async () => {
        let userId: any = await keyValueStorage.get("userID");
        let response = await withdrawUser(userId);
        if (response) {
            onLogout(true);
        } else {
            Alert.alert('오류57', '잠시 후 다시 시도해주세요');
        }
    }

    const removeKeySeacrh = async () => {
        await keyValueStorage.delete("datasearchlivetalk");
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {

            removeKeySeacrh()
        });
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        getPersonProfileData();
        setTendencyData(tendency);
        setProfileData(profile);
    }, []);

    useEffect(() => {
        setTendencyData(tendency);
        let hashTagTmp = '';
        for (let i = 0; i < tendency.length; i++) {
            let curTendency = tendency[i];
            hashTagTmp += `${curTendency.tag} `
        }
        setHashtag(hashTagTmp);
    }, [tendency]);

    useEffect(() => {
        setProfileData(profile);
        if (profile) {
            let gameFilterTagTmp = '';
            for (let i = 0; i < profile?.gameFilters?.length; i++) {
                let curGame = profile.gameFilters[i];
                gameFilterTagTmp += `#${curGame.name} `
            }
            setGameFilterTag(gameFilterTagTmp);
        }
    }, [profile])

    const onConfirm = (isWithdrawl = false) => {
        let content = '로그아웃 하시겠습니까?';
        let title = '로그아웃';
        if (isWithdrawl) {
            title = '탈퇴';
            content = '정말 탈퇴 하시겠어요? 탈퇴 후 15일 전까지 재접속 시 탈퇴처리가 취소됩니다.';
            setStatus('withdrawl');
        } else {
            setStatus('logout');
        }
        setContent(content);
        setTitle(title);
        setLogoutConfirmVisible(true);
    }

    const onConfirmDone = async (status: 'logout' | 'withdrawl') => {
        if (status === 'logout') {
            await onLogout();
            setLogoutConfirmVisible(false);
        } else {
            await onWithdrawUser();
            setLogoutConfirmVisible(false);
        }
    }

    const onLogout = async (isWithdrawl = false) => {
        try {
            if (isWithdrawl === false) {
                const result = await logout();
                if (result.status !== 200) {
                    console.log('logout result: ', result);
                }
            }
            DeviceEventEmitter.emit("showWaitingLocalRoom", { visible: false });
            DeviceEventEmitter.emit("showWattingMatch", { visible: false });
            DeviceEventEmitter.emit("showWattingMatchTwoPoint", { visible: false });
            await removeTopics();
            await keyValueStorage.delete("access-token");
            await keyValueStorage.delete("userID");
            await keyValueStorage.delete("userName");
            await keyValueStorage.delete("paramsMatch");
            await keyValueStorage.delete("showModalMatch");
            await keyValueStorage.delete("messageIntroduction");
            await keyValueStorage.delete("datasearchlivetalk");
            await keyValueStorage.delete("dataSearchGameLivetalk");
            KakaoLogins.logout()
            LoginManager.logOut();
            GoogleSignin.configure({});
            GoogleSignin.signOut();

            await removeRefreshData();
            disconnect();
            navigation.replace('Author');

        } catch (error) {
            Alert.alert('오류58', '잠시 후 다시 시도해주세요');
        }
    }

    if (isProfileLoading || isTendencyLoading) {
        return (
            <>
                <StatusBar barStyle="dark-content" />
                <LoadingIndicator visible={true} />
            </>
        )
    }

    const onSearchGameModalSubmit = async (data: any, type: 'favorite-game' | 'game' | 'game-filter') => {
        setIsSearchGameVisible(false);
        setIsProfileLoading(true);
        let userId: any = await keyValueStorage.get("userID");
        let gameIds = [];
        for (let i = 0; i < data.length; i++) {
            const curGame = data[i];
            gameIds.push(curGame.id);
        }
        console.log('gameIds: ', gameIds);
        let response = await updateGameFilter(userId, { gameIds });
        if (response) {
            store.dispatch(personalProfileRequest({ id: userId, callback: onDone }));
        }
    }

    const onCloseModalShowImage = () => {
        setvisibleShowImg(false);
    }

    const onClickAvatar = (urls: string[]) => {
        setShowingImage(urls);
        setvisibleShowImg(true);
    }

    return (
        <>
            <StatusBar barStyle="dark-content" />
            {
                profileData && <_ChoosingGameModal
                    {...navigation}
                    visible={isSearchGameVisible}
                    onClose={() => { setIsSearchGameVisible(false) }}
                    onSubmit={onSearchGameModalSubmit}
                    numberOfChoice={5}
                    bannedGames={[]}
                    choices={profileData?.gameFilters || []}
                    type={'game'} />
            }
            {
                (visibleShowImg && showingImage.length !== 0) &&
                <_ModalShowImage
                    visible={visibleShowImg}
                    onClose={onCloseModalShowImage}
                    banner={showingImage} />
            }
            {
                logoutConfirmVisible &&
                <_ModalLogout
                    visible={logoutConfirmVisible}
                    content={content}
                    title={title}
                    status={status}
                    onClose={() => setLogoutConfirmVisible(false)}
                    onSubmit={onConfirmDone} />
            }
            <ScrollView contentContainerStyle={{ backgroundColor: colors.backgroundApp }}>
                <View style={styles.sectionHeader}>
                    <View style={styles.blueSubSectionHeader}>
                        <_Text style={styles.sectionTitle}>프로필</_Text>
                    </View>
                    <View style={styles.avatarBackground}>
                        <TouchableOpacity style={styles.avatarContainer} onPress={() => onClickAvatar(profileData?.pictures)}>
                            <Image style={styles.avatar} source={profileData?.profilePicture ?
                                { uri: profileData?.profilePicture } :
                                Images.avatar_default} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.sectionContainer}>
                    <_Text style={styles.username}>{profileData?.userName}</_Text>
                    <View style={{ flexDirection: 'row', marginTop: 16 }}>
                        <TouchableOpacity style={styles.btnEdit} onPress={() => { navigation.navigate('EditProfile') }}>
                            <Image style={{ marginRight: 13 }} source={Icons.ic_white_pencil} />
                            <_Text style={styles.editText}>프로필 수정</_Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnSetup} onPress={() => { navigation.navigate('SuggestedFriendSetting') }}>
                            <_Text style={styles.setupText}>추천 친구 설정</_Text>
                            <Image style={{ width: 18, height: 18 }} source={Icons.ic_diamond} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ backgroundColor: colors.white, borderTopLeftRadius: 16, borderTopRightRadius: 16, marginTop: 28 }}>
                    <View style={{ padding: 16 }}>
                        <TouchableOpacity style={styles.itemContainer}
                            onPress={() => {
                                navigation.navigate('Propensity', {
                                    tendency: tendencyData,
                                    hashtag,
                                })
                            }}>
                            <View style={styles.itemHeader}>
                                <Image style={styles.icon} source={Icons.ic_hashtag} />
                                <_Text>성향</_Text>
                            </View>
                            {hashtag.length > 0 && <_Text style={styles.itemContent}>{hashtag}</_Text>}
                            <View style={styles.flexRowEnd}>
                                <View style={styles.centerRow}>
                                    <_Text style={styles.seeMoreText}>더보기</_Text>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.itemContainer}>
                            <View style={styles.itemHeader}>
                                <Image style={styles.icon} source={Icons.ic_crosshair} />
                                <_Text>위치</_Text>
                            </View>
                            <_Text style={styles.itemContent}>#{profileData?.address}</_Text>
                        </View>
                        <TouchableOpacity style={styles.itemContainer}
                            onPress={() => {
                                navigation.navigate('Noti', {
                                    notiAll: profileData?.isMutedNotificationAll,
                                    notiRecommend: profileData?.isMutedNotificationRecommend
                                })
                            }}>
                            <View style={styles.centerRow}>
                                <View style={styles.itemTitle}>
                                    <Image style={styles.icon} source={Icons.ic_bell_noti} />
                                    <_Text>알림</_Text>
                                </View>
                                <View style={styles.itemIcon}>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.itemContainer}>
                            <View style={styles.itemHeader}>
                                <Image style={styles.icon} source={Icons.ic_card_bulleted} />
                                <_Text>게시판</_Text>
                            </View>
                            {gameFilterTag.length > 0 && <_Text style={styles.itemContent}>{gameFilterTag}</_Text>}
                            <View style={styles.flexRowEnd}>
                                <TouchableOpacity style={styles.centerRow} onPress={() => {
                                    let isSearchGameVisibleTmp = isSearchGameVisible;
                                    setIsSearchGameVisible(!isSearchGameVisibleTmp);
                                }}>
                                    <_Text style={styles.seeMoreText}>선호게시판  설정하기</_Text>
                                    <Image source={Icons.ic_round_right} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <TouchableOpacity style={styles.itemContainer} onPress={() => {
                            navigation.navigate('AppInfo')
                        }}>
                            <View style={styles.centerRow}>
                                <View style={styles.itemTitle}>
                                    <Image style={styles.icon} source={Icons.ic_circle_question} />
                                    <_Text>고객문의</_Text>
                                </View>
                                <View style={styles.itemIcon}>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemContainer}
                            onPress={() => { navigation.navigate('Terms') }}>
                            <View style={styles.centerRow}>
                                <View style={styles.itemTitle}>
                                    <Image style={styles.icon} source={Icons.ic_terminal_fill} />
                                    <_Text>사용자 약관</_Text>
                                </View>
                                <View style={styles.itemIcon}>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemContainer} onPress={() => onConfirm()}>
                            <View style={styles.centerRow}>
                                <View style={styles.itemTitle}>
                                    <Image style={styles.icon} source={Icons.ic_bx_bx_exit} />
                                    <_Text>로그아웃</_Text>
                                </View>
                                <View style={styles.itemIcon}>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.itemContainer, { marginBottom: 20 }]} onPress={() => onConfirm(true)}>
                            <View style={styles.centerRow}>
                                <View style={styles.itemTitle}>
                                    <Image style={styles.icon} source={Icons.ic_sign_out_alt} />
                                    <_Text>탈퇴</_Text>
                                </View>
                                <View style={styles.itemIcon}>
                                    <Image source={Icons.ic_round_right} />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </>
    );
};

const mapStateToProps = (state: AppState): Partial<Props> => ({
    profile: state.PersonalProfileReducer.profile,
    tendency: state.PersonalTendencyReducer.tendency
});

export default connect(mapStateToProps, null)(ProfileScreen);
