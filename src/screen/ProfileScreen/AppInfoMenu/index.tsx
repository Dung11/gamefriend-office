import React, { useState } from 'react'
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../../../@types";
import { ContainerMain } from '../../../components/Container/ContainerMain';
import BackHeader from '../../../components/BackHeader/BackHeader';
import { Image, StyleSheet, View, TouchableOpacity, Linking, Platform } from 'react-native';
import { Icons } from '../../../assets';
import { colors } from '../../../config/Colors';
import _Text from '../../../components/_Text';
import Environments from "../../../config/Environments";
import _ReviewModal from './ReviewModal';


interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
}

const AppInfoMenu = (props: Props) => {

    const [visible, setVisible] = useState<boolean>(false);

    const onClose = () => {
        setVisible(false);
    }

    const onSubmit = () => {
        setVisible(false);
        props.navigation.navigate('Profile');
    }

    const onOpenLink = () => {
        if (Platform.OS === 'android') {
            Linking.openURL(Environments.ANDROID_APP_URL);
        } else {
            Linking.openURL(Environments.IOS_APP_URL);
        }
    }

    return (
        <ContainerMain>
            <_ReviewModal
                {...props}
                visible={visible}
                onSubmit={onSubmit}
                onClose={onClose} />
            <BackHeader {...props} />
            <View style={{ backgroundColor: colors.white, paddingHorizontal: 16, marginTop: 12, paddingVertical: 10 }}>
                <TouchableOpacity style={styles.centerRow} onPress={() => setVisible(true)}>
                    <View style={[{ flex: .8, flexDirection: 'row', alignItems: 'center', }]}>
                        <_Text style={styles.title}>새로운 기능이 필요해요</_Text>
                    </View>
                    <View style={{ flex: .2, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Image source={Icons.ic_gray_round_right} />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ backgroundColor: colors.white, paddingHorizontal: 16, marginTop: 12, paddingVertical: 10 }}>
                <TouchableOpacity style={styles.centerRow} onPress={() => setVisible(true)}>
                    <View style={[{ flex: .8, flexDirection: 'row', alignItems: 'center', }]}>
                        <_Text style={styles.title}>이건 좀 고쳐주세요</_Text>
                    </View>
                    <View style={{ flex: .2, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Image source={Icons.ic_gray_round_right} />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ backgroundColor: colors.white, paddingHorizontal: 16, marginTop: 12, paddingVertical: 10 }}>
                <_Text style={styles.title}>지금도 좋아요</_Text>
                <View style={{ justifyContent: 'flex-end', flexDirection: 'row', marginTop: 30 }}>
                    <TouchableOpacity style={styles.centerRow} onPress={onOpenLink}>
                        <_Text style={styles.emailText}>리뷰 남기기</_Text>
                        <Image source={Icons.ic_round_right} />
                    </TouchableOpacity>
                </View>
            </View>
        </ContainerMain>
    )
}

export default AppInfoMenu;

const styles = StyleSheet.create({
    title: {
        fontWeight: '500',
        fontSize: 16,
        color: '#1C1E21',
        lineHeight: 23
    },
    emailText: {
        color: colors.blue,
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 22
    },
    centerRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
});