import React, { useEffect, useState } from "react";
import Modal from 'react-native-modal';
import { StyleSheet, TouchableOpacity, View, Alert, TextInput, ScrollView } from 'react-native';
import { RootStackParamList } from "../../../@types";
import { NavigationProp } from "@react-navigation/native";
import LoadingIndicator from "../../../components/LoadingIndicator/LoadingIndicator";
import { colors } from "../../../config/Colors";
import { feedback } from "../../../api/UserServices";
import DeviceInfo from 'react-native-device-info';
import _Text from "../../../components/_Text";
import Environments from "../../../config/Environments";
import ToastUtils from "../../../utils/ToastUtils";

interface Props {
    visible?: boolean,
    onSubmit: () => void,
    onClose: () => void,
    navigation: NavigationProp<RootStackParamList, any>,
}

const _ReviewModal = (props: Props) => {

    const { onClose } = props;
    const [isLoadingInView, setIsLoadingInView] = useState(false);
    const [visible, setVisible] = useState(props.visible);
    const [reviewText, setReviewText] = useState<string>('');

    useEffect(() => {
        setVisible(props.visible);
    }, [props.visible]);

    const onSubmit = async () => {
        if (reviewText.length === 0) {
            Alert.alert('오류', '피드백 내용을 입력해주세요');
            return;
        }
        setIsLoadingInView(true);
        const response = await feedback({
            content: {
                type: 'text',
                value: reviewText
            },
        })
        if (!response) {
            Alert.alert('오류59', '잠시 후 다시 시도해주세요');
        } else {
            setReviewText('');
            props.onSubmit();
            ToastUtils.showToast({
                text2: '제출이 완료되었습니다',
                type: 'info',
                position: 'bottom'
            });
        }
        setIsLoadingInView(false);
    }

    const _onChangeText = (txt: string) => {
        setReviewText(txt);
    };

    return (
        <Modal
            isVisible={visible} onBackdropPress={onClose}>
            <LoadingIndicator visible={isLoadingInView} />
            <View style={styles.wrapper}>
                <ScrollView style={styles.content}>
                    <View style={styles.header}>
                        <_Text style={[styles.headerTitle]}>회원님의 소중한 의견을 반영해</_Text>
                        <_Text style={[styles.headerTitle]}>더 발전하겠습니다.</_Text>
                    </View>
                    <View style={styles.bottomModal}>
                        <View style={styles.textInputModalView}>
                            <TextInput
                                style={styles.textInputStyle}
                                placeholder='리뷰를 입력해주세요'
                                numberOfLines={6}
                                multiline={true}
                                textAlignVertical={'top'}
                                onChangeText={_onChangeText}
                            />
                        </View>
                        <TouchableOpacity style={styles.btnSubmit} onPress={onSubmit}>
                            <_Text style={styles.btnSubmitText}>제출</_Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        </Modal>
    )
}

export default _ReviewModal;

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 16,
    },
    content: {
        borderRadius: 13,
        width: '100%',
        backgroundColor: colors.white,
    },
    header: {
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 14,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    headerTitle: {
        color: colors.blue,
        fontSize: 16,
        lineHeight: 23,
        fontWeight: '800',
    },
    btn: {
        backgroundColor: colors.white,
    },
    btnSubmit: {
        marginVertical: 4,
        backgroundColor: colors.blue,
        borderRadius: 30,
        marginHorizontal: 20,
        marginTop: 20
    },
    btnText: {
        color: '#65676B',
        paddingVertical: 16,
        fontWeight: '400',
        fontSize: 16,
        lineHeight: 24
    },
    btnSubmitText: {
        color: colors.white,
        textAlign: 'center',
        paddingVertical: 10,
        fontWeight: '500',
        fontSize: 20,
        lineHeight: 25,
    },
    bottomModal: {
        backgroundColor: colors.white,
        paddingVertical: 16,
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
    },
    textInputModalView: {
        backgroundColor: '#ECF1F9',
        borderRadius: 4,
        marginHorizontal: 20,
    },
    textInputStyle: {
        minHeight: 150,
        fontFamily: Environments.DefaultFont.Regular,
        fontSize: 14
    }
});