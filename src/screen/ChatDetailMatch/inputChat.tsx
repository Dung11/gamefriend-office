
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Image, ViewStyle, Platform } from 'react-native';
import { colors } from '../../config/Colors';
import { Icons } from '../../assets';
import { screen } from '../../config/Layout';
import DeviceInfo from 'react-native-device-info';
import Environments from '../../config/Environments';

interface Props {
    message: string,
    onChangeMessage: (value: string) => void,
    onChooseImage: () => void,
    _sendMessage: () => void,
    image: string
}

export const InputChat = (props: Props) => {

    const [canSend, setCanSend] = useState<boolean>(false);

    useEffect(() => {
        if (props.message !== '' || props.image !== '') {
            setCanSend(true);
        } else {
            setCanSend(false);
        }
    }, [props.message, props.image]);

    const formChatStyle: ViewStyle = {
        ...styles.formChat,
        paddingBottom: DeviceInfo.hasNotch() ? 15 : 5,
    }

    return (
        <View style={formChatStyle} >
            <View style={styles.upImageContainer}>
                <TouchableOpacity style={styles.chooseImgBtn} onPress={props.onChooseImage}>
                    <Image style={styles.upImage} source={Icons.ic_add_image} />
                </TouchableOpacity>
            </View>
            <View style={styles.input}>
                <TextInput value={props.message}
                    onChangeText={props.onChangeMessage}
                    style={styles.messageInput}
                    multiline
                    numberOfLines={1}
                />
                <TouchableOpacity style={styles.sendBtn} disabled={!canSend} onPress={props._sendMessage} >
                    <Image style={{ width: 30, height: 30, tintColor: (canSend ? colors.mainColor : colors.gray) }} source={Icons.ic_send} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        flex: 8,
        backgroundColor: '#EEEEEF',
        borderRadius: 20,
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // paddingVertical: 7.5,
        marginRight: 10,
        marginBottom: DeviceInfo.hasNotch() ? 10 : 5,
    },
    textInput: {
        flexDirection: 'row',
        width: '100%',
    },
    upImageContainer: {
        flex: 1.5,
    },
    upImage: {
        marginLeft: 5
    },
    sendBtn: {
        position: 'absolute',
        right: 10,
    },
    formChat: {
        width: screen.widthscreen,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        paddingVertical: 5,
        position: 'absolute',
        bottom: 0,
    },
    messageInput: {
        paddingLeft: 15,
        paddingRight: 40,
        fontSize: 14,
        width: '100%',
        maxHeight: 100,
        fontFamily: Environments.DefaultFont.Regular,
        minHeight: 40,
        margin: 0,
        padding: 0,
    },
    chooseImgBtn: {
        alignItems: 'center',
        marginRight: 5,
    },
});
