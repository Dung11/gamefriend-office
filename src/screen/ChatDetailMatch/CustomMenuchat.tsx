import React, { useEffect, useState } from "react";
import { DrawerContentScrollView } from "@react-navigation/drawer";

import { StyleSheet, View, Image, TouchableOpacity, Alert, Platform } from "react-native";
import { RootStackParamList } from "../../@types";
import { Images, Icons } from "../../assets";
import { colors } from '../../config/Colors';
import { keyValueStorage } from "../../storage/keyValueStorage";
import { StackNavigationProp } from "@react-navigation/stack";
import { connect, ConnectedProps } from "react-redux";
import { AppState } from "../../redux/store";
import { profileRequest } from "../../redux/profile/actions";
import _ReportModal from "../ChatDetailScreen/ReportModal";
import _ReviewModal from "../ChatDetailScreen/ReviewModal";
import LoadingIndicator from "../../components/LoadingIndicator/LoadingIndicator";
import DeviceInfo from 'react-native-device-info';
import _Text from "../../components/_Text";
import { cancelRequestAddFriend, getRoomMemberInformation, muteNoticationAction, outRoomChat, sendFriendRequest } from "../../api/ChatServices";

interface RouteItem {
    key: string,
    name: string,
    params: {
        nameFriend: string,
        roomId: string,
        roomType: string,
        roomName: string
    }
}

interface Props extends ReduxType {
    navigation: StackNavigationProp<RootStackParamList>
    dataUser: any;
    state: {
        history: any[],
        index: number,
        key: string,
        routeNames: any[],
        routes: RouteItem[],
        stale: boolean,
        type: string
    }
}
const CustomMenuChat = (props: Props) => {
    const [user, setUser] = useState('');
    const [isNotify, setIsNotify] = useState(true);
    const [isSendRequest, setIsSendRequest] = useState(false);
    const [userIdMe, setUserIdMe] = useState('')
    const [isLoading, setIsLoading] = useState(false);
    const [roomId, setRoomId] = useState('');
    const [isShowReport, setIsShowReport] = useState(false);
    const [isShowReview, setIsShowReview] = useState(false);
    const [listMember, setListMember] = useState<any>([])

    const onLeavingRoom = async () => {
        const { navigation } = props;
        try {
            const res = await outRoomChat(userIdMe, roomId)
            if (res.status == 200) {
                await keyValueStorage.delete("messageIntroduction");
                await keyValueStorage.delete("paramsMatch");
                navigation.navigate("Tabs");
            } else {
                Alert.alert('오류17', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error)
        }
    }

    const getUser = async () => {
        const username = await keyValueStorage.get("userName") as any
        setUser(username)
        const userId = await keyValueStorage.get("userID") as any
        setUserIdMe(userId)
    }

    const getListMember = async (id: string) => {
        const userId = await keyValueStorage.get("userID") as any
        try {
            const result = await getRoomMemberInformation({ userId: userId, roomId: id })
            if (result) {
                setListMember(result)
            } else {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error)
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        }
    }

    const addFriend = async (userIdB: string) => {
        try {
            const res = await sendFriendRequest(userIdMe, userIdB)

            if (res) {
                setIsSendRequest(true)
            } else {
                Alert.alert('오류18', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error)
        }
    }

    const cancelAddFriend = async (userIdB: string) => {
        try {
            const res = await cancelRequestAddFriend(userIdMe, userIdB)
            if (res) {
                setIsSendRequest(false)
            } else {
                Alert.alert('오류19', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getUser()
        let chatDetailInfo = props?.state?.routes.find((e) => e.name === 'ChatDetailMatch');
        if (chatDetailInfo) {
            getListMember(chatDetailInfo.params.roomId)
            setRoomId(chatDetailInfo.params.roomId);
        }

    }, []);

    const onCloseReport = () => {
        setIsShowReport(false);
    }

    const onOpenReport = () => {
        setIsShowReport(true);
    }

    const onReviewFriend = async () => {
        let chatDetailInfo = props.state.routes.find((e) => e.name === 'ChatDetailMatch');
        if (chatDetailInfo) {
            setIsShowReview(true)
        } else {
            Alert.alert('오류', '대화방을 찾을 수 없습니다.');
        }
    }

    const onCloseReview = () => {
        setIsShowReview(false)
    }

    const onReviewProfile = async (item: any) => {
        const userId: any = await keyValueStorage.get("userID");
        props.navigation.navigate('ViewFullProfile', {
            userId: userId,
            friendId: item.id,
            type: 'friendRequestNormalRecommendation'
        });
    }

    const onClickNotify = async () => {
        setIsLoading(true);
        let isSend = await muteNoticationAction(userIdMe,
            roomId,
            isNotify ? 'mute' : 'unmute');
        if (isSend) {
            setIsNotify(!isNotify);
        }
        setIsLoading(false);
    }

    return (
        <>
            <_ReportModal
                visible={isShowReport}
                onClose={onCloseReport}
                userId={listMember[0]?.id}
                roomId={roomId}
                roomName={listMember[0]?.userName} />
            <_ReviewModal visible={isShowReview} onClose={onCloseReview}
                onSubmit={onLeavingRoom} dataUser={listMember}
                roomName={listMember[0]?.userName} />
            <LoadingIndicator visible={isLoading} />
            <View style={styles.header}>
                <TouchableOpacity style={styles.btnWarning} onPress={onOpenReport}>
                    <Image source={Icons.ic_warning} />
                </TouchableOpacity>
                <TouchableOpacity onPress={onClickNotify}>
                    <Image style={{ tintColor: (isNotify ? colors.mainColor : colors.gray) }}
                        source={Icons.ic_notifications} />
                </TouchableOpacity>
            </View>
            <View style={styles.wrapLabel}>
                <_Text style={styles.label}>대화 상대</_Text>
            </View>
            <DrawerContentScrollView {...props}>

                {
                    listMember.map((item: any, index: number) => (
                        <TouchableOpacity style={styles.item} key={index} onPress={() => onReviewProfile(item)}>
                            <View style={styles.viewItem}>
                                <View style={styles.row}>
                                    <Image style={styles.avatar} source={item.profilePicture ? { uri: item.profilePicture } : Images.avatar_default} />
                                    <_Text style={styles.nameUser}>{item.userName.length > 14 ? `${item.userName.substring(0, 10 - 3)}...` : item.userName}</_Text>
                                </View>
                                {item.userName !== user && (
                                    item.isFriend === false && (
                                        isSendRequest || item.isRequested
                                            ? <TouchableOpacity style={[styles.btnAddFriend, { borderWidth: 1, borderColor: colors.mainColor }]} onPress={() => { cancelAddFriend(item.id) }}>
                                                <_Text style={[styles.titleAdd, { color: colors.mainColor }]}>친구 요청이 전송</_Text>
                                            </TouchableOpacity>
                                            : <TouchableOpacity style={styles.btnAddFriend} onPress={() => { addFriend(item.id) }}>
                                                <_Text style={styles.titleAdd}>친구추가</_Text>
                                            </TouchableOpacity>
                                    )
                                )
                                }
                            </View>
                        </TouchableOpacity>
                    ))
                }
            </DrawerContentScrollView>

            <View style={styles.wrapBtn}>
                <TouchableOpacity style={styles.btnLogout}
                    onPress={onReviewFriend}>
                    <_Text
                        style={styles.titleBtnOut}>방 나가기</_Text>
                    <Image style={styles.icon} source={Icons.ic_exit} />
                </TouchableOpacity>
            </View>
        </>
    )
}


const mapStateToProps = (state: AppState) => ({
    dataUser: state.ProfileReducer.profile,
    loading: state.ProfileReducer.loading,
});

const dispatch = {
    profileRequest,
};

const connector = connect(mapStateToProps, dispatch);
type ReduxType = ConnectedProps<typeof connector>;

export default connector(CustomMenuChat);

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#EEF5FF',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 20,
        borderTopLeftRadius: 24,
        paddingVertical: 15,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    btnWarning: { marginRight: 20 },
    item: { paddingHorizontal: 10, marginTop: 15 },
    wrapLabel: { paddingHorizontal: 10, marginTop: 20 },
    label: { color: colors.mainColor, fontWeight: '900', fontSize: 16 },
    viewItem: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    row: { flexDirection: 'row', alignItems: "center" },
    nameUser: { color: colors.mainColor },
    avatar: { width: 50, height: 50, borderRadius: 36, marginRight: 5 },
    titleAdd: { fontWeight: '900' },
    btnAddFriend: { backgroundColor: colors.grayLight, alignItems: 'center', justifyContent: 'center', paddingVertical: 7, paddingHorizontal: 15, borderRadius: 36 },
    btnLogout: { justifyContent: 'center', backgroundColor: colors.mainColor, borderRadius: 30, paddingVertical: 7, flexDirection: 'row', alignItems: 'center' },
    titleBtnOut: { color: colors.white, marginRight: 7, fontWeight: '900', fontSize: 20 },
    wrapBtn: { paddingHorizontal: 20, paddingVertical: 15 },
    icon: { width: 28, height: 28 }
});