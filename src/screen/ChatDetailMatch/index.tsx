
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, FlatList, Alert, Image, TouchableOpacity, Platform, Keyboard } from 'react-native';
import { ChatLineHolder } from '../../components/ChatLineHolder/ChatLineHolder';
import { colors } from '../../config/Colors';
import { renderItem, RootStackParamList } from '../../@types';
import { emitEvent, ReceiveMessage, removeHandler } from '../../socketio/socketController';
import { RouteProp } from '@react-navigation/native';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { ContainerMain } from '../../components/Container/ContainerMain';
import { TipsItem } from '../../components/TipsItem/TipsItem';
import { InputChat } from './inputChat';
import { HeaderApp } from '../../components/Header/HeaderApp';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import _ChooseImageModal from '../../components/ChooseImageModal/_ChooseImageModal';
import { Icons } from '../../assets';
import { screen } from '../../config/Layout';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import _ModalShowImage from '../../components/Modal/_ModalShowImage';
import { uploadFile } from '../../api/ImageServices';
import { getChattingData, getRoomInformation } from '../../api/ChatServices';
const RNFS = require('react-native-fs');

type Params = {
    params: {
        roomId: string,
    },
}
interface Props {
    route: RouteProp<Params, 'params'>
    navigation: DrawerNavigationProp<RootStackParamList>
}
const limit = 10;

const ChatDetailMatchScreen = (props: Props) => {

    var currentDay: number | string = 0, currentMonth: number | string = 0, currentYear: number | string = 0;
    const [message, setMessenger] = useState('');
    const [username, setUserName] = useState('');
    const [chatData, setChatData] = useState<any[]>([]);
    const [sortedData, setSortedData] = useState<any[]>([]);
    const [idRoom, setIdRoom] = useState('');
    const [isItemTips, setIsItemTips] = useState(true);
    const [messageIntro, setMessageIntro] = useState<any>();
    const [newMessage, setNewMessage] = useState<any>();
    const [nameFriend, setNameFriend] = useState('');
    const [loading, setLoading] = useState<boolean>(false);

    const [isShowCamaroll, setIsShowCamaroll] = useState<boolean>(false);
    const [selectedImage, setSelectedImage] = useState<string>('');
    const [showingImage, setShowingImage] = useState<string>('');
    const [visibleShowImg, setvisibleShowImg] = useState<boolean>(false);
    const [page, setPage] = useState<number>(1);

    const getInfo = async () => {
        let userId = await keyValueStorage.get("userID") as any;
        let username = await keyValueStorage.get("userName") as string;
        setUserName(username);
        const roomId = props.route.params.roomId;
        getHistoryChat(userId, roomId, username);
        setIdRoom(roomId);
        handleEvent(roomId);
        await getMember(userId, roomId);
    }

    const getMember = async (userId: string, roomId: string) => {
        let roomInfo = await getRoomInformation(userId, roomId);
        setNameFriend(roomInfo.name);
    }

    const checkNewDate = (checkDate: Date) => {
        let status = false;
        if (currentYear < checkDate.getFullYear()) {
            status = true;
        } else if (currentYear == checkDate.getFullYear()) {
            if (currentMonth < checkDate.getMonth() + 1) {
                status = true;
            } else if (currentMonth == checkDate.getMonth() + 1) {
                if (currentDay < checkDate.getDate()) {
                    status = true;
                }
            }
        }
        if (status) {
            currentYear = checkDate.getFullYear();
            currentMonth = checkDate.getMonth() >= 9 ? checkDate.getMonth() + 1 : `0${checkDate.getMonth() + 1}`;
            currentDay = checkDate.getDate() >= 10 ? checkDate.getDate() : `0${checkDate.getDate()}`;
        }
        return status;
    }

    const formatData = (mergedData: any) => {
        let formattedData = [];
        currentYear = 0;
        currentMonth = 0;
        currentDay = 0;
        for (let i = 0; i < mergedData.length; i++) {
            const createdTime = new Date(mergedData[i].createdAt);
            if (checkNewDate(createdTime)) {
                const hourStr = createdTime.getHours() >= 10 ? createdTime.getHours() : `0${createdTime.getHours()}`;
                const minuteStr = createdTime.getMinutes() >= 10 ? createdTime.getMinutes() : `0${createdTime.getMinutes()}`;
                const timeStr = `${currentYear}.${currentMonth}.${currentDay} ${hourStr}:${minuteStr}`;
                if (chatData.findIndex(a => a.id == `${currentYear}.${currentMonth}.${currentDay}`) == -1) {
                    formattedData.unshift({ id: `${currentYear}.${currentMonth}.${currentDay}`, date: timeStr });
                }
            }
            formattedData.unshift(mergedData[i]);
        }
        return formattedData;
    }

    const getHistoryChat = async (userId: string, roomId: string, username: string) => {
        try {
            const res = await getChattingData(userId, roomId, page, limit);
            if (res) {
                for (let i = 0; i < res.chats.length; i++) {
                    if (res.chats[i].sender.userName !== username) {
                        setMessageIntro(res.chats[i]);
                        break;
                    }
                }
            }
        } catch (error) {
            console.log(error)
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        }
    }

    const onShowCameraRoll = () => {
        setIsShowCamaroll(true);
    }

    const sendImage = async () => {
        if (selectedImage) {
            setLoading(true);
            let currentImg = selectedImage;
            if (Platform.OS === 'ios') {
                let photoPATH = `assets-library://asset/asset.JPG?id=${currentImg.substring(5)}&ext=JPG`;
                const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                currentImg = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
            }
            let locations = await uploadPic(currentImg);
            setLoading(false);
            return locations;
        }
        return [];
    }

    const _sendMessage = async () => {
        let data: any = { roomId: idRoom }
        if (selectedImage) {
            let locations: Location[] = await sendImage();
            if (locations.length === 0) {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
                return;
            }
            data = {
                ...data,
                chatContent: {
                    chatType: 'image',
                    value: message,
                    image: {
                        origin: locations[0].origin,
                        thumbnail: locations[0].thumbnail,
                    },
                },
            };
            emitEvent('chat', data);
        } else {
            data = {
                ...data,
                chatContent: {
                    chatType: 'text',
                    value: message,
                },
            };
            emitEvent('chat', data);
        }
        setSelectedImage('');
        setMessenger('');
    }

    const onShowImage = (url: string) => {
        setShowingImage(url);
        setvisibleShowImg(true);
    }

    const _renderChatLine = ({ item, index }: renderItem) => {
        if (!item?.chatContent) return null;

        if (item?.sender?.userName === username) {
            return (
                <View style={{ paddingLeft: 30, alignItems: 'flex-end' }} >
                    <ChatLineHolder contentContainerStyle={{
                        backgroundColor: colors.mainColor,
                        borderBottomRightRadius: 0,
                    }}
                        textColor={colors.white}
                        key={index}
                        item={item}
                        isSender={true}
                        onShowImage={onShowImage}
                    />
                </View>
            );

        } else {
            return (
                <View style={{ paddingRight: 30 }}>
                    <ChatLineHolder contentContainerStyle={{
                        backgroundColor: colors.grayLight,
                        borderBottomLeftRadius: 0,
                    }}
                        key={index}
                        item={item}
                        isSender={false}
                        onShowImage={onShowImage}
                    />
                </View>
            );
        }
    };

    const _renderMessage = () => {
        if (messageIntro) {
            return (
                <ChatLineHolder
                    profilePicture={messageIntro?.sender?.profilePicture}
                    contentContainerStyle={{
                        backgroundColor: colors.white,
                    }}
                    textColor="#65676B"
                    isSender={true}
                    item={messageIntro}
                    onShowImage={onShowImage} />
            );
        }
        return null;
    }

    const handleEvent = async (roomId: string) => {
        ReceiveMessage(`room:${roomId}` as any, (data: any) => {
            setNewMessage(JSON.parse(data));
        });
        ReceiveMessage(`room-terminate` as any, (data: any) => {
            if (roomId === data.roomId) {
                props.navigation.goBack();
            }
        });
    }

    const onChangeMessage = (value: any) => {
        setMessenger(value);
    }

    const hideItemTips = () => {
        setIsItemTips(false);
    }

    const refreshChatData = () => {
        if (newMessage.id === chatData[0]?.id) return;
        let newSortData = formatData([newMessage, ...chatData].reverse());
        setChatData([newMessage, ...chatData]);
        setSortedData(newSortData);
    }

    useEffect(() => {
        const params: any = props.route.params;
        setTimeout(() => {
            setIsItemTips(false);
        }, 4000);
        getInfo();
        emitEvent('room-in-use', { roomId: params.roomId });

        return () => {
            keyValueStorage.get("paramsMatch").then((data: any) => {
                let pa = JSON.parse(data)
                removeHandler(`room:${pa.roomId}` as any);
            })
            removeHandler('room-leave');
            emitEvent('room-not-in-use', {
                roomId: params.roomId
            });
        }
    }, [])

    useEffect(() => {
        if (newMessage) refreshChatData();
    }, [newMessage])

    const onMore = () => {
        Keyboard.dismiss();
        props.navigation.openDrawer()
    }

    const onBack = async () => {
        props.navigation.reset({
            index: 0,
            routes: [{ name: 'Main' }],
        });
    }

    const getImageExstension = (uri: string) => {
        if (uri) {
            const splitNameArr = uri.split('/');
            const filename = splitNameArr[splitNameArr.length - 1];
            const splitFilenameArr = filename.split('.');
            return splitFilenameArr[splitFilenameArr.length - 1];
        }
        return '';
    }

    const uploadPic = (uri: string) => {
        return new Promise<Location[]>(async (resolve, reject) => {
            let formdata = new FormData();
            const ext = getImageExstension(uri);
            const filename = `${Date.now()}.${ext}`;
            let file = {
                uri,
                type: 'image/jpeg',
                name: filename,
            };
            formdata.append('photos', file);
            await uploadFile(formdata).then((response: UploadFileResponse) => {
                if (!response.isFailed) {
                    resolve(response.data.locations);
                }
                else {
                    Alert.alert('이미지 업로드 실패', '잠시 후 다시 시도해주세요');
                    resolve([]);
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                console.log('오류', error.message);
                resolve([]);
            })
        })
    }

    const onCloseModal = () => {
        setIsShowCamaroll(false);
    }

    const onChangeModal = (locations: string[]) => {
        if (locations.length > 0) {
            var index = locations.length - 1
            setSelectedImage(locations[index].toString())
        }
        setIsShowCamaroll(false);
    }

    const onCloseModalShowImage = () => {
        setvisibleShowImg(false);
    }

    return (
        <ContainerMain>
            <HeaderApp onBack={onBack} title={`${nameFriend} 님과 채팅 중`} onPress={onMore} />
            {
                (visibleShowImg && showingImage !== '') &&
                <_ModalShowImage
                    visible={visibleShowImg}
                    onClose={onCloseModalShowImage}
                    banner={showingImage} />
            }
            <_ChooseImageModal
                visible={isShowCamaroll}
                title={'최근에'}
                doneButtonText={'완료'}
                onClose={onCloseModal}
                onChange={onChangeModal}
                numberOfImages={1} />
            <LoadingIndicator visible={loading} />
            <View style={styles.contentChat} >
                {
                    isItemTips &&
                    <TipsItem
                        onClose={hideItemTips}
                        text="지나친 표현은 신고의 대상이 될 수 있습니다." />
                }
                <FlatList
                    contentContainerStyle={styles.messageListContainer}
                    keyExtractor={item => item.id}
                    ListFooterComponent={_renderMessage}
                    data={sortedData}
                    renderItem={_renderChatLine}
                    inverted
                    extraData={sortedData} />
            </View>
            {
                selectedImage !== '' &&
                <View style={{ backgroundColor: colors.white }}>
                    <View style={styles.imgSelectedView}>
                        <Image source={{ uri: selectedImage }} style={styles.imgComment} />
                        <TouchableOpacity onPress={() => setSelectedImage('')}>
                            <Image source={Icons.ic_delete_circle} />
                        </TouchableOpacity>
                    </View>
                </View>
            }
            <View style={styles.emptySpace} />
            <InputChat
                message={message}
                image={selectedImage}
                onChangeMessage={onChangeMessage}
                _sendMessage={_sendMessage}
                onChooseImage={onShowCameraRoll} />
        </ContainerMain>
    );
};

const styles = StyleSheet.create({
    contentChat: {
        flex: 1
    },
    imgComment: {
        height: screen.heightscreen / 10,
        width: screen.widthscreen / 7,
        marginRight: '2%',
        borderRadius: 8,
        marginVertical: '3%'
    },
    imgSelectedView: {
        height: screen.heightscreen / 8,
        width: '80%',
        flexDirection: 'row',
        paddingVertical: '3%',
        marginHorizontal: '16%',
    },
    emptySpace: {
        height: 60,
    },
    messageListContainer: {
        minHeight: '100%',
        justifyContent: 'flex-end',
        paddingHorizontal: 10,
    },
});

export default ChatDetailMatchScreen;

interface UploadFileResponse {
    status: number,
    isFailed: boolean,
    data: {
        locations: Location[]
    }
}

type Location = {
    origin: string,
    thumbnail: string
}
