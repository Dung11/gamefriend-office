import { NavigationProp, RouteProp } from '@react-navigation/native';
import React, { Component } from 'react';
import { ScrollView, View, TouchableOpacity, Alert, StatusBar, Image, FlatList, processColor } from 'react-native';
import { RootStackParamList } from '../../../@types';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { colors } from '../../../config/Colors';
import styles from './styles';
import BackHeader from '../../../components/BackHeader/BackHeader';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { Icons, Images } from '../../../assets';
import ModalUserReport from '../../../components/Modal/ModalUserReport';
import { screen } from '../../../config/Layout';
import { RadarChart } from 'react-native-charts-wrapper';
import update from 'immutability-helper';
import { acceptFriendRequests, blockUser, denyFriendRequests, getUserPicture, personalTendencyById, requestedUserInfo } from '../../../api/UserServices';
import _CommentContainer from '../../../components/CommentContainer';
import * as Progress from 'react-native-progress';
import _ReportModal from '../../RecommendedFriend/ReportModal';
import _Text from '../../../components/_Text';
import _ModalShowImage from '../../../components/Modal/_ModalShowImage';
import Environments from '../../../config/Environments';
import ToastUtils from '../../../utils/ToastUtils';

type Params = {
    params: {
        requestId: string,
        userId: string,
    },
}

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    route: RouteProp<Params, 'params'>
}

const TIME_DATA = [
    {
        id: 1,
        numbertime: '6~12',
        nametime: '아침',
        isCheck: false,
    },
    {
        id: 2,
        numbertime: '12~18',
        nametime: '오후',
        isCheck: false,
    },
    {
        id: 3,
        numbertime: '18~24',
        nametime: '저녁',
        isCheck: false,
    },
    {
        id: 4,
        numbertime: '00~06',
        nametime: '새벽',
        isCheck: false,
    },
];

export class FriendProfile extends Component<Props> {

    state = {
        isLoading: true,
        isLoadingInScreen: false,
        reportVisible: false,
        listData: TIME_DATA,
        data: {},
        xAxis: {
            valueFormatter: ['게임스타일', '소통', '멘탈', '실력', '희생정신'],
            textSize: 14,
            fontWeight: '800',
            textColor: processColor(colors.blue),
            fontFamily: Environments.DefaultFont.Bold
        },
        yAxis: {
            labelCount: 6,
            labelCountForce: true,
            drawLabels: false,
            axisMaximum: 5,
            axisMinimum: 0
        },
        legend: {
            enabled: false,
        },
        friendProfile: null,
        userIndex: [],
        friendIndex: [],
        isRequested: false,
        reportModalVisible: false,
        showingImage: [],
        visibleShowImg: false,
    }

    async componentDidMount() {
        await this.loadChartData();
    }

    loadChartData = async () => {
        let defaultValue = { value: 0 };
        let hashtag = '';
        let userIndex: any[] = [defaultValue, defaultValue, defaultValue, defaultValue, defaultValue];
        let friendIndex: any[] = [defaultValue, defaultValue, defaultValue, defaultValue, defaultValue];
        let friendProfile: any;
        const { route } = this.props;
        const params: any = route.params;
        try {
            let [userIndexRes, friendProfileRes] = await Promise.all([
                personalTendencyById(params.userId),
                requestedUserInfo(params.userId, params.requestId),
            ]);
            if (userIndexRes && Object.keys(userIndexRes).length !== 0) {
                userIndex = [];
                for (let i = 0; i < userIndexRes.length; i++) {
                    let curTendency = userIndexRes[i];
                    userIndex.push(curTendency.level);
                }
            }
            ////////////
            friendProfile = friendProfileRes;
            if (friendProfileRes.hasOwnProperty('tendency')) {
                let resFriendIndex = friendProfileRes?.tendency
                if (resFriendIndex && Object.keys(resFriendIndex).length !== 0) {
                    friendIndex = [];
                    for (let i = 0; i < resFriendIndex.length; i++) {
                        let curTendency = resFriendIndex[i];
                        friendIndex.push(curTendency.level);
                        hashtag += `${curTendency.tag} `;
                    };
                }
            }
        } catch (error) {
            Alert.alert('오류', error.message);
        } finally {
            this.setState({
                isLoading: false,
                userIndex,
                friendIndex,
                friendProfile,
            }, () => {
                this.setState(
                    update(this.state, {
                        data: {
                            $set: {
                                dataSets: [
                                    {
                                        values: this.state.friendIndex,
                                        label: '내 정보',
                                        config: {
                                            color: processColor(colors.blurPink),
                                            drawFilled: true,
                                            fillColor: processColor('rgba(236, 96, 131, 0.23)'),
                                            fillAlpha: 100,
                                            lineWidth: 3,
                                            drawValues: false,
                                        },
                                    },
                                    {
                                        values: this.state.userIndex,
                                        label: '상대정보',
                                        config: {
                                            color: processColor(colors.blue),

                                            drawFilled: true,
                                            fillColor: processColor('rgba(67, 135, 239, 0.19)'),
                                            fillAlpha: 150,
                                            lineWidth: 3,
                                            drawValues: false,
                                        },
                                    },
                                    {
                                        values: [{ value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }],
                                        config: {
                                            drawValues: false,
                                        },
                                        label: '',
                                    },
                                ],
                            }
                        },
                        xAxis: {
                            $set: {
                                valueFormatter: ['게임스타일', '소통', '멘탈', '실력', '희생정신'],
                                textSize: 14,
                                fontWeight: '800',
                                textColor: processColor(colors.blue),
                                fontFamily: Environments.DefaultFont.Bold
                            },
                        },
                    }),
                );
            });
        }
    }

    onCloseReportFriend = () => {
        this.setState({ reportVisible: false });
    }

    onReportFriend = () => {
        this.setState({ reportVisible: true });
    }

    renderItem = ({ item, index }: any) => {
        let friendProfile: any = this.state?.friendProfile;
        let playTimes: any[] = friendProfile.playTime;
        let checkExisted = this.checkExistItem(playTimes, item.nametime);
        return (
            <View style={styles.listTimeItem} key={index}>
                <View style={{
                    borderTopColor: checkExisted ? colors.blue : colors.inActiveBlue,
                    borderTopWidth: 7,
                    alignItems: 'center',
                    width: screen.widthscreen / 7
                }}>
                    <_Text style={styles.nameListTimeItem}>{item.numbertime}</_Text>
                    <_Text style={styles.nameListTimeItem}>{item.nametime}</_Text>
                </View>
            </View>
        )
    }

    checkExistItem(arrData: any[], value: any) {
        let index = arrData.findIndex((d) => d === value);
        if (index >= 0) {
            return true;
        }
        return false;
    }

    formatPercent = (percent: any) => {
        if (percent) {
            return `${(percent * 100).toFixed(0)}%`;
        }
        return '0%';
    }

    onConfirmSendFQ = () => {
        const { navigation } = this.props;
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'friendRequestNormalRecommendation',
                onSubmit: () => this.onSendFriendReq(),
            }
        });
    }

    onSendFriendReq = () => {
        let friendProfile: any = this.state.friendProfile;
        const { navigation, route } = this.props;
        const params: any = route.params;

        this.setState({ isLoadingInScreen: true });
        acceptFriendRequests(params.userId, params.requestId).then((response: any) => {
            if (response) {
                this.setState({ isRequested: true }, () => {
                    const sendedParams: any = {
                        roomId: response.roomId,
                        nameFriend: friendProfile.userName
                    }
                    navigation.navigate('ChatScreen', { screen: 'ChatDetail', params: sendedParams })
                });
            } else {
                Alert.alert('오류', '친구 요청 오류.');
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({ isLoadingInScreen: false });
        });
    }

    onDenyFriendReq = () => {
        const { navigation, route } = this.props;
        const params: any = route.params;

        this.setState({ isLoadingInScreen: true });
        denyFriendRequests(params.userId, params.requestId).then((response: any) => {
            if (response) {
                this.setState({ isRequested: true }, () => {
                    navigation.goBack();
                });
            } else {
                Alert.alert('오류', '친구 요청 오류.');
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({
                isLoadingInScreen: false,
            });
        });
    }

    onCloseReportModal = () => {
        this.setState({
            reportModalVisible: false,
        });
    }

    submitReportModal = () => {
        ToastUtils.showToast({
            text1: '신고가 정상적으로 접수되었습니다',
            type: 'info',
            position: 'bottom'
        });
        this.onCloseReportModal();
    }

    onChooseOption = (option: number) => {
        if (option === 1) {
            this.onCloseReportFriend();
            setTimeout(() => {
                this.setState({
                    reportModalVisible: true,
                });
            }, 400);
        } else {
            this.setState({ isLoadingInScreen: true });
            this.onBlockUser();
            this.onCloseReportModal();
        }
    }

    onBlockUser = () => {
        const { route } = this.props;
        const params: any = route.params;
        blockUser(params.userId, params.friendId).then((response: any) => {
            if (response.status === 400) {
                Alert.alert('오류', '사용자 신고 실패');
            } else if (response.status === 409) {
                Alert.alert('오류', '이미 해당 사용자를 신고했습니다.');
            } else if (response.status === 200) {
                ToastUtils.showToast({
                    text1: '차단이 완료되었습니다',
                    type: 'info',
                    position: 'bottom'
                })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', '사용자 신고 실패');
        }).finally(() => {
            this.setState({ isLoadingInScreen: false, reportVisible: false });
        })
    }

    onCloseModalShowImage = () => {
        this.setState({ visibleShowImg: false });
    }

    onClickAvatar = async () => {
        this.setState({ isLoadingInScreen: true });
        const { route } = this.props;
        const params: any = route.params;
        let pictures = await getUserPicture(params.requestId);
        this.setState({
            showingImage: pictures,
            visibleShowImg: true,
            isLoadingInScreen: false,
        });
    }

    onClickReviewerAvatar = async (friendId: string) => {
        this.setState({ isLoadingInScreen: true });
        let pictures = await getUserPicture(friendId);
        if (pictures.length === 0) {
            this.setState({ isLoadingInScreen: false });
            return;
        }
        this.setState({
            showingImage: pictures,
            visibleShowImg: true,
            isLoadingInScreen: false,
        });
    }

    render() {
        const { route } = this.props;
        let { isLoading, reportVisible, listData, friendProfile, isRequested, isLoadingInScreen,
            reportModalVisible, visibleShowImg, showingImage, data, xAxis, legend, yAxis }: any = this.state;
        if (isLoading) {
            return (
                <ContainerMain>
                    <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                    <LoadingIndicator visible={isLoading} />
                    <BackHeader {...this.props} />
                </ContainerMain>
            );
        }
        return (
            <ContainerMain>
                {
                    (visibleShowImg && showingImage !== '') &&
                    <_ModalShowImage
                        visible={visibleShowImg}
                        onClose={this.onCloseModalShowImage}
                        banner={showingImage} />
                }
                <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                <LoadingIndicator visible={isLoadingInScreen} />
                <BackHeader {...this.props} />
                <_ReportModal
                    userId={friendProfile?.id}
                    friendName={friendProfile?.userName}
                    visible={reportModalVisible}
                    onClose={this.onCloseReportModal}
                    onSubmit={this.submitReportModal} />
                <ModalUserReport
                    visible={reportVisible}
                    onClose={this.onCloseReportFriend}
                    onSubmit={this.onChooseOption} />
                <ScrollView style={styles.container}>
                    <TouchableOpacity style={styles.icThreeDots} onPress={this.onReportFriend}>
                        <Image source={Icons.ic_vertical_3_dots} />
                    </TouchableOpacity>
                    <View style={styles.transparentSection}>
                        <TouchableOpacity onPress={this.onClickAvatar}>
                            <Image style={styles.topOneAvt}
                                source={friendProfile?.profilePicture ?
                                    { uri: friendProfile?.profilePicture } :
                                    Images.avatar_default} />
                        </TouchableOpacity>
                        <_Text style={styles.topOneUsername}>{friendProfile.userName}</_Text>
                        {/* <_Text style={styles.idTxt}>id: {friendProfile?.id}</_Text> */}
                        <_Text style={styles.idTxt}>{friendProfile?.address || 'Updating'} | {friendProfile?.age}세 | {friendProfile?.gender === 'male' ? '남성' : '여성'}</_Text>
                        <View style={styles.progressContainer}>
                            <View style={styles.leftProgressContainer}>
                                <Progress.Circle borderWidth={4}
                                    color={'#E35C5C'}
                                    showsText={true}
                                    unfilledColor={'#FFFFFF'}
                                    size={85}
                                    formatText={() => this.formatPercent(friendProfile.totalPercent)}
                                    progress={friendProfile.totalPercent || 0} />
                            </View>
                            <View style={styles.rightProgressContainer}>
                                <View style={styles.gameRow}>
                                    <_Text style={styles.gameTitle}>게임종류    <_Text style={styles.percentText}>{this.formatPercent(friendProfile.gamePercent)}</_Text></_Text>
                                    <Progress.Bar color={'#E35C5C'} progress={friendProfile.gamePercent || 0} />
                                </View>
                                <View style={styles.gameRow}>
                                    <_Text style={styles.gameTitle}>접속 시간대    <_Text style={styles.percentText}>{this.formatPercent(friendProfile.timePercent)}</_Text></_Text>
                                    <Progress.Bar color={'#E35C5C'} progress={friendProfile.timePercent} />
                                </View>
                                <View>
                                    <_Text style={styles.gameTitle}>성향    <_Text style={styles.percentText}>{this.formatPercent(friendProfile.characterPercent)}</_Text></_Text>
                                    <Progress.Bar color={'#E35C5C'} progress={friendProfile.characterPercent} />
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{ paddingHorizontal: 24, paddingTop: 20, paddingBottom: 30 }}>
                        <_Text style={styles.title}>나의 성격은</_Text>
                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            {
                                friendProfile && friendProfile.personalities.map((item: any, index: number) => {
                                    return (
                                        <View style={styles.listItem} key={index}>
                                            <_Text style={styles.nameListItem}>{item}</_Text>
                                        </View>
                                    );
                                })
                            }
                        </View>
                        {
                            (friendProfile && friendProfile.character) &&
                            <>
                                <_Text style={[styles.title, { marginTop: 20 }]}>나의 게임 스타일은</_Text>
                                <View style={styles.rowWrap}>
                                    {
                                        friendProfile?.character?.mental &&
                                        <TouchableOpacity style={styles.listItem}>
                                            <_Text style={styles.nameListItem}>{friendProfile?.character?.mental}</_Text>
                                        </TouchableOpacity>
                                    }
                                    {
                                        friendProfile?.character?.personality &&
                                        <TouchableOpacity style={styles.listItem}>
                                            <_Text style={styles.nameListItem}>{friendProfile?.character?.personality}</_Text>
                                        </TouchableOpacity>
                                    }
                                    {
                                        friendProfile?.character?.teamwork &&
                                        <TouchableOpacity style={styles.listItem}>
                                            <_Text style={styles.nameListItem}>{friendProfile?.character?.teamwork}</_Text>
                                        </TouchableOpacity>
                                    }

                                </View>
                            </>
                        }
                        <_Text style={[styles.title, { marginTop: 20 }]}>게임 접속 시간대는</_Text>
                        <View style={styles.rowWrap}>
                            <FlatList
                                scrollEnabled={false}
                                contentContainerStyle={{ alignItems: 'center' }}
                                data={listData}
                                numColumns={4}
                                renderItem={this.renderItem}
                                keyExtractor={index => index.toString()}
                            />
                        </View>
                        {
                            (friendProfile && friendProfile?.favoriteGame) &&
                            <>
                                <_Text style={[styles.title, { marginTop: 20 }]}>요즘 푹 빠진 게임</_Text>
                                <View style={styles.rowWrap}>
                                    <View style={styles.listItem}>
                                        <_Text style={styles.nameListItem}>{friendProfile?.favoriteGame?.name}</_Text>
                                    </View>
                                </View>
                            </>
                        }
                        {
                            (friendProfile && friendProfile?.games) &&
                            <>
                                <_Text style={[styles.title, { marginTop: 20 }]}>플레이 중인 게임</_Text>
                                <View style={styles.rowWrap}>
                                    {
                                        friendProfile?.games.map((item: any, index: number) => {
                                            return (
                                                <View style={styles.listItem} key={index}>
                                                    <_Text style={styles.nameListItem}>{item.name}</_Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </>
                        }
                    </View>
                    <View style={styles.chartContainer}>
                        <View style={styles.tagContainer}>
                            <_Text style={styles.tagText}>#즐겜러  #희생정신  #인싸</_Text>
                        </View>
                        <RadarChart
                            style={styles.chart}
                            data={data}
                            xAxis={xAxis as any}
                            yAxis={yAxis}
                            chartDescription={{ text: '' }}
                            legend={legend}
                            drawWeb={true}
                            webLineWidth={0}
                            webLineWidthInner={2}
                            webAlpha={255}
                            webColor={processColor("#ffffff")}
                            webColorInner={processColor("#bababa")}
                            skipWebLineCount={1}
                            touchEnabled={false}
                        />
                        <View style={{ flexDirection: 'row', alignContent: 'center', marginLeft: 25 }}>
                            <View style={styles.alignCenter}>
                                <View style={{
                                    backgroundColor: '#7AAAF4',
                                    alignItems: 'center',
                                    width: screen.widthscreen / 7,
                                    borderRadius: 6,
                                    height: 7
                                }}>
                                </View>
                            </View>
                            <View style={{
                                justifyContent: 'center',
                                alignContent: 'center',
                            }}>
                                <_Text style={{ justifyContent: 'center', alignContent: 'center', paddingLeft: 10, fontSize: 14, fontWeight: '800', color: '#7AAAF4' }}>내 정보</_Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignContent: 'center', marginLeft: 25, paddingBottom: 40 }}>
                            <View style={styles.alignCenter}>
                                <View style={{
                                    backgroundColor: '#EC6083',
                                    alignItems: 'center',
                                    width: screen.widthscreen / 7,
                                    borderRadius: 6,
                                    height: 7
                                }}>
                                </View>
                            </View>
                            <View style={styles.alignCenter}>
                                <_Text style={{ justifyContent: 'center', alignContent: 'center', paddingLeft: 10, fontSize: 14, fontWeight: '800', color: '#EC6083' }}>상대정보</_Text>
                            </View>
                        </View>
                    </View>
                    <_CommentContainer {...this.props} friendId={friendProfile.id} onClickAvatar={this.onClickReviewerAvatar} />
                    {
                        isRequested === false &&
                        <View style={styles.groupReqBtn}>
                            <TouchableOpacity style={{
                                backgroundColor: colors.blue,
                                width: screen.widthscreen / 3,
                                borderRadius: screen.widthscreen / 3,
                                marginHorizontal: 5,
                                opacity: isRequested === true ? 0.5 : 1,
                            }}
                                disabled={isRequested} onPress={this.onConfirmSendFQ}>
                                <_Text style={styles.confirmFriendReqText}>수락</_Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{
                                backgroundColor: '#C4C4C6',
                                width: screen.widthscreen / 3,
                                borderRadius: screen.widthscreen / 3,
                                marginHorizontal: 5,
                                opacity: isRequested === true ? 0.5 : 1
                            }}
                                disabled={isRequested} onPress={this.onDenyFriendReq}>
                                <_Text style={styles.denyFriendReqBtnText}>거절</_Text>
                            </TouchableOpacity>
                        </View>
                    }

                </ScrollView>
            </ContainerMain>
        );
    }
}

export default FriendProfile;