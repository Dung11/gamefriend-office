import { Platform, StyleSheet } from 'react-native';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    ItemStatusAddFriend: {
        backgroundColor: colors.white,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 15,
        marginTop: 15,
    },
    icon: {
        height: 16,
        width: 16,
    },
    titleItemStatusAddFriend: {
        color: colors.buleSky,
        fontSize: 14,
        fontWeight: "500",
        lineHeight: 22
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    wrapToast: {
        paddingHorizontal: 20,
        paddingVertical: 2,
        backgroundColor: colors.buleSky,
        borderRadius: 30,
        lineHeight: 17,
        marginRight: 10,
    },
    toast: {
        color: colors.white,
        fontWeight: '400',
        fontSize: 12,
    },
    wrapItemList: {
        paddingHorizontal: 5,
        marginTop: 15,
    },
    itemListChat: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        backgroundColor: colors.white,
        paddingHorizontal: 20,
        borderTopWidth: 0.2,
        borderTopColor: colors.gray,
        paddingVertical: 15
    },
    avatar: {
        height: screen.heightscreen / 23,
        width: screen.heightscreen / 23,
        borderRadius: screen.heightscreen / 23,
    },
    nameItem: {
        color: '#8F8F8F',
        fontSize: 12,
        fontWeight: '500'
    },
    numberOfMemberItem: {
        fontSize: 10,
        fontWeight: '500'
    },
    status: {
        backgroundColor: colors.greenActive,
        width: 5, height: 5,
        borderRadius: 5
    },
    inActiveStatus: {
        backgroundColor: colors.grayInActive,
        width: 5, height: 5,
        borderRadius: 5
    },
    time: {
        color: colors.gray,
        fontSize: 8,
        marginLeft: 5,
        fontWeight: '400',
        paddingTop: Platform.OS == 'android' ? 5 : 0,
    },
    noConverContent: {
        color: colors.gray8F,
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 17,
        paddingHorizontal: 10,
        textAlign: 'center'
    },
    btnMatch: {
        backgroundColor: colors.blue,
        width: screen.widthscreen / 2.5,
        borderRadius: 12,
        justifyContent: 'center',
        alignContent: 'center'
    },
    btnMatchTxt: {
        fontWeight: '800',
        lineHeight: 24,
        fontSize: 20,
        color: colors.white,
        textAlign: 'center',
        paddingVertical: 12
    },
    rowBack: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        flex: 1,
        backgroundColor: colors.white,
    },
    borderTopRowBack: {
        borderTopWidth: 0.2,
        borderTopColor: colors.gray,
    },
    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 15,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    removeTxt: {
        fontWeight: '500',
        fontSize: 12,
        color: colors.white,
        marginLeft: 5,
        paddingTop: Platform.OS == 'android' ? 5 : 0,
    },
    hiddenBtn: {
        flexDirection: 'row',
        backgroundColor: colors.blue,
        borderRadius: 25,
        paddingHorizontal: 12,
        alignItems: 'center'
    },
    item: {
        backgroundColor: colors.white,
        marginVertical: 7.5,
        borderRadius: 12,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    wrapInfo: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center'
    },
    col: {
        width: '55%',
    },
    txtQuantily: {
        marginLeft: 10,
        fontWeight: '500',
        fontSize: 14
    },
    wrapQuantily: {
        flexDirection: 'row',
        alignItems: 'center',
        position: "absolute",
        bottom: 5,
        right: 5
    },
    wrapAvatar: {
        height: 100,
        width: 100,
        borderRadius: 24,
        backgroundColor: colors.darkGray,
        padding: 5,
        marginRight: 20
    },
    grAvatar: {
        borderRadius: 30,
        height: 40,
        width: 40,
        margin: 3,
        borderWidth: 1,
        borderColor: colors.mainColor,
    },
    description: {
        color: colors.red,
        fontWeight: '800',
        fontSize: 16
    },
    chatItemContainer: {
        width: screen.widthscreen / 10,
        height: screen.widthscreen / 10,
        marginRight: 10,
        borderRadius: 23,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default styles;