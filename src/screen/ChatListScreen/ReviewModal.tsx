import React, { Dispatch, useEffect, useState } from "react";
import Modal from 'react-native-modal';
import { Image, StyleSheet, TouchableOpacity, View, ActivityIndicator, Alert, Text, TextInput, ScrollView, KeyboardAvoidingView, Platform, Keyboard } from 'react-native';
import { RootStackParamList } from "../../@types";
import { NavigationProp } from "@react-navigation/native";
import LoadingIndicator from "../../components/LoadingIndicator/LoadingIndicator";
import { screen } from "../../config/Layout";
import { colors } from "../../config/Colors";
import { Icons } from "../../assets";
import { addReview } from "../../api/UserServices";
import DeviceInfo from 'react-native-device-info';
import _Text from "../../components/_Text";

interface Props {
    visible?: boolean,
    onSubmit: (index: number, roomType: string, roomId: string) => void,
    onClose: () => void,
    navigation: NavigationProp<RootStackParamList, any>,
    roomInfo: any,
    rowData: any
}
const REVIEW_DATA = [{
    icon: Icons.ic_love,
    name: '유쾌하고 재밌었어요',
},
{
    icon: Icons.ic_positive_vote,
    name: '캐미가 너무 잘 맞아요',
},
{
    icon: Icons.ic_hand_love,
    name: '만나서도 같이 하고 싶어요',
}]

const _ReviewModal = (props: Props) => {

    const { onClose } = props;
    const [isLoadingInView, setIsLoadingInView] = useState(false);
    const [visible, setVisible] = useState(props.visible);
    const [selectedReview, setSelectedReview] = useState<number>(1);
    const [selected, setSelected] = useState<any>('');
    const [reviewText, setReviewText] = useState<string>('');

    useEffect(() => {
        setVisible(props.visible);
    }, [props.visible]);

    const onSubmit = async () => {
        setIsLoadingInView(true);
        let memberInfo = props.roomInfo.members[0];
        const response = await addReview(memberInfo.id, {
            text: reviewText,
            stars: selectedReview,
            title: selected,
        });
        const { rowData } = props;
        if (!response) {
            Alert.alert('오류', response);
        } else {
            setReviewText('');
            setSelected('');
            setSelectedReview(1);
            onClose();
        }
        setIsLoadingInView(false);
        props.onSubmit(rowData.index, rowData.item.roomType, rowData.item.id);
    }

    const _onChangeText = (text: string) => {
        setReviewText(text);
    };

    return (
        <Modal isVisible={visible} onBackdropPress={onClose}>
            <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "position" : "height"} >
                <LoadingIndicator visible={isLoadingInView} />
                <View style={styles.wrapper}>
                    <ScrollView style={styles.content}>
                        <View style={styles.header}>
                            <_Text style={[styles.headerTitle]}>{props.roomInfo.name} 님과의 채팅방을 나가시겠어요?</_Text>
                            <_Text style={[styles.headerTitle]}>한번 나간 채팅방은 복구할 수 없습니다.</_Text>
                            <View style={styles.reviewStarContainer}>
                                {
                                    [1, 2, 3, 4, 5].map((item: any) => {
                                        return (
                                            <TouchableOpacity key={item} style={styles.btnStar} onPress={() => { setSelectedReview(item) }}>
                                                <Image source={selectedReview >= item ? Icons.ic_full_star : Icons.ic_star_half} />
                                            </TouchableOpacity>
                                        );
                                    })
                                }
                            </View>
                        </View>
                        {REVIEW_DATA.map((item: any, index: number) => {
                            return (
                                <TouchableOpacity
                                    key={index.toString()}
                                    activeOpacity={1}
                                    style={[styles.btn, { flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }]}
                                    onPress={() => { setSelected(item.name) }}>
                                    <View style={{ flex: .1 }}>
                                        <Image source={item.icon} />
                                    </View>
                                    <View style={{ flex: .6 }}>
                                        <_Text style={[styles.btnText,
                                        { color: selected === item.name ? colors.red : '#65676B' }]}>
                                            {item.name}
                                        </_Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                        <View style={styles.bottomModal}>
                            <View style={{ marginHorizontal: '3%' }}>
                                <Image source={Icons.ic_smile} />
                            </View>
                            <View style={styles.textInputModalView}>
                                <TextInput
                                    numberOfLines={4}
                                    multiline={true}
                                    textAlignVertical={'top'}
                                    onChangeText={_onChangeText}
                                />
                            </View>
                            <TouchableOpacity style={styles.btnSubmit} onPress={onSubmit}>
                                <_Text style={styles.btnSubmitText}>나가기</_Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </KeyboardAvoidingView>
        </Modal>
    )
}

export default _ReviewModal;

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 16,
        paddingTop: 50,
    },
    content: {
        borderRadius: 13,
        width: '100%',
    },
    header: {
        backgroundColor: colors.white,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 14,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    headerTitle: {
        color: colors.blue,
        fontSize: 16,
        lineHeight: 23,
        fontWeight: '800',
        marginHorizontal: 10,
        textAlign: 'center',
    },
    btn: {
        marginVertical: '0.05%',
        backgroundColor: colors.white,
    },
    btnSubmit: {
        marginVertical: '8%',
        backgroundColor: colors.blue,
        borderRadius: 30,
        marginHorizontal: '7%',
    },
    btnText: {
        color: '#65676B',
        paddingVertical: 16,
        fontWeight: '400',
        fontSize: 16,
        lineHeight: 24,
    },
    btnSubmitText: {
        color: colors.white,
        textAlign: 'center',
        paddingVertical: 16,
        fontWeight: '500',
        fontSize: 20,
        lineHeight: 25,
    },
    bottomModal: {
        backgroundColor: colors.white,
        paddingVertical: '2%',
        marginVertical: '0.05%',
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
    },
    textInputModalView: {
        backgroundColor: '#ECF1F9',
        borderRadius: 4,
        marginHorizontal: 20,
        marginVertical: '2%',
        height: screen.heightscreen / 7,
        paddingVertical: '3%',
        paddingHorizontal: '5%',
    },
    reviewStarContainer: {
        marginTop: 20,
        flexDirection: 'row',
    },
    btnStar: {
        marginHorizontal: 5,
    },
});
