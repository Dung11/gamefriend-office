
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react';
import { View, Image, RefreshControl, Alert, TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux';
import { renderItem, RootStackParamList } from '../../@types';
import { Icons, Images } from '../../assets';
import { ContainerMain } from '../../components/Container/ContainerMain';
import { HeaderApp } from '../../components/Header/HeaderApp';
import { colors } from '../../config/Colors';
import store, { AppState } from '../../redux/store';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { chatListRequest } from "../../redux/chatlist/actions";
import { friendReqRequest } from '../../redux/friendrequest/actions';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import _Text from '../../components/_Text';
import {
    getChatListData,
    getRoomInformation,
    outRoomChat,
    getLocalRoomInformation,
    outLocalRoomChat
} from '../../api/ChatServices';
import { SwipeListView } from 'react-native-swipe-list-view';
import _ReviewModal from './ReviewModal';
import styles from './styles';
import _ModalLeaveRoom from '../../components/Modal/_ModalLeaveRoom';
import utils from '../../utils/Utils';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    chatList: any,
    friendRequests: any,
    isLoadingFriendRequests: boolean,
    isLoadingChatList: boolean
}
const limit = 10;
const ChatListScreen = (props: Props) => {

    const [listData, setListData] = useState<any[]>([]);
    const [listSwipeData, setListSwipeData] = useState<any[]>([]);
    const [friendRequests, setFriendRequests] = useState<any>();
    const [isLoadingFriendRequests, setIsLoadingFriendRequests] = useState<boolean>(true);
    const [isLoadingChatList, setIsLoadingChatList] = useState<boolean>(true);
    const [refreshing, setRefreshing] = useState<boolean>(false);
    const [visible, setVisible] = useState<boolean>(false);
    const [quantity, setQuantity] = useState<number>(1);
    const [isShowLeavingRoom, setIsShowLeavingRoom] = useState(false);

    const [isShowReview, setIsShowReview] = useState<boolean>(false);
    const [roomInfo, setRoomInfo] = useState<any>();
    const [currentRowData, setCurrentRowData] = useState<any>();
    const [page, setPage] = useState<number>(1)

    const onDone = (response: any) => {
        let pageTmp = response.page;
        ++pageTmp;
        setPage(pageTmp);

        response?.rooms && setListData(response?.rooms);
        response?.rooms && setListSwipeData(response?.rooms);
        setQuantity(response?.quantity);
        setIsLoadingChatList(false);
        setRefreshing(false);
    }

    const onFQDone = (response: any) => {
        setFriendRequests(response);
        setIsLoadingFriendRequests(false);
    }

    const getHistoryChat = async (isRefresh = false) => {
        let pageTmp = page;
        if (isRefresh) {
            pageTmp = 1;
            setPage(pageTmp);
        }
        const userId = await keyValueStorage.get("userID") as any;
        let payload: any = {
            userId: userId,
            params: {
                page: pageTmp,
                limit,
            },
            callback: onDone,
        };
        let payloadFriendReq: any = {
            userId: userId,
            callback: onFQDone,
        };
        store.dispatch(chatListRequest(payload));
        store.dispatch(friendReqRequest(payloadFriendReq));
    }

    const joinRoom = async (item: any) => {
        if (item.roomType === 'duet') {
            setVisible(true);
            const userId: any = await keyValueStorage.get("userID");
            let resRoomInfo = await getRoomInformation(userId, item.id);
            setVisible(false);
            const params: any = {
                roomId: resRoomInfo.id,
                nameFriend: resRoomInfo.name,
                roomType: resRoomInfo.roomType,
                roomName: resRoomInfo.name,
                isMuted: resRoomInfo.isMuted,
            }
            props.navigation.navigate('ChatScreen', { screen: 'ChatDetail', params });
        } else if (item.roomType === 'group') {
            setVisible(true);
            const userId: any = await keyValueStorage.get("userID");
            let resRoomInfo = await getRoomInformation(userId, item.id);
            setVisible(false);
            const params: any = {
                roomId: item.id,
                roomType: 'livetalk',
                isMuted: resRoomInfo.isMuted,
                name: resRoomInfo.name,
            }
            props.navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
        } else if (item.roomType === 'livetalk') {
            setVisible(true);
            let resRoomInfo = await getLocalRoomInformation(item.id);
            setVisible(false);
            const params: any = {
                roomId: item.id,
                roomType: 'livetalk-local',
                isMuted: resRoomInfo.isMuted,
                name: resRoomInfo.name,
            }
            props.navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
        }
    }

    const getListFriend = async () => {
        if (!refreshing) {
            let pageTmp = page;
            const userId: any = await keyValueStorage.get("userID");
            let listSwipeDataTmp = [...listSwipeData];
            let listDataTmp = [...listData];
            if (listSwipeDataTmp.length >= quantity) return;
            getChatListData(userId, {
                page: pageTmp,
                limit,
            }).then((response: any) => {
                setQuantity(response.quantity);
                let listData = listDataTmp.concat(response.rooms);
                let listSwipeData = listSwipeDataTmp.concat(response.rooms);
                setListData(listData);
                setListSwipeData(listSwipeData);
                ++pageTmp;
                setPage(pageTmp);
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
            });
        }
    }

    /// delete data search live talk
    const removeKeySeacrh = async () => {
        await keyValueStorage.delete("datasearchlivetalk");
    }

    useEffect(() => {
        getHistoryChat();
        const unsubscribe = props.navigation.addListener('focus', () => {
            onRefresh();
            setIsLoadingChatList(true);
            setIsLoadingFriendRequests(true);
            removeKeySeacrh();
        });
        return unsubscribe;
    }, []);

    const onRefresh = async () => {
        setPage(1);
        setRefreshing(true);
        setListData([]);
        setListSwipeData([]);
        setIsLoadingChatList(false);
        setIsLoadingFriendRequests(false);
        await getHistoryChat(true);
    }

    const _rendenHiddenItem = (rowData: any) => {
        if (rowData.hasOwnProperty('isRemoved') && rowData.isRemoved === true) {
            return (<></>);
        }
        let isFriend = rowData.item.isFriend;
        let roomType = rowData.item.roomType;
        return (
            <View style={[styles.rowBack, rowData?.index !== 0 && styles.borderTopRowBack]}>
                <TouchableOpacity style={styles.hiddenBtn}
                    onPress={() => onRemove(rowData)}>
                    <Image source={Icons.ic_x_sign_white} />
                    <_Text style={styles.removeTxt}>{(roomType === 'duet' && isFriend) ? '친구삭제' : '나가기'}</_Text>
                </TouchableOpacity>
            </View>
        );
    }

    const _renderItem = ({ item, index }: renderItem) => {
        if (item.hasOwnProperty('isRemoved') && item.isRemoved === true) {
            return (<></>);
        }
        let groupName = item?.name || '';
        let numberOfMembers = ''
        if (item.roomType !== 'duet') {
            numberOfMembers = `   ${item.members}명 참여 중`;
        }
        return (
            <TouchableOpacity style={[styles.itemListChat, index === 0 && { borderTopWidth: 0 }]}
                activeOpacity={1} onPress={() => joinRoom(item)}>
                <View style={[styles.row, { flexWrap: 'wrap' }]}>
                    <View style={[styles.chatItemContainer, {
                        backgroundColor: item.isFriend ? colors.blue : colors.white,
                    }]}>
                        <Image style={styles.avatar}
                            source={item.photos ?
                                { uri: item.photos[0] } : Images.avatar_default}
                        />
                    </View>
                    <_Text style={styles.nameItem}>{groupName}
                        {item.roomType !== 'duet' && <_Text style={styles.numberOfMemberItem}>{numberOfMembers}</_Text>}
                    </_Text>
                </View>
                { item?.roomType === 'duet' &&
                    <View style={styles.row}>
                        <View style={item?.isOnline ? styles.status : styles.inActiveStatus} />
                        {item.isOnline && <_Text style={styles.time}>온라인</_Text>}
                        {item.lastSeen && <_Text style={styles.time}>{utils.timeAgoHandle(item.lastSeen)}</_Text>}
                    </View>}
            </TouchableOpacity>
        );
    }

    const onGoToFriendReq = () => {
        const { navigation } = props;
        navigation.navigate('ListFriendRequest');
    }

    const onRemove = async (rowData: any) => {
        if (rowData.item.roomType === 'duet') {
            let isFriend = rowData.item.isFriend;
            if (isFriend) {
                await onLeavingRoom(rowData.index, rowData.item.roomType, rowData.item.id);
            } else {
                setVisible(true);
                const userId: any = await keyValueStorage.get("userID");
                let resRoomInfo = await getRoomInformation(userId, rowData.item.id);
                setRoomInfo(resRoomInfo);
                setCurrentRowData(rowData);
                setVisible(false);
                setTimeout(() => {
                    setIsShowReview(true);
                }, 200);
            }
        } else {
            setIsShowLeavingRoom(true);
            setCurrentRowData(rowData);
        }
    }

    const onLeavingRoom = async (index: number, roomType: string, roomId: string) => {
        try {
            const userId: any = await keyValueStorage.get("userID");
            setVisible(true);
            let payload: any = {
                userId,
                type: roomType,
            }
            let response = null;
            if (roomType === 'livetalk') {
                response = await outLocalRoomChat(roomId);
            } else {
                response = await outRoomChat(userId, roomId);
            }

            if (response) {
                store.dispatch(chatListRequest(payload));
                let listDataTmp = [...listData];
                listDataTmp.splice(index, 1);
                setListData(listDataTmp);

                let listSwipeDataTmp = [...listSwipeData];
                listSwipeData[index].isRemoved = true;
                setListSwipeData(listSwipeDataTmp);
            } else {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            setTimeout(() => {
                Alert.alert('오류', error.message);
            }, 500);
        } finally {
            setIsShowLeavingRoom(false);
            setVisible(false);
        }
    }

    if (isLoadingChatList && isLoadingFriendRequests) {
        return (
            <ContainerMain>
                <HeaderApp title="대화 목록" />
                <LoadingIndicator visible={true} />
            </ContainerMain>
        );
    }

    return (
        <ContainerMain>
            <HeaderApp title="대화 목록" />
            <LoadingIndicator visible={visible} />
            {isShowReview && <_ReviewModal {...props}
                visible={isShowReview}
                onClose={() => setIsShowReview(false)}
                onSubmit={onLeavingRoom}
                roomInfo={roomInfo}
                rowData={currentRowData}
            />}
            {isShowLeavingRoom &&
                <_ModalLeaveRoom
                    visible={isShowLeavingRoom}
                    onBackdropPress={() => setIsShowLeavingRoom(false)}
                    rowData={currentRowData}
                    onConfirm={onLeavingRoom} />
            }
            <View style={styles.container}>
                {(friendRequests && friendRequests?.quantity > 0) &&
                    <TouchableOpacity style={styles.ItemStatusAddFriend} onPress={onGoToFriendReq}>
                        <_Text style={styles.titleItemStatusAddFriend}>{`대기중인 친구 요청`}</_Text>
                        <View style={styles.row}>
                            <View style={styles.wrapToast}>
                                <_Text style={styles.toast}>{friendRequests?.quantity}</_Text>
                            </View>
                            <Image resizeMode="contain" style={styles.icon} source={Icons.ic_chevron_right} />
                        </View>
                    </TouchableOpacity>
                }
                {
                    (listData && listData.length === 0) &&
                    <View style={{ backgroundColor: colors.white, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10, paddingVertical: 20, marginBottom: 10, marginTop: 15 }}>
                        <View style={{ flexDirection: 'row', paddingHorizontal: 20, paddingBottom: 20 }}>
                            <Image style={{ paddingLeft: 10 }} source={Icons.ic_stopwatch} />
                            <_Text style={styles.noConverContent}>아직 생성된 대화방이 없습니다. 매칭이나 라이브톡을 통해서 게임 친구들과 대화를 나눠보세요!</_Text>
                        </View>
                        <TouchableOpacity style={styles.btnMatch} onPress={() => {
                            props.navigation.navigate('Home')
                        }}>
                            <_Text style={styles.btnMatchTxt}>매칭하러가기</_Text>
                        </TouchableOpacity>
                    </View>
                }
                <View style={[styles.wrapItemList, { paddingBottom: friendRequests?.quantity > 0 ? 20 : 0 }]}>
                    <SwipeListView
                        keyExtractor={(item, index) => index.toString()}
                        disableRightSwipe
                        data={listSwipeData}
                        renderItem={_renderItem}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh} />
                        }
                        renderHiddenItem={(rowData) => _rendenHiddenItem(rowData)}
                        rightOpenValue={-100}
                        onEndReachedThreshold={0.5}
                        onEndReached={getListFriend}
                        recalculateHiddenLayout={true}
                        useNativeDriver
                        ListFooterComponent={<View style={{ height: listSwipeData.length > 0 ? (Platform.OS == 'android' ? 60 : 30) : 0 }} />}
                    />
                </View>
            </View>
        </ContainerMain>
    );
};

const mapStateToProps = (state: AppState): Partial<Props> => ({
    chatList: state.ChatListReducer.chatlist,
    friendRequests: state.FriendRequestsReducer.friendRequests,
    isLoadingFriendRequests: state.FriendRequestsReducer.loading,
    isLoadingChatList: state.ChatListReducer.loading,
});

export default connect(mapStateToProps, null)(ChatListScreen);
