import { StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";
import { screen } from "../../../config/Layout";

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    itemListChat: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
        paddingHorizontal: 20,
        paddingVertical: 15
    },
    avatar: {
        height: screen.heightscreen / 23,
        width: screen.heightscreen / 23,
        borderRadius: screen.heightscreen / 23,
    },
    nameItem: {
        color: colors.gray8F,
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 17
    },
    status: {
        backgroundColor: colors.green,
        width: 5, height: 5,
        borderRadius: 5
    },
    time: {
        color: colors.gray,
        fontSize: 8, marginLeft: 5
    },
    btnAccept: {
        borderWidth: 1,
        borderColor: colors.blue,
        paddingHorizontal: 24,
        paddingVertical: 6,
        borderRadius: 30,
    },
    btnAcceptText: {
        color: colors.blue,
    },
    btnReject: {
        backgroundColor: colors.blue,
        paddingHorizontal: 24,
        paddingVertical: 6,
        borderRadius: 30,
    }
});

export default styles;