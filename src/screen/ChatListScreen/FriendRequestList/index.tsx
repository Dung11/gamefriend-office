import { NavigationProp } from '@react-navigation/native'
import React, { Component } from 'react'
import { Alert, Image, TouchableOpacity, View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { renderItem, RootStackParamList } from '../../../@types'
import { acceptFriendRequests, denyFriendRequests, getUserRequests } from '../../../api/UserServices'
import { Icons, Images } from '../../../assets'
import BackHeader from '../../../components/BackHeader/BackHeader'
import { ContainerMain } from '../../../components/Container/ContainerMain'
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator'
import _Text from '../../../components/_Text'
import { colors } from '../../../config/Colors'
import { keyValueStorage } from '../../../storage/keyValueStorage'
import styles from './styles'

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
}

export class ListFriendRequest extends Component<Props> {

    _unsubscribe: any;
    page = 1;
    limit = 10;

    state = {
        isLoading: true,
        requests: []
    }

    _getData = async () => {
        let { requests } = this.state;
        const userId: any = await keyValueStorage.get("userID");
        getUserRequests(userId, this.page, this.limit).then((response: any) => {
            if (this.page == 1) {
                this.setState({ requests: [] });
            }
            let requestsData = this.page == 1 ? response.requests : requests.concat(response.requests);
            this.setState({ requests: requestsData }, () => {
                if (requestsData.length === 0) {
                    this.props.navigation.goBack();
                }
            });
            if (response.requests.length !== 0) {
                this.page++;
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({
                isLoading: false
            });
        });
    }

    async componentDidMount() {
        const { navigation } = this.props;
        this._unsubscribe = navigation.addListener('focus', async () => {
            this.setState({ isLoading: true });
            await this._getData();
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    onAcceptFriendReq = async (requestId: string, nameFriend: string) => {
        this.setState({
            isLoading: true
        });
        const { navigation } = this.props;
        let { requests } = this.state;
        const userId: any = await keyValueStorage.get("userID");
        acceptFriendRequests(userId, requestId).then((response: any) => {
            let filterRequests = requests.filter((d: any) => d?.id !== requestId);
            this.setState({
                requests: filterRequests
            }, () => {
                const params: any = {
                    roomId: response.roomId,
                    nameFriend: nameFriend
                }
                navigation.navigate('ChatScreen', { screen: 'ChatDetail', params: params });
            });
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({ isLoading: false });
        });
    }

    onDenyFriendReq = async (requestId: string) => {
        this.setState({ isLoading: true });
        let { requests } = this.state;
        const userId: any = await keyValueStorage.get("userID");
        denyFriendRequests(userId, requestId).then((response: any) => {
            let filterRequests = requests.filter((d: any) => d?.id !== requestId);
            this.setState({
                requests: filterRequests
            });
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({ isLoading: false });
        });
    }

    onReviewProfile = async (item: any) => {
        const userId: any = await keyValueStorage.get("userID");
        this.props.navigation.navigate('FriendProfile', {
            userId,
            requestId: item?.id,
        });
    }

    _renderItem = ({ item, index }: renderItem) => {
        if (!item) return (<></>);
        return (
            <View style={{ paddingHorizontal: 10, paddingVertical: 10 }}>
                <View style={[styles.itemListChat]}>
                    <TouchableOpacity style={styles.row} onPress={() => this.onReviewProfile(item)}>
                        <View style={{ flex: .1, marginRight: 10 }}>
                            <Image style={styles.avatar} source={item?.profilePicture ?
                                { uri: item.profilePicture } :
                                Images.avatar_default} />
                        </View>
                        <View style={{ flex: .4 }}>
                            <_Text style={styles.nameItem}>{item.userName}</_Text>
                        </View>
                        <View style={{ flex: .3, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={styles.btnAccept} onPress={() => this.onAcceptFriendReq(item?.id, item?.userName)}>
                                <_Text style={styles.btnAcceptText}>수락</_Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: .3, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={styles.btnReject} onPress={() => this.onDenyFriendReq(item?.id)}>
                                <_Text style={{ color: colors.white }}>거절</_Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        let { isLoading, requests } = this.state;
        return (
            <ContainerMain>
                <LoadingIndicator visible={isLoading} />
                <BackHeader {...this.props} />
                <FlatList
                    keyExtractor={(item: any, index: number) => index.toString()}
                    data={requests}
                    renderItem={this._renderItem}
                />
            </ContainerMain>
        )
    }
}

export default ListFriendRequest