import React, { useEffect, useState } from 'react';
import Modal from 'react-native-modal';
import { screen } from '../../config/Layout';
import { Icons, Images } from '../../assets';
import { Image, StyleSheet, TouchableOpacity, View, Alert, TouchableWithoutFeedback } from 'react-native';
import { colors } from '../../config/Colors';
import ModalUserReport from '../../components/Modal/ModalUserReport';
import * as Progress from 'react-native-progress';
import MessageBubble from '../../components/MessageBubble/MessageBubble';
import { NavigationProp, RouteProp } from '@react-navigation/native';
import { RootStackParamList } from '../../@types';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import { getMatchingPercentage, sendFriendRequest } from '../../api/RecommendedServices';
import { ScrollView } from 'react-native-gesture-handler';
import _ReportModal from './ReportModal';
import { blockUser } from '../../api/UserServices';
import _Text from '../../components/_Text';
import ToastUtils from '../../utils/ToastUtils';
type Params = {
    params: {
        friendInfo: {
            id: string,
            recommendedUser: {
                id: string,
            }
        }
        itemForRequest: number,
        userId: string,
    },
}
interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    route: RouteProp<Params, 'params'>
}

const ViewProfileModal = (props: Props) => {

    const { navigation } = props;
    const [isLoading, setIsLoading] = useState(true);
    const [isLoadingInView, setIsLoadingInView] = useState(false);
    const [visible, setVisible] = useState();
    const [reportModalVisible, setReportModalVisible] = useState(false);
    const [friendInfo, setFriendInfo] = useState<any>();
    const [userId, setUserId] = useState('');
    const [itemForRequest, setItemForRequest] = useState(0);
    const [reportVisible, setReportVisible] = useState(false);
    const [data, setData] = useState<any>();

    const onReportFriend = () => {
        setReportVisible(!reportVisible);
    }

    const onCloseReportFriend = () => {
        setReportVisible(false);
    }

    const onCloseReportModal = () => {
        setReportModalVisible(false);
    }

    const onSeeMore = () => {
        props.navigation.navigate('ViewFullProfile', {
            userId: userId,
            friendId: friendInfo.recommendedUser?.id,
            type: 'friendRequestNormalRecommendation',
            isRecommended: true
        });
    }

    const loadData = () => {
        const item = props.route.params;

        getMatchingPercentage(item.userId, item.friendInfo.id).then((response: any) => {
            if (response) {
                setData(response.recommendation);
                setIsLoading(false);
            } else {
                Alert.alert('오류', '잠시 후 다시 시도해주세요')
            }
        });
    }

    useEffect(() => {
        const item = props.route.params;
        setFriendInfo(item.friendInfo);
        setUserId(item.userId);
        setItemForRequest(item.itemForRequest)
        loadData();
    }, []);

    const submitReportModal = () => {
        setReportVisible(false);
        ToastUtils.showToast({
            text1: '신고가 정상적으로 접수되었습니다',
            type: 'info',
            position: 'bottom'
        });
    }

    const formatPercent = (percent: any) => {
        if (percent) {
            return `${(percent * 100).toFixed(0)}%`;
        }
        return '0%'
    }

    const onConfirmSendFQ = () => {
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem',
            params: {
                type: 'friendRequestNormalRecommendation',
                onSubmit: sendFR,
            },
        });
    }

    const sendFR = () => {
        setIsLoadingInView(true);
        sendFriendRequest(userId, friendInfo?.recommendedUser?.id).then(response => {
            if (response.isFailed === true) {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
                return;
            }
            if (response.status === 200) {
                let friendInfoTmp: any = friendInfo;
                friendInfoTmp.isRequested = true;
                setFriendInfo(friendInfoTmp);
            } else if (response.status === 402) {
                // props.onClose();
                props.navigation.navigate("Modal", { screen: "AlertNoItem" })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            setIsLoadingInView(false);
        });
    }

    if (isLoading) {
        return (
            <LoadingIndicator visible={isLoading} />
        )
    }

    const onChooseOption = (option: number) => {
        if (option === 1) {
            setReportVisible(false);
            setTimeout(() => {
                setReportModalVisible(true);
            }, 500);
        } else {
            setIsLoadingInView(true);
            onBlockUser();
            setReportVisible(false);
        }
    }

    const onBlockUser = () => {
        blockUser(userId, friendInfo?.recommendedUser?.id).then((response: any) => {
            if (response.status === 400) {
                console.log('response: ', response);
                Alert.alert('오류', '사용자 신고 실패.');
            } else if (response.status === 409) {
                Alert.alert('오류', '이미 해당 사용자를 신고했습니다.');
            } else if (response.status === 200) {
                setIsLoadingInView(false);
                ToastUtils.showToast({
                    text1: '제출이 완료되었습니다',
                    type: 'info',
                    position: 'bottom'
                })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', '사용자 신고 실패.')
        }).finally(() => {
            setIsLoadingInView(false);
        });
    }

    const onClose = () => {
        props.navigation.goBack()
    }

    return (
        <View style={styles.container}>
            <ModalUserReport
                visible={reportVisible}
                onClose={onCloseReportFriend}
                onSubmit={onChooseOption} />
            <_ReportModal
                friendName={data?.recommendedUser?.userName}
                userId={data?.recommendedUser?.id}
                visible={reportModalVisible}
                onClose={onCloseReportModal}
                onSubmit={submitReportModal} />
            <TouchableWithoutFeedback onPress={onClose}>
                <View style={styles.backgroundDropPress} />
            </TouchableWithoutFeedback>
            <View style={styles.wrapper}>
                <TouchableWithoutFeedback onPress={onClose}>
                    <View style={styles.backgroundDropPress} />
                </TouchableWithoutFeedback>
                <LoadingIndicator visible={isLoadingInView} />
                <View style={{ position: 'relative', zIndex: 10 }}>
                    <Image style={styles.topAvatar} source={data?.recommendedUser?.profilePicture ?
                        { uri: data?.recommendedUser?.profilePicture } :
                        Images.avatar_default}
                    />
                </View>
                <ScrollView style={styles.content}>
                    <TouchableOpacity style={styles.icThreeDots} onPress={onReportFriend}>
                        <Image source={Icons.ic_vertical_3_dots} />
                    </TouchableOpacity>
                    <View style={styles.wrapperItem}>
                        <_Text style={styles.username}>{data?.recommendedUser?.userName}</_Text>
                        {/* <_Text style={styles.idTxt}>id: {data?.recommendedUser.id}</_Text> */}
                        <_Text style={styles.idTxt}>{data?.recommendedUser?.address} | {data?.recommendedUser?.age}세 | {data?.recommendedUser?.gender === 'male' ? '남성' : '여성'}</_Text>
                    </View>
                    <View style={styles.progressContainer}>
                        <View style={styles.leftProgressContainer}>
                            <Progress.Circle borderWidth={4}
                                color={'#E35C5C'}
                                showsText={true}
                                unfilledColor={'#FFFFFF'}
                                size={85}
                                formatText={() => formatPercent(data?.totalPercent)}
                                progress={data?.totalPercent || 0} />
                        </View>
                        <View style={styles.rightProgressContainer}>
                            <View style={styles.gameRow}>
                                <View style={styles.gameRowContainer}>
                                    <_Text style={styles.gameTitle}>게임종류</_Text>
                                    <_Text style={styles.percentText}>{formatPercent(data?.gamePercent / data.maxGamePercent)}</_Text>
                                </View>
                                <Progress.Bar color={'#E35C5C'} progress={data?.gamePercent / data.maxGamePercent || 0} />
                            </View>
                            <View style={styles.gameRow}>
                                <View style={styles.gameRowContainer}>
                                    <_Text style={styles.gameTitle}>접속 시간대</_Text>
                                    <_Text style={styles.percentText}>{formatPercent(data?.timePercent / data.maxTimePercent)}</_Text>
                                </View>
                                <Progress.Bar color={'#E35C5C'} progress={data?.timePercent / data.maxTimePercent} />
                            </View>
                            <View style={styles.gameRow}>
                                <View style={styles.gameRowContainer}>
                                    <_Text style={styles.gameTitle}>성향</_Text>
                                    <_Text style={styles.percentText}>{formatPercent(data?.characterPercent / data.maxCharacterPercent)}</_Text>
                                </View>
                                <Progress.Bar color={'#E35C5C'} progress={data?.characterPercent / data.maxCharacterPercent} />
                            </View>
                        </View>
                    </View>
                    {
                        (typeof data?.recommendedUser?.bio !== 'undefined' && data?.recommendedUser?.bio.length !== 0) &&
                        <View style={{ height: screen.heightscreen / 8 }}>
                            <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                <_Text style={styles.settingText}>자기소개</_Text>
                            </View>
                            <MessageBubble content={data?.recommendedUser?.bio} />
                        </View>
                    }
                    <TouchableOpacity style={styles.btnSeeMore} onPress={onSeeMore}>
                        <_Text style={styles.btnSeeMoreText}>자세히 보기</_Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.btnRefreshRecomendedList,
                        { opacity: friendInfo.isRequested === true ? 0.5 : 1 }]}
                        disabled={friendInfo.isRequested}
                        onPress={onConfirmSendFQ}>
                        <View style={styles.confirmButtonTextContainer}>
                            <_Text style={styles.buttonText}>친구추가</_Text>
                        </View>
                        <View style={styles.btnGameController}>
                            <Image source={Icons.ic_game_controller} />
                            <_Text style={styles.numberGameControler}>{itemForRequest}</_Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        </View>
    );
}

export default ViewProfileModal;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 16,
        alignItems: 'center',
        justifyContent: "center",
        backgroundColor: 'transparent',
    },
    backgroundDropPress: {
        position: 'absolute',
        zIndex: 0,
        height: screen.heightscreen,
        width: screen.widthscreen,
    },
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 16,
        paddingTop: 50,
        width: '100%',
        maxHeight: screen.heightscreen / 1.2,
    },
    content: {
        backgroundColor: colors.white,
        borderRadius: 13,
        width: '100%',
    },
    wrapperItem: {
        paddingTop: 50,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        width: '100%',
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10
    },
    _Text: {
        fontWeight: '900',
    },
    topAvatar: {
        width: screen.widthscreen / 4,
        height: screen.widthscreen / 4,
        borderRadius: screen.widthscreen / 4 / 2,
        position: 'absolute',
        top: -50,
        left: -50,
        zIndex: 10,
    },
    username: {
        color: colors.blue,
        fontSize: 16,
        fontWeight: '800',
        marginBottom: 10
    },
    idTxt: {
        fontWeight: '400',
        fontSize: 12,
        color: '#8F8F8F',
        paddingHorizontal: 12
    },
    icThreeDots: {
        position: 'absolute',
        top: 50,
        right: 30,
        zIndex: 10,
    },
    progressContainer: {
        flexDirection: 'row',
        marginTop: 10,
    },
    leftProgressContainer: {
        flex: .4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    leftProgressChart: {
        color: '#E35C5C'
    },
    rightProgressContainer: {
        flex: .6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    gameRow: {
        paddingBottom: 20
    },
    gameRowContainer: {
        flexDirection: 'row',
    },
    gameTitle: {
        color: '#8E8E92',
        fontWeight: '400',
        fontSize: 12
    },
    percentText: {
        color: '#E35C5C',
        fontWeight: '800',
        fontSize: 12,
        marginLeft: 15,
    },
    settingText: {
        textAlign: 'left',
        marginLeft: 20,
        color: colors.blue,
        fontWeight: '800',
        fontSize: 16,
    },
    btnSeeMore: {
        marginTop: 25,
        marginHorizontal: 20,
        backgroundColor: '#F8F3F3',
        borderRadius: 12,
        paddingVertical: 16
    },
    btnSeeMoreText: {
        fontWeight: '800',
        fontSize: 20,
        textAlign: 'center',
    },
    btnRefreshRecomendedList: {
        marginVertical: 20,
        marginHorizontal: 20,
        borderRadius: 12,
        paddingVertical: 12,
        backgroundColor: colors.blue,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center'
    },
    confirmButtonTextContainer: {
        justifyContent: 'center',
        alignContent: 'center',
        paddingRight: 10,
    },
    buttonText: {
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center',
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    btnGameController: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EEEEEF',
        width: 60,
        height: 40,
        borderRadius: 20
    },
    numberGameControler: {
        color: colors.black,
        fontWeight: '800',
        fontSize: 14,
        paddingLeft: 5
    },
});