import React, { Component } from 'react';
import { Image, View, TouchableOpacity, Alert } from 'react-native';
import { ContainerMain } from '../../components/Container/ContainerMain';
import styles from './styles';
import MainHeader from '../../components/MainHeader/MainHeader';
import { NavigationProp, RouteProp } from '@react-navigation/native';
import { RootStackParamList } from '../../@types';
import { ScrollView } from 'react-native-gesture-handler';
import { Icons, Images } from '../../assets';
import LinearGradient from 'react-native-linear-gradient';
import {
    getExtendOption,
    refreshRecommendationList,
    sendFriendRequest
} from '../../api/RecommendedServices';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { onLogout } from '../../utils/UserUtils';
import ViewProfileModal from './ViewProfileModal';
import store, { AppState } from '../../redux/store';
import { recommendedFriendListReqRequest } from '../../redux/recommendedfriendlist/actions';
import { connect } from 'react-redux';
import _Text from '../../components/_Text';

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    recommendations: any,
    route: RouteProp<any, any>
}

const MAP_OPTION_ICONS = [
    { id: 0, icon: Icons.ic_yellow_smile_face, },
    { id: 1, icon: Icons.ic_yellow_position, },
    { id: 2, icon: Icons.ic_red_reward, },
    { id: 3, icon: Icons.ic_love_game_controller, },
    { id: 4, icon: Icons.ic_yellow_moon, },
];

export class RecommendedFriend extends Component<Props> {

    animatedValue: any;
    unsubscribe: any;
    unsubscribeState: any;

    state = {
        isLoading: true,
        listFriends: [],
        extendOptions: [],
        userId: '',
        topOneUser: null,
        isVisibleModal: false,
        selectedFriend: null,
        itemForRequest: 0
    }

    async componentDidMount() {
        const { navigation } = this.props;
        let options: any[] = [];

        let userId: any = await keyValueStorage.get('userID');
        if (!userId) {
            await onLogout(navigation);
        }

        let payload: any = {
            userId,
            callback: this.onDoneReFriendReq,
        };

        this.unsubscribeState = navigation.addListener('state', async () => {
            await this.setState({ isLoading: true });
            let payload: any = {
                userId,
                callback: this.onDoneReFriendReq,
            };
            store.dispatch(recommendedFriendListReqRequest(payload));
        });
        this.unsubscribe = navigation.addListener('focus', async () => {
            await this.setState({ isLoading: true });
            let payload: any = {
                userId,
                callback: this.onDoneReFriendReq,
            };
            store.dispatch(recommendedFriendListReqRequest(payload));
        });

        store.dispatch(recommendedFriendListReqRequest(payload));
        getExtendOption().then((response: any) => {
            let extendOptionRes = response;
            extendOptionRes?.options && (options = extendOptionRes.options);
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({
                extendOptions: options,
                userId,
            });
        });
    }

    componentWillUnmount() {
        this.unsubscribe();
        this.unsubscribeState();
    }

    onDoneReFriendReq = (response: any) => {
        let listFriends: any[] = [];
        let topOneUser: any;
        let recommendationsRes = response;
        recommendationsRes?.recommendations?.length > 0
            && (topOneUser = recommendationsRes?.recommendations[0])
        recommendationsRes?.recommendations?.length > 1
            && (listFriends = recommendationsRes.recommendations.slice(1));
        this.setState({
            listFriends,
            isLoading: false,
            topOneUser,
            itemForRequest: recommendationsRes.itemForRequest,
        })
    }

    refreshRecommendation = async () => {
        this.setState({
            isLoading: true,
        });
        let response = await refreshRecommendationList(this.state.userId);
        if (response.isFailed === true) {
            Alert.alert('오류', '이미 친구 요청이 완료된 유저입니다');
            return;
        }

        if (response.status === 200) {
            this.setState({
                listFriends: [],
                topOneUser: null,
            });
            let payload: any = {
                userId: this.state.userId,
                callback: this.onDoneReFriendReq,
            };
            store.dispatch(recommendedFriendListReqRequest(payload));
        } else if (response.status === 402) {
            this.props.navigation.navigate("Modal", { screen: "AlertNoItem" });
            this.setState({ isLoading: false });
        }
    }

    renderFriendItem(item: any, index: number) {
        let { itemForRequest } = this.state;
        let rankIc = Icons.ic_top_3;
        index === 0 && (rankIc = Icons.ic_top_1);
        index === 1 && (rankIc = Icons.ic_top_2);
        return (
            <TouchableOpacity style={styles.rowFriend} onPress={() => this.onShowModal(item)}
                key={index}>
                <View style={{ flex: .1 }}>
                    <Image style={{ marginTop: 10 }} source={rankIc} />
                </View>
                <View style={{ flex: .15 }}>
                    <Image style={styles.rowFriendAvt}
                        source={item.recommendedUser?.profilePicture ?
                            { uri: item.recommendedUser?.profilePicture } :
                            Images.avatar_default} />
                </View>
                <View style={{ flex: .22 }}>
                    <_Text numberOfLines={1} style={styles.friendName}>{item?.recommendedUser.userName}</_Text>
                </View>
                <View style={{ flex: .2 }}>
                    {
                        item?.totalPercent && <_Text style={styles.matchingPercent}>매칭률 {(item?.totalPercent * 100).toFixed(0)}%</_Text>
                    }
                </View>
                <View style={{ flex: .1 }}>
                    <View style={[styles.btnGameController]}>
                        <Image source={Icons.ic_game_controller} />
                        <_Text style={styles.numberGameControler}>{itemForRequest || 0}</_Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    onChooseExtendOption = (id: any, index: number, name: string) => {
        const { navigation } = this.props;
        navigation.navigate('FilterRecommendedFriend', {
            userId: this.state.userId,
            option: { index, id, name },
        });
    }

    onConfirmSendFQ = (friendId: string, index = 0, isTopUser = false) => {
        const { navigation } = this.props;
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'friendRequestNormalRecommendation',
                onSubmit: () => this.onSendFriendRequest(friendId, index, isTopUser),
            },
        });
    }

    onConfirmResetFQ = () => {
        const { navigation } = this.props;
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'refreshNormalRecommendation',
                onSubmit: this.refreshRecommendation,
            }
        });
    }

    onConfirmFilterFriend = (id: any, index: number, name: string) => {
        // const { navigation } = this.props;
        // if (index === 2 || index === 3) {
        //     navigation.navigate('Modal', {
        //         screen: 'AlertConfirmItem', params: {
        //             type: 'refreshSpecialRecommendation',
        //             onSubmit: () => this.onChooseExtendOption(id, index, name),
        //         },
        //     });
        // } else {
        //     this.onChooseExtendOption(id, index, name);
        // }
        this.onChooseExtendOption(id, index, name);
    }

    onSendFriendRequest = (friendId: string, index = 0, isTopUser = false) => {
        this.setState({ isLoading: true });
        sendFriendRequest(this.state.userId, friendId).then(response => {
            if (response.isFailed === true) {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
                return;
            }
            if (response.status === 200) {
                if (isTopUser) {
                    let topOneUser: any = this.state.topOneUser;
                    topOneUser.isRequested = true;
                    this.setState({ topOneUser })
                } else {
                    let listFriends: any = [...this.state.listFriends];
                    listFriends[index] = { ...listFriends[index], isRequested: true };
                    this.setState({ listFriends })
                }
            } else if (response.status === 402) {
                this.props.navigation.navigate("Modal", { screen: "AlertNoItem" });
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({ isLoading: false });
        });
    }

    renderExtendOption(item: any, index: number) {
        return (
            <TouchableOpacity style={[styles.extendedContainer,
            {
                borderColor: '#A6A8AA', borderWidth: 1,
                opacity: item.isAvailable ? 1 : .6
            }]}
                disabled={!item.isAvailable}
                onPress={() => this.onConfirmFilterFriend(item.id, index, item.name)}
                key={index}>
                <View style={{ flex: .1, justifyContent: 'center', alignItems: 'center', marginHorizontal: 10 }}>
                    <Image source={MAP_OPTION_ICONS[index].icon} />
                </View>
                <View style={{ flex: .7 }}>
                    <_Text style={styles.extendedText}>{item.name}</_Text>
                </View>
            </TouchableOpacity>
        )
    }



    onCloseModal = () => {
        this.setState({
            isVisibleModal: false,
        });
    }

    onShowModal = (selectedFriend: any) => {
        const { navigation } = this.props;
        const { userId, itemForRequest } = this.state;
        if (selectedFriend.id) {
            this.setState({ selectedFriend }, () => {
                navigation.navigate("Modal", {
                    screen: 'ModalViewProfileRecommend',
                    params: {
                        friendInfo: selectedFriend,
                        itemForRequest: itemForRequest,
                        userId: userId,
                    }
                })
                // this.setState({ isVisibleModal: true });
            });
        } else {
            navigation.navigate('ViewFullProfile', {
                userId: this.state.userId,
                friendId: selectedFriend?.recommendedUser?.id,
                type: 'friendRequestNormalRecommendation',
                isRecommended: true
            });
        }
    }

    render() {
        let { listFriends,
            extendOptions,
            isLoading,
            topOneUser,
            isVisibleModal,
            selectedFriend,
            userId,
            itemForRequest
        }: any = this.state;
        return (
            <ContainerMain>
                <LoadingIndicator visible={isLoading} />
                <MainHeader />
                {/* {
                    (isVisibleModal && selectedFriend) &&
                    <ViewProfileModal
                        {...this.props}
                        visible={isVisibleModal}
                        onClose={this.onCloseModal}
                        friendInfo={selectedFriend}
                        itemForRequest={itemForRequest}
                        userId={userId}
                    />
                } */}
                <ScrollView>
                    <View style={styles.whiteSection} key={'section1'}>
                        <_Text style={styles.topOneTitle}>오늘의 추천 친구</_Text>
                        <Image style={{ marginTop: 10 }} source={Icons.ic_diamond} />
                        {
                            (topOneUser && topOneUser.recommendedUser) ? (
                                <>
                                    <TouchableOpacity onPress={() => this.onShowModal(topOneUser)}>
                                        <Image
                                            style={styles.topOneAvt}
                                            source={topOneUser?.recommendedUser?.profilePicture ?
                                                { uri: topOneUser?.recommendedUser?.profilePicture } :
                                                Images.avatar_default} />
                                    </TouchableOpacity>
                                    <_Text style={styles.topOneUsername}>{topOneUser?.recommendedUser?.userName}</_Text>
                                    {
                                        topOneUser?.recommendedUser.id &&
                                        <TouchableOpacity
                                            style={[styles.btnAddFriend, { opacity: topOneUser.isRequested === true ? 0.5 : 1 }]}
                                            disabled={topOneUser?.isRequested}
                                            onPress={() => this.onConfirmSendFQ(topOneUser?.recommendedUser.id, 0, true)}>
                                            <_Text style={styles.addFriendTxt}>+ 친구 추가</_Text>
                                        </TouchableOpacity>
                                    }
                                </>
                            ) :
                                <View style={{ marginTop: 20 }}>
                                    <_Text>{'완벽한 겜친을 찾는 중이에요\n조금만 기다려주세요 :)'}</_Text>
                                </View>
                        }
                    </View>
                    <View style={styles.listFriendSection} key={'section2'}>
                        {
                            listFriends && listFriends.map((item: any, index: number) => {
                                return this.renderFriendItem(item, index);
                            })
                        }
                    </View>
                    <View style={[styles.whiteSection, { paddingBottom: 20 }]} key={'section3'}>
                        <View>
                            <TouchableOpacity style={styles.btnRefreshRecomendedList} onPress={this.onConfirmResetFQ}>
                                <LinearGradient
                                    start={{ x: 0.0, y: 0.05 }}
                                    end={{ x: 0.5, y: 1.0 }}
                                    colors={['#4A3DD2', '#47A2F3']}
                                    style={styles.linearGradient}>
                                    <_Text style={styles.buttonText}>
                                        한번 더 추천받기
                                </_Text>
                                    <Image source={Icons.ic_game_controller_outline} />
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                            <_Text style={styles.settingText}>맞춤친구</_Text>
                        </View>
                        {
                            extendOptions && extendOptions.map((item: any, index: number) => {
                                return this.renderExtendOption(item, index)
                            })
                        }
                    </View>
                </ScrollView>
            </ContainerMain>
        )
    }
}

const mapStateToProps = (state: AppState): Partial<Props> => ({
    recommendations: state.RecommendedFriendListRequestsReducer.recommendations
});

export default connect(mapStateToProps, null)(RecommendedFriend);
