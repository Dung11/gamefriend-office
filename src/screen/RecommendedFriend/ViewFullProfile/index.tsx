import { NavigationProp, RouteProp } from '@react-navigation/native';
import React, { Component } from 'react';
import { ScrollView, View, TouchableOpacity, Alert, StatusBar, Image, FlatList, processColor } from 'react-native';
import { RootStackParamList } from '../../../@types';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { colors } from '../../../config/Colors';
import styles from './styles';
import BackHeader from '../../../components/BackHeader/BackHeader';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { Icons, Images } from '../../../assets';
import ModalUserReport from '../../../components/Modal/ModalUserReport';
import { screen } from '../../../config/Layout';
import { RadarChart } from 'react-native-charts-wrapper';
import update from 'immutability-helper';
import { sendFriendRequest } from '../../../api/RecommendedServices';
import { blockUser, getUserDetailProfile, getUserPicture, personalTendencyById } from '../../../api/UserServices';
import _CommentContainer from '../../../components/CommentContainer';
import store from '../../../redux/store';
import { editRecommendedFriendRequest } from '../../../redux/recommendedfriendlist/actions';
import _ReportModal from '../ReportModal';
import _Text from '../../../components/_Text';
import _ModalShowImage from '../../../components/Modal/_ModalShowImage';
import { cancelRequestAddFriend } from '../../../api/ChatServices';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import Environments from '../../../config/Environments';
import ToastUtils from '../../../utils/ToastUtils';

type Params = {
    params: {
        friendId: string,
        userId: string,
        type: 'friendRequestNormalRecommendation' |
        'friendRequestSpecialRecommendation' |
        'leaveLiveTalkRoom' |
        'match' |
        'priorMatch' |
        'refreshNormalRecommendation' |
        'refreshSpecialRecommendation' |
        'viewDetailSpecialRecommendation' |
        'friendRequestLiveTalk',
        isRecommended?: boolean
    }
}

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    route: RouteProp<Params, 'params'>
}

const TIME_DATA = [
    {
        id: 1,
        numbertime: '6~12',
        nametime: '아침',
        isCheck: false
    },
    {
        id: 2,
        numbertime: '12~18',
        nametime: '오후',
        isCheck: false
    },
    {
        id: 3,
        numbertime: '18~24',
        nametime: '저녁',
        isCheck: false
    },
    {
        id: 4,
        numbertime: '00~06',
        nametime: '새벽',
        isCheck: false
    },
];

export class ViewFullProfile extends Component<Props> {

    _unsubscribe: any;

    state = {
        isLoading: true,
        isLoadingInScreen: false,
        reportVisible: false,
        listData: TIME_DATA,
        data: {},
        xAxis: {
            valueFormatter: ['게임스타일', '소통', '멘탈', '실력', '희생정신'],
            textSize: 14,
            fontWeight: '900',
            textColor: processColor(colors.blue),
            fontFamily: Environments.DefaultFont.Bold,
        },
        yAxis: {
            labelCount: 6,
            labelCountForce: true,
            drawLabels: false,
            axisMaximum: 5,
            axisMinimum: 0
        },
        legend: {
            enabled: false,
        },
        friendProfile: null,
        userIndex: [],
        friendIndex: [],
        reviews: [],
        isRequested: false,
        hashtag: '',
        reportModalVisible: false,
        userTendency: null,
        showingImage: [],
        visibleShowImg: false
    }

    async componentDidMount() {
        const { navigation } = this.props;
        this._unsubscribe = navigation.addListener('focus', async () => {
            await this.loadChartData()
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    loadChartData = async () => {
        let userTendency = null;
        let hashtag = '';
        let userIndex: any[] = [{ value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }];
        let friendIndex: any[] = [{ value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }, { value: 0 }];
        let friendProfile: any;
        const { route } = this.props;
        const params: any = route.params;

        try {
            let [resUserIndex, friendProfileRes] = await Promise.all([
                personalTendencyById(params.userId),
                getUserDetailProfile(params.friendId, params?.isRecommended)
            ]);
            userTendency = resUserIndex;
            console.log('resUserIndex: ', params.userId);
            if (resUserIndex && Object.keys(resUserIndex).length !== 0) {
                userIndex = [];
                for (let i = 0; i < resUserIndex.length; i++) {
                    let curTendency = resUserIndex[i];
                    userIndex.push(curTendency.level);
                }
            }
            friendProfile = friendProfileRes;
            if (friendProfileRes?.hasOwnProperty('tendency')) {
                let resFriendIndex = friendProfileRes.tendency
                if (resFriendIndex && Object.keys(resFriendIndex).length !== 0) {
                    friendIndex = [];
                    for (let i = 0; i < resFriendIndex.length; i++) {
                        let curTendency = resFriendIndex[i];
                        friendIndex.push(curTendency.level);
                        hashtag += `${curTendency.tag} `
                    }
                }
            }
        } catch (error) {
            Alert.alert('오류', error.message);
        } finally {
            this.setState({
                isLoading: false,
                userIndex,
                friendIndex,
                friendProfile,

                hashtag,
                userTendency,
                isRequested: friendProfile?.isRequested
            }, () => {
                this.setState(
                    update(this.state, {
                        data: {
                            $set: {
                                dataSets: [
                                    {
                                        values: this.state.friendIndex,
                                        label: '내 정보',
                                        config: {
                                            color: processColor(colors.blurPink),
                                            drawFilled: true,
                                            fillColor: processColor('rgba(236, 96, 131, 0.23)'),
                                            fillAlpha: 100,
                                            lineWidth: 3,
                                            textColor: processColor('white'),
                                            drawValues: false
                                        },
                                    },
                                    {
                                        values: this.state.userIndex,
                                        label: '상대정보',
                                        config: {
                                            color: processColor(colors.blue),

                                            drawFilled: true,
                                            fillColor: processColor('rgba(67, 135, 239, 0.19)'),
                                            fillAlpha: 150,
                                            lineWidth: 3,
                                            textColor: processColor('white'),
                                            drawValues: false
                                        }
                                    }
                                ],
                            }
                        },
                        xAxis: {
                            $set: {
                                valueFormatter: ['게임스타일', '소통', '멘탈', '실력', '희생정신'],
                                textSize: 14,
                                fontWeight: '900',
                                textColor: processColor(colors.blue),
                                fontFamily: Environments.DefaultFont.Bold
                            },
                        }
                    })
                );
            });
        }
    }

    onCloseReportFriend = () => {
        this.setState({ reportVisible: false });
    }

    onReportFriend = () => {
        this.setState({ reportVisible: true });
    }

    renderItem = ({ item, index }: any) => {
        let friendProfile: any = this.state?.friendProfile;
        let playTimes: any[] = friendProfile?.playTime;
        let checkExisted = this.checkExistItem(playTimes, item.nametime);
        return (
            <View style={styles.listTimeItem} key={index}>
                <View style={{
                    borderTopColor: checkExisted ? colors.blue : colors.inActiveBlue,
                    borderTopWidth: 7,
                    alignItems: 'center',
                    width: screen.widthscreen / 7
                }}>
                    <_Text style={styles.nameListTimeItem}>{item.numbertime}</_Text>
                    <_Text style={styles.nameListTimeItem}>{item.nametime}</_Text>
                </View>
            </View>
        )
    }

    checkExistItem(arrData: any[], value: any) {
        try {
            let index = arrData.findIndex((d) => d === value);
            if (index >= 0) {
                return true;
            }
            return false;
        } catch (error) {
            return false;
        }
    }

    onConfirmSendFQ = () => {
        const { navigation, route } = this.props;
        const params: any = route.params;
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: params.type,
                onSubmit: this.onSendFriendReq
            }
        });
    }

    onSendFriendReq = async () => {
        let userId: any = await keyValueStorage.get('userID');
        let friendProfile: any = this.state.friendProfile;

        this.setState({ isLoadingInScreen: true });
        sendFriendRequest(userId, friendProfile?.id).then((response: any) => {
            if (response.isFailed === false) {
                if (response.status === 200) {
                    this.setState({ isRequested: true });
                    store.dispatch(editRecommendedFriendRequest({ friendId: friendProfile?.id }));
                } else {
                    this.props.navigation.navigate("Modal", { screen: "AlertNoItem" });
                }
            } else {
                Alert.alert('오류', '친구 요청 오류.');
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({ isLoadingInScreen: false });
        });
    }

    onCloseReportModal = () => {
        this.setState({
            reportModalVisible: false
        });
    }

    submitReportModal = () => {
        ToastUtils.showToast({
            text1: '신고가 정상적으로 접수되었습니다',
            type: 'info',
            position: 'bottom'
        });
        this.onCloseReportModal();
    }

    onChooseOption = (option: number) => {
        if (option === 1) {
            this.onCloseReportFriend();
            setTimeout(() => {
                this.setState({
                    reportModalVisible: true
                });
            }, 400);
        } else {
            this.setState({ isLoadingInScreen: true });
            this.onBlockUser();
            this.onCloseReportModal();
        }
    }

    onBlockUser = () => {
        const { route } = this.props;
        const params: any = route.params;
        blockUser(params.userId, params.friendId).then((response: any) => {
            if (response.status === 400) {
                Alert.alert('오류', '사용자 신고 실패.');
            } else if (response.status === 409) {
                Alert.alert('오류', '이미 해당 사용자를 신고했습니다.');
            } else if (response.status === 200) {
                ToastUtils.showToast({
                    text1: '차단이 완료되었습니다',
                    type: 'info',
                    position: 'bottom'
                })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', '사용자 신고 실패.')
        }).finally(() => {
            this.setState({ isLoadingInScreen: false, reportVisible: false });
        })
    }

    onDenyFriendReq = async () => {
        let userId: any = await keyValueStorage.get('userID');
        let { friendProfile }: any = this.state;
        const { route } = this.props;

        this.setState({ isLoadingInScreen: true });
        cancelRequestAddFriend(userId, friendProfile?.id).then((response: any) => {
            if (response) {
                store.dispatch(editRecommendedFriendRequest({ friendId: friendProfile?.id }));
                this.setState({ isRequested: false });
            } else {
                Alert.alert('오류', '친구 요청 오류.');
            }
        }).catch((error: Error) => {
            console.log('clm');
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({
                isLoadingInScreen: false
            });
        });
    }

    onCloseModalShowImage = () => {
        this.setState({ visibleShowImg: false })
    }

    onClickAvatar = async () => {
        this.setState({ isLoadingInScreen: true });
        const { route } = this.props;
        const params: any = route.params;
        let pictures = await getUserPicture(params.friendId);
        this.setState({
            showingImage: pictures,
            visibleShowImg: true,
            isLoadingInScreen: false
        })
    }

    onClickReviewerAvatar = async (friendId: string) => {
        this.setState({ isLoadingInScreen: true });
        let pictures = await getUserPicture(friendId);
        if (pictures.length === 0) {
            this.setState({ isLoadingInScreen: false });
            return;
        }
        this.setState({
            showingImage: pictures,
            visibleShowImg: true,
            isLoadingInScreen: false
        });
    }

    render() {
        const { route } = this.props;
        const params: any = route.params;
        let { isLoading,
            reportVisible,
            listData,
            friendProfile,
            isRequested,
            isLoadingInScreen,
            hashtag,
            reportModalVisible,
            userTendency,
            visibleShowImg,
            showingImage,
            xAxis,
            yAxis
        }: any = this.state;
        if (isLoading) {
            return (
                <ContainerMain>
                    <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                    <LoadingIndicator visible={isLoading} />
                    <BackHeader {...this.props} />
                </ContainerMain>
            )
        }
        return (
            <ContainerMain>
                {
                    (visibleShowImg) &&
                    <_ModalShowImage
                        visible={visibleShowImg}
                        onClose={this.onCloseModalShowImage}
                        banner={showingImage} />
                }
                <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                <LoadingIndicator visible={isLoadingInScreen} />
                <BackHeader {...this.props} />
                <_ReportModal
                    userId={friendProfile?.id}
                    friendName={friendProfile?.userName}
                    visible={reportModalVisible}
                    onClose={this.onCloseReportModal}
                    onSubmit={this.submitReportModal} />
                <ModalUserReport
                    visible={reportVisible}
                    onClose={this.onCloseReportFriend}
                    onSubmit={this.onChooseOption} />
                <ScrollView style={styles.container}>
                    {
                        params.friendId !== params.userId &&
                         <TouchableOpacity style={styles.icThreeDots} onPress={this.onReportFriend}>
                        <Image source={Icons.ic_vertical_3_dots} />
                    </TouchableOpacity>
                    }
                   
                    <View style={styles.transparentSection}>
                        <TouchableOpacity onPress={() => this.onClickAvatar()}>
                            <Image
                                style={styles.topOneAvt}
                                source={friendProfile?.profilePicture ?
                                    { uri: friendProfile?.profilePicture } :
                                    Images.avatar_default} />
                        </TouchableOpacity>
                        <_Text style={styles.topOneUsername}>{friendProfile?.userName}</_Text>
                        {/* <_Text style={styles.idTxt}>id: {friendProfile?.id}</_Text> */}
                        <_Text style={styles.idTxt}>{friendProfile?.address || 'Updating'} | {friendProfile?.age}세 | {friendProfile?.gender === 'male' ? '남성' : '여성'}</_Text>
                    </View>
                    <View style={{ paddingHorizontal: 24, paddingTop: 20, paddingBottom: 30 }}>
                        <_Text style={styles.title}>나의 성격은</_Text>
                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            {
                                friendProfile && friendProfile?.personalities.map((item: any, index: number) => {
                                    return (
                                        <View style={styles.listItem} key={index}>
                                            <_Text style={styles.nameListItem}>{item}</_Text>
                                        </View>
                                    )
                                })
                            }
                        </View>
                        {
                            (friendProfile && friendProfile?.character) &&
                            <>
                                <_Text style={[styles.title, { marginTop: 20 }]}>나의 게임 스타일은</_Text>
                                <View style={styles.rowWrap}>
                                    {
                                        friendProfile?.character?.mental &&
                                        <TouchableOpacity style={styles.listItem}>
                                            <_Text style={styles.nameListItem}>{friendProfile?.character?.mental}</_Text>
                                        </TouchableOpacity>
                                    }
                                    {
                                        friendProfile?.character?.personality &&
                                        <TouchableOpacity style={styles.listItem}>
                                            <_Text style={styles.nameListItem}>{friendProfile?.character?.personality}</_Text>
                                        </TouchableOpacity>
                                    }
                                    {
                                        friendProfile?.character?.teamwork &&
                                        <TouchableOpacity style={styles.listItem}>
                                            <_Text style={styles.nameListItem}>{friendProfile?.character?.teamwork}</_Text>
                                        </TouchableOpacity>
                                    }

                                </View>
                            </>
                        }
                        <_Text style={[styles.title, { marginTop: 20 }]}>게임 접속 시간대는</_Text>
                        <View style={styles.rowWrap}>
                            <FlatList
                                scrollEnabled={false}
                                contentContainerStyle={{ alignItems: 'center' }}
                                data={listData}
                                numColumns={4}
                                renderItem={this.renderItem}
                                keyExtractor={index => index.toString()}
                            />
                        </View>
                        {
                            (friendProfile && friendProfile?.favoriteGame) &&
                            <>
                                <_Text style={[styles.title, { marginTop: 20 }]}>요즘 푹 빠진 게임</_Text>
                                <View style={styles.rowWrap}>
                                    <View style={styles.listItem}>
                                        <_Text style={styles.nameListItem}>{friendProfile?.favoriteGame?.name}</_Text>
                                    </View>
                                </View>
                            </>
                        }
                        {
                            (friendProfile && friendProfile?.games) &&
                            <>
                                <_Text style={[styles.title, { marginTop: 20 }]}>플레이 중인 게임</_Text>
                                <View style={styles.rowWrap}>
                                    {
                                        friendProfile?.games.map((item: any, index: number) => {
                                            return (
                                                <View style={styles.listItem} key={index}>
                                                    <_Text style={styles.nameListItem}>{item.name}</_Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </>
                        }
                    </View>
                    <View style={styles.chartContainer}>
                        {
                            hashtag.length > 0 &&
                            <View style={styles.tagContainer}>
                                <_Text style={styles.tagText}>{hashtag}</_Text>
                            </View>
                        }

                        <RadarChart
                            style={styles.chart}
                            data={this.state.data}
                            xAxis={xAxis}
                            yAxis={yAxis}
                            chartDescription={{ text: '' }}
                            legend={this.state.legend}
                            drawWeb={true}

                            webLineWidth={0}
                            webLineWidthInner={2}
                            webAlpha={255}
                            webColor={processColor("#ffffff")}
                            webColorInner={processColor("#bababa")}

                            skipWebLineCount={1}
                            touchEnabled={false}
                        />
                        <View style={{ flexDirection: 'row', alignContent: 'center', marginLeft: 25 }}>
                            <View style={{
                                justifyContent: 'center',
                                alignContent: 'center',
                            }}>
                                <View style={{
                                    backgroundColor: '#7AAAF4',
                                    alignItems: 'center',
                                    width: screen.widthscreen / 7,
                                    borderRadius: 6,
                                    height: 7
                                }}>
                                </View>
                            </View>
                            <View style={{
                                justifyContent: 'center',
                                alignContent: 'center',
                            }}>
                                <_Text style={[styles.legendText, { color: '#7AAAF4' }]}>내 정보</_Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignContent: 'center', marginLeft: 25, paddingBottom: 40 }}>
                            <View style={{ flex: .5, flexDirection: 'row' }}>
                                <View style={{
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                }}>
                                    <View style={{
                                        backgroundColor: '#EC6083',
                                        alignItems: 'center',
                                        width: screen.widthscreen / 7,
                                        borderRadius: 6,
                                        height: 7
                                    }}>
                                    </View>
                                </View>
                                <View style={{
                                    justifyContent: 'center',
                                    alignContent: 'center',
                                }}>
                                    <_Text style={[styles.legendText, { color: '#EC6083' }]}>상대정보</_Text>
                                </View>
                            </View>
                            {
                                userTendency?.length === 0 &&
                                <View style={{ flex: .5 }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ flexDirection: 'row' }}
                                            onPress={() => {
                                                this.props.navigation.navigate('SetupPropensityChat', {
                                                    tendency: userTendency
                                                });
                                            }}>
                                            <_Text style={styles.btnSetting}>내 성향 설정하기</_Text>
                                            <Image source={Icons.ic_round_right} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }
                        </View>
                    </View>
                    <View style={{ marginHorizontal: 12.5 }}>
                        <_CommentContainer
                            friendId={params.friendId}
                            {...this.props}
                            onClickAvatar={this.onClickReviewerAvatar}
                        />
                    </View>
                    {
                        (params.userId !== params.friendId && friendProfile?.isFriend === false) &&
                        (
                            <View style={{ justifyContent: 'center', alignItems: 'center', paddingBottom: 60, flexDirection: 'row' }}>
                                <TouchableOpacity style={[
                                    {
                                        backgroundColor: colors.blue,
                                        width: screen.widthscreen / 3,
                                        borderRadius: screen.widthscreen / 3,
                                        marginHorizontal: 5
                                    },
                                    {
                                        opacity: isRequested === true ? 0.5 : 1
                                    }]}
                                    disabled={isRequested} onPress={this.onConfirmSendFQ}>
                                    <_Text style={{
                                        fontWeight: '800',
                                        fontSize: 20,
                                        lineHeight: 24,
                                        color: colors.white,
                                        textAlign: 'center',
                                        paddingVertical: 10
                                    }}>친구추가</_Text>
                                </TouchableOpacity>
                                {/* <TouchableOpacity style={[
                                    {
                                        backgroundColor: '#C4C4C6',
                                        width: screen.widthscreen / 3,
                                        borderRadius: screen.widthscreen / 3,
                                        marginHorizontal: 5
                                    },
                                    {
                                        opacity: isRequested === false ? 0.5 : 1
                                    }]}
                                    disabled={!isRequested} onPress={this.onDenyFriendReq}>
                                    <_Text style={{
                                        fontWeight: '800',
                                        fontSize: 20,
                                        lineHeight: 24,
                                        color: colors.gray8F,
                                        textAlign: 'center',
                                        paddingVertical: 10
                                    }}>거절</_Text>
                                </TouchableOpacity> */}
                            </View>
                        )
                    }
                </ScrollView>
            </ContainerMain>
        );
    }
}

export default ViewFullProfile;