import { Dimensions, StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";
import { screen } from "../../../config/Layout";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    transparentSection: {
        backgroundColor: colors.backgroundApp,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 60,
        paddingBottom: 25,
    },
    // Section 1
    topOneTitle: {
        fontWeight: '800',
        fontSize: 20,
        color: colors.dark,
        marginTop: 20
    },
    topOneUsername: {
        color: colors.blue,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center',
        lineHeight: 24,
        marginTop: 10
    },
    topOneAvt: {
        width: screen.widthscreen / 4,
        height: screen.widthscreen / 4,
        borderRadius: screen.widthscreen / 4 / 2,
        marginTop: 10
    },
    idTxt: {
        fontWeight: '400',
        fontSize: 12,
        color: colors.black05,
        lineHeight: 22,
        textAlign: 'center',
        paddingHorizontal: 12
    },
    icThreeDots: {
        position: 'absolute',
        top: 20,
        right: 15,
        zIndex: 10
    },
    // Section 2
    listItem: {
        marginHorizontal: 2,
        marginVertical: 5,
        paddingHorizontal: 20,
        paddingVertical: 5,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: colors.mainColor
    },
    listTimeItem: {
        marginHorizontal: 2,
        marginVertical: 5,
        paddingHorizontal: 20,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: colors.mainColor
    },
    nameListItem: {
        color: colors.mainColor,
    },
    nameListTimeItem: {
        fontWeight: '400',
        fontSize: 12,
        lineHeight: 14,
        paddingTop: 5
    },
    title: {
        color: colors.blue,
        fontWeight: '800',
        fontSize: 14,
        lineHeight: 20
    },
    chartContainer: {
        paddingTop: 30,
        justifyContent: 'center',
        alignContent: 'center',
        borderRadius: 12,
        borderWidth: 1,
        marginHorizontal: 10,
        borderColor: '#bababa'
    },
    chart: {
        height: screen.heightscreen / 2,
    },
    commentContainer: {
        paddingBottom: 30,
        paddingHorizontal: 24,
        maxHeight: 400
    },
    tagContainer: {
        backgroundColor: '#EEF5FF',
        borderColor: colors.blue,
        borderWidth: 1,
        width: screen.widthscreen / 2, marginLeft: -10,
        borderTopRightRadius: 12,
        borderBottomRightRadius: 12
    },
    tagText: {
        color: colors.blue,
        fontWeight: '400',
        fontSize: 14,
        textAlign: 'center',
        paddingVertical: 10
    },
    rowFriendAvt: {
        marginVertical: 10,
        marginLeft: 12,
        width: screen.widthscreen / 8,
        height: screen.widthscreen / 8,
        borderRadius: screen.widthscreen / 8 / 2,
    },
    rowWrap: {
        flexDirection: 'row',
        marginTop: 10,
        flexWrap: 'wrap'
    },
    commentItem: {
        backgroundColor: '#F5F5F5',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        marginBottom: 10,
    },
    reviewName: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 20,
        color: colors.dark
    },
    reviewContent: {
        fontSize: 10,
        lineHeight: 14,
        paddingRight: 10,
    },
    btnSetting: {
        color: colors.blue,
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 14
    },
    legendText: {
        justifyContent: 'center',
        alignContent: 'center',
        paddingLeft: 10,
        fontSize: 14,
        fontWeight: '800',
    }
});

export default styles;