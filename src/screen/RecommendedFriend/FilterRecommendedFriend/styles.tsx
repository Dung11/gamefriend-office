import { StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";
import { screen } from "../../../config/Layout";

const styles = StyleSheet.create({
    whiteSection: {
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center'
    },
    // Section 1
    topOneTitle: {
        fontWeight: '800',
        fontSize: 20,
        color: colors.dark,
        marginTop: 20
    },
    topOneUsername: {
        color: colors.blue,
        fontSize: 18,
        fontWeight: '900',
        textAlign: 'center'
    },
    btnAddFriend: {
        marginTop: 10,
        backgroundColor: colors.mainColor,
        paddingVertical: 6,
        paddingHorizontal: 20,
        borderRadius: 12,
        marginBottom: 20,
    },
    addFriendTxt: {
        fontSize: 18,
        fontWeight: '900',
        color: colors.white,
    },
    topOneAvt: {
        width: screen.widthscreen / 4,
        height: screen.widthscreen / 4,
        borderRadius: screen.widthscreen / 4 / 2,
        marginTop: 10
    },
    // Section 2
    listFriendSection: {
        justifyContent: 'center',
    },
    rowFriend: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnGameController: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
        width: 50,
        height: 50,
        borderRadius: 50
    },
    rowFriendAvt: {
        marginTop: 10, width: screen.widthscreen / 8,
        height: screen.widthscreen / 8,
        borderRadius: screen.widthscreen / 8 / 2,
    },
    matchingPercent: {
        fontSize: 12,
        fontWeight: '400',
        color: '#909195'
    },
    friendName: {
        fontSize: 12,
        fontWeight: '500',
        color: '#65676B'
    },
    numberGameControler: {
        color: colors.black,
        fontWeight: '800',
        fontSize: 12
    },
    // Other Section
    linearGradient: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        paddingVertical: 10
    },
    buttonText: {
        fontSize: 18,
        fontWeight: '900',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    btnRefreshRecomendedList: {
        marginTop: 20,
        width: screen.widthscreen - 50
    },
    settingText: {
        textAlign: 'left',
        flex: 1,
        marginLeft: 20,
        color: colors.blue,
        fontWeight: '900',
        fontSize: 16,
        marginTop: 30
    },
    extendedContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        borderWidth: 1,
        borderColor: '#A6A8AA',
        paddingVertical: 10,
        marginBottom: 16
    },
    extendedText: {
        fontWeight: '800',
        fontSize: 14,
        color: '#1C1E21',
    }
});

export default styles;