import { NavigationProp, RouteProp } from '@react-navigation/native';
import React, { Component } from 'react'
import { Alert, Image, ScrollView, TouchableOpacity, View } from 'react-native'
import { RootStackParamList } from '../../../@types';
import { getExtendOption, sendFriendRequest, getRecommendationList, updateFilterRecommendationList } from '../../../api/RecommendedServices';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import MainHeader from '../../../components/MainHeader/MainHeader';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';
import { Icons, Images } from '../../../assets';
import { colors } from '../../../config/Colors';
import _Text from '../../../components/_Text';

type Params = {
    params: {
        option: any,
        userId: string,
    }
}

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    recommendations: any,
    route: RouteProp<Params, 'params'>
}

const MAP_OPTION_ICONS = [
    {
        id: 0,
        icon: Icons.ic_yellow_smile_face
    },
    {
        id: 1,
        icon: Icons.ic_yellow_position
    },
    {
        id: 2,
        icon: Icons.ic_red_reward
    },
    {
        id: 3,
        icon: Icons.ic_love_game_controller
    },
    {
        id: 4,
        icon: Icons.ic_yellow_moon
    }
];

export class FilterRecommendedFriend extends Component<Props> {

    _unsubscribe: any;

    state = {
        isLoading: true,
        listFriends: [],
        extendOptions: [],
        selectedExtendOption: null,
        userId: '',
        topOneUser: null,
        isVisibleModal: false,
        selectedFriend: null,
        itemForRequest: 0
    }

    async componentDidMount() {
        const { route } = this.props;
        const params: any = route.params;
        let userId: any = params.userId;
        let selectedExtendOption = params.option;

        let options: any[] = [];
        let topOneUser: any;
        let listFriends: any[] = [];
        let itemForRequest: number = 0;

        let optionParam = {
            option: params.option.id
        };

        try {
            let [extendOptionRes, recommendationsRes] = await Promise.all([
                getExtendOption(),
                getRecommendationList(userId, optionParam)
            ]);
            extendOptionRes?.options && (options = extendOptionRes.options);
            if (recommendationsRes.isFailed === true) {
                Alert.alert('오류', '친구 요청 오류.');
                return;
            }
            if (recommendationsRes.status === 200) {
                let data = recommendationsRes.data;
                itemForRequest = data.itemToConsume;
                data?.recommendations?.length > 0
                    && (topOneUser = data?.recommendations[0])
                data?.recommendations?.length > 1
                    && (listFriends = data.recommendations.slice(1));
            } else if (recommendationsRes.status === 402) {
                this.props.navigation.goBack();
                this.props.navigation.navigate("Modal", { screen: "AlertNoItem" });
                this._unsubscribe && this._unsubscribe();
            }
        } catch (error) {
            Alert.alert('오류', error.message);
        } finally {
            this.setState({
                extendOptions: options,
                userId,
                selectedExtendOption,
                listFriends,
                topOneUser,
                isLoading: false,
                itemForRequest
            });
        }
    }

    componentWillUnmount() {
        this._unsubscribe && this._unsubscribe();
    }

    onShowModal = (selectedFriend: any) => {
        this._unsubscribe = this.props.navigation.addListener('focus', async () => {
            await this.refreshRecommendation()
        });
        let type = 'friendRequestNormalRecommendation';
        const { selectedExtendOption }: any = this.state;
        if (selectedExtendOption.index === 2 || selectedExtendOption.index === 3) {
            type = 'friendRequestSpecialRecommendation'
        }

        this.props.navigation.navigate('ViewFullProfile', {
            userId: this.state.userId,
            friendId: selectedFriend?.recommendedUser?.id,
            type,
            isRecommended: true
        });
    }

    onSendFriendRequest = (friendId: string, index = 0, isTopUser = false) => {
        this.setState({ isLoading: true });
        sendFriendRequest(this.state.userId, friendId).then(response => {
            if (response.isFailed === true) {
                Alert.alert('오류', '친구 요청 오류.');
                return;
            }
            if (response.status === 200) {
                if (isTopUser) {
                    let topOneUser: any = this.state.topOneUser;
                    topOneUser.isRequested = true;
                    this.setState({ topOneUser })
                } else {
                    let listFriends: any = [...this.state.listFriends];
                    listFriends[index] = { ...listFriends[index], isRequested: true };
                    this.setState({ listFriends })
                }
            } else if (response.status === 402) {
                this.props.navigation.navigate("Modal", { screen: "AlertNoItem" });
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            this.setState({ isLoading: false });
        });
    }

    renderExtendOption(item: any, index: number) {
        let { selectedExtendOption }: any = this.state;

        return (
            <TouchableOpacity
                disabled={!item.isAvailable}
                style={[styles.extendedContainer,
                {
                    borderColor: (selectedExtendOption && (selectedExtendOption.index === index)) ?
                        colors.blue : '#A6A8AA',
                    opacity: item.isAvailable ? 1 : .6
                },
                {
                    borderWidth: (selectedExtendOption && (selectedExtendOption.index === index)) ?
                        2 : 1
                }]}
                onPress={() => this.onConfirmFilterFriend(item.id, index, item.name)}
                key={index}>
                <View style={{ flex: .1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={MAP_OPTION_ICONS[index].icon} />
                </View>
                <View style={{ flex: .7 }}>
                    <_Text style={styles.extendedText}>{item.name}</_Text>
                </View>
            </TouchableOpacity>
        )
    }

    onChooseExtendOption = (id: any, index: number, name: string) => {
        let { selectedExtendOption }: any = this.state;
        if (id === selectedExtendOption.id) return;
        this.setState({
            selectedExtendOption: { index, id, name }
        }, () => {
            this.refreshRecommendation();
        });
    }

    async refreshRecommendation() {
        this._unsubscribe && this._unsubscribe();
        let { selectedExtendOption }: any = this.state;

        let listFriends: any[] = [];
        let topOneUser: any;
        let itemForRequest: number = 0;
        this.setState({
            isLoading: true,
            listFriends: [],
            topOneUser: null,
        });
        let body = {};
        body = {
            option: selectedExtendOption.id
        }

        updateFilterRecommendationList(this.state.userId, body)
            .then((recommendationsRes: any) => {
                if (recommendationsRes.isFailed === true) {
                    Alert.alert('오류', '친구 요청 오류.');
                    return;
                }
                if (recommendationsRes.status === 200) {
                    let data = recommendationsRes.data;
                    itemForRequest = data.itemToConsume;
                    data?.recommendations?.length > 0
                        && (topOneUser = data?.recommendations[0])
                    data?.recommendations?.length > 1
                        && (listFriends = data.recommendations.slice(1));
                } else if (recommendationsRes.status === 402) {
                    this.props.navigation.navigate("Modal", { screen: "AlertNoItem" });
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
            }).finally(() => {
                this.setState({
                    listFriends,
                    isLoading: false,
                    topOneUser,
                    itemForRequest
                });
            });
    }

    onConfirmSendFQ = (friendId: string, index = 0, isTopUser = false) => {
        const { navigation } = this.props;
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'friendRequestSpecialRecommendation',
                onSubmit: () => this.onSendFriendRequest(friendId, index, isTopUser)
            }
        });
    }

    onConfirmFilterFriend = (id: any, index: number, name: string) => {
        // const { navigation } = this.props;
        // const { selectedExtendOption }: any = this.state;
        // if (index === selectedExtendOption.index) return;
        // if (index === 2 || index === 3) {
        //     navigation.navigate('Modal', {
        //         screen: 'AlertConfirmItem', params: {
        //             type: 'refreshSpecialRecommendation',
        //             onSubmit: () => this.onChooseExtendOption(id, index, name)
        //         }
        //     });
        // } else {
        //     this.onChooseExtendOption(id, index, name);
        // }
        this.onChooseExtendOption(id, index, name);
    }

    onConfirmResetFQ = () => {
        const { navigation } = this.props;
        navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'refreshSpecialRecommendation',
                onSubmit: () => this.refreshRecommendation
            }
        });
    }

    renderFriendItem(item: any, index: number) {
        let { itemForRequest } = this.state;
        let rankIc = Icons.ic_top_3;
        index === 0 && (rankIc = Icons.ic_top_1);
        index === 1 && (rankIc = Icons.ic_top_2);

        return (
            <TouchableOpacity style={styles.rowFriend} onPress={() => this.onShowModal(item)}
                key={index}>
                <View style={{ flex: .1 }}>
                    <Image style={{ marginTop: 10 }} source={rankIc} />
                </View>
                <View style={{ flex: .15 }}>
                    <Image style={styles.rowFriendAvt}
                        source={item.recommendedUser?.profilePicture ?
                            { uri: item.recommendedUser?.profilePicture } :
                            Images.avatar_default} />
                </View>
                <View style={{ flex: .22 }}>
                    <_Text numberOfLines={1} style={styles.friendName}>{item?.recommendedUser.userName}</_Text>
                </View>
                <View style={{ flex: .2 }}>
                    {
                        item?.totalPercent && <_Text style={styles.matchingPercent}>매칭률 {(item?.totalPercent * 100).toFixed(0)}%</_Text>
                    }
                </View>
                <View style={{ flex: .1 }}>
                    <View style={[styles.btnGameController]}>
                        <Image source={Icons.ic_game_controller} />
                        <_Text style={styles.numberGameControler}>{itemForRequest}</_Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        let { listFriends,
            extendOptions,
            isLoading,
            topOneUser,
            selectedExtendOption }: any = this.state;
        return (
            <ContainerMain>
                <LoadingIndicator visible={isLoading} />
                <MainHeader isBack={true} {...this.props} />
                <ScrollView>
                    <View style={styles.whiteSection} key={'section1'}>
                        <_Text style={styles.topOneTitle}>{selectedExtendOption && selectedExtendOption?.name}</_Text>
                        {
                            (topOneUser && topOneUser.recommendedUser) ? (
                                <>
                                    <Image style={{ marginTop: 10 }} source={Icons.ic_diamond} />
                                    <TouchableOpacity onPress={() => this.onShowModal(topOneUser)}>
                                        <Image
                                            style={styles.topOneAvt}
                                            source={topOneUser?.recommendedUser?.profilePicture ?
                                                { uri: topOneUser?.recommendedUser?.profilePicture } :
                                                Images.avatar_default} />
                                    </TouchableOpacity>
                                    <_Text style={styles.topOneUsername}>{topOneUser?.recommendedUser?.userName}</_Text>
                                    {
                                        topOneUser?.recommendedUser.id &&
                                        <TouchableOpacity
                                            style={[styles.btnAddFriend, { opacity: topOneUser.isRequested === true ? 0.5 : 1 }]}
                                            disabled={topOneUser?.isRequested}
                                            onPress={() => this.onConfirmSendFQ(topOneUser?.recommendedUser.id, 0, true)}>
                                            <_Text style={styles.addFriendTxt}>+ 친구 추가</_Text>
                                        </TouchableOpacity>
                                    }
                                </>
                            ) :
                                <View style={{ marginTop: 20 }}>
                                    <_Text>{'완벽한 겜친을 찾는 중이에요\n조금만 기다려주세요:)'}</_Text>
                                </View>
                        }
                    </View>
                    <View style={styles.listFriendSection} key={'section2'}>
                        {
                            listFriends && listFriends.map((item: any, index: number) => {
                                return this.renderFriendItem(item, index);
                            })
                        }
                    </View>
                    <View style={[styles.whiteSection, { paddingBottom: 20 }]} key={'section3'}>
                        <TouchableOpacity style={styles.btnRefreshRecomendedList}
                            onPress={this.onConfirmResetFQ}>
                            <LinearGradient
                                start={{ x: 0.0, y: 0.05 }}
                                end={{ x: 0.5, y: 1.0 }}
                                colors={['#4A3DD2', '#47A2F3']}
                                style={styles.linearGradient}>
                                <_Text style={styles.buttonText}>
                                    한번 더 추천받기
                                </_Text>
                                <Image source={Icons.ic_game_controller_outline} />
                            </LinearGradient>
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                            <_Text style={styles.settingText}>맞춤친구</_Text>
                        </View>
                        {
                            extendOptions && extendOptions.map((item: any, index: number) => {
                                return this.renderExtendOption(item, index)
                            })
                        }
                    </View>
                </ScrollView>
            </ContainerMain>
        )
    }
}

export default FilterRecommendedFriend;
