import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp, StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { Platform, StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import { colors } from '../../../config/Colors';
import DateTimePicker from '@react-native-community/datetimepicker';
import { screen } from '../../../config/Layout';
import moment from 'moment';
import { RouteProp } from '@react-navigation/native';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default function GenderScreen({
    navigation, route
}: Props) {
    const [isNext, setIsNext] = useState(false)
    const [gender, setGender] = useState('');
    const [chooseMale, setChooseMale] = useState(false)
    const [chooseFemale, setChooseFemale] = useState(false)

    const onNext = () => {
        const param: any = {
            ...route?.params,
            gender: gender
        }
        navigation.navigate('Address', param)
    }

    const onBack = () => {
        navigation.goBack()
    }

    const onClick = (value: number) => {
        if (value === 1) {
            setChooseMale(true)
            setChooseFemale(false)
            setGender('male')
            setIsNext(true)
        }
        if (value === 2) {
            setChooseFemale(true)
            setChooseMale(false)
            setGender('female')
            setIsNext(true)
        }
    };

    React.useEffect(() => {
        console.log('setChooseFemale', chooseFemale);
    });

    return (
        <Container isBack isNext={isNext ? true : false} onGoBack={onBack} onNext={onNext} styles={styles.container}>
            <_Text style={styles.title}>성별은 무엇인가요?</_Text>
            <View style={styles.wrapItem}>
                <_Text style={styles.titleItem}>변경이 어려우니 신중히 입력해주세요.</_Text>
                <View style={styles.wrapBtn}>
                    <TouchableOpacity onPress={() => { onClick(1) }} style={[styles.btn, { backgroundColor: chooseMale ? '#2E507B' : colors.white }]}>
                        <Image source={Icons.ic_male} />
                        <_Text style={[styles.name, { color: chooseMale ? colors.white : colors.black }]}>남성</_Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { onClick(2) }} style={[styles.btn, { backgroundColor: chooseFemale ? '#2E507B' : colors.white }]}>
                        <Image source={Icons.ic_female} />
                        <_Text style={[styles.name, { color: chooseFemale ? colors.white : colors.black }]}>여성</_Text>
                    </TouchableOpacity>
                </View>

            </View>

        </Container>
    );
}

const styles = StyleSheet.create({
    container: { paddingHorizontal: 16},
    title: {
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        marginBottom: 10
    },
    wrapItem: {
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        alignItems: 'center',
        width: '100%',
    },
    wrapBtn: { flexDirection: "row", marginVertical: 10 },
    btn: {
        width: screen.widthscreen / 3, height: screen.widthscreen / 3, backgroundColor: 'white', borderRadius: 8, marginHorizontal: 10,
        shadowColor: 'gray',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.8,
        shadowRadius: 4.65,
        elevation: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    name: {
        marginTop: 10,
        fontWeight: '500'
    }

})