import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../../../@types';
import { InputApp } from '../../../components/Input/InputApp';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import setupfirebase from '../../../utils/setupfirebase';
import Utils from '../../../utils/Utils';
import { RouteProp } from '@react-navigation/native';
import { Alert, StyleSheet } from 'react-native';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { checkPhoneNumberExist } from '../../../api/AuthorServices';
import _Text from '../../../components/_Text';

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default function PhoneNumberScreen({ navigation, route }: Props) {

    const { auth } = setupfirebase();
    const [state, setState] = useState({
        phoneNumberValue: '',
        isValidPhoneNumber: false,
        errorMesage: '',
        isLoading: false,
        confirmation: null,
        verificationId: null
    });

    const onNext = () => {
        let params: any = {
            ...route?.params
        };
        if (!state.confirmation || !state.verificationId) {
            params = {
                ...params,
                isBack: true
            };
            navigation.navigate('PolicyTerms', params);
        } else {
            params = {
                ...params,
                confirmation: state.confirmation,
                verificationId: state.verificationId,
            };
            navigation.navigate("EnterCode", params);
        }
    }

    const onChangePhone = (value: any) => {
        let checkValidPhoneNumber = Utils.validatePhone(value);
        if (!checkValidPhoneNumber) {
            setState({
                ...state,
                phoneNumberValue: value,
                errorMesage: '핸드폰 번호를 확인해주세요',
                isValidPhoneNumber: false,
                confirmation: null,
                verificationId: null
            });
        } else {
            setState({
                ...state,
                phoneNumberValue: value,
                errorMesage: '',
                isValidPhoneNumber: true,
                confirmation: null,
                verificationId: null
            });
        }
    }

    const onSubmitPhoneNumber = async () => {
        let phoneNumber = state.phoneNumberValue;
        let phone;
        if (phoneNumber.charAt(0) == '0') {
            phone = '82' + phoneNumber.slice(1)
            setState({
                ...state,
                isLoading: true
            });
            const response = await checkPhoneNumberExist(phone);
            if (!response) {
                setState({
                    ...state,
                    isLoading: false,
                    errorMesage: '잠시 후 다시 시도해주세요'
                });
            }
            if (response.isFailed) {
                Alert.alert('오류', response.errors[0]);
                return;
            }
            if (!response.data.isExist) {
                signInWithPhoneNumber(phone);
            } else {
                setState({
                    ...state,
                    isValidPhoneNumber: false,
                    errorMesage: '이미 등록된 번호입니다'
                });
            }
        }
        else {
            setState({
                ...state,
                errorMesage: '잘못된 휴대폰 번호입니다.'
            });
        }
    }

    async function signInWithPhoneNumber(phoneNumber: any) {
        try {
            const confirmation = await auth().signInWithPhoneNumber(`+${phoneNumber}`) as any;
            if (confirmation._auth._authResult) {
                setState({
                    ...state,
                    isValidPhoneNumber: true,
                    errorMesage: '',
                    isLoading: false,
                    confirmation: confirmation,
                    verificationId: confirmation._verificationId,
                });
                const params: any = {
                    confirmation: confirmation,
                    verificationId: confirmation._verificationId
                }
                navigation.navigate('EnterCode', params);
            } else {
                setState({
                    ...state,
                    errorMesage: '잘못된 휴대전화입니다.',
                    isLoading: false
                });
            }
        } catch (error) {
            setState({
                ...state,
                errorMesage: error,
                isLoading: false
            });
        }
    }

    const onBack = () => {
        navigation.goBack()
    }

    return (
        <Container isAuthor isBack isNext={true} onGoBack={onBack} onNext={onNext}>
            <LoadingIndicator visible={state.isLoading} />
            <InputApp
                isError
                errorMessage={state.errorMesage}
                keyboardType="numeric"
                returnKeyType="next"
                iconLabel={Icons.ic_phone}
                label="휴대폰 번호"
                placeholder="휴대폰 번호를 입력해주세요..."
                value={state.phoneNumberValue}
                onChangeText={onChangePhone} />
            <ButtonApp
                onPress={onSubmitPhoneNumber}
                title="본인 인증"
                backgroundColor={colors.mainColor}
                disabled={!state.isValidPhoneNumber} />
            <_Text style={styles.note}>{'휴대폰 번호는 계정찾기 이외의\n 다른 목적으로 사용되지 않습니다.'}</_Text>
        </Container>
    );
}

const styles = StyleSheet.create({
    note: {
        textAlign: 'center',
        fontSize: 14,
        fontWeight: '600'
    }
})
