import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { StyleSheet, View, TouchableOpacity, FlatList, Alert } from 'react-native';
import { colors } from '../../../config/Colors';
import { RouteProp } from '@react-navigation/native';
import { screen } from '../../../config/Layout';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import _Text from '../../../components/_Text';

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        profilePicture: string,
        pictures: any,
        gamePictures: any,
        userName: string
        favoriteGameId: any,
        gameIds: any,
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}

const TIME_DATA = [
    {
        id: 1,
        numbertime: '6~12',
        nametime: '아침',
        isCheck: false
    },
    {
        id: 2,
        numbertime: '12~18',
        nametime: '오후',
        isCheck: false
    },
    {
        id: 3,
        numbertime: '18~24',
        nametime: '저녁',
        isCheck: false
    },
    {
        id: 4,
        numbertime: '00~06',
        nametime: '새벽',
        isCheck: false
    },
];

export default function ChoosePlayTimeScreen({ navigation, route }: Props) {
    const [isNext, setIsNext] = useState(false);
    const [playTime, setPlayTime] = useState<string[]>([]);
    const [listData, setListData] = useState(TIME_DATA);

    const onNext = async () => {
        const param: any = {
            ...route?.params,
            playTime: playTime
        }
        navigation.navigate('ChooseCharacterPlay', param);
    }

    const onBack = () => {
        navigation.goBack()
    }

    const onChoose = (name: string, index: number) => {
        const selectedItem = listData[index];
        let tempData = [...listData];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };

        let tmpPlayTime = [...playTime];
        if (!selectedItem.isCheck) {
            tmpPlayTime.push(name);
        } else {
            tmpPlayTime = tmpPlayTime.filter((d) => d !== selectedItem.nametime);
        }

        setIsNext(true);
        setListData(tempData);
        setPlayTime(tmpPlayTime);
    }

    const renderItem = ({ item, index }: any) => {
        return (
            <TouchableOpacity style={styles.listItem} key={index} onPress={() => onChoose(item.nametime, index)}>
                <View style={{
                    borderTopColor: item.isCheck ? colors.mainColor : colors.grayLight,
                    borderTopWidth: 7,
                    alignItems: 'center',
                    width: screen.widthscreen / 7
                }}>
                    <_Text style={styles.nameListItem}>{item.numbertime}</_Text>
                    <_Text style={styles.nameListItem}>{item.nametime}</_Text>
                </View>
            </TouchableOpacity>
        )
    }
    return (
        <Container isBack isNext={isNext ? true : false} onGoBack={onBack} onNext={onNext} styles={styles.container}>
            <_Text style={styles.title}>게임 플레이 시간대는 언제인가요?</_Text>
            <View style={styles.wrapItem}>
                <_Text style={styles.titleItem}>추천친구에 반영됩니다</_Text>
                <View style={styles.sectionItem}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FlatList
                            scrollEnabled={false}
                            contentContainerStyle={{ alignItems: 'center' }}
                            data={listData}
                            numColumns={4}
                            renderItem={renderItem}
                            keyExtractor={index => index.toString()}
                        />
                    </View>
                </View>
            </View>
        </Container>
    );
}

const styles = StyleSheet.create({
    container: { paddingHorizontal: 16, justifyContent: 'flex-start', paddingTop: 60 },
    title: {
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize:14,
        marginBottom: 10
    },
    wrapItem: {
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        alignItems: 'center',
        width: '100%',
    },
    sectionItem: { width: '100%' },
    infoItem: { marginVertical: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' },
    label: { marginLeft: 10 },
    listItem: { marginHorizontal: 5, marginVertical: 5, paddingHorizontal: 10, paddingVertical: 5 },
    nameListItem: { marginVertical: 5, textAlign: "center" }
})