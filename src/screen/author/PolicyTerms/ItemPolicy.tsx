import React, { useEffect, useState } from 'react';
import { StyleSheet, View, ViewStyle, Switch, ScrollView, Dimensions } from "react-native";
import { getBoardServicePolicy, getPrivacyPolicy, getServicePolicy } from '../../../api/UserServices';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';

interface Props {
    styles?: ViewStyle;
    isEnabled: boolean;
    toggleSwitch: () => void;
    type: 'privacy' | 'service' | 'board-service';
}

export const ItemPolicy = (props: Props) => {

    const [isEnable, setIsEnabled] = useState(props.isEnabled);
    // const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');

    useEffect(() => {
        if (props.type == 'privacy') {
            getPrivacyPolicy().then((response: any) => {
                if (response?.data?.content) {
                    setTitle(response.data.title);
                    setContent(response.data.content);
                }
            });
        } else if (props.type == 'service') {
            getServicePolicy().then((response: any) => {
                if (response?.data?.content) {
                    setTitle(response.data.title);
                    setContent(response.data.content);
                }
            });
        } else if (props.type == 'board-service') {
            getBoardServicePolicy().then((response: any) => {
                if (response?.data) {
                    setTitle(response.data.title);
                    setContent(response.data.content);
                }
            });
        }
        return () => { }
    }, [])

    return (
        <View style={styles.container}>
            <ScrollView nestedScrollEnabled={true}>
                <_Text style={styles.title}>{title}</_Text>
                <_Text style={styles.textContent}>{content}</_Text>
            </ScrollView>
            <View style={styles.contentContainer}>
                <_Text>이용약관 동의 (필수)</_Text>
                <Switch
                    trackColor={{ false: "#C4C4C6", true: colors.mainColor }}
                    thumbColor={isEnable ? colors.white : colors.white}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={props.toggleSwitch}
                    value={props.isEnabled}
                />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: '100%',
        borderRadius: 13,
        height: Dimensions.get('screen').height / 2.5,
        marginBottom: 20,
    },
    title: {
        color: colors.mainColor,
        fontSize: 18,
        textAlign: 'center',
        marginBottom: 16,
    },
    textContent: {
        fontSize: 12,
    },
    contentContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 16,
    },
});
