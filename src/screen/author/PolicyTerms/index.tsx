import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types/types';
import { Container } from '../../../components/Container/InputApp';
import { ItemPolicy } from './ItemPolicy';
import { RouteProp } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { StyleSheet, View } from 'react-native';

type Params = {
    params: {
        email?: string,
        password?: string,
        confirmPassword?: string,
        phoneNumber?: string,
        isBack?: boolean,
        kakaoId?: string,
        facebookId?: string,
        googleId?: string,
        appleId?: string,
        firebaseId?: string,
        accessToken?: string
    }
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}

export default function PolicyTermsScreen({ navigation, route }: Props) {

    const [isEnabledOne, setIsEnabledOne] = useState(false);
    const [isEnabledTwo, setIsEnabledTwo] = useState(false);
    const [isEnabledThree, setIsEnabledThree] = useState(false);

    const onBack = () => {
        navigation.goBack()
    }

    const onNext = () => {
        let params: any = {
            ...route?.params,
        }
        delete params?.isBack;
        navigation.navigate("BirthDay", params);
    }

    const toggleSwitchOne = () => setIsEnabledOne(previousState => !previousState);
    const toggleSwitchTwo = () => setIsEnabledTwo(previousState => !previousState);
    const toggleSwitchThree = () => setIsEnabledThree(previousState => !previousState);

    return (
        <Container
            onGoBack={onBack}
            isNotBack={!route?.params?.isBack}
            isBack={route?.params?.isBack}
            isNext={(isEnabledOne && isEnabledTwo && isEnabledThree) ? true : false}
            onNext={onNext}
            styles={styles.mainContainer}>
            <ScrollView style={styles.container}>
                <ItemPolicy
                    toggleSwitch={toggleSwitchOne}
                    isEnabled={isEnabledOne}
                    type="privacy" />
                <ItemPolicy
                    toggleSwitch={toggleSwitchTwo}
                    isEnabled={isEnabledTwo}
                    type="service" />
                <ItemPolicy
                    toggleSwitch={toggleSwitchThree}
                    isEnabled={isEnabledThree}
                    type="board-service" />
                <View style={styles.emptySpace} />
            </ScrollView>
        </Container>
    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingHorizontal: 16
    },
    content: {
        marginTop: 20,
        marginBottom: 70
    },
    container: {
        flex: 1,
        padding: 15,
        paddingBottom: 70,
        width: '100%',
    },
    emptySpace: {
        height: 60,
    },
})