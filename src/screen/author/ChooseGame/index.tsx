import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { StyleSheet, View, TouchableOpacity, Image, FlatList } from 'react-native';
import { colors } from '../../../config/Colors';
import { RouteProp } from '@react-navigation/native';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        profilePicture: string,
        pictures: any,
        gamePictures: any,
        userName: string
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}

export default function ChooseGameScreen({
    navigation, route
}: Props) {

    const [favoriteGame, setFavoriteGame] = useState<any>();
    const [game, setGame] = useState<any[]>([]);
    const [gameIds, setGameIds] = useState<any[]>([]);

    const onNext = () => {
        const param: any = {
            ...route?.params,
            favoriteGameId: favoriteGame?.id,
            gameIds: gameIds
        };
        navigation.navigate('ChoosePlayTime', param);
    }

    const onBack = () => {
        navigation.goBack()
    }

    const resultFavoriteGame = (value: any) => {
        setFavoriteGame(value);
    }

    const result = (value: any) => {
        setGame(value);
        let tmpGameIds: any[] = [];
        value.forEach((element: any) => {
            tmpGameIds.push(element.id)
        });
        setGameIds(tmpGameIds);
    }

    const onAddGameFavorite = () => {
        navigation.navigate('SearchFiterGameFavorite',
            {
                resultFavoriteGame,
                numberOfChoice: 1,
                bannedGames: [...game],
                choices: favoriteGame ? [favoriteGame] : [],
                type: 'favorite-game'
            });
    };

    const onAddGame = () => {
        navigation.navigate('SearchFiterGameFavorite',
            {
                result,
                numberOfChoice: 3,
                bannedGames: favoriteGame ? [favoriteGame] : [],
                choices: game,
                type: 'game'
            });
    };

    const onRemoveGame = (item: any) => {
        let tmpGame = [...game];
        tmpGame = tmpGame.filter((d) => d.id !== item.id);
        setGame(tmpGame);

        let tmpGameIds = [...gameIds];
        tmpGameIds = tmpGameIds.filter((d) => d !== item.id);
        setGameIds(tmpGameIds);
    }


    const onRemoveFavoriteGame = () => {
        setFavoriteGame(null);
    }

    const renderItem = (item: any, index: number) => {
        return (
            <TouchableOpacity style={styles.listItem}
                key={index}
                onPress={() => onRemoveGame(item)}>
                <_Text style={styles.nameListItem}>{item.name}</_Text>
                <Image source={Icons.ic_closed} />
            </TouchableOpacity>
        )
    }

    return (
        <Container
            isBack
            isNext={(favoriteGame && gameIds.length > 0) ? true : false}
            onGoBack={onBack}
            onNext={onNext}
            styles={styles.container}>
            <_Text style={styles.title}>가장 좋아하는 게임은 무엇인가요?</_Text>
            <View style={styles.wrapItem}>
                <_Text style={styles.titleItem}>추천친구에 반영됩니다</_Text>
                <View style={styles.sectionItem}>
                    <View style={styles.infoItem}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '60%' }}>
                            <Image source={Icons.ic_star} />
                            <_Text style={styles.label}>가장 좋아하는 게임</_Text>
                        </View>
                        <_Text style={{ color: colors.red }}>한개를 선택하세요</_Text>
                    </View>
                    <View style={styles.flexRowCenter}>
                        <View style={styles.gameContainer}>
                            {favoriteGame &&
                                <TouchableOpacity style={styles.listItem} onPress={onRemoveFavoriteGame}>
                                    <_Text style={styles.nameListItem}>{favoriteGame?.name}</_Text>
                                    <Image source={Icons.ic_closed} />
                                </TouchableOpacity>
                            }
                        </View>
                        <TouchableOpacity onPress={onAddGameFavorite}>
                            <Image source={Icons.ic_add} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.sectionItem}>
                    <View style={styles.infoItem}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '60%' }}>
                            <Image source={Icons.ic_game_controller} />
                            <_Text style={styles.label}>좋아하는 게임</_Text>
                        </View>
                    </View>
                    <View style={styles.flexRowCenter}>
                        <View style={styles.gameContainer}>
                            {
                                game.length > 0 &&
                                game.map((item: any, index: number) => renderItem(item, index))
                            }
                        </View>
                        <TouchableOpacity onPress={onAddGame}>
                            <Image source={Icons.ic_add} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Container>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        justifyContent: 'flex-start',
        paddingTop: 60
    },
    flexRowCenter: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    gameContainer: {
        paddingLeft: 20,
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: .98
    },
    title: {
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        marginBottom: 10
    },
    wrapItem: {
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        alignItems: 'center',
        width: '100%',
    },
    sectionItem: {
        width: '100%'
    },
    infoItem: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    label: {
        marginLeft: 10
    },
    listItem: {
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 1,
        borderColor: colors.mainColor
    },
    nameListItem: {
        color: colors.mainColor,
        marginRight: 10,
        fontWeight: '400',
        fontSize: 12,
        marginTop: 5
    }
})