import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../../../@types';
import { InputApp } from '../../../components/Input/InputApp';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import { Alert, StyleSheet } from 'react-native';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import { RouteProp } from '@react-navigation/native';
import _Text from '../../../components/_Text';
import { newPassword } from '../../../api/AuthorServices';
import ToastUtils from '../../../utils/ToastUtils';

type Params = {
    params: {
        userId: string,
        accessToken: string,
        email: string,
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}

export default function NewPasswordScreen({
    navigation, route
}: Props) {
    const [password, setPassword] = useState('')
    const [cPassword, setCPassword] = useState('')

    const onSubmit = async () => {
        try {
            if (password !== cPassword) {
                Alert.alert('비밀번호가 일치하지 않습니다.');
                return;
            }
            const result = await newPassword(
                route.params.userId,
                route.params.accessToken,
                password,
                cPassword);

            if (result.status === 200) {
                ToastUtils.showToast({
                    text1: '성공적으로!',
                    position: 'bottom',
                    type: 'success'
                })
                setTimeout(() => {
                    navigation.navigate('Login');
                }, 4000);
            }
        } catch (error) {
            Alert.alert("비밀번호 변경 실패!")
            console.log("error =>>", error.response)
        }
    }


    const onChangePassword = (text: string) => {
        setPassword(text);
    }

    const onChangeCPassword = (text: string) => {
        setCPassword(text);
    }

    const onBack = () => {
        navigation.goBack()
    }

    return (
        <Container isAuthor onGoBack={onBack} isBack>
            <_Text style={styles.title}>가입 이메일</_Text>
            <_Text style={styles.infoEmail}>{route.params.email}</_Text>
            <InputApp iconRight returnKeyType="next" iconLabel={Icons.ic_password} label="비밀번호" placeholder="영어+숫자 8자리 이상이 필요합니다..." value={password} onChangeText={(text) => onChangePassword(text)} />
            <InputApp iconRight returnKeyType="next" iconLabel={Icons.ic_password} label="비밀번호확인" placeholder="영어+숫자 8자리 이상이 필요합니다..." value={cPassword} onChangeText={(text) => onChangeCPassword(text)} />
            {password && cPassword !== '' ? <ButtonApp style={{ marginTop: 20 }} onPress={onSubmit} title="휴대폰 인증" backgroundColor={colors.mainColor} /> : null}
        </Container>
    );
}
const styles = StyleSheet.create({
    title: {
        color: colors.mainColor,
        fontSize: 23,
        marginBottom: 5,
        fontWeight: '900'

    },
    infoEmail: {
        fontWeight: "800",
        fontSize: 16,
        marginBottom: 5,
        color: '#65676B'
    }
})
