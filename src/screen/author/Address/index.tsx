import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { Platform, StyleSheet, View, Image, TouchableOpacity, Alert } from 'react-native';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import { RouteProp } from '@react-navigation/native';
import { Icons } from '../../../assets';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Environments from '../../../config/Environments';
import _Text from '../../../components/_Text';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getAddrFromLatLng } from '../../../api/GeneralServices';
import { PERMISSIONS, request, RESULTS } from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default function AddressScreen({
    navigation, route
}: Props) {

    let locationInput: any = null;

    const [isNext, setIsNext] = useState(false);
    const [address, setAddress] = useState('');
    const [focusBorder, setFocusBorder] = useState(false);
    const [logitude, setLongitude] = useState<any>();
    const [latitude, setLatitude] = useState<any>();
    const [visible, setVisible] = useState<boolean>(false)

    const onNext = () => {
        const param: any = {
            ...route?.params,
            address: address,
            location: {
                longitude: `${logitude}`,
                latitude: `${latitude}`
            }
        }
        navigation.navigate('UploadImage', param);
    }

    const onBack = () => {
        navigation.goBack()
    }

    const onChangeInput = (data: any, details: any) => {
        if (details) {
            let { location } = details.geometry;
            setLatitude(location.lat);
            setLongitude(location.lng);
            setAddress(data.description);
            setIsNext(true);
        }
    }

    const handleChangeAddress = (value: string) => {
        setAddress(value);
    }

    const _renderItem = (data: any) => {
        return (
            <View style={styles.searchResultContainer}>
                <Image resizeMode="contain" source={Icons.ic_address} />
                <View style={styles.itemSearchResult}>
                    <View style={{ width: screen.widthscreen / 1.7 }}>
                        <_Text ellipsizeMode="tail" numberOfLines={1} style={[styles.searchResultText, { fontWeight: '900' }]}>
                            {data.structured_formatting.main_text}
                        </_Text>
                        <_Text ellipsizeMode="tail" numberOfLines={1} style={styles.searchResultText}>
                            {data.structured_formatting.secondary_text}
                        </_Text>
                    </View>
                    <Image source={Icons.ic_arrowTopLeft} />
                </View>
            </View>
        )
    }

    const EmptyListMessage = (item: any) => {
        return (
            <View style={styles.wrappEmpty}>
                <_Text style={styles.emptyListStyle}>주소 검색결과가 없습니다</_Text>
            </View>

        );
    };

    const onGetMyLocation = async () => {
        const AccessPermission = Platform.select({
            ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
            android: (PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        });
        if (!AccessPermission) return;

        request(AccessPermission).then(async (result) => {
            switch (result) {
                case RESULTS.GRANTED:
                    Geolocation.getCurrentPosition(
                        async (position) => {
                            let coords = position.coords;
                            let { latitude, longitude } = coords;
                            setVisible(true);
                            let response = await getAddrFromLatLng(latitude, longitude);
                            if (response.results && response.results.length > 0) {
                                let addressComponents = response.results[0].address_components.reverse();
                                let addrWithoutStreet = '';
                                addressComponents.forEach((element: any) => {
                                    if (!element.types.includes('street_number')
                                        && !element.types.includes('sublocality_level_2')
                                        && !element.types.includes('premise')
                                        && !element.types.includes('country')
                                        && !element.types.includes('postal_code')) {
                                        addrWithoutStreet += `${element.long_name} `;
                                    }
                                });
                                setAddress(addrWithoutStreet);
                                setLatitude(latitude);
                                setLongitude(longitude);
                                setIsNext(true);
                            } else {
                                Alert.alert('오류', '위치를 찾을 수 없습니다');
                            }
                            setVisible(false);
                        }
                        ,
                        (error) => {
                            console.log('Error: ', error);
                        },
                        {
                            enableHighAccuracy: true,
                            timeout: 12000,
                            maximumAge: 30000,
                            showLocationDialog: true
                        }
                    );
                    break;
                default:
                    Alert.alert('오류', '위치 사용 권한을 부여해주세요');
                    break;
            }
        }).finally(() => {
            setVisible(false);
        });
    }

    const onRefreshLocation = () => {
        setLatitude(null);
        setLongitude(null);
        setAddress('');
        setIsNext(false);
    }

    return (
        <Container isBack isNext={isNext ? true : false} onGoBack={onBack} onNext={onNext} styles={styles.container}>
            <LoadingIndicator visible={visible} />
            <_Text style={styles.title}>사는 곳은 어디인가요?</_Text>
            <_Text style={styles.titleItem}>추천 친구에 반영됩니다</_Text>
            <GooglePlacesAutocomplete
                enableHighAccuracyLocation={true}
                placeholder='지역 이름으로 검색하기'
                minLength={2}
                fetchDetails={true}
                onPress={onChangeInput}
                ref={input => { locationInput = input }}
                query={{
                    key: Environments.GOOGLE_API_KEY,
                    language: 'ko',
                }}
                textInputProps={{
                    value: address,
                    onChangeText: handleChangeAddress,
                    onFocus: () => { setFocusBorder(!focusBorder) }
                }}
                renderRightButton={() => address.length === 0 ?
                    <TouchableOpacity style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }}
                        onPress={onGetMyLocation}>
                        <Image
                            source={Icons.ic_location}
                            style={{ width: 20, height: 20 }}
                        />
                    </TouchableOpacity> :
                    <TouchableOpacity style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }}
                        onPress={onRefreshLocation}>
                        <Icon name="times" />
                    </TouchableOpacity>
                }
                renderRow={(rowData) =>
                    _renderItem(rowData)
                }
                listEmptyComponent={(data: any) => EmptyListMessage(data)}
                styles={{
                    container: styles.searchContainer,
                    textInputContainer: [styles.searchInputContainer, {
                        borderWidth: 1,
                        borderColor: focusBorder ? colors.mainColor : colors.white,
                    }],
                    textInput: styles.searchInput,
                    listView: styles.listView,
                    row: styles.row,
                    poweredContainer: { display: 'none' },
                    powered: { display: 'none' },
                    separator: { display: 'none' },
                }}
                nearbyPlacesAPI='GooglePlacesSearch'
            // debounce={200}
            />
        </Container>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: Platform.OS === 'ios' ? 16 : 0, marginHorizontal: 16,
        alignItems: 'center',
        marginTop: 16
    },
    title: {
        marginTop: 30,
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        marginBottom: 10
    },
    // input
    listView: { marginTop: 10, width: screen.widthscreen - 32 },
    row: { alignItems: 'center', height: screen.heightscreen / 12 },
    searchContainer: {
    },
    searchInputContainer: {
        width: '100%',
        borderRadius: 30,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingVertical: Platform.OS === 'ios' ? 7 : 0,
        flexDirection: 'row',
    },
    searchInput: {
        marginTop: 5,
        color: '#5d5d5d',
        fontSize: 12,
        height: 30,
        fontWeight: '400',
        // fontFamily: Environments.DefaultFont.Regular,
    },
    searchResultContainer: {
        flexDirection: 'row',
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        alignItems: "center",
        justifyContent: 'flex-start',
        width: '100%',
        height: screen.heightscreen / 10,

    },
    itemSearchResult: {
        marginLeft: 10,
        borderBottomColor: colors.backgroundApp,
        borderBottomWidth: 1,
        paddingBottom: 5,
        width: '90%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    searchResultText: {
        fontSize: 14,
    },
    wrappEmpty: {
        width: screen.widthscreen - 32,
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        height: screen.heightscreen / 4,
        backgroundColor: colors.white

    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        fontStyle: "italic",
        color: colors.grayLight
    },

})