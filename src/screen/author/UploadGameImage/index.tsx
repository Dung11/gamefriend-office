import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { StyleSheet, View, TouchableOpacity, Image, ScrollView, Dimensions } from 'react-native';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import { RouteProp } from '@react-navigation/native';
import { Images } from '../../../assets';
import _ChooseImageModal from '../../../components/ChooseImageModal/_ChooseImageModal';
import _Text from '../../../components/_Text';

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        pictures: string[],
        profilePicture: string,
        gamePictures: string[]
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default function UploadGameImageScreen({ navigation, route }: Props) {

    const [isNext, setIsNext] = useState(false);
    const [show, setShow] = useState(false);
    const [selectedIndex, setSelectedIndex] = useState<number>(0);

    const [gamePictures, setGamePictures] = useState<string[]>([]);

    const onNext = () => {
        const param: any = {
            ...route?.params,
            gamePictures: gamePictures
        };
        navigation.navigate('NickName', param);
    }

    const onBack = () => {
        navigation.goBack();
    }

    const onClickOnImg = (index: number) => {
        setSelectedIndex(index);
        setShow(true);
    }


    const onChangeModal = (locations: string[]) => {
        let picturesTmp = [...gamePictures];
        if (typeof picturesTmp[selectedIndex] !== 'undefined') {
            picturesTmp[selectedIndex] = locations[0];
        } else {
            picturesTmp.push(locations[0]);
        }

        setGamePictures(picturesTmp);
        setIsNext(true);
        setShow(false);
    }

    const onCloseModal = () => {
        setShow(false);
    }

    return (
        <Container isBack isNext={isNext ? true : false} onGoBack={onBack} onNext={onNext} styles={styles.container}>
             <_ChooseImageModal
                visible={show}
                title={'최근사진'}
                doneButtonText={'완료'}
                onClose={onCloseModal}
                onChange={onChangeModal}
                numberOfImages={1} />
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <_Text style={styles.title}>즐겨하는 게임 사진을 추가해주세요</_Text>
                <_Text style={styles.titleItem}>사진은 검수가 되니 신중히 입력해주세요.</_Text>
            </View>
            <View style={{ width: '100%', alignItems: 'flex-end' }}>
                <ScrollView indicatorStyle="white" horizontal style={{ marginTop: 20, height: screen.heightscreen / 6 }} showsHorizontalScrollIndicator={false}>
                    <View style={{ width: Dimensions.get('screen').width * 33 / 100 }}></View>
                    {
                        [0, 1, 2, 3, 4].map((item: number, index: number) => {
                            return (
                                <View key={index}>
                                    <TouchableOpacity onPress={() => { onClickOnImg(index) }}>
                                        <Image style={styles.avatar}
                                            source={typeof gamePictures[index] !== 'undefined' ?
                                                { uri: gamePictures[index] } :
                                                Images.avatar_default} />
                                    </TouchableOpacity>
                                    {
                                        index === 0 && <_Text style={styles.textRequired}>*필수</_Text>
                                    }
                                </View>
                            )
                        })
                    }
                </ScrollView>
            </View>
        </Container>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        justifyContent: 'flex-start'
    },
    title: {
        marginTop: 30,
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        marginBottom: 10
    },
    avatar: {
        marginHorizontal: 10,
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    selectedAvatar: {
        borderWidth: 3,
        borderColor: '#3766bc'
    },
    textRequired: {
        color: colors.red,
        textAlign: 'center',
        paddingTop: 14,
        fontWeight: '400',
        fontSize: 14
    },
    cAvtRequired: {
        color: colors.red,
        textAlign: 'center',
        paddingTop: 14,
        fontWeight: '800',
        fontSize: 14
    },
})