import * as React from 'react';

import { useState, useEffect } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { StyleSheet, View, TouchableOpacity, Image, FlatList, Alert } from 'react-native';
import { colors } from '../../../config/Colors';
import { RouteProp } from '@react-navigation/native';
import { Icons } from '../../../assets';
import { screen } from '../../../config/Layout';
import { getCharacterByType } from '../../../api/GameServices';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import _Text from '../../../components/_Text';

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        profilePicture: string,
        pictures: any,
        gamePictures: any,
        userName: string
        favoriteGameId: any,
        gameIds: any,
        playTime: string
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}

export default function ChooseCharacterPlayScreen({
    navigation, route
}: Props) {
    const [mental, setMental] = useState<any>();
    const [personality, setPersonality] = useState<any>();
    const [teamwork, setTeamwork] = useState<any>();
    const [dataMental, setDataMental] = useState<any[]>([]);
    const [dataPersonality, setDataPersonality] = useState<any[]>([]);
    const [dataTeamwork, setDataTeamwork] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState(true);

    const onNext = () => {
        const param: any = {
            ...route?.params,
            character: {
                mental: mental ? mental.name : '',
                personality: personality ? personality.name : '',
                teamwork: teamwork ? teamwork.name : ''
            }
        };
        navigation.navigate('ChoosePeronalities', param);
    }

    const loadData = async () => {
        try {
            let [teamworkRes, mentalRes, personalityRes] = await Promise.all([
                getCharacterByType('teamwork'),
                getCharacterByType('mental'),
                getCharacterByType('personality'),
            ]);
            setDataTeamwork(teamworkRes);
            setDataMental(mentalRes);
            setDataPersonality(personalityRes);
        } catch (error) {
            Alert.alert('오류', error.message);
        } finally {
            setIsLoading(false);
        }
    }

    const onBack = () => {
        navigation.goBack()
    }
    /// set date time

    const onChooseMental = (name: string, index: number) => {
        const selectedItem = dataMental[index];
        setMental(selectedItem);
    }
    const onChoosePersonality = (name: string, index: number) => {
        const selectedItem = dataPersonality[index];
        setPersonality(selectedItem);
    }

    const onChooseTeamwork = (name: string, index: number) => {
        const selectedItem = dataTeamwork[index];
        setTeamwork(selectedItem);
    }

    const renderItemMental = ({ item, index }: any) => {
        return (
            <TouchableOpacity
                key={index}
                style={[styles.listItem, mental?.name === item.name && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                onPress={() => onChooseMental(item.name, index)}>
                <_Text style={[styles.nameListItem, { color: mental?.name === item.name ? colors.mainColor : '#65676B' }]}>
                    {item.name}
                </_Text>
            </TouchableOpacity>
        )
    }
    const renderItemPersonality = ({ item, index }: any) => {
        return (
            <TouchableOpacity
                key={index}
                style={[styles.listItem, personality?.name === item.name && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                onPress={() => onChoosePersonality(item.name, index)}>
                <_Text style={[styles.nameListItem, { color: personality?.name === item.name ? colors.mainColor : '#65676B' }]}>
                    {item.name}
                </_Text>
            </TouchableOpacity>
        )
    }
    const renderItemTeamwork = ({ item, index }: any) => {
        return (
            <TouchableOpacity
                key={index}
                style={[styles.listItem, teamwork?.name === item.name && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                onPress={() => onChooseTeamwork(item.name, index)}>
                <_Text
                    style={[styles.nameListItem, { color: teamwork?.name === item.name ? colors.mainColor : '#65676B' }]}>
                    {item.name}
                </_Text>
            </TouchableOpacity>
        )
    }

    useEffect(() => {
        loadData();
    }, [])

    return (
        <Container
            isBack
            isNext={(mental && personality && teamwork) ? true : false}
            onGoBack={onBack}
            onNext={onNext}
            styles={styles.container}>
            <LoadingIndicator visible={isLoading} />
            <_Text style={styles.title}>나의 게임 스타일은 무엇인가요?</_Text>
            <View style={styles.wrapItem}>
                <_Text style={styles.titleItem}>추천친구에 반영됩니다</_Text>
                <View style={styles.sectionItem}>
                    <View style={styles.infoItem}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '60%' }}>
                            <Image source={Icons.ic_mental_health} />
                            <_Text style={styles.label}>나의 멘탈은</_Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FlatList
                            data={dataMental}
                            numColumns={3}
                            renderItem={renderItemMental}
                            keyExtractor={index => index.toString()}
                        />
                    </View>
                </View>
                <View style={styles.sectionItem}>
                    <View style={styles.infoItem}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '60%' }}>
                            <Image source={Icons.ic_stars} />
                            <_Text style={styles.label}>게임친구에게 바라는 것은</_Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FlatList
                            data={dataPersonality}
                            numColumns={3}
                            renderItem={renderItemPersonality}
                            keyExtractor={index => index.toString()}
                        />
                    </View>
                </View>
                <View style={styles.sectionItem}>
                    <View style={styles.infoItem}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '60%' }}>
                            <Image source={Icons.ic_group} />
                            <_Text style={styles.label}>나의 게임 스타일은</_Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FlatList
                            data={dataTeamwork}
                            numColumns={3}
                            renderItem={renderItemTeamwork}
                            keyExtractor={index => index.toString()}
                        />
                    </View>
                </View>
            </View>
        </Container>
    );
}

const styles = StyleSheet.create({
    container: { paddingHorizontal: 16, justifyContent: 'flex-start', paddingTop: 60 },
    title: {
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        marginBottom: 10
    },
    wrapItem: {
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        alignItems: 'center',
        width: '100%',
    },
    sectionItem: { width: '100%' },
    infoItem: { marginVertical: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' },
    label: { marginLeft: 10 },
    listItem: { borderColor: '#EDEDED', backgroundColor: '#EDEDED', height: screen.heightscreen / 23, width: screen.widthscreen / 4, alignItems: 'center', justifyContent: 'center', marginHorizontal: 5, marginVertical: 5, paddingHorizontal: 10, paddingVertical: 5, borderRadius: 30, borderWidth: 1 },
    nameListItem: { color: '#65676B', textAlign: 'center', fontSize: 12 },
})