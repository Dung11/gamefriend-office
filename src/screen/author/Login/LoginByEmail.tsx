import * as React from 'react';
import { StyleSheet, Alert } from 'react-native';

import { useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import ButtonApp from '../../../components/Button/ButtonApp';
import { colors } from '../../../config/Colors';
import { InputApp } from '../../../components/Input/InputApp';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import { onLogout, setRefreshData } from '../../../utils/UserUtils';
import { connect } from '../../../socketio/socketController';
import { personalProfileRequest } from '../../../redux/personal-profile/actions';
import store from '../../../redux/store';
import _Text from '../../../components/_Text';
import { getUserProfile, loginByEmail } from '../../../api/AuthorServices';

export default function LoginByEmail({ navigation }: StackScreenProps<RootStackParamList>) {


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const onDone = (response: any) => {
        if (Object.keys(response).length === 0) {
            Alert.alert('오류', '사용자 정보를 가져오지 못했습니다.');
            onLogout(navigation);
            return;
        }
        navigation.replace('Main');
    }

    const LoginEmail = async () => {
        try {
            const res = await loginByEmail(email, password);
            if (res.status === 404) {
                Alert.alert('오류', '사용자를 찾을 수 없습니다.');
                return;
            }
            if (res.status === 400) {
                Alert.alert('오류', '잘못된 이메일 또는 비밀번호입니다.');
                return;
            }
            if (res.status === 200) {
                if (res && res.data.isOnline === true) {
                    Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                    return;
                }
                if (res.data.hasOwnProperty('isReset')) {
                    await setRefreshData({
                        resetToken: res.data?.resetToken,
                        userId: res.data?.userId,
                        isReset: res.data?.isReset
                    });
                    if (!res.data.isReset) {
                        const resProfile = await getUserProfile(res.data?.userId);
                        navigation.reset({
                            index: 0, routes: [{
                                name: "Waiting", params: {
                                    profilePicture: resProfile.data.user.profilePicture
                                }
                            }]
                        });
                    } else {
                        navigation.reset({ index: 0, routes: [{ name: "BirthDay" }] });
                    }
                } else {
                    //start socket io
                    connect(res.data.accessToken, navigation)
                    keyValueStorage.save("access-token", res.data.accessToken);
                    keyValueStorage.save("refreshToken", res.data.refreshToken);
                    keyValueStorage.save("userID", res.data.userId);
                    keyValueStorage.save("userName", res.data.userName);
                    keyValueStorage.save("isFirstlogin", JSON.stringify(res.data.isFirstlogin));
                    store.dispatch(
                        personalProfileRequest({
                            id: res.data.userId,
                            callback: onDone,
                        },
                    ));
                }
            } else if (res.status === 423) {
                Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                return;
            } else {
                Alert.alert("잠시 후 다시 시도해주세요")
            }
        } catch (error) {
            console.log("error =>>", error)
            Alert.alert("잠시 후 다시 시도해주세요")
        }
    }

    const onChangeEmail = (text: string) => {
        setEmail(text);
    }

    const onChangePassword = (text: string) => {
        setPassword(text);
    }

    const onBack = () => {
        navigation.goBack()
    }

    const onGoto = (text: any) => () => {
        navigation.navigate(text)
    }
    return (
        <Container isBack onGoBack={onBack} isAuthor>
            <InputApp returnKeyType="next" iconLabel={Icons.ic_email} label="이메일 주소" placeholder=" " value={email} onChangeText={(text) => onChangeEmail(text)} />
            <InputApp iconRight returnKeyType="next" iconLabel={Icons.ic_password} label="비밀번호" placeholder=" " value={password} onChangeText={(text) => onChangePassword(text)} />
            <TouchableOpacity onPress={onGoto('ForgotPassword')}>
                <_Text style={styles.textAction}>계정찾기</_Text>
            </TouchableOpacity>
            <ButtonApp onPress={LoginEmail} title="로그인" backgroundColor={colors.mainColor} />
            <TouchableOpacity onPress={onGoto('Register')}>
                <_Text style={styles.textAction}>Email로 회원가입</_Text>
            </TouchableOpacity>
        </Container>
    );
}

const styles = StyleSheet.create({

    textAction: {
        marginVertical: 10,
        color: colors.mainColor
    }

});
