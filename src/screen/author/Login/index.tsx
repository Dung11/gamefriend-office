import * as React from 'react';
import { StyleSheet, View, StatusBar, Dimensions, Image, ScrollView, SafeAreaView, Alert, Platform } from 'react-native';
import { useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { colors } from '../../../config/Colors';
import { Icons, Images } from '../../../assets';
import KakaoLogins, { KAKAO_AUTH_TYPES } from '@react-native-seoul/kakao-login';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { fbGraphService } from '../../../api/FbGraphService';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { toastPopup } from '../../../components/ToastPopup/toast';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import { connect } from '../../../socketio/socketController';
import { onLogout, removeRefreshData, setRefreshData, getRefreshData } from '../../../utils/UserUtils';
import { removeTopics } from '../../../utils/setupfirebase';
import { HeaderApp } from '../../../components/Header/HeaderApp';
import ButtonSocial from '../../../components/Button/ButtonSocial';
import store from '../../../redux/store';
import { personalProfileRequest } from '../../../redux/personal-profile/actions';
import appleAuth, { AppleButton } from '@invertase/react-native-apple-authentication';
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { useNavigation } from '@react-navigation/native';
import { getUserProfile, loginApple, loginFacebook, loginGoogles, loginKakaotalk } from '../../../api/AuthorServices';

export interface IAppleUser {
    displayName?: string,
    email?: string,
    emailVerified?: boolean,
    isAnonymous?: boolean,
    metadata?: {
        creationTime?: number,
        lastSignInTime?: number,
    },
    phoneNumber?: string,
    photoURL?: string,
    providerData?: object,
    providerId?: string,
    refreshToken?: string,
    uid?: string,
}

export default function LoginScreen({ navigation }: StackScreenProps<RootStackParamList>) {

    const [visible, setVisible] = useState<boolean>(false);
    if (!KakaoLogins) {
        console.error('Module is Not Linked');
    }

    const onDone = (response: any) => {
        if (Object.keys(response).length === 0) {
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
            onLogout(navigation);
            return;
        }
        navigation.replace('Main');
    }

    const logCallback = (log: any, callback: any) => {
        console.log(log);
        callback;
    };

    const resetNotiSubscription = async () => {
        removeTopics();
        let refreshData = await getRefreshData();
        if (refreshData) {
            const userId = refreshData.userId;

            messaging()
                .unsubscribeFromTopic(`gamefriend_${userId}`)
                .then(() => console.log(`Unsubscribed from gamefriend_${userId} topic in Login screen !!`));

        }
    }

    const loginKakaoTalk = async () => {
        setVisible(true);
        resetNotiSubscription();
        await removeRefreshData();
        try {
            const res = await KakaoLogins.login([KAKAO_AUTH_TYPES.Talk, KAKAO_AUTH_TYPES.Account]);
            console.log('KAKAO LOGIN RESPONSE: ', res);
            if (!res || !res?.accessToken) {
                toastPopup.error("An error occurred, please try again!")
                logCallback('Notification', ' An error occurred, please try again!');
                return;
            }

            const info = await KakaoLogins.getProfile();
            if (!info.id) {
                toastPopup.error("An error occurred, please try again!")
                logCallback('Notification', ' An error occurred, please try again!');
                return;
            }

            const resultLogin = await loginKakaotalk(info.id, res.accessToken)
            if (resultLogin.status === 200 && resultLogin.data.hasOwnProperty('isReset')) {
                await setRefreshData({
                    resetToken: resultLogin.data?.resetToken,
                    userId: resultLogin.data?.userId,
                    isReset: resultLogin.data?.isReset
                });
                if (!resultLogin.data.isReset) {
                    const resProfile = await getUserProfile(resultLogin.data?.userId);
                    navigation.reset({
                        index: 0, routes: [{
                            name: "Waiting", params: {
                                profilePicture: resProfile.data.user.profilePicture
                            }
                        }]
                    });
                } else {
                    navigation.reset({ index: 0, routes: [{ name: "BirthDay" }] });
                }
            } else if (resultLogin.status === 200) {
                if (resultLogin && resultLogin.data.isOnline === true) {
                    Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                    return;
                }
                // star socket
                connect(resultLogin.data.accessToken, navigation);
                keyValueStorage.save("userID", resultLogin.data.userId);
                keyValueStorage.save("userName", resultLogin.data.userName);
                keyValueStorage.save("access-token", resultLogin.data.accessToken);
                keyValueStorage.save("refreshToken", resultLogin.data.refreshToken);
                keyValueStorage.save("isFirstlogin", JSON.stringify(resultLogin.data.isFirstlogin));
                store.dispatch(personalProfileRequest({
                    id: resultLogin.data.userId
                    , callback: onDone
                }));
            } else if (resultLogin.status === 404) {
                navigation.navigate('PolicyTerms', {
                    kakaoId: info.id,
                    accessToken: res.accessToken
                });
            } else if (resultLogin.status === 423) {
                Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                return;
            } else {
                console.log('response login with kakao talk', resultLogin);
                Alert.alert('오류9', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log("error ==>>", error)
        } finally {
            setVisible(false);
        }
    };

    const loginFb = async () => {
        try {
            resetNotiSubscription();
            await removeRefreshData();
            const loginPermission = await LoginManager.logInWithPermissions(['public_profile', 'email']);
            if (loginPermission.error) {
                if ((await AccessToken.getCurrentAccessToken()) != null) LoginManager.logOut();
                toastPopup.error(loginPermission.error);
            }

            if (loginPermission.isCancelled) {
                toastPopup.error("Login is cancelled.")
            }

            const accessTokenData = await AccessToken.getCurrentAccessToken();
            if (!accessTokenData) {
                toastPopup.error("Not found data.");
                console.log('Not found data');
                return;
            }

            const { id: openId } = await fbGraphService.getProfile();
            const resultLogin = await loginFacebook(openId, accessTokenData.accessToken);
            if (resultLogin.status === 200 && resultLogin.data.hasOwnProperty('isReset')) {
                await setRefreshData({
                    resetToken: resultLogin.data?.resetToken,
                    userId: resultLogin.data?.userId,
                    isReset: resultLogin.data?.isReset
                });
                if (!resultLogin.data.isReset) {
                    const resProfile = await getUserProfile(resultLogin.data?.userId);
                    navigation.reset({
                        index: 0, routes: [{
                            name: "Waiting", params: {
                                profilePicture: resProfile.data.user.profilePicture
                            }
                        }]
                    });
                } else {
                    navigation.reset({ index: 0, routes: [{ name: "BirthDay" }] });
                }
            } else if (resultLogin.status === 200) {
                if (resultLogin && resultLogin.data.isOnline === true) {
                    Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                    return;
                }
                console.log('resultLogin', resultLogin.data.isFirstlogin);
                // star socket
                connect(resultLogin.data.accessToken, navigation);
                keyValueStorage.save("access-token", resultLogin.data.accessToken);
                keyValueStorage.save("refreshToken", resultLogin.data.refreshToken);
                keyValueStorage.save("userID", resultLogin.data.userId);
                keyValueStorage.save("userName", resultLogin.data.userName);
                keyValueStorage.save("isFirstlogin", JSON.stringify(resultLogin.data.isFirstlogin));
                store.dispatch(personalProfileRequest({
                    id: resultLogin.data.userId
                    , callback: onDone
                }));
            } else if (resultLogin.status === 404) {
                navigation.navigate('PolicyTerms', {
                    facebookId: openId,
                    accessToken: accessTokenData.accessToken
                });
            } else if (resultLogin.status === 423) {
                Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                return;
            } else {
                console.log('response login with kakao talk', resultLogin);
                Alert.alert('오류10', '잠시 후 다시 시도해주세요');
            }
        } catch (e) {
            console.log("error res2>>>", e.response)
        }
    }

    const loginGoogle = async () => {
        try {
            resetNotiSubscription();
            await removeRefreshData();
            GoogleSignin.configure({});
            await GoogleSignin.hasPlayServices();

            const userInfo = await GoogleSignin.signIn();
            if (!userInfo) {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
            };
            try {
                const { accessToken } = await GoogleSignin.getTokens();
                const { user: { id: openId } } = userInfo;
                const resultLogin = await loginGoogles(openId, accessToken)

                if (resultLogin.status === 200 && resultLogin.data.hasOwnProperty('isReset')) {
                    await setRefreshData({
                        resetToken: resultLogin.data?.resetToken,
                        userId: resultLogin.data?.userId,
                        isReset: resultLogin.data?.isReset
                    });
                    if (!resultLogin.data.isReset) {

                        const resProfile = await getUserProfile(resultLogin.data?.userId);
                        navigation.reset({
                            index: 0, routes: [{
                                name: "Waiting", params: {
                                    profilePicture: resProfile.data.user.profilePicture
                                }
                            }]
                        });
                    } else {
                        navigation.reset({ index: 0, routes: [{ name: "BirthDay" }] });
                    }
                } else if (resultLogin.status === 200) {
                    if (resultLogin && resultLogin.data.isOnline === true) {
                        Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                        return;
                    }
                    // star socket

                    connect(resultLogin.data.accessToken, navigation);
                    keyValueStorage.save("access-token", resultLogin.data.accessToken);
                    keyValueStorage.save("refreshToken", resultLogin.data.refreshToken);
                    keyValueStorage.save("userID", resultLogin.data.userId);
                    keyValueStorage.save("userName", resultLogin.data.userName);
                    keyValueStorage.save("isFirstlogin", JSON.stringify(resultLogin.data.isFirstlogin));
                    store.dispatch(personalProfileRequest({
                        id: resultLogin.data.userId
                        , callback: onDone
                    }));
                } else if (resultLogin.status === 404) {
                    navigation.navigate('PolicyTerms', {
                        googleId: openId,
                        accessToken: accessToken
                    });
                } else if (resultLogin.status === 423) {
                    Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                    return;
                } else {
                    console.log('response login with Google', resultLogin);
                    Alert.alert('오류11', '잠시 후 다시 시도해주세요');
                }
            } catch (e) {
                Alert.alert('오류', '로그인 실패, 다시 시도해주세요');

                console.log('===>> Error: ', e)
                GoogleSignin.signOut();
            }
        } catch (e) {
            Alert.alert('오류', '로그인 실패, 다시 시도해주세요');

            console.log('===>> Error: ', e)
            /// navigation to screen create info user

            if (e.code !== statusCodes.SIGN_IN_CANCELLED) {
                console.log("err>>>>")
            }
        }
    }

    return (
        <SafeAreaView style={styles.container} >
            <StatusBar barStyle="dark-content" backgroundColor={colors.white} />
            <LoadingIndicator visible={visible} />
            <HeaderApp title="" styleContainer={styles.backgroundHeader} styleTitle={styles.titleHeader} />
            <ScrollView style={styles.scrollView}>
                <View style={styles.body}>
                    <Image
                        style={styles.logo}
                        source={Images.logo}
                    />
                    <ButtonSocial
                        urlIcon={Icons.sc_kakaotalks}
                        iconLeft
                        borderRadius={8}
                        onPress={loginKakaoTalk}
                        title="카카오로 시작"
                        backgroundColor="#FEE500"
                        titleBlack />
                    <ButtonSocial
                        urlIcon={Icons.sc_facebook}
                        iconLeft
                        borderRadius={8}
                        onPress={loginFb}
                        title="페이스북으로 시작"
                        backgroundColor="#35528B" />
                    <ButtonSocial
                        urlIcon={Icons.sc_google}
                        iconLeft
                        borderRadius={8}
                        onPress={loginGoogle}
                        title="구글로 시작"
                        backgroundColor="#FFFFFF"
                        titleBlack style={{ borderColor: colors.black, borderWidth: 1 }} />
                    <AppleSignInView />
                    <Image style={styles.or} source={Images.login_or} />
                    <ButtonSocial borderRadius={8} onPress={() => { navigation.navigate('LoginByEmail') }} title="이메일로 시작" backgroundColor={colors.black} iconRight />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const AppleSignInView = () => {
    // if (Platform.OS == 'ios' || (Platform.OS == 'android' && appleAuthAndroid.isSupported)) {
    if (Platform.OS == 'ios') {
        const navigation: any = useNavigation();
        const onAppleButtonPress = async () => {
            let idToken: any = '', nonce: any = '';
            const appleAuthRequestResponse = await appleAuth.performRequest({
                requestedOperation: appleAuth.Operation.LOGIN,
                requestedScopes: [
                    appleAuth.Scope.EMAIL,
                    appleAuth.Scope.FULL_NAME,
                ],
            }).catch(error => {
                console.log('PERFORM REQUEST ERROR: ', error);
                return;
            });
            if (!appleAuthRequestResponse) {
                return;
            }
            idToken = appleAuthRequestResponse.identityToken;
            nonce = appleAuthRequestResponse.nonce
            if (!idToken || !nonce) {
                Alert.alert('오류', '로그인 실패, 다시 시도해주세요');
                return;
            }
            const appleCredential = auth.AppleAuthProvider.credential(idToken, nonce);
            auth().signInWithCredential(appleCredential).then(async appleResponse => {
                const appleUser = appleResponse.user as IAppleUser;
                if (!appleUser?.uid || !appleUser?.email) {
                    Alert.alert('오류', '로그인 실패, 다시 시도해주세요');
                    return;
                }
                const appleLoginResponse = await loginApple(appleAuthRequestResponse.user, appleUser?.uid)
                if (appleLoginResponse?.status === 200) {
                    if (appleLoginResponse.data.hasOwnProperty('isReset')) {
                        await setRefreshData({
                            resetToken: appleLoginResponse.data?.resetToken,
                            userId: appleLoginResponse.data?.userId,
                            isReset: appleLoginResponse.data?.isReset
                        });
                        if (!appleLoginResponse.data.isReset) {
                            const resProfile = await getUserProfile(appleLoginResponse.data?.userId);
                            navigation.reset({
                                index: 0, routes: [{
                                    name: "Waiting", params: {
                                        profilePicture: resProfile.data.user.profilePicture
                                    }
                                }]
                            });
                        } else {
                            navigation.reset({ index: 0, routes: [{ name: "BirthDay" }] });
                        }
                    } else {
                        if (appleLoginResponse && appleLoginResponse.data.isOnline === true) {
                            Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                            return;
                        }
                        // star socket
                        connect(appleLoginResponse.data.accessToken, navigation);
                        keyValueStorage.save("access-token", appleLoginResponse.data.accessToken);
                        keyValueStorage.save("refreshToken", appleLoginResponse.data.refreshToken);
                        keyValueStorage.save("userID", appleLoginResponse.data.userId);
                        keyValueStorage.save("userName", appleLoginResponse.data.userName);
                        keyValueStorage.save("isFirstlogin", JSON.stringify(appleLoginResponse.data.isFirstlogin));
                        store.dispatch(personalProfileRequest({
                            id: appleLoginResponse.data.userId,
                            callback: () => {
                                if (Object.keys(appleLoginResponse).length === 0) {
                                    Alert.alert('오류', '잠시 후 다시 시도해주세요');
                                    onLogout(navigation);
                                    return;
                                }
                                navigation.replace('Main');
                            },
                        }));
                    }
                } else {
                    switch (appleLoginResponse.status) {
                        case 404:
                            navigation.navigate('PolicyTerms', {
                                appleId: appleAuthRequestResponse.user,
                                firebaseId: appleUser.uid,
                                accessToken: idToken
                            });
                            break;
                        case 423:
                            Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                            break;
                        default:
                            Alert.alert('오류12', '잠시 후 다시 시도해주세요');
                            break;
                    }
                }
            }).catch(error => {
                Alert.alert('오류', '로그인 실패, 다시 시도해주세요');
                console.log('APPLE SIGN IN CATCH: ', error);
            });
        }

        return (
            <AppleButton
                buttonStyle={AppleButton.Style.BLACK}
                buttonType={AppleButton.Type.SIGN_IN}
                style={{
                    marginVertical: 5,
                    height: 50,
                    width: '100%',
                }}
                onPress={onAppleButtonPress} />
        )
    }
    return null;
}

const screen = Dimensions.get('window');
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: 'white' },
    backgroundHeader: { backgroundColor: colors.white },
    titleHeader: { color: '#65676B' },
    scrollView: { flex: 1 },
    body: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 60
    },
    logo: {
        alignSelf: 'center',
        resizeMode: 'contain',
        marginVertical: 30
    },
    text: {
        color: 'gray'
    },
    title: {
        fontWeight: '900',
        marginTop: 20,
        fontSize: 16
    },
    or: {
        marginVertical: 7
    }

});
