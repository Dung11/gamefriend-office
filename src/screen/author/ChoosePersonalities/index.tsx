import * as React from 'react';

import { useEffect, useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { StyleSheet, View, TouchableOpacity, Alert, Platform } from 'react-native';
import { colors } from '../../../config/Colors';
import { RouteProp } from '@react-navigation/native';
import { toastPopup } from '../../../components/ToastPopup/toast';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { getRefreshData, setRefreshData } from '../../../utils/UserUtils';
import _Text from '../../../components/_Text';
import { getPersonality, resgiterByApple, resgiterByEmail, resgiterByFacebook, resgiterByGoogle, resgiterByKakao, updateProfile } from '../../../api/AuthorServices';
import { uploadImages } from '../../../api/ProfileServices';
const RNFS = require('react-native-fs');

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        profilePicture: string,
        pictures: any,
        gamePictures: any,
        userName: string
        favoriteGameId: any,
        gameIds: any,
        playTime: string,
        character: any,
        kakaoId?: string,
        facebookId?: string,
        googleId?: string,
        accessToken?: string
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}

export default function ChoosePersonalitiesScreen({
    navigation, route
}: Props) {

    const [personality, setPersonality] = useState<any[]>([]);
    const [listPersonality, setListpersonality] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [numberCheck, setNumberCheck] = useState(0)

    let param: any = {
        ...route?.params,
        personalities: personality
    };
    // handle next
    const onNext = async () => {
        setIsLoading(true);
        if (Platform.OS === 'ios') {
            if (param.profilePicture) {
                let photoPATH = `assets-library://asset/asset.JPG?id=${param.profilePicture.substring(5)}&ext=JPG`;
                const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                param.profilePicture = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
            }
            if (param.pictures && param.pictures instanceof Array) {
                for (let i = 0; i < param.pictures.length; i++) {
                    let photoPATH = `assets-library://asset/asset.JPG?id=${param.pictures[i].substring(5)}&ext=JPG`;
                    const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                    param.pictures[i] = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
                }
            }
            if (param.gamePictures && param.gamePictures instanceof Array) {
                for (let i = 0; i < param.gamePictures.length; i++) {
                    let photoPATH = `assets-library://asset/asset.JPG?id=${param.gamePictures[i].substring(5)}&ext=JPG`;
                    const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                    param.gamePictures[i] = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
                }
            }
        }
        Promise.all([
            uploadPic(param.profilePicture),
            upLoadMulPic(param.pictures, 'pictures'),
            upLoadMulPic(param.gamePictures, 'gamePictures')
        ]).then(async (responseAll: any) => {
            param.pictures.push(param.profilePicture);
            let refreshData = await getRefreshData();
            if (refreshData) {
                const response = await updateProfile(param, refreshData.userId, refreshData.resetToken);
            
                if (response && response.isFailed) {
                    Alert.alert('오류1', '잠시 후 다시 시도해주세요');
                    return;
                }
                refreshData.isReset = false;
                await setRefreshData(refreshData);
            } else if (param.hasOwnProperty('kakaoId') && param.kakaoId !== null) {
                const response = await resgiterByKakao(param);
                if (response && response.isFailed) {
                    Alert.alert('오류2', '잠시 후 다시 시도해주세요');
                    return;
                }
                if (response.data?.userId && response.data?.resetToken) {
                    setRefreshData({
                        resetToken: response.data?.resetToken,
                        userId: response.data?.userId,
                        isReset: false
                    });
                }
            } else if (param.hasOwnProperty('facebookId') && param.facebookId !== null) {
                const response = await resgiterByFacebook(param);
                if (response && response.isFailed) {
                    Alert.alert('오류3', '잠시 후 다시 시도해주세요');
                    return;
                }
                if (response.data?.userId && response.data?.resetToken) {
                    setRefreshData({
                        resetToken: response.data?.resetToken,
                        userId: response.data?.userId,
                        isReset: false
                    });
                }
            } else if (param.hasOwnProperty('googleId') && param.googleId !== null) {
                const response = await resgiterByGoogle(param);
                if (response && response.isFailed) {
                    Alert.alert('오류4', '잠시 후 다시 시도해주세요');
                    return;
                }
                if (response.data?.userId && response.data?.resetToken) {
                    setRefreshData({
                        resetToken: response.data?.resetToken,
                        userId: response.data?.userId,
                        isReset: false
                    });
                }
            } else if (param.hasOwnProperty('appleId') && param.appleId !== null) {
                const response = await resgiterByApple(param);
                if (response && response.isFailed) {
                    Alert.alert('오류5', '잠시 후 다시 시도해주세요');
                    return;
                }
                if (response.data?.userId && response.data?.resetToken) {
                    setRefreshData({
                        resetToken: response.data?.resetToken,
                        userId: response.data?.userId,
                        isReset: false
                    });
                }
            } else {
                const response = await resgiterByEmail(param);
                if (response && response.isFailed) {
                    Alert.alert('오류6', '잠시 후 다시 시도해주세요');
                    return;
                }
                if (response.data?.userId && response.data?.resetToken) {
                    setRefreshData({
                        resetToken: response.data?.resetToken,
                        userId: response.data?.userId,
                        isReset: false
                    });
                }
            }
            navigation.navigate('Waiting', param);
        }).catch((error: Error) => {
            console.log('error', error.message);
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        }).finally(() => {
            setIsLoading(false);
        });
    }

    const onBack = () => {
        navigation.goBack()
    }

    const uploadPic = (uri: string) => {
        return new Promise((resolve, reject) => {
            let formdata = new FormData();
            const ext = getImageExstension(uri);
            const filename = `${Date.now()}.${ext}`;
            let file = {
                uri,
                type: 'image/jpeg',
                name: filename
            };
            formdata.append('photos', file);
            uploadImages(formdata).then((response: any) => {
                if (response && response.data?.locations) {
                    let data = response.data;
                    param.profilePicture = data.locations.length > 0 ?
                        data.locations[0] : '';
                }
                resolve(true);
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                console.log('오류', error.message);
                reject(false);
            })
        })
    }

    const upLoadMulPic = (uriArr: string[], type: 'gamePictures' | 'pictures') => {
        return new Promise((resolve, reject) => {
            let formdata = new FormData();
            if (type === 'pictures' && uriArr.length > 1) {
                uriArr = uriArr.splice(1);
            } else if (type === 'pictures' && uriArr.length === 1) {
                param.pictures = [];
                resolve(true);
                return;
            }
            for (let i = 0; i < uriArr.length; i++) {
                let uri = uriArr[i];
                const ext = getImageExstension(uri);
                const filename = `${Date.now()}.${ext}`;
                let file = {
                    uri,
                    type: 'image/jpeg',
                    name: filename
                };
                formdata.append('photos', file);
            }
            uploadImages(formdata).then((response: any) => {
                if (response && response.data?.locations) {
                    let data = response.data
                    if (type === 'gamePictures') {
                        param.gamePictures = data.locations;
                    } else {
                        param.pictures = data.locations;
                    }
                }
                resolve(true);
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                reject(false);
            });
        });
    }

    const getImageExstension = (uri: string) => {
        if (uri) {
            const splitNameArr = uri.split('/');
            const filename = splitNameArr[splitNameArr.length - 1];
            const splitFilenameArr = filename.split('.');
            return splitFilenameArr[splitFilenameArr.length - 1];
        }
        return '';
    }

    // handle Event click
    const onChoose = (name: string, index: number) => {
        let countNumberCheck = numberCheck
        const selectedItem = listPersonality[index];
        let tempData = [...listPersonality];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };
        if (selectedItem.isCheck) {
            countNumberCheck--
            setNumberCheck(countNumberCheck)

            let tmpFilter = [...personality];
            tmpFilter = tmpFilter.filter((d) => d !== selectedItem.name);
            setPersonality(tmpFilter);
        } else {
            countNumberCheck++
            if (countNumberCheck > 4) {
                Alert.alert('오류', `${numberCheck}개까지만 선택 가능합니다`)
                return
            }
            setNumberCheck(countNumberCheck)
            let tmpPersonality = [...personality];
            tmpPersonality.push(name);
            setPersonality(tmpPersonality);
        }
        setListpersonality(tempData);

    }

    //get list data
    const getListPersonality = async () => {
        try {
            const result = await getPersonality()
            if (result.status === 200) {
                setIsLoading(false);
                const dataResult = result.data.personalities;
                const tempData = dataResult.map((nitem: any, nindex: any) => {
                    return {
                        name: nitem,
                        id: nindex
                    }
                })
                setListpersonality(tempData)
            }
        } catch (error) {
            console.log(error)
            setIsLoading(false);
            toastPopup.error("The system is under maintenance, please try again later.")
        } finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        getListPersonality();
    }, [])

    return (
        <Container isBack isNext={personality.length > 0 ? true : false} onGoBack={onBack} onNext={onNext} styles={styles.container}>
            <LoadingIndicator visible={isLoading} />
            <View style={styles.wrapTitle}>
                <_Text style={styles.title}>성격을 선택해주세요</_Text>
                <_Text style={styles.title}>(최대 4가지 선택 가능)</_Text>
            </View>
            <View style={styles.wrapItem}>
                <_Text style={styles.titleItem}>추천친구에 반영됩니다</_Text>
                <View style={styles.sectionItem}>
                    <View style={{ flexWrap: "wrap", flexDirection: 'row', alignItems: 'center' }}>
                        {
                            listPersonality.map((item: any, index: any) =>
                            (
                                <TouchableOpacity key={index}
                                    style={[styles.listItem, item.isCheck && { borderColor: colors.mainColor, backgroundColor: colors.white }]}
                                    onPress={() => onChoose(item.name, index)}>
                                    <_Text style={[styles.nameListItem, { color: item.isCheck ? colors.mainColor : '#65676B' }]}>
                                        {item.name}
                                    </_Text>
                                </TouchableOpacity>
                            ))
                        }
                    </View>
                </View>
            </View>
        </Container>
    );
}

const styles = StyleSheet.create({
    container: { paddingHorizontal: 16, justifyContent: 'flex-start', paddingTop: 60 },
    wrapTitle:{
        width: '100%',
        alignItems: 'center',
        marginBottom: 20,
    },
    title: {
        color: colors.mainColor,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        marginBottom: 10
    },
    wrapItem: {
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        alignItems: 'center',
        width: '100%',
    },
    sectionItem: {
        marginHorizontal: 10
    },
    listItem: {
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        alignItems: 'center',
        borderRadius: 30,
        borderWidth: 1,
        borderColor: '#EDEDED',
        backgroundColor: '#EDEDED'
    },
    nameListItem: {
        color: colors.mainColor
    }
})