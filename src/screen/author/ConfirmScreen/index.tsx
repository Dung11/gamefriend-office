import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp, StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { View, StyleSheet, ImageBackground, StatusBar, SafeAreaView, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SplashScreen from 'react-native-splash-screen';
import { RootStackParamList } from '../../../@types';
import { Icons, Images } from '../../../assets';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        profilePicture: string,
        pictures: any,
        gamePictures: any,
        userName: string
        favoriteGameId: any,
        gameIds: any,
        playTime: string,
        character: any,
        personality: any
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
const ConfirmScreen = ({
    navigation, route
}: Props) => {
    const onReset = () => {
        navigation.replace('Main')
    }

    return (
        <>
            <StatusBar barStyle="dark-content" backgroundColor="white" />
            <View style={styles.container}>
                <Image resizeMode="contain" source={Images.confirm_logo} />
                <_Text style={styles.title}>게임친구</_Text>
                <Image resizeMode="contain" style={{ width: '100%', height: screen.widthscreen / 3, borderRadius: 100, marginVertical: 15 }} source={Images.confirm_frame} />
                <_Text style={styles.title}>환염합니다</_Text>
                <_Text style={styles.subTitle}>가입이 완료되었습니다</_Text>

                <TouchableOpacity onPress={onReset}>
                    <ImageBackground resizeMode="contain" style={styles.wrapBTN} source={Images.confirm_btn}>
                        <Image source={Images.confirm_vector} />
                        <_Text style={styles.titleBTN}>프로필 재설정하기</_Text>
                    </ImageBackground>
                </TouchableOpacity>
            </View>
        </>
    )
};

export default ConfirmScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    title: {
        fontWeight: '900',
        fontSize: 26,
        marginVertical: 10
    },

    subTitle: {
        color: '#989B9D',
        marginBottom: 30
    },
    wrapBTN: { width: screen.widthscreen, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', paddingVertical: 5 },
    titleBTN: {
        color: colors.white,
        marginLeft: 10
    }
});
