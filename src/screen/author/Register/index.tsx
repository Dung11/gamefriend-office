import * as React from 'react';

import { useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

import { RootStackParamList } from '../../../@types';
import { InputApp } from '../../../components/Input/InputApp';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import Utils from '../../../utils/Utils';
import { Alert, StyleSheet } from 'react-native';
import { removeRefreshData, getRefreshData } from '../../../utils/UserUtils';
import messaging from '@react-native-firebase/messaging';
import { checkEmailExist } from '../../../api/AuthorServices';

export default function RegisterScreen({
    navigation,
}: StackScreenProps<RootStackParamList>) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    const [cPassword, setCPassword] = useState('')
    const [errorEmail, setErrorEmail] = useState('')
    const [errorPassword, setErrorPassword] = useState('')
    const [errorCPassword, setErrorCPassword] = useState('')
    const [successEmail, setSuccessEmail] = useState(false);
    const [successPass, setSuccessPass] = useState(false);
    const [successCPass, setSuccessCPass] = useState(false);

    const resetNotiSubscription = async () => {
        let refreshData = await getRefreshData();
        if (refreshData) {
            const userId = refreshData.userId;
            // refreshData.resetToken

            messaging()
                .unsubscribeFromTopic(`gamefriend_${userId}`)
                .then(() => console.log(`Unsubscribed from gamefriend_${userId} topic in Register screen !!`));
        }
    }

    const confirmPassword = async () => {
        if (successEmail && successPass && successCPass) {
            resetNotiSubscription();
            await removeRefreshData();
            let params: any = {
                email: email,
                password: password,
                confirmPassword: cPassword
            }
            navigation.navigate("PhoneNumber", params);
        }
    }

    const onChangeEmail = (value: string) => {
        setEmail(value);
        const checkMail = Utils.validateEmail(value)
        if (!checkMail) {
            setSuccessEmail(false)
            setErrorEmail('이메일을 확인해주세요')
        } else {
            setErrorEmail('');
            setSuccessEmail(true);
        }
    }

    const onSubmitEmail = async () => {
        const checkMail = Utils.validateEmail(email);
        if (checkMail) {
            const response = await checkEmailExist(email);
            if (response) {
                if (response.isFailed) {
                    Alert.alert('오류', response.errors);
                    return;
                }
                if (response.data.isExist) {
                    setSuccessEmail(false);
                    setErrorEmail('해당 이메일이 이미 존재합니다.');
                } else {
                    setErrorEmail('');
                    setSuccessEmail(true);
                }
            } else {
                Alert.alert('오류16', '잠시 후 다시 시도해주세요');
            }
        }
    }

    const checkMatchPassword = () => {
        if (password !== '' && cPassword !== '') {
            if (password !== cPassword) {
                setSuccessCPass(false)
                setErrorCPassword('비밀번호가 일치하지 않습니다.')
                return false;
            } else {
                setSuccessCPass(true)
                setErrorCPassword('')
            }
        }
    }

    const onChangePassword = (value: string) => {
        setPassword(value);
        const checkPass = Utils.validatePassword(value)
        if (checkPass) {
            setErrorPassword('')
            setSuccessPass(true)
        } else {
            setSuccessPass(false)
            setErrorPassword('비밀번호를 확인해 주세요 (영문과 숫자 8자 이상)')
        }
    }

    const onChangeCPassword = (value: string) => {
        setCPassword(value);
    }

    const onBack = () => {
        navigation.goBack()
    }

    React.useEffect(() => {
        checkMatchPassword();
    }, [password, cPassword])

    return (
        <Container isAuthor onGoBack={onBack} isBack>
            <InputApp
                isError
                errorMessage={errorEmail}
                returnKeyType="next"
                iconLabel={Icons.ic_email}
                label="이메일 주소"
                placeholder="이메일 주소를 입력해주세요..."
                value={email}
                onChangeText={(value) => onChangeEmail(value)}
                onSubmitEditing={onSubmitEmail}
                onBlur={onSubmitEmail}
            />
            <InputApp
                isError
                errorMessage={errorPassword}
                iconRight
                returnKeyType="next"
                iconLabel={Icons.ic_password}
                label="비밀번호"
                placeholder="영어+숫자 8자리 이상이 필요합니다..."
                value={password}
                onChangeText={(value) => onChangePassword(value)}
            />
            <InputApp
                isError
                errorMessage={errorCPassword}
                iconRight
                returnKeyType="next"
                iconLabel={Icons.ic_password}
                label="비밀번호확인"
                placeholder="영어+숫자 8자리 이상이 필요합니다..."
                value={cPassword}
                onChangeText={(value) => onChangeCPassword(value)}
            />
            {successEmail && successPass && successCPass ? <ButtonApp style={{ marginTop: 20 }} onPress={confirmPassword} title="회원가입" backgroundColor={colors.mainColor} /> : null}
        </Container>
    );
}

const styles = StyleSheet.create({

})