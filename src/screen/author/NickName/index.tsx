import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { RouteProp } from '@react-navigation/native';
import { Alert, Image, StyleSheet, View } from 'react-native';
import { colors } from '../../../config/Colors';
import { InputApp } from '../../../components/Input/InputApp';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
import { checkNickname } from '../../../api/AuthorServices';
type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        profilePicture: string,
        pictures: any,
        gamePictures: any
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}

export default function NickNameScreen({
    navigation, route
}: Props) {

    let checkWaiting: any;

    const [nickName, setNickName] = useState('');
    const [isSubmit, setIsSubmit] = useState(false);
    const [isValid, setIsValid] = useState(false);
    const [isLoadingCheck, setIsLoadingCheck] = useState(false);

    const onNext = () => {
        const param: any = {
            ...route?.params,
            userName: nickName
        };
        navigation.navigate('ChooseGame', param);
    }

    const onBack = () => {
        navigation.goBack()
    }

    const onChange = (value: string) => {
        if (checkWaiting) {
            clearTimeout(checkWaiting);
        }
        setNickName(value)
        setIsLoadingCheck(true)
        checkWaiting = setTimeout(() => {
            onSubmitEdit(value)
        }, 500);
    }

    const onSubmitEdit = (value: string) => {
        let nickName = value;
        if (nickName.length >= 2 && nickName.length <= 8) {
            checkNickname(nickName).then((response: any) => {
                if (response.status === 200) {
                    if (response.data.isExist === true) {
                        setIsSubmit(true);
                        setIsValid(false);
                        setIsLoadingCheck(false)
                    } else {
                        setIsSubmit(true);
                        setIsValid(true);
                        setIsLoadingCheck(false)
                    }
                } else {
                    Alert.alert('오류13', '잠시 후 다시 시도해주세요');
                    setIsLoadingCheck(false)
                }
            });
        } else {
            setIsSubmit(false);
            setIsValid(false);
            setIsLoadingCheck(false);
        }
    }

    return (
        <Container isAuthor isBack isNext={isValid ? true : false} onGoBack={onBack} onNext={onNext}>
            <_Text style={styles.title}>닉네임</_Text>
            <InputApp checkNickName={isLoadingCheck} iconLabel={Icons.ic_rename}
                label="닉네임을 설정해주세요"
                placeholder="닉네임을 입력해주세요"
                value={nickName}
                onChangeText={(value) => onChange(value)}
            />
            {
                (isSubmit === true && isValid === true) &&
                <View style={styles.textContainer}>
                    <Image source={Icons.ic_sign_success} />
                    <_Text style={styles.successText}>사용가능한 닉네임입니다</_Text>
                </View>
            }
            {
                isValid === false &&
                <View style={styles.textContainer}>
                    <Image source={Icons.ic_sign_error} />
                    <_Text style={styles.errorText}>해당 닉네임은 이미 사용중입니다!</_Text>
                </View>
            }
            {
                (isSubmit === false && nickName.length > 8) &&
                <View style={styles.textContainer}>
                    <Image source={Icons.ic_sign_error} />
                    <_Text style={styles.errorText}>닉네임은 8자 미만입니다.</_Text>
                </View>
            }
            {
                (isSubmit === false && nickName.length < 2) &&
                <View style={styles.textContainer}>
                    <Image source={Icons.ic_sign_error} />
                    <_Text style={styles.errorText}>별명에 2 자 이상이 포함되어 있습니다.</_Text>
                </View>
            }
        </Container>
    );
}

const styles = StyleSheet.create({
    title: {
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        marginBottom: 10
    },
    textContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 20
    },
    successText: {
        color: '#00C637',
        fontWeight: '400',
        fontSize: 10,
        paddingLeft: 10,
    },
    errorText: {
        color: '#E35C5C',
        fontWeight: '400',
        fontSize: 10,
        paddingLeft: 10,
    }
})