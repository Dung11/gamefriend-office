import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../../../@types';
import { InputApp } from '../../../components/Input/InputApp';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import Utils from '../../../utils/Utils';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { checkEmailExist, verifyEmail } from '../../../api/AuthorServices';
import { Alert } from 'react-native';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}
export default function EmailInputScreen({ navigation }: Props) {

    const [state, setState] = useState({
        emailValue: '',
        isValidEmail: false,
        errorMesage: '',
        isNext: false,
        isLoading: false
    });

    const onNext = () => {
        if (state.isValidEmail) {
            let params: any = {
                type: 'email',
                email: state.emailValue
            };
            navigation.navigate('VerifiedCodeForgotPassword', params);
        }
    }

    const onChangeEmail = (value: any) => {
        let checkValidEmail = Utils.validateEmail(value);
        if (!checkValidEmail) {
            setState({
                ...state,
                emailValue: value,
                errorMesage: '이메일을 다시 확인하세요',
                isValidEmail: false,
                isNext: false
            });
        } else {
            setState({
                ...state,
                emailValue: value,
                errorMesage: '',
                isValidEmail: true,
                isNext: false
            });
        }
    }

    const onSubmitPhoneNumber = async () => {
        try {
            setState({
                ...state,
                isLoading: true
            });

            let response = await checkEmailExist(state.emailValue);
            if (!response) {
                Alert.alert('오류99', '잠시 후 다시 시도해주세요');
            }

            if (response.isFailed) {
                setState({
                    ...state,
                    errorMesage: response.error[0],
                });
                return;
            }

            if (response.data.isExist) {
                let verifyEmailRes = await verifyEmail(state.emailValue);
                if (verifyEmailRes?.status === 200) {
                    console.log('verifyEmailRes: ', verifyEmailRes);
                    setState({
                        ...state,
                        isNext: true,
                        errorMesage: '',
                        isLoading: false
                    });

                    const params: any = {
                        type: 'email',
                        email: state.emailValue
                    };
                    navigation.navigate('VerifiedCodeForgotPassword', params);
                } else {
                    setState({
                        ...state,
                        isNext: false,
                        errorMesage: '잠시 후 다시 시도해주세요',
                        isLoading: false
                    });
                }
            } else {
                setState({
                    ...state,
                    isNext: false,
                    errorMesage: '이메일이 없습니다.',
                    isLoading: false
                });
            }

        } catch (error) {
            Alert.alert('오류100', '잠시 후 다시 시도해주세요');
            setState({
                ...state,
                isLoading: false
            });
        }
    }

    const onBack = () => {
        navigation.goBack()
    }

    return (
        <Container isAuthor isBack isNext={state.isNext} onGoBack={onBack} onNext={onNext}>
            <LoadingIndicator visible={state.isLoading} />
            <InputApp
                isError
                errorMessage={state.errorMesage}
                returnKeyType="next"
                iconLabel={Icons.ic_phone}
                label="가입 이메일"
                value={state.emailValue}
                onChangeText={onChangeEmail}
            />
            <ButtonApp
                onPress={onSubmitPhoneNumber}
                title="보내기"
                backgroundColor={colors.mainColor}
                disabled={!state.isValidEmail}
            />
        </Container>
    );
}

