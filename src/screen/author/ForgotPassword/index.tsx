import * as React from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

import { StackScreenProps } from '@react-navigation/stack';

import { RootStackParamList } from '../../../@types';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';

export default function ForgotPasswordScreen({
    navigation,
}: StackScreenProps<RootStackParamList>) {

    const onGoto = () => {
        navigation.navigate("PhoneNumberForgotPassword")
    }

    const onBack = () => {
        navigation.goBack()
    }

    const onGoToEmailInput = () => {
        navigation.navigate('EmailInput');
    }

    return (
        <Container isAuthor isBack onGoBack={onBack}>
            <View style={styles.wrapTitle}>
                <Image source={Icons.ic_user_contact} />
                <_Text style={styles.title}>휴대폰 인증이 필요합니다</_Text>
            </View>
            <ButtonApp
                style={{ marginTop: 20 }}
                onPress={onGoto}
                title="휴대폰 인증"
                backgroundColor={colors.mainColor}
            />
            <TouchableOpacity style={styles.btnNote} onPress={onGoToEmailInput}>
                <_Text style={styles.note}>휴대전화 번호 없이 가입했나요?</_Text>
            </TouchableOpacity>
        </Container>
    );
}
const styles = StyleSheet.create({
    wrapTitle: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%'
    },
    title: {
        marginLeft: 10,
        fontWeight: '900'
    },
    btnNote: {
        marginTop: 15
    },
    note: {
        color: '#FB6F6E',
        fontWeight: '800',
        fontSize: 14
    }
})
