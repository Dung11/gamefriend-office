import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../../../@types';
import { InputApp } from '../../../components/Input/InputApp';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import setupfirebase from '../../../utils/setupfirebase';
import Utils from '../../../utils/Utils';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { Alert } from 'react-native';
import { checkPhoneNumberExist } from '../../../api/AuthorServices';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}
export default function PhoneNumberForgotPasswordScreen({ navigation }: Props) {

    const [state, setState] = useState({
        phoneNumberValue: '',
        isValidPhoneNumber: false,
        errorMesage: '',
        isNext: false,
        isLoading: false,
        confirmation: null,
        verificationId: null
    });

    const { auth } = setupfirebase();

    const onGoto = () => {
        let params: any = {
            confirmation: state.confirmation,
            verificationId: state.verificationId,
            type: 'phone'
        };
        navigation.navigate('VerifiedCodeForgotPassword', params);
    }

    const onChangePhone = (value: any) => {
        let checkValidPhoneNumber = Utils.validatePhone(value);
        if (!checkValidPhoneNumber) {
            setState({
                ...state,
                phoneNumberValue: value,
                errorMesage: '핸드폰 번호를 확인해주세요',
                isValidPhoneNumber: false,
                isNext: false
            });
        } else {
            setState({
                ...state,
                phoneNumberValue: value,
                errorMesage: '',
                isValidPhoneNumber: true,
                isNext: false
            });
        }
    }

    const onSubmitPhoneNumber = async () => {
        let phoneNumber = state.phoneNumberValue;
        let phone;
        if (phoneNumber.charAt(0) == '0') {
            phone = '82' + phoneNumber.slice(1)
            setState({
                ...state,
                isLoading: true
            });
            const response = await checkPhoneNumberExist(phone);
            if (!response) {
                setState({
                    ...state,
                    isLoading: false,
                    errorMesage: '잠시 후 다시 시도해주세요'
                });
            }
            if (response.isFailed) {
                Alert.alert('오류', response.errors[0]);
                return;
            }
            if (response.data.isExist) {
                signInWithPhoneNumber(phone);
            } else {
                setState({
                    ...state,
                    isValidPhoneNumber: false,
                    errorMesage: '전화가 존재하지 않습니다.'
                });
            }
        }
        else {
            setState({
                ...state,
                errorMesage: '잘못된 휴대폰 번호입니다.'
            });
        }
    }

    async function signInWithPhoneNumber(phoneNumber: any) {
        try {
            const confirmation = await auth().signInWithPhoneNumber(`+${phoneNumber}`) as any;
            if (confirmation._auth._authResult) {
                setState({
                    ...state,
                    isValidPhoneNumber: true,
                    errorMesage: '',
                    isNext: true,
                    isLoading: false,
                    confirmation: confirmation,
                    verificationId: confirmation._verificationId,
                });
                const params: any = {
                    confirmation: confirmation,
                    verificationId: confirmation._verificationId,
                    type: 'phone'
                }
                navigation.navigate("VerifiedCodeForgotPassword", params);
            } else {
                setState({
                    ...state,
                    errorMesage: '잘못된 휴대전화입니다.',
                    isLoading: false
                });
            }
        } catch (error) {
            setState({
                ...state,
                errorMesage: error,
                isLoading: false
            });
        }
    }

    const onBack = () => {
        navigation.goBack()
    }

    return (
        <Container isAuthor isBack isNext={state.isNext} onGoBack={onBack} onNext={onGoto}>
            <LoadingIndicator visible={state.isLoading} />
            <InputApp
                isError
                errorMessage={state.errorMesage}
                keyboardType="numeric"
                returnKeyType="next"
                iconLabel={Icons.ic_phone}
                label="휴대폰 번호"
                placeholder="휴대폰 번호를 입력해주세요..."
                value={state.phoneNumberValue}
                onChangeText={onChangePhone}
            />
            <ButtonApp
                onPress={onSubmitPhoneNumber}
                title="보내기"
                backgroundColor={colors.mainColor}
                disabled={!state.isValidPhoneNumber}
            />
        </Container>
    );
}

