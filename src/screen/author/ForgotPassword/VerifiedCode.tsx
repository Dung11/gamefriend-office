import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../../../@types';
import { InputApp } from '../../../components/Input/InputApp';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import setupfirebase from '../../../utils/setupfirebase';
import { RouteProp } from '@react-navigation/native';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { Alert } from 'react-native';
import { phoneNumber, verifyEmailOtp } from '../../../api/AuthorServices';

type Params = {
    params: {
        confirmation: {
            confirm: (value: string) => void
        },
        type: 'phone' | 'email',
        email?: string,
    }
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default function VerifiedCodeForgotPasswordScreen({
    navigation, route
}: Props) {

    const { auth } = setupfirebase();

    const [state, setState] = useState({
        code: '',
        isLoading: false,
        isNext: false,
        errorMesage: '',
        userId: '',
        accessToken: '',
        userEmail: ''
    });

    const onChangeCode = (text: string) => {
        setState({
            ...state,
            code: text
        });
    }

    const onNext = () => {
        navigation.navigate('NewPassword', {
            userId: state.userId,
            accessToken: state.accessToken,
            email : state.userEmail
        });
    }

    const onBack = () => {
        navigation.goBack()
    }

    const handlePhoneNumber = async (phoneNum: string) => {
        setState({
            ...state,
            isLoading: true
        });

        try {
            const idToken = await auth().currentUser?.getIdToken(true) as string;
            try {
                const result = await phoneNumber(phoneNum, idToken);
                if (result.status === 200) {
                    setState({
                        ...state,
                        isLoading: false,
                        isNext: true,
                        userId: result.data.userId,
                        accessToken: result.data.accessToken,
                        userEmail: result.data.userEmail
                    });
                    navigation.navigate("NewPassword", {
                        userId: result.data.userId,
                        accessToken: result.data.accessToken,
                        email: result.data.userEmail
                    });
                } else {
                    setState({
                        ...state,
                        isLoading: false,
                        isNext: false,
                        errorMesage: '오류8: 잠시 후 다시 시도해주세요.'
                    });
                }
            } catch (error) {
                setState({
                    ...state,
                    isLoading: false,
                    isNext: false,
                    errorMesage: '오류99: 잠시 후 다시 시도해주세요'
                });
                console.log("error =>>>", error);
            }

        } catch (error) {
            console.log("error =>>>", error);
        }
    }


    async function confirmC() {
        try {
            if (route.params.type === 'phone') {
                const result = await route.params.confirmation.confirm(state.code) as any;
                if (result.user._auth._authResult) {
                    handlePhoneNumber(result.user._user.phoneNumber);
                }
            } else {
                setState({
                    ...state,
                    isLoading: true
                });
                if (route.params?.email) {
                    let response = await verifyEmailOtp(route.params.email, state.code);
                    if (response.status === 400) {
                        setState({
                            ...state,
                            isLoading: false,
                            isNext: false,
                            errorMesage: '잘못된 코드입니다'
                        });
                        return;
                    }
                    if (response.status !== 200) {
                        setState({
                            ...state,
                            isLoading: false,
                            isNext: false,
                            errorMesage: '잠시 후 다시 시도해주세요'
                        });
                        return;
                    }
                    let data = response.data;
                    setState({
                        ...state,
                        isLoading: false,
                        isNext: true,
                        userId: data.userId,
                        accessToken: data.accessToken,
                        userEmail: data.userEmail
                    });
                    navigation.navigate("NewPassword", {
                        userId: data.userId,
                        accessToken: data.accessToken,
                        email: data.userEmail
                    });
                } else {
                    setState({
                        ...state,
                        isLoading: false,
                        isNext: false,
                        errorMesage: '오류99: 잘못된 이메일입니다'
                    });
                }
            }
        } catch (error) {
            setState({
                ...state,
                isLoading: false
            });
            switch (error.code) {
                case 'auth/invalid-verification-code':
                    Alert.alert('유효하지 않은 코드.');
                    break;
                case 'auth/session-expired':
                    Alert.alert('세션 만료.');
                    break;
                default:
                    Alert.alert("잠시 후 다시 시도해주세요");
                    break;
            }
        }
    }

    return (
        <Container isAuthor isBack isNext={state.isNext} onGoBack={onBack} onNext={onNext}>
            <LoadingIndicator visible={state.isLoading} />

            <InputApp
                isError
                errorMessage={state.errorMesage}
                keyboardType="numeric"
                returnKeyType="next"
                iconLabel={Icons.ic_verified}
                label="코드 입력"
                placeholder=" "
                value={state.code}
                onChangeText={onChangeCode} />

            <ButtonApp
                onPress={confirmC}
                title="제출"
                backgroundColor={colors.mainColor}
                disabled={!(state.code.length > 0)}
            />

        </Container>
    );
}
