import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { StyleSheet, ImageBackground, StatusBar, Alert, DeviceEventEmitter, Platform, Linking } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Images } from '../../assets';
import { RootStackParamList } from '../../@types';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { useEffect } from 'react';
import { connect, disconnect } from '../../socketio/socketController';
import { onLogout } from '../../utils/UserUtils';
import store from '../../redux/store';
import { personalProfileRequest } from '../../redux/personal-profile/actions';
import NetInfo from '@react-native-community/netinfo';
import ModalDisConnectLoading from '../../components/Modal/ModalDisConnectLoading';
import messaging from '@react-native-firebase/messaging';
import { removeTopics } from '../../utils/setupfirebase';
import { checkAppUpdate } from '../../api/GeneralServices';
import Environments from '../../config/Environments';
import DeviceInfo from 'react-native-device-info';
import { gameRuleRequest } from '../../redux/gamerule/actions';
import { verifyToken } from '../../api/AuthorServices';

const AuthLoadingScreen = ({ navigation }: StackScreenProps<RootStackParamList>) => {

    const onDone = (response: any) => {
        if (Object.keys(response).length === 0) {
            Alert.alert('오류', '사용자 정보를 가져 오지 못했습니다. 다시 로그인하십시오.');
            onLogout(navigation);
            return;
        }
        navigation.replace('Main');
    }

    const onGameRuleDone = (response: any) => {
        console.log('onGameRuleDone: ', response)
    };

    const checkToken = async () => {
        const authStatus = await messaging().requestPermission();
        const token = await keyValueStorage.get("access-token") as string;
        store.dispatch(gameRuleRequest({
            callback: onGameRuleDone
        }));
        if (token && token !== '') {
            try {
                removeTopics();
                disconnect();
                const result = await verifyToken();
                console.log('object: ', result);
                if (result?.status === 423 || result?.status === 404) {
                    Alert.alert('오류', '이 사용자는 온라인 상태입니다.');
                    // onLogout(navigation);
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Author' }],
                    });
                    return;
                }
                if (result.data.isValid) {
                    connect(token, navigation);
                    let userID = await keyValueStorage.get("userID");
                    await keyValueStorage.save('isFirstlogin',
                        JSON.stringify(result.data.isFirstlogin))
                    store.dispatch(personalProfileRequest({
                        id: userID,
                        callback: onDone
                    }));
                } else {
                    navigation.replace("Author");
                }

            } catch (error) {
                console.log("Error token", error.response)
            }
        } else {
            navigation.replace("Author")
        }
    }

    const registerNetInfo = () => {
        NetInfo.addEventListener(state => {
            if (state.isConnected == false) {
                DeviceEventEmitter.emit("showDisConnectLoadingModal", { visible: false });
                navigation.navigate('Modal', { screen: "AlertDisconnect" });
            } else {
                if (navigation.canGoBack()) {
                    navigation.pop();
                }
            }
        });
    }

    const checkAppStatus = async () => {
        const isValid = await checkAppUpdate();
        if (isValid) {
            checkToken();
        } else {
            Alert.alert('새로운 버전 업데이트가 있습니다', '어플리케이션 업데이트가 필요합니다', [
                {
                    text: 'Update', onPress: async () => {
                        if (Platform.OS == 'android') {
                            await Linking.openURL(Environments.ANDROID_APP_URL);
                        } else {
                            await Linking.openURL(Environments.IOS_APP_URL);
                        }
                    }
                }
            ]);
        }
    }

    useEffect(() => {
        registerNetInfo();
        checkAppStatus();
        SplashScreen.hide();
    })

    return (
        <>
            <ModalDisConnectLoading />
            <StatusBar hidden />
            <ImageBackground source={Images.splash_screen} style={styles.container}>
            </ImageBackground>
        </>


    );
};

export default AuthLoadingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
        backgroundColor: "rgba(128, 88, 246, 100)"
    }
});
