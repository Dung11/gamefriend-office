import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import * as React from 'react';
import { useState } from 'react';
import { StyleSheet, ImageBackground, StatusBar, Image, Alert } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RootStackParamList } from '../../../@types';

import { Icons, Images } from '../../../assets';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import { getRefreshData } from '../../../utils/UserUtils';
import messaging from '@react-native-firebase/messaging';
import _Text from '../../../components/_Text';
import { resetAccount } from '../../../api/AuthorServices';

type Params = {
    params: {
        email: string,
        password: string,
        confirmPassword: string,
        phoneNumber: string,
        dob: string,
        gender: string,
        address: string,
        location: any,
        profilePicture: string,
        pictures: any,
        gamePictures: any,
        userName: string
        favoriteGameId: any,
        gameIds: any,
        playTime: string,
        character: any,
        userId: string,
        resetToken: string
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
const WaitingScreen = ({ navigation, route }: Props) => {
    const [mainProfilePic, setMainProfilePic] = useState('');
    const [userId, setUserId] = useState();

    const onReset = async () => {
        messaging()
            .unsubscribeFromTopic(`gamefriend_${userId}`)
            .then(() => console.log(`Unsubscribed from gamefriend_${userId} topic!`));

        const response = await resetAccount();
        if (!response.isFailed) {
            navigation.reset({ index: 0, routes: [{ name: "BirthDay" }] });
        } else {
            Alert.alert('오류', '등록 실패');
        }
    }

    const subscribeNotiTopic = async () => {
        let refreshData = await getRefreshData();
        if (refreshData) {
            const userId = refreshData.userId;
            // refreshData.resetToken
            setUserId(userId);

            messaging()
                .subscribeToTopic(`gamefriend_${userId}`)
                .then(() => console.log(`Subscribed to gamefriend_${userId} topic!`));
        }
    }

    const getFCMToken = async () => {
        try {
            const token = await messaging().getToken();
            if (token) {
                // console.log('FCM token: ', token);
                return token;
            }
        } catch (error) {
            // console.log(error);
        }
    }

    React.useEffect(() => {
        setMainProfilePic(route.params.profilePicture);
        getFCMToken();
        subscribeNotiTopic();
    })

    return (
        <>
            <StatusBar backgroundColor="#665ACD" />
            <ImageBackground source={Images.waiting_screen} style={styles.container}>
                <Image resizeMode="contain" source={Images.waiting_logo} />
                <ImageBackground style={{
                    width: screen.widthscreen / 1.7,
                    height: screen.widthscreen / 1.7,
                    alignItems: 'center',
                    justifyContent: 'center'
                }} source={Images.waiting_shadow}>
                    <Image style={{
                        width: screen.widthscreen / 3,
                        height: screen.widthscreen / 3,
                        borderRadius: screen.widthscreen / 3 / 2
                    }} source={mainProfilePic !== '' ?
                        { uri: mainProfilePic } :
                        Images.avatar_default} />
                    <_Text style={styles._text}>가입 승인 대기중...</_Text>
                </ImageBackground>
                <TouchableOpacity style={styles.wrapBTN} onPress={onReset}>
                    <Image source={Icons.ic_reset} />
                    <_Text style={styles.titleBTN}>
                        프로필 재설정하기
                    </_Text>
                </TouchableOpacity>
            </ImageBackground>
        </>
    )
};

export default WaitingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    _text: {
        marginTop: 12,
        color: colors.white,
        fontWeight: '800',
        fontSize: 20
    },
    wrapBTN: { flexDirection: 'row', marginTop: 60 },
    titleBTN: {
        color: colors.white,
        marginLeft: 10,
    }
});
