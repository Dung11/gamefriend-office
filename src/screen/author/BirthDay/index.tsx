import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp, StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { Container } from '../../../components/Container/InputApp';
import { Platform, StyleSheet, View, TouchableOpacity } from 'react-native';
import { colors } from '../../../config/Colors';
import DateTimePicker from '@react-native-community/datetimepicker';
import { screen } from '../../../config/Layout';
import moment from 'moment';
import { RouteProp } from '@react-navigation/native';
import _Text from '../../../components/_Text';
type Params = {
    params: {
      email: string,
      password: string,
      confirmPassword: string,
      phoneNumber: string,
      kakaoId?: string,
      facebookId?: string,
      googleId?: string,
      accessToken?: string
    },
  }
  interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
  }
export default function BirthDayScreen({
    navigation, route
}: Props) {

    const [isNext, setIsNext] = useState(false)
    const dateNow = new Date()
    const [date, setDate] = useState(dateNow);
    const [show, setShow] = useState(false);

    const onNext = () => {
        let params: any = {
            ...route?.params,
            dob: (moment(date).format('YYYYMMDD')).toString()
          }
        navigation.navigate('Gender', params)
    }
    /// set date time

    const onChange = (event: any, selectedDate: any) => {
        setShow(false);
        setIsNext(true)
        const currentDate = selectedDate || date
        setDate(currentDate);
    };

    const showMode = () => {
        setShow(true);
    };

    return (
        <Container isNotBack isNext={isNext ? true : false} onNext={onNext} styles={styles.container}>
            <_Text style={styles.title}>생일이 언제인가요?</_Text>
            {/* <DateTime date={birthDate} onDateChange={setDate} /> */}
            {Platform.OS === "android" ?
                <TouchableOpacity style={styles.wrapItem} onPress={showMode}>
                    <_Text style={styles.titleItem}>변경이 어려우니 신중히 입력해주세요</_Text>
                    <_Text style={styles.date}>{moment(date).format('YYYY-MM-DD')}</_Text>
                    {show ? <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={'date'}
                        locale="ko"
                        display={"spinner"}
                        onChange={(event: any, date: any) => { onChange(event, date) }}
                        style={styles.dateTimePicker} //style
                        textColor={colors.mainColor}
                    /> : null}
                </TouchableOpacity> // mode android
                :
                <View style={styles.wrapItem}>
                    <_Text >변경이 어려우니 신중히 입력해주세요.</_Text>
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={'date'}
                        display={"spinner"}
                        locale="ko"
                        onChange={(event: any, date: any) => { onChange(event, date) }}
                        style={styles.dateTimePicker} //style
                        textColor={colors.mainColor}
                    />
                </View> // mode ios
            }
        </Container>
    );
}

const styles = StyleSheet.create({
    container: { marginHorizontal: 0 },
    title: {
        color: colors.mainColor,
        marginBottom: 20,
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center'
    },
    titleItem: {
        fontSize: 14,
        fontWeight: '400'
    },
    wrapItem: {
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.white,
        alignItems: 'center'
    },
    date: { color: colors.mainColor, fontWeight: '900', marginTop: 10, fontSize: 28 },
    dateTimePicker: { width: screen.widthscreen / 1.2, backgroundColor: "white", borderRadius: 12, }
})