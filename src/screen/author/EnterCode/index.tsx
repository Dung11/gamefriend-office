import * as React from 'react';

import { useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import { InputApp } from '../../../components/Input/InputApp';
import { colors } from '../../../config/Colors';
import ButtonApp from '../../../components/Button/ButtonApp';
import { Container } from '../../../components/Container/InputApp';
import { Icons } from '../../../assets';
import { toastPopup } from '../../../components/ToastPopup/toast';
import { Alert } from 'react-native';
import { RouteProp } from '@react-navigation/native';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';

type Params = {
    params: {
        confirmation: {
            confirm: (value: string) => void
        },
        email: string,
        password: string,
        confirmPassword?: string,
        phoneNumber: string
    },

}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default function EnterCodeScreen({ navigation, route }: Props) {

    const [code, setCode] = useState('');
    const [isNext, setIsNext] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState('');
    const [isLoading, setIsLoading] = useState(false)

    const onChangeCode = (text: string) => {
        setCode(text);
    }

    const onNext = () => {
        const params: any = {
            ...route.params,
            phoneNumber: phoneNumber,
            isBack: true
        }
        navigation.navigate("PolicyTerms", params);
    }

    const onBack = () => {
        navigation.goBack()
    }

    const confirmC = async () => {
        setIsLoading(true)
        try {
            const result = await route.params.confirmation.confirm(code) as any;
            if (result.user._auth._authResult) {
                setIsLoading(false)
                setCode('');
                setPhoneNumber(result.user._user.phoneNumber);
                setIsNext(true);

                const params: any = {
                    ...route.params,
                    phoneNumber: result.user._user.phoneNumber,
                    isBack: true
                }
                navigation.navigate("PolicyTerms", params);
            }
        } catch (error) {
            setIsLoading(false);
            setIsNext(false);
            setCode('')
            console.log("error =>>", error)
            switch (error.code) {
                case 'auth/invalid-verification-code':
                    toastPopup.error('잘못된 코드입니다')
                    break;
                case 'auth/session-expired':
                    toastPopup.error('잘못된 코드입니다')
                default:
                    toastPopup.error("Error system.")
                    Alert.alert('오류', error.message)
                    console.log('Error system.', error);
                    break;
            }
        }
    }

    return (
        <Container isAuthor isBack isNext={isNext ? true : false} onGoBack={onBack} onNext={isNext ? onNext : undefined}>
            <LoadingIndicator visible={isLoading} />

            <InputApp keyboardType="numeric"
                returnKeyType="next"
                iconLabel={Icons.ic_verified}
                label="코드 입력하기"
                placeholder="코드를 여기 입력해주세요..."
                value={code}
                onChangeText={(text) => onChangeCode(text)}
            />
            {code !== '' ? <ButtonApp onPress={confirmC} title="제출" backgroundColor={colors.mainColor} /> : null}
        </Container>
    );
}

