import React from "react";
import Modal from 'react-native-modal';
import { StyleSheet, TouchableOpacity, View, Image } from 'react-native';
import { colors } from "../../config/Colors";
import { screen } from "../../config/Layout";
import { Images } from "../../assets";
import DeviceInfo from 'react-native-device-info';
import _Text from "../../components/_Text";

interface Props {
    visible?: boolean,
    onSubmit?: () => void,
    onClose?: () => void
}

const _ModalBuyItem = (props: Props) => {

    return (
        <Modal
            style={styles.view}
            swipeDirection={['up', 'left', 'right', 'down']}
            isVisible={props.visible}
            onBackdropPress={props.onClose}>
            {/* <LoadingIndicator visible={isLoadingInView} /> */}
            <View style={styles.wrapper}>
                <View style={styles.header}>
                    <_Text style={[styles.headerTitle]}>App Store</_Text>
                    <TouchableOpacity onPress={props.onClose}>
                        <_Text style={styles.btnText}>Cancel</_Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.content}>
                    <View style={styles.item}>
                        <Image style={styles.avatar} source={Images.logo} />
                        <View>
                            <View style={styles.row}>
                                <_Text style={styles.title}>OFFER OF 10 GOLD BARS </_Text>
                                <_Text style={styles.rating}>4+</_Text>
                            </View>

                            <_Text style={styles.subTitle}>GAMEFRIEND</_Text>
                            <_Text style={styles.subTitle}>BUY IN-APP</_Text>
                        </View>
                    </View>
                    <View style={styles.item}>
                        {/* <_Text style={styles.subTitle}>ACCOUNT</_Text>
                        <View>
                            <_Text style={styles.title}>GAMEFRIEND@GMAIL.COM</_Text>
                        </View> */}
                    </View>
                </View>
                <View style={styles.btn}>
                    <TouchableOpacity activeOpacity={1} style={styles.btnSubmit} onPress={props.onSubmit}>
                        <_Text style={styles.btnSubmitText}>Buy</_Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

export default _ModalBuyItem;

const styles = StyleSheet.create({
    view: {
        justifyContent: 'flex-end',
        margin: 0,
    },
    wrapper: {
        backgroundColor: colors.white,
    },
    header: {
        backgroundColor: colors.white,
        borderBottomWidth: 1,
        borderBottomColor: colors.grayLight,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    content: {
        width: '100%',
        paddingLeft: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontWeight: '500',
        fontSize: 16
    },
    subTitle: {
        color: colors.grayLight,
        fontWeight: '500',
        fontSize: 16

    },
    rating: {
        color: colors.grayLight,
        borderWidth: 1,
        borderColor: colors.grayLight,
        paddingHorizontal: 5,
        fontSize: 9
        // paddingVertical: 0,
    },

    item: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: colors.grayLight,
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    headerTitle: {
        fontSize: 16,
        lineHeight: 18,
        fontWeight: '900'
    },
    btn: {
        width: '100%',
        alignItems: 'center'
    },
    btnSubmit: {
        marginVertical: 10,
        backgroundColor: colors.mainColor,
        borderRadius: 30,
        paddingHorizontal: 5,
        width: screen.widthscreen / 3
    },
    btnText: {
        color: colors.blue,
        textAlign: 'center',
        paddingVertical: 16,
        fontWeight: '400',
        fontSize: 18,
        lineHeight: 25
    },
    btnSubmitText: {
        color: colors.white,
        textAlign: 'center',
        paddingVertical: 16,
        fontWeight: '500',
        fontSize: 20,
        lineHeight: 25
    },
    avatar: {
        height: screen.widthscreen / 5,
        width: screen.widthscreen / 5,
        borderRadius: 8
    }
});
