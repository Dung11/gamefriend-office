
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { StyleSheet, View, Image, StatusBar } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RootStackParamList } from '../../@types';
import { Icons } from '../../assets';
import { colors } from '../../config/Colors';
import DeviceInfo from 'react-native-device-info';
import _Text from '../../components/_Text';

declare const global: { HermesInternal: null | {} };
interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}
const HeaderGameItem = ({ navigation }: Props) => {

    return (
        <View style={styles.header}>
            <StatusBar backgroundColor={colors.white} />
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Image style={styles.iconBack} source={Icons.ic_back_outline} />
            </TouchableOpacity>
            <_Text style={styles.titleHeader}>게임 아이템</_Text>
            <View />
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center',
        backgroundColor: colors.white,
        borderBottomColor: colors.grayLight,
        borderBottomWidth: 1,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
        height: DeviceInfo.hasNotch() ? 90 : 60,
    },
    iconBack: {
        paddingHorizontal: 10,
    },
    titleHeader: {
        color: colors.mainColor,
        fontSize: 16,
        fontWeight: '900',
        marginRight: 40,
    },
});

export default HeaderGameItem;
