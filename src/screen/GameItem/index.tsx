
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState, useCallback } from 'react';
import { StyleSheet, View, FlatList, TouchableOpacity, StatusBar, Alert, Image, Platform, EmitterSubscription, ViewStyle } from 'react-native';
import RNIap, {
    InAppPurchase,
    PurchaseError,
    SubscriptionPurchase,
    finishTransaction,
    finishTransactionIOS,
    purchaseErrorListener,
    purchaseUpdatedListener,
    IAPErrorCode,
} from 'react-native-iap';
import { renderItem, RootStackParamList } from '../../@types';
import { getGameItem, getTotalItem, purchaseGameItem, updateReceipt } from '../../api/GameItemServices';
import { Icons } from '../../assets';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import _Text from '../../components/_Text';
import { colors } from '../../config/Colors';
import HeaderGameItem from './Header';
import _ModalBuyItem from './ModalBuyItem';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}

const itemSkus = Platform.select({
    default: [ // iOS here
        'package_10_items',
        'package_20_items',
        'package_50_items',
        'package_100_items',
        'package_200_items',
    ],
    android: [
        'package_10_items',
        'package_20_items',
        'package_50_items',
        'package_100_items',
        'package_200_items',
    ],
});

let purchaseUpdateSubscription: EmitterSubscription;
let purchaseErrorSubscription: EmitterSubscription;

const GameItemScreen = ({ navigation }: Props) => {

    const [listData, setListData] = useState<any[]>([]);
    const [loading, setLoading] = useState(true);
    const [totalItem, setTotalItem] = useState(0);
    const [isCheck, setIsCheck] = useState();

    const initIAP = useCallback(async (): Promise<void> => {
        // RNIap.clearProductsIOS();

        RNIap.initConnection().then(() => {
            RNIap.flushFailedPurchasesCachedAsPendingAndroid().catch(() => {
                // do nothing here
            }).then(() => {
                getPurchases();
                getProducts();
            })
        });

        purchaseUpdateSubscription = purchaseUpdatedListener(
            async (purchase: InAppPurchase | SubscriptionPurchase) => {
                if (Platform.OS === 'android') {
                    const receipt = purchase.transactionReceipt;
                    if (receipt) {
                        const receiptObj = JSON.parse(receipt);
                        try {
                            const ackResult = await finishTransaction(purchase, true);
                            // let purchaseResult = await purchaseGameItem(receiptObj);
                            let purchaseResult = await updateReceipt(receiptObj);
                            if (purchaseResult.status == 200) {
                                if (purchaseResult?.data) {
                                    setTotalItem(purchaseResult.data.items);
                                }
                            }
                        } catch (ackErr) {
                            console.warn('ackErr', ackErr);
                            setTimeout(() => {
                                Alert.alert('결제 실패', '아이템 결제가 실패하였습니다. 잠시 후 다시 시도해주세요.');
                            }, 500);
                        } finally {
                            setLoading(false);
                        }
                    }
                } else if (Platform.OS === 'ios') {
                    try {
                        if (purchase?.transactionId) {
                            const ackResult = await finishTransactionIOS(purchase.transactionId);
                        }
                        // let purchaseResult = await purchaseGameItem(purchase);
                        let purchaseResult = await updateReceipt(purchase);
                        console.log('PURCHASE RESULT: ', purchaseResult);
                        if (purchaseResult.status == 200) {
                            if (purchaseResult?.data) {
                                setTotalItem(purchaseResult.data.items);
                            }
                        }
                        setLoading(false);
                    } catch (error) {
                        await setLoading(false);
                        setTimeout(() => {
                            Alert.alert('결제 실패', '아이템 결제가 실패하였습니다. 잠시 후 다시 시도해주세요.');
                        }, 500);
                    }
                }
            },
        );

        purchaseErrorSubscription = purchaseErrorListener(async (error: PurchaseError) => {
            console.log('purchaseErrorListener', error);
            // Alert.alert('purchase error', JSON.stringify(error));
            // Alert.alert('payment is cancelled');
            await setLoading(false);
            if (error.code !== IAPErrorCode.E_USER_CANCELLED) {
                setTimeout(() => {
                    Alert.alert('결제 실패', '아이템 결제가 실패하였습니다. 잠시 후 다시 시도해주세요.');
                }, 500);
            }
        });
    }, []);

    const getProducts = async () => {
        const test = await RNIap.getSubscriptions(itemSkus);
        const products = await RNIap.getProducts(itemSkus);
        console.log('PRODUCTS: ', products);
        products.forEach((product) => {
            //   product.type = 'inapp';
        });
        // console.log('products', JSON.stringify(products));
    }

    const getPurchases = async () => {
        try {
            const purchases = await RNIap.getAvailablePurchases();
            purchases.forEach(purchase => {
                console.log('have items which has not been comsumed!');
                switch (purchase.productId) {
                    case 'package_10_items':
                        finishTransaction(purchase, true);
                        purchaseGameItem(purchase.productId);
                        break;
                    case 'package_20_items':
                        finishTransaction(purchase, true);
                        purchaseGameItem(purchase.productId);
                        break;
                    case 'package_50_items':
                        finishTransaction(purchase, true);
                        purchaseGameItem(purchase.productId);
                        break;
                    case 'package_100_items':
                        finishTransaction(purchase, true);
                        purchaseGameItem(purchase.productId);
                        break;
                    case 'package_200_items':
                        finishTransaction(purchase, true);
                        purchaseGameItem(purchase.productId);
                        break;
                }
            });

        } catch (err) {
            console.warn(err); // standardized err.code and err.message available
        }
    }

    const getData = async () => {
        try {
            let [gameItemRes, totalItemRes] = await Promise.all([
                getGameItem(),
                getTotalItem()
            ]);
            setListData(gameItemRes.data.packages);
            setTotalItem(totalItemRes.data.items);
        } catch (error) {
            Alert.alert('오류', error.message)
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getData();
        initIAP();

        return (): void => {
            if (purchaseUpdateSubscription) {
                purchaseUpdateSubscription.remove();
            }
            if (purchaseErrorSubscription) {
                purchaseErrorSubscription.remove();
            }
        };
    }, []);

    const purchase = (productId: any): void => {
        setLoading(true);
        RNIap.requestPurchase(productId);
        // RNIap.requestPurchaseWithQuantityIOS(productId, 1);
        // RNIap.requestPurchase('android.test.purchased');
    };

    const onCheck = (productId: any, index: any) => {
        setIsCheck(index);
        purchase(productId);
    }

    const _renderItem = ({ item, index }: renderItem) => {
        const itemContainerStyle: ViewStyle = {
            ...styles.item,
            borderColor: isCheck === index ? colors.blue : '#E3E3E3',
            borderWidth: isCheck === index ? 2 : 1,
        }
        return (
            <TouchableOpacity key={index} style={itemContainerStyle}
                onPress={() => { onCheck(item.id, index) }}>
                {isCheck === index &&
                    <View style={styles.selectedTag}>
                        <_Text style={styles.chooseText}>선택</_Text>
                        <View style={[styles.triangleCorner]} />
                    </View>
                }
                <View style={styles.row}>
                    <Image source={Icons.ic_game_outline} />
                    <_Text style={styles.point}>{`${item.items}개`}</_Text>
                    {item.extra !== 0 &&
                        <View style={styles.addPointContainer}>
                            <_Text style={styles.addPoint}>{`${item.extra}개 더! `}</_Text>
                        </View>}
                </View>
                <View style={styles.row}>
                    <_Text style={styles.money}>{`US${item.price}`}</_Text>
                </View>
            </TouchableOpacity>
        );
    }
    const _listHeaderComponent = () => {
        return (
            <View style={styles.listHeaderContainer}>
                <_Text style={styles.total}>내 아이템</_Text>
                <_Text style={styles.resultTotal}>{totalItem}</_Text>
            </View>
        );
    }
    const _renderDataEmpty = () => {
        return (
            <View style={styles.wrappEmpty}>
                <_Text style={styles.emptyListStyle}>  </_Text>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <LoadingIndicator visible={loading} />
            <HeaderGameItem navigation={navigation} />
            <FlatList
                data={listData}
                keyExtractor={item => item.id}
                contentContainerStyle={styles.body}
                renderItem={_renderItem}
                ListHeaderComponent={_listHeaderComponent}
                ListEmptyComponent={_renderDataEmpty} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    selectedTag: {
        position: 'absolute',
        top: 0,
    },
    triangleCorner: {
        width: 0,
        height: 0,
        backgroundColor: "transparent",
        borderStyle: "solid",
        borderRightWidth: 30,
        borderTopWidth: 30,
        borderRightColor: "transparent",
        borderTopColor: colors.blue,
        zIndex: 0,
    },
    chooseText: {
        color: colors.white,
        fontWeight: '500',
        fontSize: 7,
        lineHeight: 10,
        transform: [{ rotate: '-45deg' }],
        zIndex: 1,
        position: 'absolute',
        top: 5,
    },
    body: {
        backgroundColor: colors.white,
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    item: {
        paddingVertical: 15,
        marginHorizontal: 8,
        paddingRight: 15,
        paddingLeft: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between",
        marginVertical: 3,
        borderRadius: 6,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    point: {
        fontSize: 22,
        marginHorizontal: 5,
        fontWeight: '400',
    },
    addPointContainer: {
        backgroundColor: colors.blue,
        borderRadius: 6,
        paddingVertical: 4,
        paddingHorizontal: 8,
    },
    addPoint: {
        fontSize: 12,
        color: colors.white,
        fontWeight: '400',
        alignItems: 'center',
        textAlign: 'center',
        lineHeight: 17,
    },
    money: {
        fontSize: 20,
        fontWeight: '400',
        color: '#585859',
    },
    total: {
        fontSize: 17,
        fontWeight: '400',
        color: '#585858',
        marginHorizontal: 8,
        marginVertical: 8,
    },
    resultTotal: {
        fontSize: 26,
        color: colors.mainColor,
        fontWeight: '400',
    },
    wrappEmpty: {
        width: '100%',
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        paddingVertical: 10,
    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        fontStyle: "italic",
        color: colors.grayLight,
    },
    listHeaderContainer: {
        flexDirection: 'row',
    },
});

export default GameItemScreen;
