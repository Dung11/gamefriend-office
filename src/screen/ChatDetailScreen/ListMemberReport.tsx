import React from "react";
import { View, TouchableHighlight, StyleSheet, Image } from "react-native";
import Modal from 'react-native-modal';
import { Images } from "../../assets";
import _Text from "../../components/_Text";
import { colors } from "../../config/Colors";

export default ({
    visible,
    onSellect,
    onBackdropPress,
    data
}: {
    visible: boolean;
    onSellect: (value: string) => void;
    onBackdropPress: () => void;
    data: any[]
}) => {
    return (
        <Modal isVisible={visible} onBackdropPress={onBackdropPress}>
            <View style={styles.content}>
                {data && data.map((item, index) => {
                    return (
                        <TouchableHighlight key={index}  onPress={()=>{onSellect(item)}}>
                            <View style={[styles.wrapperItem,index < data.length - 1 ? styles.borderItem : null]}>
                                  <Image style={styles.avatar} source={item.profilePicture ? { uri: item.profilePicture } : Images.avatar_default} />
                            <_Text style={styles.nameUser}>{item.userName}</_Text>
                            </View>
                          
                        </TouchableHighlight>
                    )
                })}
            </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    Wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        paddingHorizontal: 16,
    },
    content: {
        backgroundColor: "white",
        borderRadius: 13,
    },
    wrapperItem: {
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 16,
        width: '100%',
        flexDirection: 'row',
    },
    borderItem: {
        borderBottomWidth: 1,
        borderBottomColor: '#e3f0ee',
    },
    nameUser: {
        color: colors.mainColor
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 36,
        marginRight: 10
    },
  
});
