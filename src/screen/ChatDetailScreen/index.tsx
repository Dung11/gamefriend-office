
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, FlatList, ActivityIndicator, Alert, Image, TouchableOpacity, Platform, Dimensions } from 'react-native';
import { ChatLineHolder } from '../../components/ChatLineHolder/ChatLineHolder';
import { colors } from '../../config/Colors';
import { renderItem, RootStackParamList } from '../../@types';
import { emitEvent, ReceiveMessage, removeHandler } from '../../socketio/socketController';
import { RouteProp } from '@react-navigation/native';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { ContainerMain } from '../../components/Container/ContainerMain';
import { TipsItem } from '../../components/TipsItem/TipsItem';
import { connect } from "react-redux";
import { HeaderApp } from '../../components/Header/HeaderApp';
import { InputChat } from '../ChatDetailMatch/inputChat';
import store from '../../redux/store';
import { profileRequest } from '../../redux/profile/actions';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { getChattingData, getTip } from '../../api/ChatServices';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import _ChooseImageModal from '../../components/ChooseImageModal/_ChooseImageModal';
import { Icons, Images } from '../../assets';
import { screen } from '../../config/Layout';
import _ModalShowImage from '../../components/Modal/_ModalShowImage';
import { uploadFile } from '../../api/ImageServices';
import DeviceInfo from 'react-native-device-info';
import { Keyboard } from 'react-native';
import _Text from '../../components/_Text';
const RNFS = require('react-native-fs');

type Params = {
    params: {
        roomId?: any;
        nameFriend?: string;
        roomType?: string;
        roomName?: string;
    }
}

interface Props {
    route: RouteProp<Params, 'params'>
    navigation: DrawerNavigationProp<RootStackParamList>
    dataUser: any
}

const ChatDetailScreen = (props: Props) => {

    var currentDay: number | string = 0, currentMonth: number | string = 0, currentYear: number | string = 0;
    const limit = 10;
    const [message, setMessenger] = useState('');
    const [username, setUserName] = useState('');
    const [nameFriend, setNameFriend] = useState('');
    const [chatData, setChatData] = useState<any[]>([]);
    const [sortedData, setSortedData] = useState<any[]>([]);
    const [idRoom, setIdRoom] = useState('')
    const [isItemTips, setIsItemTips] = useState(true);
    const [newMessage, setNewMessage] = useState<any>();
    const [page, setPage] = useState<number>(1);
    const [loading, setLoading] = useState<boolean>(false);
    const [isHideFooter, setIsHideFooter] = useState<boolean>(true);
    const [quantity, setQuantity] = useState<number>(1);
    const [visibleShowImg, setvisibleShowImg] = useState<boolean>(false);
    const [isShowCamaroll, setIsShowCamaroll] = useState<boolean>(false);
    const [selectedImage, setSelectedImage] = useState<string>('');
    const [showingImage, setShowingImage] = useState<string>('');
    const [textTip, setTextTip] = useState([]);
    const [msgIntro, setMsgIntro] = useState<any>();

    const getInfoChat = async () => {
        const routeParams = props.route.params as any;
        await keyValueStorage.save("roomId", routeParams.roomId);
        setIdRoom(routeParams.roomId);
        setNameFriend(routeParams.nameFriend);
        getUser();
        getMember(routeParams.roomId);
        getHistoryChat();
        handleEvent(routeParams.roomId);
        getTipMatch()
    }

    const getUser = async () => {
        let username = await keyValueStorage.get("userName") as string;
        setUserName(username);
    }
    const getMember = async (roomId: string) => {
        const userId = await keyValueStorage.get("userID") as any;

        let payload: any = {
            userId: userId,
            roomId,
        }
        store.dispatch(profileRequest(payload));
    }

    const getTipMatch = async () => {
        try {
            let result = await getTip()
            if (result) {
                setTextTip(result.room_matching)
            } else {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
            console.log(error)
        }
    }

    const checkNewDate = (checkDate: Date) => {
        let status = false;
        if (currentYear < checkDate.getFullYear()) {
            status = true;
        } else if (currentYear == checkDate.getFullYear()) {
            if (currentMonth < checkDate.getMonth() + 1) {
                status = true;
            } else if (currentMonth == checkDate.getMonth() + 1) {
                if (currentDay < checkDate.getDate()) {
                    status = true;
                }
            }
        }
        if (status) {
            currentYear = checkDate.getFullYear();
            currentMonth = checkDate.getMonth() >= 9 ? checkDate.getMonth() + 1 : `0${checkDate.getMonth() + 1}`;
            currentDay = checkDate.getDate() >= 10 ? checkDate.getDate() : `0${checkDate.getDate()}`;
        }
        return status;
    }

    const formatData = (mergedData: any) => {
        let formattedData = [];
        currentYear = 0;
        currentMonth = 0;
        currentDay = 0;
        for (let i = 0; i < mergedData.length; i++) {
            const createdTime = new Date(mergedData[i].createdAt);
            if (checkNewDate(createdTime)) {
                const hourStr = createdTime.getHours() >= 10 ? createdTime.getHours() : `0${createdTime.getHours()}`;
                const minuteStr = createdTime.getMinutes() >= 10 ? createdTime.getMinutes() : `0${createdTime.getMinutes()}`;
                const timeStr = `${currentYear}.${currentMonth}.${currentDay} ${hourStr}:${minuteStr}`;
                if (chatData.findIndex(a => a.id == `${currentYear}.${currentMonth}.${currentDay}`) == -1) {
                    formattedData.unshift({ id: `${currentYear}.${currentMonth}.${currentDay}`, date: timeStr });
                }
            }
            formattedData.unshift(mergedData[i]);
        }
        return formattedData;
    }

    const getHistoryChat = async () => {
        const routeParams: any = props.route.params;
        const userId = await keyValueStorage.get("userID") as any;

        try {
            if (!isHideFooter) return;
            if (chatData.length >= quantity) return;
            setIsHideFooter(false);
            const response = await getChattingData(userId, routeParams.roomId, page, limit);
            if (response?.chats) {
                let resultData = response?.chats
                let newResultData = [];
                // let dataIntro = []
                for (let index = 0; index < resultData.length; index++) {
                    const data = resultData[index]
                    if (data.chatContent && !data.chatContent.value.includes('introGF-')) {
                        newResultData.push(data)
                    } else if (!msgIntro) {
                        if (data.sender.id !== userId) {
                            setMsgIntro(data);
                        }
                    }
                }
                resultData = newResultData
                setQuantity(response.quantity)
                let formattedData = formatData([...chatData, ...resultData].reverse());
                setChatData([...chatData, ...resultData]);
                setSortedData(formattedData);
                setPage(page + 1);
            }
            setIsHideFooter(true);
        } catch (error) {
            console.log(error);
        } finally {
            setLoading(false);
        }
    }

    const _sendMessage = async () => {
        let data: any = { roomId: idRoom }
        if (selectedImage) {
            let locations: Location[] = await sendImage();
            if (locations.length === 0) {
                Alert.alert('오류', '이미지 업로드 실패.');
                return;
            }
            data = {
                ...data,
                chatContent: {
                    chatType: 'image',
                    value: message,
                    image: {
                        origin: locations[0].origin,
                        thumbnail: locations[0].thumbnail,
                    },
                },
            };
            emitEvent('chat', data);
        } else {
            data = {
                ...data,
                chatContent: {
                    chatType: 'text',
                    value: message,
                },
            };
            emitEvent('chat', data);
        }
        setSelectedImage('');
        setMessenger('');
    }

    const onShowImage = (url: string) => {
        setShowingImage(url);
        setvisibleShowImg(true);
    }


    const _renderChatLine = ({ item, index }: renderItem) => {
        if (item.date) {
            return (
                <View style={styles.dateMessageContainer}>
                    <_Text style={styles.messageDate}>{item.date}</_Text>
                </View>
            );
        }
        if (item?.sender?.userName === username) {
            return (
                <View style={{ paddingLeft: 30, alignItems: 'flex-end' }} >
                    <ChatLineHolder contentContainerStyle={{
                        backgroundColor: colors.mainColor,
                        borderBottomRightRadius: 0,
                    }}
                        textColor="white"
                        key={index}
                        item={item}
                        isSender={true}
                        onShowImage={onShowImage} />
                </View>
            );

        }
        return (
            <View style={{ paddingRight: 30 }}>
                <ChatLineHolder contentContainerStyle={{
                    backgroundColor: colors.grayLight,
                    borderBottomLeftRadius: 0,
                }}
                    profilePicture={item?.sender?.profilePicture}
                    key={index}
                    item={item}
                    isSender={false}
                    onShowImage={onShowImage} />
            </View>
        );
    };

    const refreshChatData = () => {
        if (newMessage.id === chatData[0]?.id) return;
        let newSortData = formatData([newMessage, ...chatData].reverse());
        setChatData([newMessage, ...chatData]);
        setSortedData(newSortData);
    }

    // listion event socket
    const handleEvent = async (roomId: string) => {
        ReceiveMessage(`room:${roomId}` as any, (data: any) => {
            let msg = JSON.parse(data);
            setNewMessage(msg);
        });
        ReceiveMessage(`room-terminate` as any, (data: any) => {
            if (roomId === data.roomId) {
                props.navigation.goBack();
            }
        });
    }

    // handle ui
    const onChangeMessage = (value: any) => {
        setMessenger(value);
    }

    const hideItemTips = () => {
        setIsItemTips(false);
    }

    const onMore = () => {
        Keyboard.dismiss();
        props.navigation.openDrawer()
    }

    const onShowCameraRoll = () => {
        setIsShowCamaroll(true);
    }

    useEffect(() => {
        const params: any = props.route.params;

        getInfoChat();
        setTimeout(() => {
            setIsItemTips(false);
        }, 10000);

        emitEvent('room-in-use', {
            roomId: params.roomId
        });

        return () => {
            const params: any = props.route.params;
            removeHandler(`room:${params.roomId}` as any);
            removeHandler('room-leave');
            emitEvent('room-not-in-use', {
                roomId: params.roomId
            });
        }
    }, []);

    useEffect(() => {
        if (newMessage) refreshChatData()
    }, [newMessage]);

    const getImageExstension = (uri: string) => {
        if (uri) {
            const splitNameArr = uri.split('/');
            const filename = splitNameArr[splitNameArr.length - 1];
            const splitFilenameArr = filename.split('.');
            return splitFilenameArr[splitFilenameArr.length - 1];
        }
        return '';
    }

    const uploadPic = (uri: string) => {
        return new Promise<Location[]>(async (resolve, reject) => {
            let formdata = new FormData();
            const ext = getImageExstension(uri);
            const filename = `${Date.now()}.${ext}`;
            let file = {
                uri,
                type: 'image/jpeg',
                name: filename,
            };
            formdata.append('photos', file);
            await uploadFile(formdata).then((response: UploadFileResponse) => {
                console.log('UPLOAD FILE RESPONSE: ', response);
                if (!response.isFailed) {
                    resolve(response.data.locations);
                }
                else {
                    Alert.alert('오류', '이미지 업로드 실패');
                    resolve([]);
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                console.log('오류', error.message);
                resolve([]);
            });
        })
    }

    const sendImage = async () => {
        try {
            if (selectedImage) {
                setLoading(true);
                let currentImg = selectedImage;
                if (Platform.OS === 'ios') {
                    let photoPATH = `assets-library://asset/asset.JPG?id=${currentImg.substring(5)}&ext=JPG`;
                    const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                    currentImg = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
                }
                let locations = await uploadPic(currentImg);
                setLoading(false);
                return locations;
            }
            return [];
        } catch (error) {
            setLoading(false);
            return [];
        }
    }

    const onBack = () => {
        props.navigation.goBack();
    }

    const _listFooterComponent = () => {
        return (
            <>
                {
                    !isHideFooter ?
                        <View style={[styles.container, styles.horizontal]}>
                            <ActivityIndicator size={36} color={colors.blue} />
                        </View>

                        : <View style={{ height: 20 }}></View>
                }
                {msgIntro &&
                    <ChatLineHolder
                        profilePicture={msgIntro?.sender?.profilePicture}
                        contentContainerStyle={{
                            backgroundColor: colors.white,
                            marginTop: 10
                        }}
                        textColor="#65676B"
                        isSender={true}
                        item={msgIntro}
                        onShowImage={onShowImage} />
                }
            </>
        );
    }

    const onCloseModal = () => {
        setIsShowCamaroll(false);
    }

    const onChangeModal = (locations: string[]) => {
        if (locations.length > 0) {
            var index = locations.length - 1;
            setSelectedImage(locations[index].toString());
        }
        setIsShowCamaroll(false);
    }

    const onCloseModalShowImage = () => {
        setvisibleShowImg(false);
    }

    return (
        <ContainerMain>
            <HeaderApp onBack={onBack} title={`${nameFriend} 님과 채팅 중`} onPress={onMore} />
            {(visibleShowImg && showingImage !== '') &&
                <_ModalShowImage
                    visible={visibleShowImg}
                    onClose={onCloseModalShowImage}
                    banner={showingImage} />}
            <_ChooseImageModal
                visible={isShowCamaroll}
                title={'최근에'}
                doneButtonText={'완료'}
                onClose={onCloseModal}
                onChange={onChangeModal}
                numberOfImages={1} />
            <LoadingIndicator visible={loading} />
            <View style={styles.contentChat} >
                {isItemTips && <TipsItem
                    onClose={hideItemTips}
                    text={textTip[0]}
                    text1={textTip[1]}
                />
                }
                <FlatList
                    contentContainerStyle={styles.messageListContainer}
                    data={sortedData} renderItem={_renderChatLine}
                    onEndReached={getHistoryChat}
                    onEndReachedThreshold={0.7}
                    ListFooterComponent={_listFooterComponent}
                    inverted
                    extraData={sortedData} />
            </View>
            { selectedImage !== '' &&
                <View style={{ flexDirection: 'row', backgroundColor: colors.white }}>
                    <View style={{ flex: 2 }} />
                    <View style={styles.imgSelectedView}>
                        <Image source={{ uri: selectedImage }} style={styles.imgComment} />
                        <TouchableOpacity onPress={() => setSelectedImage('')}>
                            <Image source={Icons.ic_delete_circle} />
                        </TouchableOpacity>
                    </View>
                </View>
            }
            <View style={styles.emptySpace} />
            <InputChat
                message={message}
                image={selectedImage}
                onChangeMessage={onChangeMessage}
                _sendMessage={_sendMessage}
                onChooseImage={onShowCameraRoll} />
        </ContainerMain>
    );
};

const styles = StyleSheet.create({
    contentChat: {
        flex: 1,
    },
    container: {
        flex: 1,
        justifyContent: "center",
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10,
    },
    imgComment: {
        height: screen.heightscreen / 10,
        width: screen.widthscreen / 7,
        marginRight: '2%',
        borderRadius: 8,
        marginVertical: '3%',
    },
    imgSelectedView: {
        flex: 8,
        flexDirection: 'row',
        paddingTop: 5,
    },
    emptySpace: {
        height: DeviceInfo.hasNotch() ? 70 : 60,
    },
    messageListContainer: {
        minHeight: '100%',
        justifyContent: 'flex-end',
        paddingHorizontal: 10,
    },
    dateMessageContainer: {
        width: Dimensions.get('screen').width,
        marginVertical: 5,
    },
    messageDate: {
        textAlign: 'center',
    },
});
const mapStateToProps = (state: any) => ({
    dataUser: state.ProfileReducer.profile,
    loading: state.ProfileReducer.loading,
});

export default connect(mapStateToProps, null)(ChatDetailScreen);

interface UploadFileResponse {
    status: number,
    isFailed: boolean,
    data: {
        locations: Location[],
    },
}

interface Location {
    origin: string,
    thumbnail: string,
}
