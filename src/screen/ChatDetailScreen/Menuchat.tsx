import React, { useEffect, useState } from "react";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import { StyleSheet, View, Image, TouchableOpacity, Alert } from "react-native";
import { RootStackParamList } from "../../@types";
import { Images, Icons } from "../../assets";
import { colors } from '../../config/Colors';
import { keyValueStorage } from "../../storage/keyValueStorage";
import { connect, ConnectedProps } from "react-redux";
import store, { AppState } from "../../redux/store";
import { profileRequest } from "../../redux/profile/actions";
import { profileLocalRequest } from "../../redux/profile-local/actions";
import { chatListRequest } from "../../redux/chatlist/actions";
import _ReportModal from "./ReportModal";
import _ReviewModal from "./ReviewModal";
import { StackNavigationProp } from "@react-navigation/stack";
import LoadingIndicator from "../../components/LoadingIndicator/LoadingIndicator";
import ModalLeaveRoom from "../../components/Modal/ModalLeaveRoom";
import {
    muteNoticationAction,
    getRoomMemberInformation,
    cancelRequestAddFriend,
    getLocalRoomMemberInformation,
    muteNoticationLocalAction,
    outRoomChat
} from "../../api/ChatServices";
import DeviceInfo from 'react-native-device-info';
import _Text from "../../components/_Text";
import { sendFriendRequest } from "../../api/RecommendedServices";
import { useIsDrawerOpen } from '@react-navigation/drawer';
import ModalViewProfile from "../LiveTalkChatScreen/ModalViewProfile";
import { memberLivetalkRequest } from "../../redux/members-livetalk/actions";
import { memberLivetalkLocalRequest } from "../../redux/members-livetalklocal/actions";
import { leaveLocalRoom } from "../../api/LivetalkServices";
import ListMemberReport from "./ListMemberReport";

interface RouteItem {
    key: string,
    name: string,
    params: {
        nameFriend: string,
        roomId: string,
        roomType: string,
        roomName: string,
        isMuted: boolean
    }
}

interface Props extends ReduxType {
    navigation: StackNavigationProp<RootStackParamList>
    dataUser: any;
    dataUserLocal: any;
    state: {
        history: any[],
        index: number,
        key: string,
        routeNames: any[],
        routes: RouteItem[],
        stale: boolean,
        type: string
    }
}

const MenuChat = (props: Props) => {

    const isOpen = useIsDrawerOpen();
    const [dataMember, setDataMember] = useState<any[]>([]);
    const [user, setUser] = useState('');
    const [isNotify, setIsNotify] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    const [userIdMe, setUserIdMe] = useState('');
    const [friendInfo, setFriendInfo] = useState();

    const [roomId, setRoomId] = useState('');
    const [roomName, setRoomName] = useState('');
    const [isShowListMemberReport, setIsShowListMemberReport] = useState(false);
    const [isShowReport, setIsShowReport] = useState(false);
    const [isShowReview, setIsShowReview] = useState(false);
    const [isShowLeavingRoom, setIsShowLeavingRoom] = useState(false);
    const [isLiveTalk, setIsLiveTalk] = useState(false);
    const [isLocalLiveTalk, setIsLocalLiveTalk] = useState(false)

    const [mDataUser, setDataUser] = useState(props?.dataUser);
    const [memberInfo, setMemberInfo] = useState();

    const onLeavingRoom = async () => {
        try {
            const { navigation } = props;
            let chatDetailInfo = props.state.routes.find((e) => e.name === 'ChatDetail');
            if (chatDetailInfo) {
                setIsLoading(true);
                let payload: any = {
                    userId: userIdMe,
                    type: chatDetailInfo.params.roomType,
                }
                const res = await outRoomChat(userIdMe, chatDetailInfo.params.roomId);
                if (res.status === 200) {
                    store.dispatch(chatListRequest(payload))
                    navigation.replace("Tabs", { screen: 'Chatlist' });
                } else {
                    Alert.alert('오류20', '잠시 후 다시 시도해주세요');
                }
                setIsLoading(false);
            } else {
                Alert.alert('오류', '대화방을 찾을 수 없습니다.');
            }
        } catch (error) {
            setIsLoading(false);
            Alert.alert('오류', error.message);
        }
    }

    const onLeavingLiveTalkRoom = async () => {
        try {
            const { navigation } = props;
            let chatDetailInfo = props.state.routes.find((e) => e.name === 'LiveTalkChat');
            if (chatDetailInfo) {
                setIsLoading(true);

                let res;
                if (chatDetailInfo.params.roomType === 'livetalk') {
                    res = await outRoomChat(userIdMe, chatDetailInfo.params.roomId);
                } else if (chatDetailInfo.params.roomType === 'livetalk-local') {
                    res = await leaveLocalRoom(chatDetailInfo.params.roomId);
                }

                if (res.status === 200) {
                    navigation.replace("Tabs", { screen: 'LiveTalk' });
                } else if (res.status === 402) { // OUT OF MONEY
                    navigation.navigate("Modal", { screen: "AlertNoItem" });
                } else {
                    Alert.alert('오류21', '잠시 후 다시 시도해주세요');
                }
                setIsLoading(false);
            } else {
                Alert.alert('오류', '대화방을 찾을 수 없습니다.');
            }
        } catch (error) {
            setIsLoading(false);
            Alert.alert('오류', error.message);
        }
    }

    const getUser = async () => {
        const username = await keyValueStorage.get("userName") as any
        setUser(username)
        const userId = await keyValueStorage.get("userID") as any
        setUserIdMe(userId)
    }

    const addFriend = async (userIdB: string) => {
        try {
            let listMembers = [...dataMember];
            const response = await sendFriendRequest(userIdMe, userIdB);
            if (response) {
                let selectedIndex = listMembers.findIndex((d) => d.id === userIdB);
                if (selectedIndex >= 0) {
                    listMembers[selectedIndex].isRequested = true;
                    setDataMember(listMembers);

                    let chatDetailInfo = props.state.routes.find((e) => e.name === 'LiveTalkChat');
                    if (chatDetailInfo?.params?.roomType === 'livetalk') {
                        let payload: any = { userId: userIdMe, roomId }

                        store.dispatch(memberLivetalkRequest(payload));

                    } else if (chatDetailInfo?.params?.roomType === 'livetalk-local') {
                        store.dispatch(memberLivetalkLocalRequest(roomId))
                    }
                }
            } else {
                Alert.alert('오류22', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error)
            Alert.alert('오류23', '잠시 후 다시 시도해주세요');
        }
    }

    const cancelAddFriend = async (userIdB: string) => {
        try {
            let listMembers = [...dataMember];
            const response = await cancelRequestAddFriend(userIdMe, userIdB)
            if (response) {
                let selectedIndex = listMembers.findIndex((d) => d.id === userIdB);
                if (selectedIndex >= 0) {
                    listMembers[selectedIndex].isRequested = false;
                    setDataMember(listMembers);

                    let chatDetailInfo = props.state.routes.find((e) => e.name === 'LiveTalkChat');
                    if (chatDetailInfo?.params?.roomType === 'livetalk') {
                        let payload: any = { userId: userIdMe, roomId }

                        store.dispatch(memberLivetalkRequest(payload));

                    } else if (chatDetailInfo?.params?.roomType === 'livetalk-local') {
                        store.dispatch(memberLivetalkLocalRequest(roomId))
                    }
                }
            } else {
                Alert.alert('오류24', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error)
            Alert.alert('오류25', '잠시 후 다시 시도해주세요');
        }
    }

    const onSelectReportMember = (value: any) => {
        setMemberInfo(value)
        setIsShowListMemberReport(false)
        setTimeout(() => {
            setIsShowReport(true);
        }, 400);
    }

    const onCloseReport = () => {
        setIsShowReport(false);
    }

    const onOpenReport = () => {
        if (isLiveTalk) {
            setIsShowListMemberReport(true)
        } else {
            setIsShowReport(true);
        }
    }

    const onConfirmLeavingRoom = async () => {
        await setIsShowLeavingRoom(false);
        setTimeout(() => {
            onLeavingLiveTalkRoom();
        }, 100);
    }

    const onReviewFriend = async () => {
        let chatDetailInfo = props.state.routes.find((e) => e.name === 'ChatDetail');
        if (chatDetailInfo) {
            if (chatDetailInfo.params.roomType === 'duet') {
                setIsShowReview(true);
            } else {
                await onLeavingRoom();
            }
        } else {
            chatDetailInfo = props.state.routes.find((e) => e.name === 'LiveTalkChat');
            if (chatDetailInfo) {
                if (chatDetailInfo.params.roomType === 'livetalk' ||
                    chatDetailInfo.params.roomType === 'livetalk-local') {
                    setIsShowLeavingRoom(true);
                }
            }
        }
    }

    const onCloseReview = () => {
        setIsShowReview(false)
    }

    const onReviewProfile = async (item: any) => {
        setFriendInfo(item)
        const userId: any = await keyValueStorage.get("userID");

        let chatDetailInfo: any = props.state.routes.find((e) => e.name === 'ChatDetail' || e.name === 'LiveTalkChat');
        if (chatDetailInfo.name === 'LiveTalkChat') {
            if (chatDetailInfo.params.roomType === 'livetalk-local') {
                props.navigation.navigate("Modal", {
                    screen: "ModalViewProfile", params: {
                        friendInfo: item,
                        itemForRequest: 1,
                        userId: userId,
                        roomId: roomId,
                        typeLocalLivetalk: true
                    }
                })
            } else {
                props.navigation.navigate("Modal", {
                    screen: "ModalViewProfile", params: {
                        friendInfo: item,
                        itemForRequest: 1,
                        userId: userId,
                        roomId: roomId,
                        typeLocalLivetalk: false
                    }
                })
            }
        } else {
            props.navigation.navigate('ViewFullProfile', {
                userId: userId,
                friendId: item.id,
                type: 'friendRequestNormalRecommendation'
            });
        }
    }

    const onClickNotify = async () => {
        setIsLoading(true);
        let chatDetailInfo: any = props.state.routes.find((e) => e.name === 'ChatDetail' || e.name === 'LiveTalkChat');
        if (chatDetailInfo.params.roomType === 'livetalk-local') {
            let isSend = await muteNoticationLocalAction(
                roomId,
                isNotify ? 'mute' : 'unmute');
            if (isSend) {
                setIsNotify(!isNotify);
            }
        } else {
            let isSend = await muteNoticationAction(userIdMe,
                roomId,
                isNotify ? 'mute' : 'unmute');
            if (isSend) {
                setIsNotify(!isNotify);
            }
        }
        setIsLoading(false);
    }

    useEffect(() => {
        if (!isOpen) return;
        let chatDetailInfo: any = props.state.routes.find((e) => e.name === 'ChatDetail' || e.name === 'LiveTalkChat');
        if (chatDetailInfo) {
            if (chatDetailInfo.params.roomType === 'livetalk-local') {
                getLocalRoomMemberInformation(chatDetailInfo.params.roomId).then((response: any) => {
                    setDataMember(response);
                })
            } else {
                keyValueStorage.get("userID").then((userId: any) => {
                    getRoomMemberInformation({ userId: userId, roomId: chatDetailInfo.params.roomId }).then((response: any) => {
                        setDataMember(response);
                    })
                });
            }
        }
        setDataUser(props.dataUser);
    }, [isOpen]);

    useEffect(() => {
        getUser();
        let chatDetailInfo: any = props.state.routes.find((e) => e.name === 'ChatDetail' || e.name === 'LiveTalkChat');
        if (chatDetailInfo.name === 'LiveTalkChat') {
            setIsLiveTalk(true)
        }
        if (chatDetailInfo) {
            setRoomId(chatDetailInfo.params.roomId);
            setRoomName(chatDetailInfo.params.roomName);
            setIsNotify(!chatDetailInfo.params.isMuted);
            if (chatDetailInfo.params.roomType === 'livetalk-local') {
                setIsLocalLiveTalk(true)
                setRoomId(chatDetailInfo.params.roomId);
                setDataUser(props?.dataUserLocal);
            } else {
                setDataUser(props?.dataUser);
            }
        } else {
            setDataUser(props.dataUser);
        }
    }, [])


    return (
        <>
            <ListMemberReport
                visible={isShowListMemberReport}
                onSellect={onSelectReportMember}
                onBackdropPress={() => { setIsShowListMemberReport(false) }}
                data={dataMember}
            />
            {isShowReport && <_ReportModal
                visible={isShowReport}
                onClose={onCloseReport}
                memberInfo={isLiveTalk ? memberInfo : dataMember[0]}
                roomId={roomId}
                isLocalRoom={isLocalLiveTalk}
            />}
            <_ReviewModal visible={isShowReview} onClose={onCloseReview}
                onSubmit={onLeavingRoom} dataUser={mDataUser}
                roomName={roomName} />
            <ModalLeaveRoom
                visible={isShowLeavingRoom}
                onBackdropPress={() => setIsShowLeavingRoom(false)}
                onConfirm={onConfirmLeavingRoom} />
            <LoadingIndicator visible={isLoading} />
            <View style={styles.header}>
                {dataMember.length >= 1 && <TouchableOpacity style={styles.btnWarning} onPress={onOpenReport}>
                    <Image source={Icons.ic_warning} />
                </TouchableOpacity>}
                <TouchableOpacity onPress={onClickNotify}>
                    <Image style={{ tintColor: (isNotify ? colors.mainColor : colors.gray) }}
                        source={Icons.ic_notifications} />
                </TouchableOpacity>
            </View>
            {dataMember.length >= 1 && <View style={styles.wrapLabel}>
                <_Text style={styles.label}>대화 상대</_Text>
            </View>}
            <DrawerContentScrollView {...props} contentContainerStyle={styles.drawerContentScrollView}>
                {
                    dataMember && dataMember.map((item: any, index: number) => (
                        <View style={styles.item} key={index}>
                            <TouchableOpacity style={styles.viewItem} onPress={() => onReviewProfile(item)}>
                                <View style={styles.row}>
                                    <Image style={styles.avatar} source={item.profilePicture ? { uri: item.profilePicture } : Images.avatar_default} />
                                    <_Text style={styles.nameUser}>{item.userName.length > 10 ? `${item.userName.substring(0, 10 - 3)}...` : item.userName}</_Text>
                                </View>
                                {item.userName !== user && (
                                    item.isFriend === false && (
                                        item.isRequested
                                            ? <TouchableOpacity
                                                style={[styles.btnAddFriend, { borderWidth: 1, borderColor: colors.mainColor }]}
                                                onPress={() => { cancelAddFriend(item.id) }}>
                                                <_Text style={[styles.titleAdd, { color: colors.mainColor }]}>친구 요청이 전송</_Text>
                                            </TouchableOpacity>
                                            : <TouchableOpacity style={styles.btnAddFriend} onPress={() => { addFriend(item.id) }}>
                                                <_Text style={styles.titleAdd}>친구추가</_Text>
                                            </TouchableOpacity>
                                    ))}
                            </TouchableOpacity>
                        </View>
                    ))
                }
            </DrawerContentScrollView>
            {
                (isLiveTalk || isLocalLiveTalk || (!isLocalLiveTalk && !isLiveTalk && !dataMember[0]?.isFriend)) &&
                <View style={styles.wrapBtn}>
                    <TouchableOpacity style={styles.btnLogout}
                        onPress={onReviewFriend}>
                        <_Text
                            style={styles.titleBtnOut}>방 나가기</_Text>
                        <Image style={styles.icon} source={Icons.ic_exit} />
                    </TouchableOpacity>
                </View>
            }
        </>
    )
}


const mapStateToProps = (state: AppState) => ({
    dataUser: state.ProfileReducer.profile,
    dataUserLocal: state.ProfileLocalReducer.profile_local,
    loading: state.ProfileReducer.loading,
});

const dispatch = {
    profileRequest,
    profileLocalRequest
};

const connector = connect(mapStateToProps, dispatch);
type ReduxType = ConnectedProps<typeof connector>;

export default connector(MenuChat);

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#EEF5FF',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 20,
        borderTopLeftRadius: 24,
        paddingVertical: 15,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    btnWarning: {
        marginRight: 20
    },
    item: {
        paddingHorizontal: 10,
        marginTop: 15
    },
    wrapLabel: {
        paddingHorizontal: 10,
        marginTop: 20
    },
    label: {
        color: colors.mainColor,
        fontWeight: '900',
        fontSize: 16
    },
    viewItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        alignItems: "center"
    },
    nameUser: {
        color: colors.mainColor
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 36,
        marginRight: 5
    },
    titleAdd: {
        fontWeight: '900',
        fontSize: 12
    },
    btnAddFriend: {
        backgroundColor: colors.grayLight,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 7,
        paddingHorizontal: 10,
        borderRadius: 36
    },
    btnLogout: {
        justifyContent: 'center',
        backgroundColor: colors.mainColor,
        borderRadius: 30,
        paddingVertical: 7,
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleBtnOut: {
        color: colors.white,
        marginRight: 7,
        fontWeight: '900',
        fontSize: 20
    },
    wrapBtn: {
        paddingHorizontal: 20,
        paddingVertical: 15
    },
    icon: {
        width: 28,
        height: 28
    },
    drawerContentScrollView: {
        paddingTop: 0,
    },
});