import React from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import Modal from 'react-native-modal';
import _Text from "../../../components/_Text";

const data = [2, 3, 4, 5];

export default ({
    visible,
    onSellect,
    onBackdropPress,
}: {
    visible: boolean;
    onSellect: (value: string) => void;
    onBackdropPress: () => void;
}) => {
    const data = [2, 3, 4, 5 , '상관없음'];
    return (
        <Modal isVisible={visible} onBackdropPress={onBackdropPress}>
            <View style={styles.content}>
                {data.map((item, index) => {
                    return (
                        <TouchableOpacity key={index.toString()}
                            style={[styles.wrapperItem, index < data.length - 1 ? styles.borderItem : null]}
                            onPress={() => { onSellect(item.toString()) }}>
                            <_Text style={styles.text}>{item}</_Text>
                        </TouchableOpacity>
                    )
                })}
            </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    Wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        paddingHorizontal: 16,
    },
    content: {
        backgroundColor: "white",
        borderRadius: 13,
    },
    wrapperItem: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 15,
        width: '100%',
        flexDirection: 'row',
    },
    borderItem: {
        borderBottomWidth: 1,
        borderBottomColor: '#e3f0ee',
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10
    },
    text: {
        fontWeight: '900',
    },
});
