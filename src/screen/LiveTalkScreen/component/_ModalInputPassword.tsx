import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import ButtonApp from '../../../components/Button/ButtonApp';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';

export default ({
    visible,
    onBackdropPress,
    onSubmit,
}: {
    visible: boolean;
    onBackdropPress: () => void,
    onSubmit?: (password: string) => void
}) => {

    const [isVisible, setVisible] = useState(visible);
    const [focusBorder, setFocusBorder] = useState(false);
    const [password, setPassword] = useState('');

    useEffect(() => {
        setVisible(visible);
    }, [visible]);

    return (
        <Modal isVisible={isVisible} onBackdropPress={onBackdropPress} animationIn="fadeIn" animationOut="fadeOut" avoidKeyboard>
            <View style={styles.content}>
                <View style={styles.row}>
                    <_Text style={styles.txtEnterPass}>비공개 방입니다</_Text>
                    <_Text style={styles.txtEnterPass}>비밀번호를 입력 해주세요</_Text>
                </View>

                <View style={[styles.wrapInput, focusBorder && {
                    borderColor: colors.mainColor,
                    borderWidth: 1,
                }]}>
                    <TextInput style={styles.txtInput}
                        onFocus={() => { setFocusBorder(!focusBorder) }}
                        placeholder=' '
                        value={password}
                        onChangeText={(value: any) => { setPassword(value) }}
                    />
                </View>

                <View style={styles.wrapSubmitBtn}>
                    <ButtonApp title="확인" backgroundColor={colors.mainColor} onPress={() => { onSubmit && onSubmit(password) }} />
                </View>

            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    content: {
        backgroundColor: colors.white,
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 30
    },
    row: {
        // flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10
    },
    txtEnterPass: {
        fontSize: 18,
        color: colors.mainColor,
        fontWeight: '800',
        alignItems: 'center'
    },
    wrapInput: {
        width: '100%',
        height: 50,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        marginTop: 5,
        paddingVertical: Platform.OS === 'ios' ? 12 : 0,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.gray,
    },
    txtInput: {
        paddingHorizontal: 5,
        fontSize: 15,
        width: '90%'
    },
    wrapSubmitBtn: {
        width: '100%',
        paddingHorizontal: 50,
        marginVertical: 10
    },
});