import React from 'react';
import { StyleSheet, View, Image, FlatList, TouchableOpacity, TextInput, Platform } from 'react-native';
import { renderItem } from '../../../@types';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Environments from '../../../config/Environments';

interface Props {
    deleteTag: (id: string, name: string) => void;
    onChooseGame: () => void;
    search: string;
    lisTags: any,
    onChangeSearch: (value: any) => void;
    onEndEditing: () => void,
}

const ItemSearchFillter = (props: Props) => {

    const renderTagItem = ({ item, index }: renderItem) => {
        return (
            <View key={index} style={styles.tagItemActive}>
                <_Text style={styles.txtTagActive}>{item.name}</_Text>
                <TouchableOpacity style={styles.btnDeleteTag}
                    onPress={() => props.deleteTag(item.id, item.name)}>
                    <Icon name="times" style={styles.btnDeleteIcon} />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View>
            <View style={styles.wrapInput}>
                <Image source={Icons.ic_search} />
                <TextInput style={styles.textInput}
                    placeholder='검색어를 입력해주세요'
                    value={props.search}
                    onChangeText={(value: any) => { props.onChangeSearch(value) }}
                    onSubmitEditing={props.onEndEditing}
                    multiline={false}
                />
            </View>
            <View style={styles.tagView}>
                {(props.lisTags && props.lisTags.length > 0) &&
                    <View style={styles.tagsView}>
                        <FlatList
                            horizontal
                            data={props.lisTags}
                            numColumns={1}
                            renderItem={renderTagItem}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                }
                <TouchableOpacity style={styles.btnAdd}
                    onPress={props.onChooseGame}>
                    <Image source={Icons.ic_addbulletin} style={styles.imgAdd} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapInput: {
        width: '95%',
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        marginBottom: 10,
        marginTop: 20,
        paddingVertical: Platform.OS === 'ios' ? 15 : 0,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: 14,
        width: '90%',
        fontWeight: '400',
        fontFamily: Environments.DefaultFont.Regular,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        height: Platform.OS == 'android' ? 50 : 'auto',
    },
    tagView: {
        width: '95%',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',

    },
    btnAdd: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
    },
    imgAdd: {
        height: screen.widthscreen / 8,
        width: screen.widthscreen / 8
    },
    scene: {
        flex: 1,
    },
    tabbar: {
        backgroundColor: colors.grey,
    },
    tab: {
        width: (screen.widthscreen - 20) / 3,
    },
    indicator: {
        backgroundColor: 'white',
        height: '100%'
    },
    label: {
        fontWeight: '700',
        color: colors.blue
    },
    postView: {
        width: '94%',
        backgroundColor: 'white',
        alignSelf: 'center'
    },
    btnPost: {
        justifyContent: 'center',
        alignSelf: 'flex-end',
        position: 'absolute',
        bottom: '2%',
        marginHorizontal: '5%',
    },
    imgPost: {

    },
    tagItemInActive: {
        height: screen.widthscreen / 12,
        paddingHorizontal: screen.widthscreen / 25,
        backgroundColor: '#EBEBEB',
        borderRadius: screen.heightscreen / 10,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: screen.widthscreen / 70,
    },
    tagItemActive: {
        height: screen.widthscreen / 13,
        borderRadius: screen.heightscreen / 10,
        paddingLeft: screen.widthscreen / 25,
        paddingRight: screen.widthscreen / 25 + 15,
        justifyContent: 'center',
        borderColor: '#4387EF',
        borderWidth: 1,
        marginHorizontal: screen.widthscreen / 70,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 3,

    },
    txtTag: {
        fontSize: 12,
        color: '#65676b',
        textAlign: 'center',
    },
    txtTagActive: {
        fontSize: 12,
        color: '#4387EF',
        textAlign: 'center',
        fontWeight: '400',
        lineHeight: 22,
    },
    txtTagInActive: {
        fontSize: 12,
        color: '#65676B',
        textAlign: 'center',
    },
    tagsView: {
        marginTop: '1%',
        width: '88%',
        alignItems: 'flex-end',
    },
    loadingView: {
        backgroundColor: '#fff',
    },
    txtDeleteTag: {
        fontSize: 16,
        color: colors.blue,
        marginHorizontal: screen.widthscreen / 90,
        alignSelf: 'center',
        textAlign: 'center',
    },
    btnDeleteTag: {
        alignSelf: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 10,
    },
    btnDeleteIcon: {
        color: colors.blue,
    },
    tabViewContainer: {
        flex: 1,
        marginHorizontal: 10,
    },
    tabView: {
        // marginHorizontal: screen.widthscreen - 10
    },
});

export default ItemSearchFillter;
