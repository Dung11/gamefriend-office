import React, { useEffect, useState } from 'react';
import { TextInput, StyleSheet, View, Platform, Image, TouchableOpacity, FlatList, Alert } from "react-native";
import { Icons } from '../../../assets';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import Modal from 'react-native-modal';
import { ScrollView } from 'react-native-gesture-handler';
import _Text from '../../../components/_Text';
import DeviceInfo from 'react-native-device-info';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getListGame } from '../../../api/GameServices';

interface Props {
    visible: boolean,
    onSubmit: (data: any, type: 'favorite-game' | 'game' | 'game-filter') => void,
    onClose: () => void,
    numberOfChoice: number,
    bannedGames: any[],
    choices: any[],
    type: 'favorite-game' | 'game' | 'game-filter'
}
export const _ModalChooseGame = (props: Props) => {
    const [fullLisDataPC, setFullListDataPC] = useState<any[]>([]);
    const [fullLisDataMB, setFullListDataMB] = useState<any[]>([]);
    const [fullLisDataST, setFullListDataST] = useState<any[]>([]);
    const [showItemPC, setShowItemPC] = useState(true);
    const [showItemMB, setShowItemMB] = useState(true);
    const [showItemST, setShowItemST] = useState(true);
    const [lisDataPC, setListDataPC] = useState<any[]>([]);
    const [lisDataMB, setListDataMB] = useState<any[]>([]);
    const [lisDataST, setListDataST] = useState<any[]>([]);
    const [search, setSearch] = useState<string>('');
    const [data, setData] = useState<any[]>(props.choices);

    const [isDisable, setIsDisable] = useState(true)
    const [visible, setVisible] = useState(props.visible)
    const [bannedGames, setBannedGames] = useState(props.bannedGames)
    const [numberOfChoice, setNumberOfChoice] = useState(props.numberOfChoice)

    const onDropDowmPC = () => {
        setShowItemPC(!showItemPC);
    }

    const onDropDowmMB = () => {
        setShowItemMB(!showItemMB);
    }

    const onDropDowmST = () => {
        setShowItemST(!showItemST);
    }

    const onChangeSearch = (value: any) => {
        setSearch(value);
        if (value.length !== 0) {
            let searchValue = value.toLowerCase();

            let filteredPC = fullLisDataPC.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);
            let filteredST = fullLisDataST.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);
            let filteredMB = fullLisDataMB.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);

            setListDataPC(filteredPC);
            setListDataST(filteredST);
            setListDataMB(filteredMB);

            setShowItemPC(true);
            setShowItemST(true);
            setShowItemMB(true);
        } else {
            let fullPC = [...fullLisDataPC];
            let fullST = [...fullLisDataST];
            let fullMB = [...fullLisDataMB];
            setListDataPC(fullPC);
            setListDataST(fullST);
            setListDataMB(fullMB);

            setShowItemPC(true);
            setShowItemST(true);
            setShowItemMB(true);
        }
    }

    const checkIsExisted = (_id: number) => {
        if (!data) {
            return false;
        }
        let index = data.findIndex((d) => d.id === _id);
        if (index != -1) return true;
        return false;
    }

    const checkIsBanned = (item: any) => {
        let itemIndex = bannedGames.findIndex((d: any) => d.name === item.name);
        if (itemIndex >= 0) {
            return true;
        }
        return false;
    }

    const onClickGame = (item: any, index: number) => {
        let tmpData = [...data];
        if (numberOfChoice === 1) {
            tmpData = [];
            tmpData.push(item);
            setIsDisable(false)
        } else {
            let checkExisted = checkIsExisted(item.id);
            if (!checkExisted) {
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `${numberOfChoice}개까지만 선택 가능합니다`);
                } else {
                    setIsDisable(false)
                    tmpData.push(item);
                }
            } else {
                if (tmpData.length < 1) {
                    setIsDisable(true)
                }
                tmpData = tmpData.filter((d) => d.id !== item.id);
            }
        }
        setData(tmpData);

    }

    const getGamePC = async () => {
        try {
            const response = await getListGame('pc');
            setListDataPC(response?.data.games);
            setFullListDataPC(response?.data.games);
        } catch (error) {
            console.log("error >>>", error)
        }
    }

    const getGameMB = async () => {
        try {
            const response = await getListGame('mobile');
            setListDataMB(response?.data.games);
            setFullListDataMB(response?.data.games);
        } catch (error) {
            console.log("error >>>", error)
        }
    }

    const getGameST = async () => {
        try {
            const response = await getListGame('steam');
            setListDataST(response?.data.games);
            setFullListDataST(response?.data.games);
        } catch (error) {
            console.log("error >>>", error)
        }
    }

    const onValidSubmit = (data: any, type: 'favorite-game' | 'game' | 'game-filter') => {
        if (type === 'game-filter' && data.length === 0) {
            setIsDisable(true)
            // Alert.alert('오류', '최소 3 개의 게임을 선택해야합니다.');
            return;
        }
        props.onSubmit(data, type);
    }

    useEffect(() => {
        getGamePC()
        getGameMB()
        getGameST()
    }, []);

    useEffect(() => {
        setVisible(props.visible);
        setBannedGames(props.bannedGames);
        setNumberOfChoice(props.numberOfChoice)
        setData(props.choices);
        console.log(props.choices)
    }, [props.visible]);

    const renderItemPC = (item: any, index: number) => {
        return (
            <TouchableOpacity key={index}
                disabled={checkIsBanned(item)}
                style={[styles.listItem, checkIsExisted(item.id) === true &&
                    { borderColor: colors.mainColor, backgroundColor: colors.white },
                { opacity: checkIsBanned(item) === true ? .5 : 1 }]}
                onPress={() => onClickGame(item, index)}>
                <_Text style={[
                    styles.nameListItem,
                    { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }
                ]}>{item.name}</_Text>
            </TouchableOpacity>
        )
    }
    const renderItemMB = (item: any, index: number) => {
        return (
            <TouchableOpacity key={index}
                disabled={checkIsBanned(item)}
                style={[styles.listItem, checkIsExisted(item.id) === true &&
                    { borderColor: colors.mainColor, backgroundColor: colors.white },
                { opacity: checkIsBanned(item) === true ? .5 : 1 }]}
                onPress={() => onClickGame(item, index)}>
                <_Text style={[
                    styles.nameListItem,
                    { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }
                ]}>{item.name}</_Text>
            </TouchableOpacity>
        )
    }

    const renderItemST = (item: any, index: number) => {
        return (
            <TouchableOpacity key={index}
                disabled={checkIsBanned(item)}
                style={[styles.listItem, checkIsExisted(item.id) === true &&
                    { borderColor: colors.mainColor, backgroundColor: colors.white },
                { opacity: checkIsBanned(item) === true ? .3 : 1 }]}
                onPress={() => onClickGame(item, index)}>
                <_Text style={[
                    styles.nameListItem,
                    { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }
                ]}>{item.name}</_Text>
            </TouchableOpacity>
        )
    }

    const renderTagItem = ({ item, index }: any) => {
        let dataTmp = [...data];
        const render = dataTmp.length > 0 ?
            <TouchableOpacity key={index} style={styles.tagItemActive} onPress={() => onClickGame(item, index)}>
                <_Text style={styles.txtTagActive}>{item.name}</_Text>
                <View style={styles.tagCloseBtn}>
                    <Icon name="times" style={styles.tagCloseIcon} />
                </View>
            </TouchableOpacity>
            : null
        return render;
    }

    return (
        <Modal isVisible={visible} style={styles.background} >
            <ContainerMain styles={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={props.onClose}>
                        <Image source={Icons.ic_back} style={styles.backIcon} />
                    </TouchableOpacity>
                    <TouchableOpacity disabled={isDisable} onPress={() => { onValidSubmit(data, props.type) }}>
                        <_Text style={[styles.ok, { color: data.length === 0 ? '#c1d0e4' : colors.white }]}>완료</_Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.wrapInput}>
                    <Image source={Icons.ic_search} />
                    <TextInput style={styles.textInput}
                        value={search}
                        onChangeText={(value: any) => { onChangeSearch(value) }} />
                    {/* <Image source={Icons.ic_microphone} /> */}
                </View>
                <View style={styles.wrapDataItem}>
                    <FlatList
                        horizontal
                        data={data}
                        numColumns={1}
                        renderItem={renderTagItem}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <ScrollView>
                    <View style={styles.wrapItem}>
                        <TouchableOpacity onPress={onDropDowmPC}
                            style={[
                                styles.wrapDropDowm,
                                { backgroundColor: showItemPC ? '#EEF5FF' : colors.white }
                            ]}>
                            <_Text style={styles.labelDropDowm}>PC게임</_Text>
                            <Image style={{ marginHorizontal: 5, transform: showItemPC ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                            <Image source={Icons.ic_raphael_pc} />
                        </TouchableOpacity>
                        {showItemPC &&
                            <ScrollView contentContainerStyle={styles.flatList}>
                                {
                                    lisDataPC.map((item: any, index: number) => renderItemPC(item, index))
                                }
                            </ScrollView>
                        }
                    </View>
                    <View style={styles.wrapItem}>
                        <TouchableOpacity onPress={onDropDowmMB} style={[styles.wrapDropDowm, { backgroundColor: showItemMB ? '#EEF5FF' : colors.white }]}>
                            <_Text style={styles.labelDropDowm}>Mobile게임</_Text>
                            <Image style={{ marginHorizontal: 5, transform: showItemMB ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                            <Image source={Icons.ic_mobile_vibration} />
                        </TouchableOpacity>
                        {showItemMB &&
                            <ScrollView contentContainerStyle={styles.flatList}>
                                {
                                    lisDataMB.map((item: any, index: number) => renderItemMB(item, index))
                                }
                            </ScrollView>
                        }
                    </View>
                    <View style={styles.wrapItem}>
                        <TouchableOpacity onPress={onDropDowmST} style={[styles.wrapDropDowm, { backgroundColor: showItemST ? '#EEF5FF' : colors.white }]}>
                            <_Text style={styles.labelDropDowm}>Steam게임</_Text>
                            <Image style={{ marginHorizontal: 5, transform: showItemST ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                            <Image source={Icons.ic_cib_steam} />
                        </TouchableOpacity>
                        {
                            showItemST &&
                            <ScrollView contentContainerStyle={styles.flatList}>
                                {
                                    lisDataST.map((item: any, index: number) => renderItemST(item, index))
                                }
                            </ScrollView>
                        }
                    </View>
                </ScrollView>
            </ContainerMain>
        </Modal>
    )
}


const styles = StyleSheet.create({
    background: {
        margin: 0,
        padding: 0,
    },
    header: {
        width: '100%',
        minHeight: 60,
        backgroundColor: colors.mainColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
        marginBottom: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20
    },
    backIcon: {
        width: 35,
        height: 35,
        marginLeft: -5,
    },
    container: {
        alignItems: 'center',
        flex: 1
    },
    ok: {
        color: colors.white,
        fontSize: 17,
        fontWeight: '400',
        paddingTop: 10,
    },
    wrapInput: {
        width: screen.widthscreen - 20,
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingVertical: Platform.OS === 'ios' ? 15 : 0,
        flexDirection: 'row'
    },
    wrapDataItem: {
        width: '95%',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingVertical: Platform.OS === 'ios' ? 15 : 0,
        flexDirection: 'row'
    },

    textInput: {
        paddingHorizontal: 5,
        fontSize: screen.widthscreen / 30,
        width: '90%'
    },
    iconLeft: {
        resizeMode: 'contain',
        height: 20,
        width: 20,
    },
    listItem: {
        borderColor: '#EDEDED',
        backgroundColor: '#EDEDED',
        height: screen.heightscreen / 23,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 30,
        borderWidth: 1
    },
    nameListItem: {
        color: '#65676B',
        textAlign: 'center',
        fontSize: 12
    },
    wrapItem: {
        width: screen.widthscreen - 30,
        paddingHorizontal: 3,
        paddingVertical: 3,
        backgroundColor: colors.white,
        borderRadius: 10,
        marginVertical: 10,
        flexWrap: 'wrap'
    },
    wrapDropDowm: {
        paddingHorizontal: 15,
        justifyContent: "flex-end",
        borderRadius: 10,
        backgroundColor: 'white',
        width: screen.widthscreen - 36,
        height: screen.heightscreen / 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    labelDropDowm: {
        color: colors.mainColor,
        fontSize: 16
    },
    flatList: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    tagItemActive: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        paddingLeft: screen.widthscreen / 25,
        paddingRight: screen.widthscreen / 25 + 10,
        borderColor: '#4387EF',
        backgroundColor: 'rgba(67, 135, 239, 0.24)',
        marginHorizontal: screen.widthscreen / 70,
        paddingVertical: 7
    },
    tagCloseBtn: {
        position: 'absolute',
        right: 10,
    },
    tagCloseIcon: {
        color: colors.blue,
    },
    txtTagActive: {
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 20,
        color: colors.blue,
        textAlign: 'center',
        marginRight: screen.widthscreen / 40,
    },
    txtX: {
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 20,
        color: colors.blue,
        textAlign: 'center',
    },
    flatListTag: {
        alignItems: 'center',
        marginTop: 10
    }
})
