import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, } from 'react-native';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';

interface Props {
    onSelect: () => void,
    result: any,
}

const SelectRoom = (props: Props) => {
    return (
        <View style={styles.filterRoom}>
            <_Text style={styles.label}>최대 인원수</_Text>
            <TouchableOpacity style={styles.select} onPress={props.onSelect}>
                <_Text style={styles.resutlSelect}>{props.result === '' ? '상관없음' : props.result}</_Text>
                <Image style={{ tintColor: colors.mainColor }} source={Icons.ic_select} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    label: {
        fontWeight: '500',
    },
    filterRoom: {
        flexDirection: 'row',
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 20,
        width: '100%',
    },
    select: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5,
        paddingLeft: 20,
    },
    resutlSelect: {
        color: colors.mainColor,
        marginRight: 10,
    },
});

export default SelectRoom;
