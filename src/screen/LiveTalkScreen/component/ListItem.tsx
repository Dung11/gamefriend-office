import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { StyleSheet, View, Image, FlatList, TouchableOpacity, ActivityIndicator, RefreshControl, Dimensions } from 'react-native';
import { renderItem, RootStackParamList } from '../../../@types';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';

interface Props {
    data: any[];
    navigation: StackNavigationProp<RootStackParamList>;
    onSelect: (roomObj: any) => void;
    onPagination: () => void;
    loadingMore: boolean;
    onRefresh: () => void;
    children?: Element[];
}

const ListItem = (props: Props) => {

    const ListFooterComponent = () => {
        if (props.loadingMore) {
            return (
                <ActivityIndicator size={54} color={colors.mainColor} />
            );
        }
        return null;
    }

    const _renderItem = ({ item, index }: renderItem) => {
        return (
            <TouchableOpacity key={index} style={styles.item}
                onPress={() => { props.onSelect(item) }}>
                <View style={styles.wrapInfo}>
                    <View style={styles.row}>
                        <View style={styles.wrapAvatar}>
                            <Image
                                key={index}
                                style={styles.avatar}
                                source={{ uri: item?.photos[0] }}
                                resizeMode='contain' />
                        </View>
                        <View style={styles.col}>
                            <_Text style={styles.description} ellipsizeMode="tail" numberOfLines={2}>{item.name}</_Text>
                            <View style={styles.row}>
                                <_Text numberOfLines={2}>{item?.owner?.userName} | {item?.owner?.gender === 'male' ? '남성' : '여성'}</_Text>
                            </View>
                            <Image style={styles.directionIcon} resizeMode="contain" source={Icons.ic_next_livetalk} />
                        </View>
                    </View>
                </View>
                <View style={styles.wrapQuantily}>
                    <Image source={Icons.ic_member} />
                    <_Text style={styles.txtQuantily}>{`${item.members} / ${item.limit} 참여 중`}</_Text>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <View style={styles.wrapItemList}>
            <FlatList
                removeClippedSubviews
                refreshControl={
                    <RefreshControl
                        refreshing={false}
                        onRefresh={props.onRefresh} />
                }
                contentContainerStyle={{ paddingBottom: 50, width: '100%', paddingTop: 10 }}
                onEndReached={props.onPagination}
                onEndReachedThreshold={0.8}
                keyExtractor={(item, index) => item.id}
                data={props?.data}
                renderItem={_renderItem}
                ListFooterComponent={ListFooterComponent}
                ListHeaderComponent={<View style={styles.headerContainer}>{props.children}</View>}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    wrapItemList: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 10,
        width: Dimensions.get('screen').width,
    },
    headerContainer: {
        width: Dimensions.get('screen').width,
    },
    item: {
        backgroundColor: colors.white,
        marginVertical: 5,
        borderRadius: 12,
        paddingHorizontal: 10,
        paddingVertical: 20,
        marginHorizontal: 16,
    },
    wrapInfo: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    col: {
        flex: 2,
    },
    description: {
        color: colors.red,
        fontWeight: '900',
        fontSize: 16,
    },
    wrapAvatar: {
        minHeight: 60,
        flex: 1,
        // borderRadius: 24,
        // backgroundColor: colors.darkGray,
        padding: 5,
        marginRight: 20,
        alignItems: 'center',
    },
    avatar: {
        // borderRadius: 30,
        height: 70,
        width: 70,
        margin: 3,
        // borderWidth: 1,
        borderColor: colors.mainColor,
    },
    wrapQuantily: {
        flexDirection: 'row',
        alignItems: 'center',
        position: "absolute",
        bottom: 10,
        right: 10,
    },
    txtQuantily: {
        marginLeft: 10,
        fontWeight: '500',
    },
    directionIcon: {
        position: 'absolute',
        right: 0,
    },
});

export default ListItem;
