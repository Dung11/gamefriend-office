import React, { useEffect, useState } from "react";
import { View, TouchableOpacity, Image, StyleSheet, Switch, Platform, Alert } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import Modal from 'react-native-modal';
import { createRoom } from "../../../api/LivetalkServices";
import { Icons } from "../../../assets";
import ButtonApp from "../../../components/Button/ButtonApp";
import LoadingIndicator from "../../../components/LoadingIndicator/LoadingIndicator";
import _Text from "../../../components/_Text";
import { colors } from "../../../config/Colors";
import { keyValueStorage } from "../../../storage/keyValueStorage";
import { _ChoosingGameModal } from "../../ProfileScreen/EditProfile/ChoosingGameModal";
import { _ModalChooseGame } from "./_ModalChooseGame";
import _ModalSelectRoom from "./_ModalSelectRoom";

export default ({
    visible,
    onBackdropPress,
    onHideModal,
    navigation
}: {
    visible: boolean;
    onBackdropPress: () => void;
    onHideModal: (value: boolean) => void
    navigation: any
}) => {
    let count = 2
    const [nameRoom, setNameRoom] = useState('');
    const [password, setPassword] = useState('');
    const [quantily, setQuantily] = useState(count);
    const [isDisableIncrease, setIsDisableIncrease] = useState(false);
    const [isDisableReduction, setIsDisableReduction] = useState(true);
    const [isEnablePw, setIsEnablePw] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    const [focusBorder, setFocusBorder] = useState(false);
    const [historyChooseGame, setHistoryChooseGame] = useState<any>([]);
    const [gameId, setGameId] = useState('');
    const [nameGame, setNameGame] = useState('');;
    const [showChooseGame, setShowChooseGame] = useState(false);
    const [isVisible, setIsvisible] = useState(visible);

    const onSubmit = async () => {
        if (nameRoom !== '' && gameId !== '') {
            setIsLoading(true)
            let userId = await keyValueStorage.get("userID")
            if (userId) {
                let params = {
                    roomType: "group",
                    isPublic: isEnablePw,
                    password: password,
                    name: nameRoom,
                    limit: quantily,
                    description: "",
                    gameId: gameId,
                }

                try {
                    const result = await createRoom(userId, params)
                    if (!result.isFailed) {
                        setIsLoading(false);
                        setIsvisible(false);
                        onHideModal(false);
                        resetData();
                        setHistoryChooseGame([])
                        const params: any = {
                            roomId: result.data.roomId,
                            roomType: 'livetalk',
                            name: nameRoom,
                        };
                        navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
                    } else if (result.status === 409) {
                        Alert.alert('오류', '이미 존재하는 방입니다');
                        setIsLoading(false);
                        resetData();
                        setHistoryChooseGame([]);
                    }
                    else {
                        Alert.alert('오류49', '잠시 후 다시 시도해주세요');
                        setIsLoading(false);
                        resetData();
                        setHistoryChooseGame([]);
                    }
                } catch (error) {
                    Alert.alert('오류50', '잠시 후 다시 시도해주세요');
                    resetData();
                    setHistoryChooseGame([]);
                }
            } else {
                setIsLoading(false);
                resetData();
                setHistoryChooseGame([]);
                Alert.alert('오류51', '잠시 후 다시 시도해주세요');
            }
        } else {
            if (gameId === '') {
                Alert.alert('오류', '게임을 선택 해주세요');
            } else if (nameRoom === '') {
                Alert.alert('오류', '방 제목을 입력 해주세요');
            }
        }
    }

    const resetData = () => {
        setNameRoom('');
        setNameGame('');
        setGameId('');
        setPassword('');
    }

    const onChooseGame = () => {
        setShowChooseGame(true);
    };

    const onRedution = () => {
        count = quantily;
        count--;
        setQuantily(count);
        if (count == 2) {
            setIsDisableReduction(true);
            setIsDisableIncrease(false);
        } else {
            setIsDisableIncrease(false);
        }
    }

    const onIncrease = () => {
        if (count > 1 && count < 6) {
            count = quantily;
            count++;
            setQuantily(count);
            if (count == 5) {
                setIsDisableIncrease(true);
                setIsDisableReduction(false);
            } else {
                setIsDisableReduction(false);
            }
        }
    }

    const toggleSwitch = () => setIsEnablePw(previousState => !previousState);

    const resultChooseGame = (data: any) => {
        setHistoryChooseGame(data);
        setGameId(data[0].id);
        setNameGame(data[0].name);
        setShowChooseGame(false);
    }

    useEffect(() => {
        setIsvisible(visible);
    }, [visible]);

    return (
        <Modal isVisible={isVisible} onBackdropPress={onBackdropPress} avoidKeyboard>
            <_ModalChooseGame
                visible={showChooseGame}
                onClose={() => { setShowChooseGame(false) }}
                onSubmit={resultChooseGame}
                numberOfChoice={1}
                bannedGames={historyChooseGame}
                choices={historyChooseGame}
                type={"favorite-game"} />
            <LoadingIndicator visible={isLoading} />

            <View style={styles.content}>
                <View style={styles.row}>
                    <Image style={styles.imgTitle} source={Icons.ic_chatlive_livetalk} />
                    <_Text style={styles.title}>방 만들기</_Text>
                </View>

                <TouchableOpacity style={[styles.wrapGame, nameGame !== '' && { borderColor: colors.mainColor, borderWidth: 1 }]} onPress={onChooseGame}>
                    <_Text style={styles.txt}>{nameGame ? nameGame : '게임을 선택해주세요'}</_Text>
                    <Image source={Icons.ic_search} />
                </TouchableOpacity>

                <View style={[styles.wrapInput, focusBorder && {
                    borderColor: colors.mainColor,
                    borderWidth: 1
                }]}>
                    <TextInput style={styles.textInput}
                        onFocus={() => { setFocusBorder(!focusBorder) }}
                        placeholder='제목'
                        value={nameRoom}
                        onChangeText={(value: any) => { setNameRoom(value) }}
                    />
                </View>
                <View style={styles.wrapQuatily}>
                    <_Text style={styles.txt}>인원</_Text>
                    <View style={[styles.row, { justifyContent: "center" }]}>
                        <TouchableOpacity disabled={isDisableReduction} onPress={onRedution}>
                            <Image style={styles.btnQuatity} resizeMode="contain" source={Icons.ic_redution} />
                        </TouchableOpacity>
                        <_Text style={styles.txtQuantily}>{quantily}</_Text>
                        <TouchableOpacity disabled={isDisableIncrease} onPress={onIncrease}>
                            <Image style={styles.btnQuatity} resizeMode="contain" source={Icons.ic_increase} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.wrapPassword}>
                    {!isEnablePw ?
                        <View style={[styles.wrapInput, { width: '60%' }, focusBorder && {
                            borderColor: colors.mainColor,
                            borderWidth: 1
                        }]}>
                            <TextInput style={styles.textInput}
                                onFocus={() => { setFocusBorder(!focusBorder) }}
                                placeholder='비밀번호 입력'
                                value={password}
                                onChangeText={(value: any) => { setPassword(value) }}
                            />
                        </View>
                        :
                        <View style={styles.uiEmptry} />
                    }
                    <_Text style={styles.txt}>공개설정</_Text>
                    <Switch
                        trackColor={{ false: colors.grayLight, true: '#34C759', }}
                        thumbColor={isEnablePw ? colors.white : colors.white}
                        ios_backgroundColor={colors.grayLight}
                        onValueChange={toggleSwitch}
                        value={isEnablePw}
                    />
                </View>

                <View style={styles.wrapBtn}>
                    <ButtonApp title="만들기" backgroundColor={colors.mainColor} onPress={onSubmit} />
                </View>
            </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    content: {
        backgroundColor: colors.white,
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 30,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        fontSize: 16,
        color: colors.mainColor,
        fontWeight: '800',
    },
    imgTitle: {
        height: 50,
        width: 50,
    },
    wrapQuatily: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    txt: {
        fontWeight: '400',
        fontSize: 12,
    },
    wrapPassword: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    btnQuatity: {
        width: 25,
        height: 25
    },
    txtQuantily: {
        fontSize: 12,
        fontWeight: '400',
        marginHorizontal: 10,
    },
    wrapBtn: {
        width: '100%',
        paddingHorizontal: 50,
    },
    wrapInput: {
        width: '100%',
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        marginVertical: 10,
        paddingVertical: Platform.OS === 'ios' ? 12 : 0,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.gray,
    },
    wrapGame: {
        width: '100%',
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        marginVertical: 10,
        paddingVertical: 12,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.gray,
        marginTop: 10,
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: 15,
        width: '90%',
    },
    uiEmptry: {
        width: '60%',
        paddingHorizontal: 20,
        paddingVertical: 22,
        marginVertical: 10,
    },
});
