import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    Platform,
    FlatList,
    Alert,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationProp } from '@react-navigation/native';
import { renderItem, RootStackParamList } from '../../../@types';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import BackTitleHeader from './BackTitleHeader';
import ListItem from '../component/ListItem';
import _Text from '../../../components/_Text';
import { Icons, Images } from '../../../assets';
import { screen } from '../../../config/Layout';
import { colors } from '../../../config/Colors';
import _ModalCreateNeighborRoom from './component/_ModalCreateNeighborRoom';
import { StackNavigationProp } from '@react-navigation/stack';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import { getNeighborRooms, joinLocalRoom } from '../../../api/LivetalkServices';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}

const LiveTalkNeighborhood = (props: Props) => {

    let limit = 10;

    const [showCreateRoom, setShowCreateRoom] = useState(false);
    const [localRoomList, setLocalRoomList] = useState<any>([]);
    const [joiningRoomList, setJoiningRoomList] = useState<any>([]);
    const [isLoading, setLoading] = useState(false);

    const [page, setPage] = useState<number>(1);
    const [loadingMore, setLoadingMore] = useState<boolean>(false);
    const [quantityItem, setQuantityItem] = useState(1)

    const [isWaiting, setIsWaiting] = useState(false);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            getWaitingLocalRoom(true);
            getReadyLocalRoom();
        });
        return unsubscribe;
    }, [props.navigation]);

    useEffect(() => {
        // getWaitingLocalRoom(true);
        // getReadyLocalRoom();
    }, []);

    const getWaitingLocalRoom = async (isNeedShowPopup: boolean) => {
        try {
            setLoading(true);
            const response = await getNeighborRooms(true, 1, limit);
            if (response.status == 200) {
                setLoading(false);
                const joiningLocalRooms = response.data.localRooms;
                setJoiningRoomList(joiningLocalRooms);

                const disableWaitingRoom = await keyValueStorage.get('disableWaitingRoom');

                if (disableWaitingRoom === 'true')
                    return;

                if (isNeedShowPopup) {
                    let isNeedDisableMatching = false;
                    for (let i = 0; i < joiningLocalRooms.length; i++) {
                        const isOwner = joiningLocalRooms[i].isOwner;
                        const isMember = joiningLocalRooms[i].isMember;
                        const members = joiningLocalRooms[i].members;
                        const limit = joiningLocalRooms[i].limit;
                        const roomId = joiningLocalRooms[i].id;

                        setIsWaiting(false);
                        if (isOwner || isMember) {
                            isNeedDisableMatching = true;
                            setIsWaiting(true);
                            DeviceEventEmitter.emit("showWaitingLocalRoom", {
                                visible: true,
                                curMember: members,
                                limitMember: limit,
                                roomId: roomId
                            });

                            DeviceEventEmitter.emit("changeMatchStatus", {
                                disable: true,
                            });
                            break;
                        }
                    }

                    if (!isNeedDisableMatching) {
                        DeviceEventEmitter.emit("changeMatchStatus", {
                            disable: false,
                        });
                    }
                }
            }
        } catch (e) {
            console.log(e);
            setLoading(false);
        }
    }

    const getReadyLocalRoom = async () => {
        try {
            setLoading(true);
            const response = await getNeighborRooms(false, 1, limit);
            if (response.status == 200) {
                setLoading(false);
                const localRooms = response.data.localRooms;
                setLocalRoomList(localRooms);
            }
        } catch (e) {
            setLoading(false);
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
            console.log(e);
        }
    }

    const onBack = () => {
        props.navigation.goBack()
    }

    const onMore = () => {

    }

    const onEnterWaitingLocalRoom = async (roomObj: any) => {
        setLoading(true);

        var result = await joinLocalRoom(roomObj.id);

        setLoading(false);

        if (result.status == 200 || result.status == 409) {
            DeviceEventEmitter.emit("showWaitingLocalRoom", {
                visible: false,
            });

            const params: any = {
                roomId: roomObj.id,
                roomType: 'livetalk-local'
            }
            props.navigation.navigate('WattingJoinRoom', { screen: 'WaitingLocalChat', params });
        }
        else {
            setTimeout(() => {
                Alert.alert('오류', `${result.errors[0]}`);
            }, 500);
        }
    }

    const onEnterLocalRoom = async (roomObj: any) => {
        setLoading(true);

        var result = await joinLocalRoom(roomObj.id);

        setLoading(false);

        if (result.status == 200 || result.status == 409) {
            const params: any = {
                roomId: roomObj.id,
                roomType: 'livetalk-local'
            }
            props.navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
        }
        else {
            setTimeout(() => {
                Alert.alert('오류', `${result.errors[0]}`);
            }, 500);
        }
    }

    const onSubmitCreateRoom = async (roomInfo: any) => {
        setShowCreateRoom(false);
        // getWaitingLocalRoom(true);
        // getReadyLocalRoom();

        if (roomInfo == null) return;

        const roomId = roomInfo.localRoomId;
        const isWaiting = roomInfo.needWaiting;

        if (isWaiting) {
            const params: any = {
                roomId: roomId,
                roomType: 'livetalk-local'
            }
            props.navigation.navigate('WattingJoinRoom', { screen: 'WaitingLocalChat', params });
        } else {
            const params: any = {
                roomId: roomId,
                roomType: 'livetalk-local'
            }
            props.navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
        }
    }

    const onRefresh = () => {
        getReadyLocalRoom();
        getWaitingLocalRoom(true);
    }

    const onPagination = async () => {
        setLoadingMore(true);
        try {
            let pageTmp = page;
            if (quantityItem === 0) {
                setLoadingMore(false);
            } else {
                ++pageTmp;
                const result = await getNeighborRooms(false, pageTmp, limit);
                if (!result.isFailed) {
                    setLoadingMore(false);
                    setQuantityItem(result.data.localRooms.length);
                    setPage(pageTmp);
                    let dataUpdate = [...localRoomList, ...result.data.localRooms];
                    setLocalRoomList(dataUpdate);
                } else {
                    setLoadingMore(false)
                    Alert.alert('오류52', '잠시 후 다시 시도해주세요');
                }
            }

        } catch (e) {
            setLoadingMore(false)
            Alert.alert('오류53', '잠시 후 다시 시도해주세요');
            console.log(e);
        }
    }

    const _renderItem = ({ item, index }: renderItem) => {
        return (
            <TouchableOpacity style={styles.waitingRoomButton} onPress={() => { onEnterWaitingLocalRoom(item) }}>
                <_Text style={styles.txtWaitingRoom}>
                    {`${item.name} ${item.members}/${item.limit} 대기중`}
                </_Text>
                <Image style={{ marginLeft: 10 }} resizeMode="contain" source={Icons.ic_next_livetalk} />
            </TouchableOpacity>
        );
    }

    return (
        <ContainerMain>
            <_ModalCreateNeighborRoom
                {...props}
                isWaitingRoomInProgress={isWaiting}
                visible={showCreateRoom}
                onSubmitCreate={onSubmitCreateRoom}
                onBackdropPress={() => setShowCreateRoom(false)}
            />

            <BackTitleHeader {...props} title={"실시간 동네친구 채팅방"} />

            <LoadingIndicator visible={isLoading} />
            <View style={styles.container}>
                <FlatList
                    style={styles.list}
                    keyExtractor={(item, index) => item.id}
                    extraData={joiningRoomList}
                    data={joiningRoomList}
                    renderItem={_renderItem}
                />
                <ListItem
                    {...props}
                    data={localRoomList}
                    onSelect={onEnterLocalRoom}
                    onPagination={onPagination}
                    loadingMore={loadingMore}
                    onRefresh={onRefresh}
                />
                {
                    // isWaiting ? 
                    // <TouchableOpacity style={styles.btnWaiting}>
                    //     <Image resizeMode="contain" style={{width: 13, height: 22, marginRight: 5}} source={Images.confirm_vector} />
                    //     <_Text style={styles.txtWaiting}>방 만들기</_Text>
                    // </TouchableOpacity> :
                    <TouchableOpacity style={styles.btnCreateRoom} onPress={() => { setShowCreateRoom(true) }}>
                        <Image source={Icons.ic_btn_creater_room_livetalk} />
                    </TouchableOpacity>
                }
            </View>
        </ContainerMain>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    wrapWaitingRoomList: {
        height: 120,
    },
    list: {
        flexGrow: 0
    },
    waitingRoomButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        width: screen.widthscreen - 64,
        height: 50,
        borderRadius: 12,
        marginTop: 20,
    },
    btnCreateRoom: {
        justifyContent: 'center',
        //backgroundColor: 'red',
        alignSelf: 'flex-end',
        position: 'absolute',
        bottom: '2%',
        marginHorizontal: '5%'
    },
    btnWaiting: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.mainColor,
        alignSelf: 'center',
        position: 'absolute',
        bottom: '8%',
        // marginHorizontal: 15,
        borderRadius: 10,
        width: 110,
        height: 35
    },
    txtWaitingRoom: {
        fontSize: 15,
        color: colors.black,
    },
    txtWaiting: {
        fontSize: 15,
        color: colors.white,
        fontWeight: '900'
    }
});

const mapStateToProps = (state: any) => ({
    // dataUser: state.ProfileReducer.profile,
    // loading: state.ProfileReducer.loading,
});

export default connect(mapStateToProps, null)(LiveTalkNeighborhood);