import React, { useEffect, useState } from 'react';
import { 
    View, 
    DeviceEventEmitter,
    TouchableOpacity,
    StyleSheet,
    Image,
    Platform,
    ViewStyle,
    ScrollView 
} from 'react-native';
import { connect } from 'react-redux';
import { Icons, Images } from '../../../assets';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import _Text from '../../../components/_Text';
import { screen } from '../../../config/Layout';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import DeviceInfo from 'react-native-device-info';
import { colors } from '../../../config/Colors';
import { StackNavigationProp } from '@react-navigation/stack';
import { StackActions } from '@react-navigation/native';
import { RouteProp } from '@react-navigation/native';
import { RootStackParamList } from '../../../@types';
import { emitEvent, ReceiveMessage, removeHandler, addHandler } from '../../../socketio/socketController';
import { fromPairs } from 'lodash';
import { getLocalRoomInfo, leaveLocalRoom } from '../../../api/LivetalkServices';


type Params = {
    params: {
        roomId?: any;
        roomType?: any;
    }
}

interface Props {
    route: RouteProp<Params, 'params'>
    navigation: StackNavigationProp<RootStackParamList>
}

export const WattingJoinRoom = (props: Props) => {

    let max = 2;
    const [userList, setUserList] = useState([]);
    const [maxUser, setMaxUser] = useState(max);
    const [roomName, setRoomName] = useState('');
    const [isOwner, setIsOwner] = useState(false);

    useEffect(() => {
        init();
        return () => {
            console.log('remove handler in waiting room !!');
            removeHandler('local-room-update' as any);
            removeHandler('local-room-full' as any);
            removeHandler('local-room-terminate' as any);
        }
    }, []);

    const init = async () => {
        getRoomInfo();
        handleMemberJoinedEvent();
        handleMemberFullEvent();
        handleRoomTerminated();
    } 

    const handleMemberJoinedEvent = async () => {
        addHandler('local-room-update' as any, (data: any) => {
            setUserList(data.members);
        });
    }

    const enterLocalChatScreen = (roomId: string) => {
        const params: any = {
            roomId: roomId,
            roomType: 'livetalk-local'
        }

        props.navigation.pop();
        props.navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
    }

    const handleMemberFullEvent = async () => {
        addHandler('local-room-full' as any, (data: any) => {
            console.log(`Room ${data.roomId} is full !!!!`);
            removeHandler('local-room-update' as any);
            removeHandler('local-room-full' as any);
            removeHandler('local-room-terminate' as any);

            enterLocalChatScreen(data.roomId);
        });
    }

    const handleRoomTerminated = async () => {
        console.log('listen room terminated');
        addHandler('local-room-terminate' as any, (data: any) => {
            console.log(`Room ${data.roomId} has been terminated !!!!`);
            props.navigation.goBack()
        });
    }

    const getRoomInfo = async () => {
        const params = props.route.params as any;
        const roomId = params.params.roomId;
       
        try {
          
            console.log('getting waiting local room info: ', roomId);
            const response = await getLocalRoomInfo(roomId);

            if (response.status === 200) {
                // console.log(response);

                const roomId =  response.data.localRoom.id;
                const roomName = response.data.localRoom.name;
                const members = response.data.localRoom.members;
                const maxUser = response.data.localRoom.limit;

                setRoomName(roomName);
                setUserList(members);
                setMaxUser(maxUser);

                if (members.length == maxUser) {
                    enterLocalChatScreen(roomId);
                }
            }

        } catch (e) {
            console.log(e);
        }
    }

    const onBack = () => {
        props.navigation.goBack()
    }

    const onExit = async () => {
        const params = props.route.params as any;
        const roomId = params.params.roomId;
       
        try {
            // console.log('getting waiting local room info: ', roomId);
            const response = await leaveLocalRoom(roomId);

            if (response.status === 200) {
                // console.log(response);
                props.navigation.goBack()
            }

        } catch (e) {
            console.log(e);
        }
    }

    return (
        <>
            <View style={styles.container}>
                <View style={styles.wrapHeader}>
                    <View style={styles.header}>
                        <TouchableOpacity style={{}} onPress={onBack}>
                            <Image source={Icons.ic_back} />
                        </TouchableOpacity>
                        <_Text style={styles.titleHeader}>실시간 동네친구 채팅방</_Text>
                        <TouchableOpacity style={styles.exits} onPress={onExit} activeOpacity={0.1}>
                            <_Text style={styles.textExits}>취소</_Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.wrap}>
                            <_Text style={styles.title}>{roomName}</_Text>
                        </View>
                        <View style={{ width: '100%', alignItems: 'flex-start' }}>
                            <ScrollView indicatorStyle="white" horizontal style={{ marginTop: 20, height: screen.heightscreen / 4 }} showsHorizontalScrollIndicator={false}>
                                <View style={{ width: screen.widthscreen * 5 / 100 }}></View>
                                {
                                    userList.map((item: any, index: number) => {
                                        return (
                                            <View key={index} style={{ alignItems: 'center' }}>
                                                <TouchableOpacity onPress={() => { }}>
                                                    <Image style={styles.avatar}
                                                        source={{ uri: item.profilePicture }} />
                                                </TouchableOpacity>
                                                <View style={{ width: 50 }}>
                                                    <_Text ellipsizeMode="tail" numberOfLines={2} style={styles.txtName}>{item.userName}</_Text>
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                            </ScrollView>
                        </View>
                        <View style={styles.wrapWatting}>
                            <_Text style={styles.txtWatting}>{`${userList.length}/${maxUser} 대기중`}</_Text>
                            <AnimatedEllipsis style={styles.txtWatting}/>
                        </View>
                    </View>

                </View>
            </View>
        </>
    )


}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    wrapHeader: {
        flex: 1
    },
    wrap: {
        width: '100%',
        alignItems: 'center'
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        paddingVertical: 10,
        alignItems: 'center'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        alignItems: 'center',
        paddingBottom: 10,
        paddingRight: 5,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleHeader: {
        color: colors.white,
        fontWeight: '800',
        fontSize: 16
    },
    title: {
        fontSize: 20,
        fontWeight: '800'
    },
    exits: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 3
    },
    textExits: {
        color: colors.white,
        fontWeight: '900'
    },
    avatar: {
        marginHorizontal: 10,
        width: 100,
        height: 100,
        borderRadius: 50,
    },
    txtName: {
        fontWeight: '800',
        fontSize: 12,
        textAlign: 'center'
    },
    wrapWatting: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center'
    },
    txtWatting: {
        fontWeight: '500',
        fontSize: 20,
        color: colors.mainColor
    }
});

// const mapStateToProps = (state: any) => ({
//     // dataUser: state.ProfileReducer.profile,
//     // loading: state.ProfileReducer.loading,
// });

// export default connect(mapStateToProps, null)(WattingJoinRoom);
