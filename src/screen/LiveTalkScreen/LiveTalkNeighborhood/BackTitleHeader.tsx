import { NavigationProp } from '@react-navigation/native';
import React from 'react';
import {
    StyleSheet,
    StatusBar,
    View,
    Image,
    TouchableOpacity,
    Platform
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { RootStackParamList } from '../../../@types';
import { Images, Icons } from '../../../assets';
import { colors } from '../../../config/Colors';
import _Text from '../../../components/_Text'

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    title: string,
}

const BackTitleHeader = (props: Props) => {

    const goBack = () => {
        const { navigation } = props;
        navigation.goBack();
    }

    return (
        <>
            <StatusBar barStyle="light-content"/>
            <View style={styles.header}>
                <TouchableOpacity style={styles.titleContainer} onPress={goBack}>
                    <Image source={Icons.ic_back}/>
                </TouchableOpacity>
                <_Text style={{...styles.title}}>{props.title}</_Text>
                <View style={{width: 20}}>

                </View>
            </View>
        </>
    );
}

export default BackTitleHeader;

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        paddingBottom: 5,
        paddingRight: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems:  'flex-start',
        paddingLeft: 5
    },
    title: {
        color: colors.white,
        fontSize: 18,
        fontWeight: '900',
        marginTop: 5
    },
});
