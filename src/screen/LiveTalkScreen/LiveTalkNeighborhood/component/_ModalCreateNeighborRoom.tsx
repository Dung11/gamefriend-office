import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet,
    Switch,
    Platform,
    Alert
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import { RootStackParamList } from '../../../../@types';
import { Icons } from '../../../../assets';
import ButtonApp from '../../../../components/Button/ButtonApp';
import _Text from '../../../../components/_Text';
import LoadingIndicator from '../../../../components/LoadingIndicator/LoadingIndicator';
import { colors } from '../../../../config/Colors';
import { keyValueStorage } from '../../../../storage/keyValueStorage';
import { createLocalRoom } from '../../../../api/LivetalkServices';

export default ({
    visible,
    isWaitingRoomInProgress,
    onBackdropPress,
    onSubmitCreate,
    navigation,
}: {
    visible: boolean;
    isWaitingRoomInProgress: boolean;
    navigation: StackNavigationProp<RootStackParamList>;
    onBackdropPress: () => void;
    onSubmitCreate: (roomInfo: any) => void;
}) => {

    let count = 2;

    const [roomName, setRoomName] = useState('');
    const [focusBorder, setFocusBorder] = useState(false);
    const [numberParticipant, setNumberParticipant] = useState(count);
    const [isQuickMode, setQuickMode] = useState(true)
    const [isDisableIncreasing, setDisableIncreasing] = useState(false);
    const [isDisableReduction, setDisableReduction] = useState(true);
    const [isVisible, setVisible] = useState(visible);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setVisible(visible);
    }, [visible]);

    const onSubmit = async () => {
        console.log('Room name: ', roomName);
        console.log('Number of participant: ', numberParticipant);
        console.log('Is quick mode: ', isQuickMode);

        const disableWaitingRoom = await keyValueStorage.get('disableWaitingRoom');

        if (disableWaitingRoom === 'true' && isQuickMode == false) {
            onSubmitCreate(null);
            setVisible(false);
            Alert.alert('오류', '대기실을 만들 수 없습니다.');
            return;
        }

        if (isWaitingRoomInProgress && isQuickMode == false) {
            onSubmitCreate(null);
            Alert.alert('오류', '진행중인 방에 이미 참여했습니다.');
        } else {
            let params = {
                name: roomName,
                limit: numberParticipant,
                needWaiting: !isQuickMode,
            }

            try {
                setIsLoading(true);
                const result = await createLocalRoom(params);

                setIsLoading(false);
                if (!result.isFailed) {
                    console.log(result);
                    onSubmitCreate(result.data);
                    setRoomName('')
                }

            } catch (e) {
                Alert.alert('오류54', '잠시 후 다시 시도해주세요');
                setRoomName('')

            }
        }

        setVisible(false);
    }

    const onReduce = () => {
        count = numberParticipant;
        count--;
        setNumberParticipant(count);
        if (count == 2) {
            setDisableReduction(true);
            setDisableIncreasing(false);
        } else {
            setDisableIncreasing(false);
        }
    }

    const onIncrease = () => {
        if (count <= 5) {
            count = numberParticipant;
            count++;
            setNumberParticipant(count);
            if (count == 5) {
                setDisableIncreasing(true);
                setDisableReduction(false);
            } else {
                setDisableReduction(false);
            }
        }
    }

    const toggleSwitch = () => setQuickMode(previousState => !previousState);


    return (
        <Modal isVisible={isVisible} onBackdropPress={onBackdropPress} avoidKeyboard>
            <LoadingIndicator visible={isLoading} />
            <View style={styles.content}>
                <View style={styles.row}>
                    <_Text style={styles.title}>동네 친구 채팅방 만들기</_Text>
                </View>

                <View style={[styles.wrapInput, focusBorder && {
                    borderColor: colors.mainColor,
                    borderWidth: 1
                }]}>
                    <TextInput style={styles.textInput}
                        onFocus={() => { setFocusBorder(!focusBorder) }}
                        placeholder='제목'
                        value={roomName}
                        onChangeText={(value: any) => { setRoomName(value) }}
                    />
                </View>

                <View style={styles.wrapParticipant}>
                    <_Text style={styles.txt}>인원</_Text>
                    <View style={[styles.row, { justifyContent: "center" }]}>
                        <TouchableOpacity disabled={isDisableReduction} onPress={onReduce}>
                            <Image style={styles.btnQuatity} resizeMode="contain" source={Icons.ic_redution} />
                        </TouchableOpacity>
                        <_Text style={styles.txtQuantity}>{numberParticipant}</_Text>
                        <TouchableOpacity disabled={isDisableIncreasing} onPress={onIncrease}>
                            <Image style={styles.btnQuatity} resizeMode="contain" source={Icons.ic_increase} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.wrapOption}>
                    <_Text style={styles.txt}>즉시 개설</_Text>
                    <Switch
                        trackColor={{ false: colors.grayLight, true: '#34C759', }}
                        thumbColor={colors.white}
                        ios_backgroundColor={colors.grayLight}
                        onValueChange={toggleSwitch}
                        value={isQuickMode}
                    />
                </View>

                <_Text style={styles.txtHint}>* 즉시 개설을 선택하면 대기 없이 바로 방이 만들어집니다.</_Text>

                <View style={styles.wrapSubmitBtn}>
                    <ButtonApp title="방 만들기" backgroundColor={colors.mainColor} onPress={onSubmit} />
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    content: {
        backgroundColor: colors.white,
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: 30
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10
    },
    title: {
        fontSize: 18,
        color: colors.mainColor,
        fontWeight: '800',
        alignItems: 'center'
    },
    btnQuatity: {
        width: 25,
        height: 25
    },
    txtQuantity: {
        fontSize: 15,
        fontWeight: '400',
        marginHorizontal: 10
    },
    wrapInput: {
        width: '100%',
        height: 50,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        marginTop: 5,
        paddingVertical: Platform.OS === 'ios' ? 12 : 0,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.gray,
    },
    wrapParticipant: {
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    wrapOption: {
        width: '95%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 15
    },
    wrapSubmitBtn: {
        width: '100%',
        paddingHorizontal: 50,
        marginVertical: 10
    },
    txtHint: {
        fontWeight: '400',
        fontSize: 12,
        color: colors.gray,
    },
    txt: {
        fontWeight: '400',
        fontSize: 15
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: 15,
        width: '90%'
    },
    uiEmptry: {
        width: '60%',
        paddingHorizontal: 20,
        paddingVertical: 22,
        marginVertical: 10,
    }
});

