import { NavigationProp } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Alert, Image, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { RootStackParamList } from '../../../../@types';
import { getLocalProfileMember } from '../../../../api/LivetalkServices';
import { sendFriendRequest } from '../../../../api/RecommendedServices';
import { blockUser } from '../../../../api/UserServices';
import { Icons, Images } from '../../../../assets';
import LoadingIndicator from '../../../../components/LoadingIndicator/LoadingIndicator';
import MessageBubble from '../../../../components/MessageBubble/MessageBubble';
import ModalUserReport from '../../../../components/Modal/ModalUserReport';
import _Text from '../../../../components/_Text';
import { colors } from '../../../../config/Colors';
import { screen } from '../../../../config/Layout';
import ToastUtils from '../../../../utils/ToastUtils';
import _ReportModal from '../../../RecommendedFriend/ReportModal';

interface Props {
    visible?: boolean,
    navigation: NavigationProp<RootStackParamList, any>,
    onBackdropPress: () => void,
    memberId: string,
    userId: string,
    roomId: string,
    onClose: () => void
}

const ModalViewProfileNeightbor = (props: Props) => {
    const [isLoadingInView, setIsLoadingInView] = useState(false);
    const [visible, setVisible] = useState(props.visible);
    const [reportModalVisible, setReportModalVisible] = useState(false);
    const [friendInfo, setFriendInfo] = useState();
    const [userId, setUserId] = useState<string>();
    const [reportVisible, setReportVisible] = useState(false);
    const [data, setData] = useState<any>();

    const onReportFriend = () => {
        setReportVisible(!reportVisible);
    }

    const onCloseReportFriend = () => {
        setReportVisible(false);
    }

    const onCloseReportModal = () => {
        setReportModalVisible(false);
    }

    const onSeeMore = () => {
        props.onClose();
        props.navigation.navigate("ViewFullProfile", {
            userId: userId,
            friendId: props.memberId,
            isRequested: false,
            type: 'friendRequestLiveTalk'
        });
    }

    const getData = async () => {
        setIsLoadingInView(true)
        try {
            const result = await getLocalProfileMember(props.roomId, props.memberId)
            if (!result.isFailed) {
                setIsLoadingInView(false)
                setData(result.data.profile)
            } else {
                setIsLoadingInView(false)
                Alert.alert('오류55', '잠시 후 다시 시도해주세요');
            }
        } catch (e) {
            setIsLoadingInView(false)
            Alert.alert('오류56', '잠시 후 다시 시도해주세요');
            console.log(e);
        }
    }

    useEffect(() => {
        console.log(props.userId, props.memberId)
        setUserId(props.userId);
        setVisible(props.visible);
        getData();
    }, [props.visible])

    const submitReportModal = () => {
        setReportVisible(false);
        ToastUtils.showToast({
            text1: '신고가 정상적으로 접수되었습니다',
            type: 'info',
            position: 'bottom'
        });
        props.onClose();
    }

    const onConfirmSendFQ = () => {
        props.onClose();
        props.navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'friendRequestLiveTalk',
                onSubmit: sendFR
            }
        });
    }

    const sendFR = () => {
        setIsLoadingInView(true);
        sendFriendRequest(props.userId, props.memberId).then(response => {
            if (response.isFailed === true) {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
                return;
            }

            if (response.status === 200) {
                let friendInfoTmp: any = friendInfo;
                friendInfoTmp.isRequested = true;
                setFriendInfo(friendInfoTmp);
            } else if (response.status === 402) {
                props.onClose();
                props.navigation.navigate("Modal", { screen: "AlertNoItem" })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            setIsLoadingInView(false);
        });
    }

    const onChooseOption = (option: number) => {
        if (option === 1) {
            setReportModalVisible(true);
            setReportVisible(false);
        } else {
            setIsLoadingInView(true);
            onBlockUser();
            setReportVisible(false);
        }
    }

    const onBlockUser = () => {
        blockUser(props.userId, props.memberId).then((response: any) => {
            if (response.status === 400) {
                Alert.alert('오류', '사용자 신고 실패.');
            } else if (response.status === 409) {
                Alert.alert('오류', '이미 해당 사용자를 신고했습니다.');
            } else if (response.status === 200) {
                ToastUtils.showToast({
                    text1: '차단이 완료되었습니다',
                    type: 'info',
                    position: 'bottom'
                })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', '사용자 신고 실패.')
        }).finally(() => {
            setIsLoadingInView(false);
        });
    }

    const renderGameItem = (item: any, index: number) => {
        return (
            <View key={index}
                style={styles.listItem}>
                <_Text numberOfLines={2} style={styles.nameListItem}>{item}</_Text>
            </View>
        )
    }

    return (
        <Modal isVisible={visible} onBackdropPress={props.onBackdropPress}>
            <ModalUserReport
                visible={reportVisible}
                onClose={onCloseReportFriend}
                onSubmit={onChooseOption} />
            <_ReportModal
                friendName={data?.userName}
                userId={props.userId}
                visible={reportModalVisible}
                onClose={onCloseReportModal}
                onSubmit={submitReportModal} />
            <View style={styles.wrapper}>
                <LoadingIndicator visible={isLoadingInView} />
                <View style={{ position: 'relative', zIndex: 10 }}>
                    <Image style={styles.topAvatar} source={data?.profilePicture ? { uri: data?.profilePicture } : Images.avatar_default} />
                </View>
                <ScrollView style={styles.content}>
                    <TouchableOpacity style={styles.icThreeDots} onPress={onReportFriend}>
                        <Image source={Icons.ic_vertical_3_dots} />
                    </TouchableOpacity>
                    <View style={styles.wrapperItem}>
                        <_Text style={styles.username}>{data?.userName}</_Text>
                        {/* <_Text style={styles.idTxt}>ID: {data?.id}</_Text> */}
                        <_Text style={styles.idTxt}>{data?.address} | {data?.age} | {data?.gender}</_Text>
                    </View>
                    <View style={styles.progressContainer}>
                        <ScrollView contentContainerStyle={styles.flatList}>
                            {
                                data?.tags && data?.tags.map((item: any, index: number) => renderGameItem(item, index))
                            }
                        </ScrollView>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <_Text style={styles.settingText}>자기소개</_Text>
                    </View>
                    {
                        (typeof data?.bio !== 'undefined' && data?.bio.length !== 0) &&
                        <MessageBubble content={data?.bio} />
                    }
                    <TouchableOpacity style={styles.btnSeeMore} onPress={onSeeMore}>
                        <_Text style={{ fontWeight: '800', fontSize: 20, textAlign: 'center' }}>자세히 보기</_Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.btnRefreshRecomendedList}
                        onPress={onConfirmSendFQ}>
                        <View style={{ justifyContent: 'center', alignContent: 'center', paddingRight: 10 }}>
                            <_Text style={styles.buttonText}>친구추가</_Text>
                        </View>
                        <View style={styles.btnGameController}>
                            <Image source={Icons.ic_game_controller} />
                            <_Text style={styles.numberGameControler}>1</_Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        </Modal>
    );
}

export default ModalViewProfileNeightbor;

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 10,
        paddingTop: 50,
    },
    content: {
        backgroundColor: "white",
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10,
    },

    wrapperItem: {
        paddingTop: 60,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        width: '100%',
    },
    topAvatar: {
        width: screen.widthscreen / 4,
        height: screen.widthscreen / 4,
        borderRadius: screen.widthscreen / 4 / 2,
        position: 'absolute',
        top: -50,
        left: -50,
        zIndex: 10
    },
    username: {
        color: colors.blue,
        fontSize: 16,
        fontWeight: '800',
        marginBottom: 10
    },
    idTxt: {
        fontWeight: '400',
        fontSize: 12,
        color: '#8F8F8F',
        paddingHorizontal: 12
    },
    icThreeDots: {
        position: 'absolute',
        top: 80,
        right: 30
    },
    progressContainer: {
        marginTop: 10,
        alignItems: 'center'
    },
    flatList: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        flexDirection: "row",
        flexWrap: "wrap",
        marginHorizontal: 10
    },
    listItem: {
        borderColor: colors.mainColor,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 15,
        paddingVertical: 2,
        borderRadius: 30,
        borderWidth: 1,
    },
    nameListItem: {
        color: colors.mainColor,
        textAlign: 'center',
        fontSize: 12
    },
    settingText: {
        textAlign: 'left',
        marginLeft: 20,
        color: colors.blue,
        fontWeight: '800',
        fontSize: 16,
    },
    btnSeeMore: {
        marginTop: 25,
        marginHorizontal: 20,
        backgroundColor: '#F8F3F3',
        borderRadius: 12,
        paddingVertical: 16
    },
    btnRefreshRecomendedList: {
        marginVertical: 20,
        marginHorizontal: 20,
        borderRadius: 12,
        paddingVertical: 12,
        backgroundColor: colors.blue,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center'
    },
    buttonText: {
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center',
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    btnGameController: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EEEEEF',
        width: 60,
        height: 40,
        borderRadius: 20
    },
    numberGameControler: {
        color: colors.black,
        fontWeight: '800',
        fontSize: 14,
        paddingLeft: 5
    },
});