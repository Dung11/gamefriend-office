import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState, useCallback } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Alert,
    ImageBackground,
    Keyboard
} from 'react-native';
import { RootStackParamList } from '../../@types';
import { colors } from '../../config/Colors';
import { ContainerMain } from '../../components/Container/ContainerMain';
import { HeaderLiveTalk } from '../../components/Header/HeaderLiveTalk';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import { Icons, Images } from '../../assets';
import ListItem from './component/ListItem';
import _Text from '../../components/_Text';
import ItemSearchFillter from './component/ItemSearchFillter';
import SelectRoom from './component/SelectRoom';
import _ModalSelectRoom from './component/_ModalSelectRoom';
import _ModalCreateRoom from './component/_ModalCreateRoom';
import _ModalInputPassword from './component/_ModalInputPassword';
import { _ModalChooseGame } from './component/_ModalChooseGame';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { addHandler, removeHandler } from '../../socketio/socketController';
import { getRoomList, joinRoom } from '../../api/LivetalkServices';
import { getLocalRoomStatus } from '../../api/RoomServices';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}
const LiveTalkScreen = (props: Props) => {

    let limit = 10;
    let keyboardDidHide: any;
    let keyboardDidShow: any;
    const [showSelectRoom, setShowSelectRoom] = useState(false);
    const [showCreateRoom, setShowCreateRoom] = useState(false);
    const [showInputPassword, setShowInputPassword] = useState(false);
    const [quantilyMember, setQuantilyMember] = useState('');
    const [listTags, setListTags] = useState([]);
    const [listRooms, setListRooms] = useState<any[]>([]);
    const [search, setSearch] = useState('');
    const [isLoading, setLoading] = useState(false);
    const [game, setGame] = useState([]);
    const [showChooseGame, setShowChooseGame] = useState(false);
    const [loadingMore, setLoadingMore] = useState<boolean>(false);
    const [quantityItem, setQuantityItem] = useState(1)
    const [page, setPage] = useState<number>(1);
    const [curSelectedRoomId, setCurSelectedRoomId] = useState('');
    const [isMuted, setIsMuted] = useState(false)
    const [nameRoom, setNameRoom] = useState('')
    const [keySearchMax, setKeySearchMax] = useState('')
    const [keySearchText, setkeySearchText] = useState('')
    const [keySearchGame, setKeySearchGame] = useState('')
    const [numberStatusRoom, setNumberStatusRoom] = useState(0)
    const [addNewBtnVisible, setAddNewBtnVisible] = useState<boolean>(true)

    const removeKeySeacrh = async () => {
        await keyValueStorage.delete("datasearchlivetalk");
        await keyValueStorage.delete("dataSearchGameLivetalk");
    }

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', async () => {
            setLoading(true);
            await handleRefreshDataSearch();
            await getLocalRoomStatusData();
        });

        keyValueStorage.save("datasearchlivetalk", '')

        addHandler('local-room-status', (data: any) => {
            setNumberStatusRoom(data.users);
        });

        keyboardDidHide = Keyboard.addListener('keyboardDidHide', (e) => {
            setAddNewBtnVisible(true);
        });
        keyboardDidShow = Keyboard.addListener('keyboardDidShow', (e) => {
            setAddNewBtnVisible(false);
        });

        return () => {
            removeKeySeacrh();
            unsubscribe();
            removeHandler('local-room-status');
            keyboardDidHide?.remove();
            keyboardDidShow?.remove();
        }
    }, []);

    const handleRefreshDataSearch = async () => {
        let dataSearch = await keyValueStorage.get("datasearchlivetalk");
        if (dataSearch != undefined) {
            await getLiveTalkRooms();
        } else {
            await keyValueStorage.save("datasearchlivetalk", '');
            setSearch('');
            setQuantilyMember('');
            setPage(1);
            setKeySearchMax('');
            setQuantityItem(1);
            setListRooms([]);
            let searchGame = await keyValueStorage.get("dataSearchGameLivetalk");
            if (searchGame) {
                let dataSearch = keyValueStorage.save("datasearchlivetalk", `${searchGame}`);
                if (dataSearch) {
                    await getLiveTalkRooms();
                }
            } else {
                await getLiveTalkRooms();
            }
        }
    }

    const onGoto = () => {
        props.navigation.navigate("LiveTalk", { screen: "GameItemLiveTalk", params: null });
    }

    const getLocalRoomStatusData = async () => {
        try {
            let result = await getLocalRoomStatus();
            setNumberStatusRoom(result);
        } catch (e) {
            Alert.alert('오류42', e);
            console.log(e);
        }
    }

    const getLiveTalkRooms = useCallback(async () => {
        const keySearch = await keyValueStorage.get("datasearchlivetalk");
        setLoading(true)
        try {
            const result = await getRoomList(1, limit, keySearch);
            if (result) {
                setLoading(false)
                let listRooms = result.rooms;
                setListRooms(listRooms);
                setQuantityItem(result.quantity);
            } else {
                setLoading(false)
                Alert.alert('오류43', '잠시 후 다시 시도해주세요');
            }

        } catch (e) {
            setLoading(false)
            Alert.alert('오류44', '잠시 후 다시 시도해주세요');
            console.log(e);
        }
    }, []);

    const onDeleteTag = (_id: any, _name: any) => {
        let tmpData: any = [];
        let filteredData = listTags.filter((d: any) => d.id != _id);

        setGame(filteredData);
        setListTags(filteredData);
        filteredData.filter((d: any) => tmpData.push(d.id));
        setKeySearchGame(`&gameId=${tmpData.toString()}`)
        getDataSearch(`${keySearchMax}${keySearchText}&gameId=${tmpData.toString()}`)
        keyValueStorage.save("dataSearchGameLivetalk", `&gameId=${tmpData.toString()}`)
        keyValueStorage.save("datasearchlivetalk", `${keySearchMax}${keySearchText}&gameId=${tmpData.toString()}`)
    }

    const onChooseGame = () => {
        setShowChooseGame(true);
    }

    const resultChooseGame = (data: any) => {
        let tmpData: any = [];
        setListTags(data);
        setGame(data);
        setShowChooseGame(false);
        data.filter((d: any) => tmpData.push(d.id));

        setKeySearchGame(`&gameId=${tmpData.toString()}`)
        getDataSearch(`${keySearchMax}${keySearchText}&gameId=${tmpData.toString()}`)
        keyValueStorage.save("dataSearchGameLivetalk", `&gameId=${tmpData.toString()}`)
        keyValueStorage.save("datasearchlivetalk", `${keySearchMax}${keySearchText}&gameId=${tmpData.toString()}`)
    }

    const onSearchTypeText = () => {
        setkeySearchText(`&name=${search}`)
        getDataSearch(`${keySearchMax}${keySearchGame}&name=${search}`)
        keyValueStorage.save("datasearchlivetalk", `${keySearchMax}${keySearchGame}&name=${search}`)
    }

    const onChangeSearch = (value: any) => {
        setSearch(value);
    }

    const getDataSearch = async (keySearch: any) => {
        setLoading(true);
        try {
            const result = await getRoomList(1, 10, keySearch)
            if (result) {
                setLoading(false)
                var listRooms = result.rooms;
                setListRooms(listRooms);
                setQuantityItem(result.quantity);
                setPage(1);
            } else {
                setLoading(false);
                Alert.alert('오류45', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            setLoading(false);
            Alert.alert('오류46', '잠시 후 다시 시도해주세요');
        }
    }

    const onSelectLimitMember = (value: any) => {
        setQuantilyMember(value);
        setShowSelectRoom(false);
        setQuantityItem(1);
        getDataSearch(`${value == '상관없음' ? '' : `max=${value}`}${keySearchGame}${keySearchText}`)
        keyValueStorage.save("datasearchlivetalk", `${value == '상관없음' ? '' : `max=${value}`}${keySearchGame}${keySearchText}`)

        setKeySearchMax(`${value == '상관없음' ? '' : `max=${value}`}`);
    }

    const enterLiveTalkNeighborhood = () => {
        props.navigation.navigate('LiveTalkNeighborhood');
    }

    const onSelectRoom = (roomObj: any) => {
        setNameRoom(roomObj.name);
        setIsMuted(roomObj.isMuted);
        setCurSelectedRoomId(roomObj.id);
        if (roomObj.isPublic) {
            enterLiveTalkRoom(roomObj.id, "", roomObj.isMuted, roomObj.name);
        } else {
            setShowInputPassword(true);
        }
    }

    const onSubmitPassword = (password: string) => {
        setShowInputPassword(false);
        enterLiveTalkRoom(curSelectedRoomId, password, isMuted, nameRoom);
    }

    const enterLiveTalkRoom = async (roomId: string, password: string, isMuted: Boolean, name: string) => {
        setLoading(true);
        var result = await joinRoom(roomId, password);
        setLoading(false);

        if (result.status == 200 || result.status == 409) {
            const params: any = {
                roomId: roomId,
                roomType: 'livetalk',
                isMuted: isMuted,
                name: name,
            };
            props.navigation.navigate('LiveTalkChatScreen', { screen: 'LiveTalkChat', params });
        } else if (result.status === 402) {
            setTimeout(() => {
                props.navigation.navigate("Modal", { screen: "AlertNoItem" });
            }, 100);
        } else {
            setTimeout(() => {
                Alert.alert('오류', `${result.errors[0]}`);
            }, 500);
        }
    }

    const updateListDataRoom = (value: any) => {
        setLoadingMore(true);
        setShowCreateRoom(value);
        setQuantityItem(1);
        setPage(1);
        getLiveTalkRooms();
        setSearch('');
        setListRooms([]);
    }
    const onRefresh = () => {
        setPage(1);
        getLiveTalkRooms();
    }
    const onPagination = async () => {
        const keySearch = await keyValueStorage.get("datasearchlivetalk");
        setLoadingMore(true);
        try {
            let pageTmp = page;
            if (listRooms.length >= quantityItem) {
                setLoadingMore(false);
            } else {
                ++pageTmp
                const result = await getRoomList(pageTmp, limit, keySearch)
                if (!result.isFailed) {
                    setLoadingMore(false);
                    setQuantityItem(result.quantity);
                    setPage(pageTmp);
                    let dataUpdate = [...listRooms, ...result.rooms];
                    setListRooms(dataUpdate);
                } else {
                    setLoadingMore(false);
                    Alert.alert('오류47', '잠시 후 다시 시도해주세요');
                }
            }

        } catch (e) {
            setLoadingMore(false);
            Alert.alert('오류48', '잠시 후 다시 시도해주세요');
            console.log(e);
        }
    }
    return (
        <ContainerMain>
            <_ModalInputPassword
                visible={showInputPassword}
                onSubmit={onSubmitPassword}
                onBackdropPress={() => setShowInputPassword(false)}
            />
            {
                showCreateRoom && <_ModalCreateRoom
                    navigation={props.navigation}
                    visible={showCreateRoom}
                    onBackdropPress={() => setShowCreateRoom(false)}
                    onHideModal={updateListDataRoom} />
            }
            <_ModalSelectRoom
                visible={showSelectRoom}
                onSellect={onSelectLimitMember}
                onBackdropPress={() => setShowSelectRoom(false)}
            />
            {showChooseGame && <_ModalChooseGame
                visible={showChooseGame}
                onClose={() => { setShowChooseGame(false) }}
                onSubmit={resultChooseGame}
                numberOfChoice={3}
                bannedGames={[]}
                choices={game}
                type={"game-filter"} />}
            <HeaderLiveTalk onPress={onGoto} />

            <LoadingIndicator visible={isLoading} />
            <View style={styles.container}>
                <ItemSearchFillter
                    onEndEditing={onSearchTypeText}
                    onChangeSearch={(value: any) => onChangeSearch(value)}
                    lisTags={listTags}
                    search={search}
                    deleteTag={onDeleteTag}
                    onChooseGame={onChooseGame}
                />
                <ListItem
                    {...props}
                    data={listRooms}
                    onSelect={onSelectRoom}
                    onPagination={onPagination}
                    loadingMore={loadingMore}
                    onRefresh={onRefresh}
                >
                    <TouchableOpacity style={{ marginBottom: 10, width: '100%' }} onPress={enterLiveTalkNeighborhood}>
                        <ImageBackground resizeMode="cover"
                            imageStyle={{ borderRadius: 12 }}
                            style={styles.btnRoomNeightborhood}
                            source={Images.banner_room_vip} >
                            <Image source={Icons.ic_word_location} />
                            <View style={styles.wrapTxt}>
                                <_Text style={styles.txtStatus}>실시간 동네친구 단톡방</_Text>
                                <View style={styles.row}>
                                    <View style={styles.statusColor} />
                                    <_Text style={styles.txtStatus}>{`${numberStatusRoom} 명 참여중`}</_Text>
                                </View>
                            </View>
                            <Image source={Icons.ic_enter_btn_livetalk} />
                        </ImageBackground>
                    </TouchableOpacity>
                    <SelectRoom
                        onSelect={() => { setShowSelectRoom(true) }}
                        result={quantilyMember}
                    />
                </ListItem>
                {
                    addNewBtnVisible &&
                    <TouchableOpacity style={styles.btnCreateRoom} onPress={() => { setShowCreateRoom(true) }}>
                        <Image source={Icons.ic_btn_creater_room_livetalk} />
                    </TouchableOpacity>
                }
            </View>
        </ContainerMain>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },

    btnCreateRoom: {
        justifyContent: 'center',
        //backgroundColor: 'red',
        alignSelf: 'flex-end',
        position: 'absolute',
        bottom: '2%',
        marginHorizontal: '5%',
        zIndex: 100,
    },
    btnRoomNeightborhood: {
        // width: screen.widthscreen - 60,
        height: 70,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 15,
    },
    statusColor: {
        backgroundColor: '#EEE462',
        width: 5,
        height: 5,
        borderRadius: 10,
        marginRight: 5
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    txtStatus: {
        color: colors.white,
        fontWeight: '900',
        fontSize: 14,
        textShadowColor: '#333333',
        textShadowOffset: { width: 0, height: 1 },
        textShadowRadius: 3,
    },
    wrapTxt: {
        alignItems: 'center'
    }
});

export default LiveTalkScreen;
