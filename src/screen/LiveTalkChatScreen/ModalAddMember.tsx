import CheckBox from "@react-native-community/checkbox";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import {
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
    Switch,
    Platform,
    FlatList,
    Alert
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import Modal from 'react-native-modal';
import { renderItem, RootStackParamList } from "../../@types";
import { Icons, Images } from "../../assets";
import ButtonApp from "../../components/Button/ButtonApp";
import _Text from "../../components/_Text";
import { colors } from "../../config/Colors";
import LoadingIndicator from "../../components/LoadingIndicator/LoadingIndicator";
import { screen } from "../../config/Layout";
import FastImage from "react-native-fast-image";
import { addUserRoom, getInvitableUsers, getMemberInRoom, kickUserRoom } from "../../api/LivetalkServices";

export default ({
    visible,
    roomId,
    userId,
    onBackdropPress,
    type,
}: {
    visible: boolean;
    roomId: string;
    userId: string;
    onBackdropPress: () => void;
    type: 'add-member' | 'rm-member';

}) => {

    const [isVisible, setIsvisible] = useState(visible);
    const [toggleCheckBoxArray, setToggleCheckBoxArray] = useState<any>([]);
    const [userDataList, setUserDataList] = useState<any>([]);
    const [data, setData] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsvisible(visible);
    }, [visible]);

    useEffect(() => {
        if (type === 'add-member') {
            getInvitableUser();
        } else {
            getRemovableUsers();
        }

    }, [roomId, userId]);

    const getInvitableUser = async () => {
        try {
            if (roomId != '' && userId != '') {
                const response = await getInvitableUsers(userId, roomId);

                if (response.status == 200) {

                    const invitators = response.data.invitators;
                    console.log(invitators)
                    let dataList = [];
                    let toggleDataList = [];
                    for (let i = 0; i < invitators.length; i++) {
                        dataList.push(invitators[i]);
                        toggleDataList.push(false);
                    }
                    setUserDataList(dataList);
                    setToggleCheckBoxArray(toggleDataList);
                }
            }

        } catch (error) {
            console.log(error);
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        }
    }

    const getRemovableUsers = async () => {
        try {
            if (roomId != '' && userId != '') {
                const response = await getMemberInRoom(userId, roomId);

                if (response.status == 200) {
                    const members = response.data.members;

                    let dataList = [];
                    let toggleDataList = [];
                    for (let i = 0; i < members.length; i++) {
                        dataList.push(members[i]);
                        toggleDataList.push(false);
                    }
                    setUserDataList(dataList);
                    setToggleCheckBoxArray(toggleDataList);
                } else {
                    Alert.alert('오류', '잠시 후 다시 시도해주세요');
                }
            }

        } catch (error) {
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
            console.log(error);
        }
    }

    const onAddMember = async () => {
        // setIsvisible(false);
        let tmpData = []
        for (let i = 0; i < toggleCheckBoxArray.length; i++) {
            if (toggleCheckBoxArray[i]) {
                tmpData.push(userDataList[i].id)
            }
            // console.log(cloneObj[i]);
        }
        try {
            const result = await addUserRoom(userId, roomId, tmpData)
            if (!result.isFailed) {
                setIsLoading(false)
                onBackdropPress()
            } else {
                setIsLoading(false)
                Alert.alert('오류35', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            Alert.alert('오류36', '잠시 후 다시 시도해주세요');
        }
    }

    const onRemoveMember = async () => {
        let tmpData = []
        for (let i = 0; i < toggleCheckBoxArray.length; i++) {
            if (toggleCheckBoxArray[i]) {
                tmpData.push(userDataList[i].id)
                // tmpData.push(userDataList[i].id)
            }
            // console.log(cloneObj[i]);
        }
        try {
            const result = await kickUserRoom(userId, roomId, tmpData.toString())

            if (!result.isFailed) {
                setIsLoading(false)
                onBackdropPress()
            } else {
                setIsLoading(false)
                Alert.alert('오류37', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            console.log(error)
            Alert.alert('오류38', '잠시 후 다시 시도해주세요1111');
        }
    }


    const onChecked = (value: boolean, index: number) => {
        const cloneObj = [...toggleCheckBoxArray];
        for (let i = 0; i < cloneObj.length; i++) {
            if (i == index) {
                cloneObj[i] = value;
                break;
            }
        }
        setToggleCheckBoxArray(cloneObj);

        for (let i = 0; i < cloneObj.length; i++) {
            // console.log(cloneObj[i]);
        }

    }

    const _EmptyListMessage = (item: any) => {
        return (
            <View style={styles.wrappEmpty}>
                <_Text style={styles.emptyListStyle}> </_Text>
            </View>

        );
    };

    const _renderItem = ({ item, index }: renderItem) => {
        return (
            <TouchableOpacity style={styles.itemCheck} key={index} onPress={() => { onChecked(!toggleCheckBoxArray[index], index) }}>
                <CheckBox
                    boxType="square"
                    style={styles.check}
                    onCheckColor={colors.mainColor}
                    onTintColor={colors.mainColor}
                    disabled={false}
                    value={toggleCheckBoxArray[index]}
                    onValueChange={(newValue) => {
                        onChecked(newValue, index);
                    }}
                />
                <View style={styles.info}>
                    <View style={[styles.row, { width: '60%' }]}>
                        <FastImage style={styles.avatar} source={{ uri: item.profilePicture }} />
                        <_Text ellipsizeMode="tail" numberOfLines={2} style={styles.txtName}>{item.userName}</_Text>
                    </View>
                    <View style={styles.row}>
                        <View style={item.isOnline ? styles.onlineStatus : styles.offlineStatus} />
                        <_Text style={styles.txtTime}>{item.isOnline ? '온라인' : '분전'}</_Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    const ButtonSubmit = () => {
        return (
            <>
                {userDataList.length > 0 && (type === 'add-member' ? <View style={styles.wrapBtn}>
                    <ButtonApp title="초대하기" backgroundColor={colors.mainColor} onPress={onAddMember} />
                </View>

                    : <View style={styles.wrapBtn}>
                        <ButtonApp title="내보내기" backgroundColor={colors.red} onPress={onRemoveMember} />
                    </View>)}
            </>
        )
    }
    return (
        <Modal isVisible={isVisible} onBackdropPress={onBackdropPress} animationIn="fadeIn" animationOut="fadeIn" avoidKeyboard>
            <LoadingIndicator visible={isLoading} />
            <View style={styles.content}>
                <View style={styles.wrapTitle}>
                    <_Text style={styles.title}>{type === 'add-member' ? `친구 초대하기` : `내보내기`}</_Text>
                </View>
                <View style={styles.wrapFlatlist}>
                    <FlatList
                        keyExtractor={(item, index) => item.id}
                        data={userDataList}
                        renderItem={_renderItem}
                        ListEmptyComponent={(data: any) => _EmptyListMessage(data)}
                        ListFooterComponent={ButtonSubmit}
                    />
                </View>
            </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    content: {
        backgroundColor: colors.white,
        borderRadius: 13,
        width: '100%',
        paddingVertical: 20,
        paddingHorizontal: 30
    },
    wrapFlatlist: {
        minHeight: 'auto',
        maxHeight: screen.heightscreen / 2
    },
    info: {
        marginHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        justifyContent: 'space-between',
    },
    row: {
        marginHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    wrapTitle: {
        width: '100%',
        alignItems: 'center'
    },
    title: {
        fontSize: 18,
        color: colors.mainColor,
        fontWeight: '800',
    },
    txt: {
        fontWeight: '400',
        fontSize: 12
    },
    txtName: {
        marginLeft: 5,
        fontWeight: '500',
        fontSize: 12
    },
    itemCheck: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
        marginHorizontal: 2
    },
    check: {
        width: 20,
        height: 20
    },
    avatar: {
        borderRadius: 30,
        height: 40,
        width: 40,
        margin: 3,
        borderWidth: 1,
        borderColor: colors.mainColor,
    },
    onlineStatus: {
        backgroundColor: colors.greenActive,
        width: 5, height: 5,
        borderRadius: 5
    },
    offlineStatus: {
        backgroundColor: colors.grayInActive,
        width: 5, height: 5,
        borderRadius: 5
    },
    txtTime: {
        marginLeft: 5,
        fontSize: 10,
        fontWeight: '400'
    },
    wrapBtn: {
        marginVertical: 10,
        width: '100%',
        paddingHorizontal: 50
    },
    wrappEmpty: {
        width: '100%',
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        height: screen.heightscreen / 6,
    },
    emptyListStyle: {
        // padding: 10,
        fontSize: 18,
        fontStyle: "italic",
        color: colors.grayLight
    },

});
