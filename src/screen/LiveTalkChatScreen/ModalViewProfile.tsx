import React, { useEffect, useState } from 'react';
import { screen } from '../../config/Layout';
import { Icons, Images } from '../../assets';
import { Alert, FlatList, Image, StyleSheet, TouchableOpacity, ScrollView, View, TouchableWithoutFeedback } from 'react-native';
import { colors } from '../../config/Colors';
import ModalUserReport from '../../components/Modal/ModalUserReport';
import MessageBubble from '../../components/MessageBubble/MessageBubble';
import { NavigationProp, RouteProp } from '@react-navigation/native';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import _Text from '../../components/_Text';
import { sendFriendRequest } from '../../api/RecommendedServices';
import { blockUser } from '../../api/UserServices';
import _ReportModal from '../RecommendedFriend/ReportModal';
import { getLocalProfileMember, getProfileMember } from '../../api/LivetalkServices';
import ToastUtils from '../../utils/ToastUtils';
import { renderItem, RootStackParamList } from '../../@types';
type Params = {
    params: {
        friendInfo: {
            id: string
        }
        itemForRequest: number,
        roomId: string,
        userId: string,
        typeLocalLivetalk?: boolean
    },
}
interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    route: RouteProp<Params, 'params'>

}

const ModalViewProfile = (props: Props) => {
    const [isLoadingInView, setIsLoadingInView] = useState(false);
    const [reportModalVisible, setReportModalVisible] = useState(false);
    const [friendInfo, setFriendInfo] = useState<any>();
    const [userId, setUserId] = useState('');
    const [itemForRequest, setItemForRequest] = useState();
    const [reportVisible, setReportVisible] = useState(false);
    const [data, setData] = useState<any>();


    const onReportFriend = () => {
        setReportVisible(!reportVisible);
    }

    const onCloseReportFriend = () => {
        setReportVisible(false);
    }

    const onCloseReportModal = () => {
        setReportModalVisible(false);
    }

    const onSeeMore = () => {
        props.navigation.navigate("ModalFullViewProfile", {
            userId: userId,
            friendId: friendInfo.id,
            isRequested: false,
            type: 'friendRequestLiveTalk'
        });
    }

    const getData = async () => {
        const item = props.route.params
        setIsLoadingInView(true)
        try {
            let result

            if (item.typeLocalLivetalk) {
                result = await getLocalProfileMember(item.roomId, item.friendInfo.id)
            } else {
                result = await getProfileMember(item.userId, item.roomId, item.friendInfo.id)
            }
            if (!result.isFailed) {
                setIsLoadingInView(false)
                setData(result.data.profile)
            } else {
                setIsLoadingInView(false)
                Alert.alert('오류39', '잠시 후 다시 시도해주세요');
            }
        } catch (e) {
            setIsLoadingInView(false)
            Alert.alert('오류40', '잠시 후 다시 시도해주세요');
            console.log(e);
        }
    }

    useEffect(() => {
        const item = props.route.params as any
        setUserId(item.userId);
        setFriendInfo(item.friendInfo);
        setItemForRequest(item.itemForRequest)
        getData();
    }, [])

    const submitReportModal = () => {
        setReportVisible(false);
        setReportModalVisible(false)
    }

    const onConfirmSendFQ = () => {
        props.navigation.navigate('Modal', {
            screen: 'AlertConfirmItem', params: {
                type: 'friendRequestLiveTalk',
                onSubmit: sendFR
            }
        });
    }

    const sendFR = () => {
        setIsLoadingInView(true);
        sendFriendRequest(userId, friendInfo.id).then(response => {
            if (response.isFailed === true) {
                Alert.alert('오류', '친구 요청 오류.');
                return;
            }

            if (response.status === 200) {
                let friendInfoTmp: any = friendInfo;
                friendInfoTmp.isRequested = true;
                setFriendInfo(friendInfoTmp);
            } else if (response.status === 402) {
                props.navigation.navigate("Modal", { screen: "AlertNoItem" })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            setIsLoadingInView(false);
        });
    }

    const onChooseOption = (option: number) => {
        if (option === 1) {
            setReportModalVisible(true);
            setReportVisible(false);
        } else {
            setIsLoadingInView(true);
            onBlockUser();
            setReportVisible(false);
        }
    }


    const onBlockUser = () => {
        blockUser(userId, friendInfo.id).then((response: any) => {
            if (response.status === 400) {
                Alert.alert('오류', '사용자 신고 실패.');
            } else if (response.status === 409) {
                Alert.alert('오류', '이미 해당 사용자를 신고했습니다.');
            } else if (response.status === 200) {
                setIsLoadingInView(false)
                ToastUtils.showToast({
                    text1: '신고가 정상적으로 접수되었습니다',
                    type: 'info',
                    position: 'bottom'
                })
            }
        }).catch((error: Error) => {
            Alert.alert('오류', '사용자 신고 실패.')
        }).finally(() => {
            setIsLoadingInView(false);
        });
    }

    const onClose = () => {
        props.navigation.goBack()
    }

    const renderGameItem = (item: any, index: number) => {
        return (
            <View key={index}
                style={styles.listItem}>
                <_Text numberOfLines={2} style={styles.nameListItem}>{item}</_Text>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <ModalUserReport
                visible={reportVisible}
                onClose={onCloseReportFriend}
                onSubmit={onChooseOption} />
            <_ReportModal
                friendName={data?.userName}
                userId={userId}
                visible={reportModalVisible}
                onClose={onCloseReportModal}
                onSubmit={submitReportModal} />
            <TouchableWithoutFeedback onPress={onClose}>
                <View style={styles.backgroundDropPress} />
            </TouchableWithoutFeedback>
            <View style={styles.wrapper}>
                <TouchableWithoutFeedback onPress={onClose}>
                    <View style={styles.backgroundDropPress} />
                </TouchableWithoutFeedback>
                <LoadingIndicator visible={isLoadingInView} />
                <View style={styles.wrapAvatar}>
                    <Image style={styles.topAvatar} source={data?.profilePicture ? { uri: data?.profilePicture } : Images.avatar_default} />
                </View>
                <ScrollView style={styles.content}>
                    {
                        userId !== friendInfo?.id &&
                        <TouchableOpacity style={styles.icThreeDots} onPress={onReportFriend}>
                            <Image source={Icons.ic_vertical_3_dots} />
                        </TouchableOpacity>
                    }
                    <View style={styles.wrapperItem}>
                        <_Text style={styles.username}>{data?.userName}</_Text>
                        {/* <_Text style={styles.idTxt}>ID: {data?.id}</_Text> */}
                        <_Text ellipsizeMode="tail" numberOfLines={3} style={styles.idTxt}>{data?.address} | {data?.age} | {data?.gender === 'male' ? '남성' : '여성'}</_Text>
                    </View>
                    <View style={styles.column}>
                        {data?.tags && data?.tags.map((item: any, index: number) => renderGameItem(item, index))}
                    </View>
                    <View style={styles.wrapLabel}>
                        <_Text style={styles.settingText}>자기소개</_Text>
                    </View>
                    {
                        (typeof data?.bio !== 'undefined' && data?.bio.length !== 0) &&
                        <View style={{ height: screen.heightscreen / 12 }}>
                            <MessageBubble content={data?.bio} />

                        </View>
                    }
                    <TouchableOpacity style={styles.btnSeeMore} onPress={onSeeMore}>
                        <_Text style={styles.titleBtnSeeMore}>자세히 보기</_Text>
                    </TouchableOpacity>
                    {
                        userId !== friendInfo?.id &&
                        <TouchableOpacity
                            style={[styles.btnRefreshRecomendedList,
                            { opacity: friendInfo?.isRequested || friendInfo?.isFriend === true ? 0.5 : 1 }]}
                            disabled={friendInfo?.isRequested || friendInfo?.isFriend}
                            onPress={onConfirmSendFQ}>
                            <View style={styles.wrapButtonText}>
                                <_Text style={styles.buttonText}>친구추가</_Text>
                            </View>
                            <View style={styles.btnGameController}>
                                <Image source={Icons.ic_game_controller} />
                                <_Text style={styles.numberGameControler}>{itemForRequest}</_Text>
                            </View>
                        </TouchableOpacity>
                    }

                </ScrollView>
            </View>
        </View>
    );
}

export default ModalViewProfile;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 16,
        alignItems: 'center',
        justifyContent: "center",
        backgroundColor: 'transparent',
    },
    backgroundDropPress: {
        position: 'absolute',
        zIndex: 0,
        height: screen.heightscreen,
        width: screen.widthscreen,
    },
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 10,
        paddingTop: 50,
        zIndex: 1,
        width: '100%',
        maxHeight: screen.heightscreen / 1.3,
    },
    content: {
        borderRadius: 13,
        backgroundColor: colors.white,
    },
    column: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: "wrap",
    },
    wrapperItem: {
        paddingTop: 60,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: screen.heightscreen / 32, // size 20
        width: '100%',
    },
    wrapLabel: {
        flexDirection: 'row',
        marginBottom: 10
    },
    wrapAvatar: {
        position: 'relative',
        zIndex: 10
    },
    topAvatar: {
        width: screen.widthscreen / 4,
        height: screen.widthscreen / 4,
        borderRadius: screen.widthscreen / 4 / 2,
        position: 'absolute',
        top: -50,
        left: -50,
        zIndex: 10
    },
    username: {
        color: colors.blue,
        fontSize: 16,
        fontWeight: '800',
        marginBottom: 10
    },
    idTxt: {
        fontWeight: '400',
        fontSize: 12,
        color: '#8F8F8F',
        paddingHorizontal: 12,
        textAlign: 'center'
    },
    icThreeDots: {
        position: 'absolute',
        top: 72,
        right: 18,
        zIndex: 10,
    },
    progressContainer: {
        marginTop: 10,
        alignItems: 'center'
    },
    flatList: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        flexDirection: "row",
        flexWrap: "wrap",
        marginHorizontal: 10,
        backgroundColor: "gray",

    },

    listItem: {
        borderColor: colors.mainColor,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 15,
        paddingVertical: 2,
        borderRadius: 30,
        borderWidth: 1,
    },
    nameListItem: {
        color: colors.mainColor,
        textAlign: 'center',
        fontSize: 12
    },
    settingText: {
        textAlign: 'left',
        marginLeft: 20,
        color: colors.blue,
        fontWeight: '800',
        fontSize: 16,
    },
    btnSeeMore: {
        marginVertical: 20,
        marginHorizontal: 20,
        backgroundColor: '#F8F3F3',
        borderRadius: 12,
        paddingVertical: 16
    },
    titleBtnSeeMore: {
        fontWeight: '800',
        fontSize: 20,
        textAlign: 'center'
    },
    btnRefreshRecomendedList: {
        marginBottom: 20,
        marginHorizontal: 20,
        borderRadius: 12,
        paddingVertical: 12,
        backgroundColor: colors.blue,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center'
    },
    wrapButtonText: {
        justifyContent: 'center',
        alignContent: 'center',
        paddingRight: 10
    },
    buttonText: {
        fontSize: 20,
        fontWeight: '800',
        textAlign: 'center',
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    btnGameController: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EEEEEF',
        width: 60,
        height: 40,
        borderRadius: 20
    },
    numberGameControler: {
        color: colors.black,
        fontWeight: '800',
        fontSize: 14,
        paddingLeft: 5
    },
});