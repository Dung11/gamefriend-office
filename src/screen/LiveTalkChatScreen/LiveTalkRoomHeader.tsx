import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Image,
    View,
    TouchableOpacity,
    Dimensions,
    FlatList,
    ScrollView,
    Alert
} from 'react-native';
import { renderItem, RootStackParamList } from '../../@types';
import { Icons, Images } from '../../assets';
import _Text from '../../components/_Text';
import { colors } from '../../config/Colors';
import ModalAddMember from './ModalAddMember';
import store from '../../redux/store';
import { memberLivetalkRequest } from '../../redux/members-livetalk/actions';

interface Props {
    navigation?: any
    roomId: string,
    userId: string,
    listData: any
}

export default ({
    navigation,
    roomId,
    userId,
    listData
}: Props) => {

    const [isShowAddMember, setIsShowAddMember] = useState(false);
    const [ownerProfile, setOwnerProfile] = useState();
    const [roomMembers, setRoomMembers] = useState();
    const [isShowRmMember, setIsShowRmMember] = useState(false);
    const [isOwner, setIsOwner] = useState();
    const [ownerData, setOwnerData] = useState<any>();

    const [isRoomFull, setIsRoomFull] = useState(false);
    const [isRoomEmpty, setIsRoomEmpty] = useState(false);

    useEffect(() => {
        getRoomInfo();

    }, [roomId, userId, listData]);

    const getRoomInfo = async () => {
        try {
            if (roomId != '' && userId != '' && listData.status !== undefined) {
                if (listData.status == 200) {
                    const roomMembers = listData?.data.room.members;

                    setOwnerData(listData?.data?.room?.owner)
                    setOwnerProfile(listData?.data?.room?.owner?.profilePicture);
                    setRoomMembers(listData?.data?.room?.members);
                    setIsOwner(listData?.data?.room?.isOwner);

                    if (roomMembers.length == listData.data.room.limit - 1) {
                        setIsRoomFull(true);
                        setIsRoomEmpty(false);
                    } else {
                        setIsRoomFull(false);
                        setIsRoomEmpty(false);
                    }

                    if (roomMembers.length == 0) {
                        setIsRoomEmpty(true);
                    }
                } else {
                    Alert.alert('오류33', '잠시 후 다시 시도해주세요');

                }
            }

        } catch (error) {
            console.log(error);
        }
    }

    const onShowProfile = (item: any) => {
        navigation.navigate("Modal", {
            screen: "ModalViewProfile", params: {
                friendInfo: item,
                itemForRequest: 1,
                userId: userId,
                roomId: roomId,
            }
        })
    }

    const onShowOwnerProfile = () => {
        navigation.navigate("Modal", {
            screen: "ModalViewProfile", params: {
                friendInfo: ownerData,
                itemForRequest: 1,
                userId: userId,
                roomId: roomId,
            }
        })
    }

    const onAddMember = () => {
        setIsShowAddMember(true);
    }

    const onRemoveMember = () => {
        setIsShowRmMember(true)
    }

    const _renderItem = ({ item, index }: renderItem) => {
        return (
            <TouchableOpacity onPress={() => onShowProfile(item)}>
                <Image style={styles.avatarMember} source={{ uri: item.profilePicture }} />
            </TouchableOpacity>
        );
    };
    const onBackdropPress = () => {
        setIsShowAddMember(false)
        setIsShowRmMember(false)
        let payload: any = { userId, roomId }
        store.dispatch(memberLivetalkRequest(payload));
    }
    return (
        <View>
            { isShowAddMember && <ModalAddMember
                roomId={roomId}
                userId={userId}
                type="add-member"
                visible={isShowAddMember}
                onBackdropPress={onBackdropPress} />}
            { isShowRmMember && <ModalAddMember
                roomId={roomId}
                userId={userId}
                type="rm-member"
                visible={isShowRmMember}
                onBackdropPress={onBackdropPress} />}
            <ScrollView horizontal contentContainerStyle={styles.container} showsHorizontalScrollIndicator={false}>
                <TouchableOpacity
                    style={styles.roomOwner}
                    onPress={onShowOwnerProfile}>
                    <View style={styles.content}>
                        <Image style={styles.profileImg} source={ownerProfile ? { uri: ownerProfile } : Images.avatar_default} />
                        <View style={styles.wrapName}>
                            <_Text style={styles.txtProfile}>방장</_Text>
                            <_Text numberOfLines={1} style={styles.txtProfile}>{ownerData?.userName}</_Text>
                        </View>
                    </View>
                </TouchableOpacity>
                {(isOwner && !isRoomFull) && <TouchableOpacity
                    onPress={onAddMember}
                    style={styles.addUser}>
                    <Image style={{ width: 35, height: 35 }} source={Icons.ic_add_user} />
                </TouchableOpacity>}
                <FlatList contentContainerStyle={styles.wrapRoomMembers}
                    horizontal={true}
                    keyExtractor={(item, index) => item.id}
                    data={roomMembers}
                    renderItem={_renderItem} />
                {(isOwner && !isRoomEmpty) && <TouchableOpacity onPress={onRemoveMember} style={styles.removeUser}>
                    <Image style={{ width: 35, height: 35 }} source={Icons.ic_remove_user} />
                </TouchableOpacity>}
            </ScrollView>
        </View>
    );
}

const screen = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 0,
        zIndex: -1
    },
    roomOwner: {
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 15,
        flexDirection: 'row',
        width: 145,
        height: 60,
        backgroundColor: colors.mainColor
    },
    wrapRoomMembers: {
        // backgroundColor: colors.mainColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    memberList: {
        flexGrow: 0
    },
    addUser: {
        paddingHorizontal: 10
    },
    removeUser: {
        paddingHorizontal: 10,
    },
    content: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10,
    },
    title: {
        marginHorizontal: 10,
        fontSize: screen.width / 20,
        color: colors.white,
        fontWeight: '900',
        textAlign: 'left',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    profileImg: {
        width: 40,
        height: 40,
        borderRadius: 40
    },
    txtProfile: {
        flex: 1,
        color: colors.white,
        // flexWrap: 'wrap',
        fontSize: 12,
        // margin: 10,
        fontWeight: '400',
        textAlign: 'center',
    },
    wrapName:{
        width: 90
    },
    avatarMember: {
        width: 40,
        height: 40,
        borderRadius: 50,
        marginHorizontal: 5
    },
    poin: {
        color: colors.white
    }
});