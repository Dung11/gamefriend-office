import React, { useEffect, useState } from 'react';
import { StyleSheet, View, TouchableOpacity, FlatList, ActivityIndicator, Image, Alert, Platform, Keyboard, Dimensions } from 'react-native';
import { profileRequest } from '../../redux/profile/actions';
import { profileLocalRequest } from '../../redux/profile-local/actions';
import { ChatLineHolder } from '../../components/ChatLineHolder/ChatLineHolder';
import { renderItem, RootStackParamList } from '../../@types';
import { ContainerMain } from '../../components/Container/ContainerMain';
import { connect, ConnectedProps } from 'react-redux';
import { RouteProp } from '@react-navigation/native';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { HeaderApp } from '../../components/Header/HeaderApp';
import { InputChat } from '../ChatDetailMatch/inputChat';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import { TipsItem } from '../../components/TipsItem/TipsItem';
import { screen } from '../../config/Layout';
import LiveTalkRoomHeader from './LiveTalkRoomHeader';
import LocalRoomHeader from './LocalRoomHeader';
import { colors } from '../../config/Colors';
import { Icons } from '../../assets';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { getChattingData, getTip } from '../../api/ChatServices';
import _ModalShowImage from '../../components/Modal/_ModalShowImage';
import _ChooseImageModal from '../../components/ChooseImageModal/_ChooseImageModal';
import { uploadFile } from '../../api/ImageServices';
import { emitEvent, ReceiveMessage, removeHandler } from '../../socketio/socketController';
import store, { AppState } from '../../redux/store';
import { memberLivetalkRequest } from '../../redux/members-livetalk/actions'
import { memberLivetalkLocalRequest } from '../../redux/members-livetalklocal/actions';
import { getLocalRoomChatMessages } from '../../api/LivetalkServices';
import _Text from '../../components/_Text';

const RNFS = require('react-native-fs');

type Params = {
    params: {
        roomId?: any;
        name: string;
        limit: number
        // nameFriend?: string;
        // roomType?: string;
        // roomName?: string;
        roomType?: any;
    }
}

interface Props {
    dataListMemberLocal: any,
    dataListMember: any
    route: RouteProp<Params, 'params'>
    navigation: DrawerNavigationProp<RootStackParamList>
}

const limit = 10;

const LiveTalkChatScreen = (props: Props) => {

    var currentDay: number | string = 0, currentMonth: number | string = 0, currentYear: number | string = 0;
    const [username, setUserName] = useState('');
    const [chatData, setChatData] = useState<any[]>([]);
    const [sortedData, setSortedData] = useState<any[]>([]);
    const [textMessage, setTextMessage] = useState('');
    const [isLoading, setLoading] = useState<boolean>(false);
    const [quantity, setQuantity] = useState<number>(1);
    const [page, setPage] = useState<number>(1);
    const [isHideFooter, setHideFooter] = useState<boolean>(true);
    const [userId, setUserId] = useState('');
    const [roomId, setRoomId] = useState('');
    const [newMessage, setNewMessage] = useState<any>();
    const [selectedImage, setSelectedImage] = useState<string>('');
    const [showingImage, setShowingImage] = useState<string>('');
    const [isShowCameraRoll, setShowCameraRoll] = useState<boolean>(false);
    const [isShowFullImage, setShowFullImage] = useState<boolean>(false);
    const [isItemTips, setIsItemTips] = useState(true);
    const [isLocal, setIsLocal] = useState(false);
    const [textTiplivetalk, setTextTiplivetalk] = useState([])
    const [textTiplivetalkLocal, setTextTiplivetalkLocal] = useState([])

    useEffect(() => {
        init();
        getTipLivetalk()
        setTimeout(() => {
            setIsItemTips(false);
        }, 10000);
        return () => {
            const pa = props.route.params as any;
            const roomType = props.route.params.roomType;

            if (roomType === 'livetalk') {
                removeHandler(`room:${pa.roomId}` as any);
                emitEvent('room-not-in-use', {
                    roomId: pa.roomId
                });
            } else if (roomType === 'livetalk-local') {
                removeHandler(`local-room:${pa.roomId}` as any);
                emitEvent('local-room-not-in-use', {
                    roomId: pa.roomId
                });
            }
        }
    }, []);

    useEffect(() => {
        if (newMessage)
            refreshChatMessages();
    }, [newMessage]);

    const init = async () => {
        const roomId = props.route.params.roomId;
        const roomType = props.route.params.roomType;

        const userId = await keyValueStorage.get("userID") as any;
        const username = await keyValueStorage.get("userName") as string;

        setUserId(userId);
        setRoomId(roomId);
        setIsLocal(roomType === 'livetalk' ? false : true);
        setUserName(username);
        getMember(roomId, userId);
        if (roomType === 'livetalk') {
            emitEvent('room-in-use', { roomId });
            await getHistoryChat();
        } else if (roomType === 'livetalk-local') {
            emitEvent('local-room-in-use', { roomId });
            await getLocalHistoryChat();
        }
        handleChatEvent(roomId);
    }

    const getMember = async (roomId: string, userId: string) => {
        const roomType = props.route.params.roomType;
        let payload: any = { userId, roomId };
        if (roomType === 'livetalk') {
            store.dispatch(profileRequest(payload));
            store.dispatch(memberLivetalkRequest(payload));
        } else if (roomType === 'livetalk-local') {
            store.dispatch(profileLocalRequest(payload));
            store.dispatch(memberLivetalkLocalRequest(roomId))
        }
    }
    const getTipLivetalk = async () => {
        try {
            let result = await getTip()
            if (result) {
                setTextTiplivetalk(result.room_livetalk_normal)
                setTextTiplivetalkLocal(result.room_livetalk_local)
            } else {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
            }
        } catch (error) {
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
            console.log(error)
        }
    }

    // listion event socket
    const handleChatEvent = async (roomId: string) => {
        const roomType = props.route.params.roomType;
        const eventRoomType = roomType === 'livetalk' ? 'room' : 'local-room';
        ReceiveMessage(`${eventRoomType}:${roomId}` as any, (data: any) => {
            let msg = JSON.parse(data);
            setNewMessage(msg);
        });
    }

    const onChangeMessage = (value: any) => {
        setTextMessage(value);
    }

    const onShowCameraRoll = () => {
        setShowCameraRoll(true);
    }

    const _sendMessage = async () => {
        const roomTypeEvent = isLocal === false ? 'chat' : 'local-chat';
        let data: any = { roomId: roomId }
        if (selectedImage) {
            let locations: Location[] = await sendImage();
            if (locations.length === 0) {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
                return;
            }
            data = {
                ...data,
                chatContent: {
                    chatType: 'image',
                    value: textMessage,
                    image: {
                        origin: locations[0].origin,
                        thumbnail: locations[0].thumbnail
                    }
                }
            };
            emitEvent(roomTypeEvent, data);
        } else {
            data = {
                ...data,
                chatContent: {
                    chatType: 'text',
                    value: textMessage
                }
            };
            emitEvent(roomTypeEvent, data);
        }

        setSelectedImage('');
        setTextMessage('')
    }

    const getImageExstension = (uri: string) => {
        if (uri) {
            const splitNameArr = uri.split('/');
            const filename = splitNameArr[splitNameArr.length - 1];
            const splitFilenameArr = filename.split('.');
            return splitFilenameArr[splitFilenameArr.length - 1];
        }
        return '';
    }

    const uploadPic = (uri: string) => {
        return new Promise<Location[]>(async (resolve, reject) => {
            let formdata = new FormData();
            const ext = getImageExstension(uri);
            const filename = `${Date.now()}.${ext}`;
            let file = {
                uri,
                type: 'image/jpeg',
                name: filename
            };
            formdata.append('photos', file);
            await uploadFile(formdata).then((response: UploadFileResponse) => {
                if (!response.isFailed) {
                    resolve(response.data.locations)
                }
                else {
                    Alert.alert('오류', '잠시 후 다시 시도해주세요');
                    resolve([]);
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                console.log('오류', error.message);
                resolve([]);
            })
        })
    }

    const sendImage = async () => {
        try {
            if (selectedImage) {
                setLoading(true);
                let currentImg = selectedImage;
                if (Platform.OS === 'ios') {
                    let photoPATH = `assets-library://asset/asset.JPG?id=${currentImg.substring(5)}&ext=JPG`;
                    const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                    currentImg = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
                }
                let locations = await uploadPic(currentImg);
                setLoading(false);
                return locations;
            }
            return [];
        } catch (error) {
            setLoading(false);
            return [];
        }
    }

    const refreshChatMessages = () => {
        if (newMessage.id === chatData[0]?.id) return;
        let newSortData = formatData([newMessage, ...chatData].reverse());
        setChatData([newMessage, ...chatData]);
        setSortedData(newSortData);
    }

    const checkNewDate = (checkDate: Date) => {
        let status = false;
        if (currentYear < checkDate.getFullYear()) {
            status = true;
        } else if (currentYear == checkDate.getFullYear()) {
            if (currentMonth < checkDate.getMonth() + 1) {
                status = true;
            } else if (currentMonth == checkDate.getMonth() + 1) {
                if (currentDay < checkDate.getDate()) {
                    status = true;
                }
            }
        }
        if (status) {
            currentYear = checkDate.getFullYear();
            currentMonth = checkDate.getMonth() >= 9 ? checkDate.getMonth() + 1 : `0${checkDate.getMonth() + 1}`;
            currentDay = checkDate.getDate() >= 10 ? checkDate.getDate() : `0${checkDate.getDate()}`;
        }
        return status;
    }

    const formatData = (mergedData: any) => {
        let formattedData = [];
        currentYear = 0;
        currentMonth = 0;
        currentDay = 0;
        for (let i = 0; i < mergedData.length; i++) {
            const createdTime = new Date(mergedData[i].createdAt);
            if (checkNewDate(createdTime)) {
                const hourStr = createdTime.getHours() >= 10 ? createdTime.getHours() : `0${createdTime.getHours()}`;
                const minuteStr = createdTime.getMinutes() >= 10 ? createdTime.getMinutes() : `0${createdTime.getMinutes()}`;
                const timeStr = `${currentYear}.${currentMonth}.${currentDay} ${hourStr}:${minuteStr}`;
                if (chatData.findIndex(a => a.id == `${currentYear}.${currentMonth}.${currentDay}`) == -1) {
                    formattedData.unshift({ id: `${currentYear}.${currentMonth}.${currentDay}`, date: timeStr });
                }
            }
            formattedData.unshift(mergedData[i]);
        }
        return formattedData;
    }

    const getHistoryChat = async () => {
        const routeParams: any = props.route.params;
        const userId = await keyValueStorage.get("userID") as any;

        try {
            if (chatData.length >= quantity) return;
            setHideFooter(false);
            const response = await getChattingData(userId, routeParams.roomId, page, limit);
            if (response) {
                setQuantity(response.quantity)
                let formattedData = formatData([...chatData, ...response.chats].reverse());
                setChatData([...chatData, ...response.chats]);
                setSortedData(formattedData);
                setPage(page + 1);
            }
            setHideFooter(true);
            setLoading(false);
        } catch (error) {
            console.log(error);
        }
    }

    const getLocalHistoryChat = async () => {
        const routeParams: any = props.route.params;

        try {
            if (chatData.length >= quantity) return;
            setHideFooter(false);
            const response = await getLocalRoomChatMessages(routeParams.roomId);
            if (response) {
                const chatObj = response.data;
                setQuantity(chatObj.quantity);
                let formattedData = formatData([...chatData, ...chatObj.chats].reverse());
                setChatData([...chatData, ...chatObj.chats]);
                setSortedData(formattedData);
                setPage(page + 1);
            }
            setHideFooter(true);
            setLoading(false);
        } catch (error) {
            console.log(error);
        }
    }

    const onMore = () => {
        Keyboard.dismiss();
        props.navigation.openDrawer();
    }

    const onBack = () => {
        props.navigation.goBack();
    }

    const onShowImage = (url: string) => {
        setShowingImage(url);
        setShowFullImage(true);
    }

    const onCloseModal = () => {
        setShowCameraRoll(false);
    }

    const onChangeModal = (locations: string[]) => {
        if (locations.length > 0) {
            var index = locations.length - 1
            setSelectedImage(locations[index].toString());
        }
        setShowCameraRoll(false);
    }

    const _listFooterComponent = () => {
        if (!isHideFooter) {
            return (
                <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size={36} color={colors.blue} />
                </View>
            )
        } else {
            return (<></>)
        }
    }

    const hideItemTips = () => {
        setIsItemTips(false);
    }

    const _renderChatLine = ({ item, index }: renderItem) => {
        if (item.date) {
            return (
                <View style={styles.dateMessageContainer}>
                    <_Text style={styles.messageDate}>{item.date}</_Text>
                </View>
            );
        }
        if (item?.sender?.userName === username) {
            return (
                <View style={{ paddingLeft: 30, alignItems: 'flex-end' }}>
                    <ChatLineHolder contentContainerStyle={{
                        backgroundColor: colors.mainColor,
                        borderBottomRightRadius: 0,
                    }}
                        textColor="white"
                        key={index}
                        item={item}
                        isSender={true}
                        onShowImage={onShowImage} />
                </View>
            );
        } else {
            return (
                <View style={{ paddingRight: 30 }}>
                    <ChatLineHolder contentContainerStyle={{
                        backgroundColor: colors.grayLight,
                        borderBottomLeftRadius: 0,
                    }}
                        profilePicture={item?.sender?.profilePicture}
                        key={index}
                        item={item}
                        isSender={false}
                        onShowImage={onShowImage} />
                </View>
            );
        }
    };

    return (
        <ContainerMain>
            <HeaderApp onBack={onBack} title={props?.route?.params?.name} onPress={onMore} />
            {(isShowFullImage && showingImage !== '') &&
                <_ModalShowImage
                    visible={isShowFullImage}
                    onClose={() => { setShowFullImage(false) }}
                    banner={showingImage} />}
            <_ChooseImageModal
                title={'최근에'}
                visible={isShowCameraRoll}
                doneButtonText={'완료'}
                onClose={onCloseModal}
                onChange={onChangeModal}
                numberOfImages={1} />
            <LoadingIndicator visible={isLoading} />
            <View style={styles.contentChat} >
                {isLocal === false ?
                    <LiveTalkRoomHeader roomId={roomId} userId={userId} navigation={props.navigation} listData={props.dataListMember} /> :
                    <LocalRoomHeader roomId={roomId} userId={userId} navigation={props.navigation} listData={props.dataListMemberLocal} />
                }
                {
                    isItemTips === true && (
                        (isLocal === true)
                            ? <TipsItem
                                text={textTiplivetalkLocal[0]}
                                onClose={hideItemTips}
                            />
                            : <TipsItem
                                text={textTiplivetalk[0]}
                                onClose={hideItemTips}
                            />
                    )

                }
                <FlatList
                    data={sortedData}
                    contentContainerStyle={styles.messageListContainer}
                    renderItem={_renderChatLine}
                    onEndReached={isLocal === false ? getHistoryChat : getLocalHistoryChat}
                    onEndReachedThreshold={0.7}
                    ListFooterComponent={_listFooterComponent}
                    inverted />
            </View>
            { selectedImage !== '' && <View style={{ backgroundColor: colors.white }}>
                <View style={styles.imgSelectedView}>
                    <Image source={{ uri: selectedImage }} style={styles.imgComment} />
                    <TouchableOpacity onPress={() => setSelectedImage('')}>
                        <Image source={Icons.ic_delete_circle} />
                    </TouchableOpacity>
                </View>
            </View>}
            <View style={styles.emptySpace} />
            <InputChat
                message={textMessage}
                image={selectedImage}
                onChangeMessage={onChangeMessage}
                _sendMessage={_sendMessage}
                onChooseImage={onShowCameraRoll}
            />
        </ContainerMain>
    );
}

const styles = StyleSheet.create({
    contentChat: {
        flex: 1
    },
    profileImg: {
        width: 40,
        height: 40,
    },
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    },
    imgComment: {
        height: screen.heightscreen / 10,
        width: screen.widthscreen / 7,
        marginRight: '2%',
        marginVertical: '3%',
        borderRadius: 8
    },
    imgSelectedView: {
        height: screen.heightscreen / 8,
        width: '80%',
        flexDirection: 'row',
        paddingVertical: '3%',
        marginHorizontal: '16%'
    },
    emptySpace: {
        height: 60
    },
    messageListContainer: {
        minHeight: '100%',
        justifyContent: 'flex-end',
        paddingHorizontal: 10,
    },
    dateMessageContainer: {
        width: Dimensions.get('screen').width,
        marginVertical: 5,
    },
    messageDate: {
        textAlign: 'center',
    },
});

const mapStateToProps = (state: AppState) => ({
    dataListMember: state.MemberLivetalkReducer.listMemberlivetalk,
    loading: state.MemberLivetalkReducer.loading,
    errorDataListMember: state.MemberLivetalkReducer.error,

    dataListMemberLocal: state.MemberLivetalkLocalReducer.listMemberivetalkLocal,
    errorDataListMemberLocal: state.MemberLivetalkLocalReducer.error,

});

const dispatch = {
    memberLivetalkRequest
};

export default connect(mapStateToProps, dispatch)(LiveTalkChatScreen);

interface UploadFileResponse {
    status: number,
    isFailed: boolean,
    data: {
        locations: Location[]
    }
}

type Location = {
    origin: string,
    thumbnail: string
}