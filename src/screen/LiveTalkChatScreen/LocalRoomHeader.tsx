import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Image,
    View,
    TouchableOpacity,
    Dimensions,
    FlatList,
    Alert
} from 'react-native';
import { renderItem, RootStackParamList } from '../../@types';
import _Text from '../../components/_Text';

interface Props {
    roomId: string,
    userId: string,
    navigation?: any
    listData: any
}

export default ({
    roomId,
    userId,
    navigation,
    listData
}: Props) => {

    const [roomMembers, setRoomMembers] = useState([]);
    const [friendInfo, setFriendInfo] = useState();

    useEffect(() => {
        getRoomInfo();
    }, [roomId,listData]);

    const getRoomInfo = async () => {
        try {
            if (roomId !== '' && listData.status !== undefined) {


                if (listData.status == 200) {
                    const roomMembers = listData?.data?.localRoom?.members;

                    setRoomMembers(roomMembers);
                }else{
                    Alert.alert('오류34', '잠시 후 다시 시도해주세요');
                }
            }

        } catch (error) {
            console.log(error);
        }
    }

    const onShowProfile = (item: any) => {
        setFriendInfo(item)
        navigation.navigate("Modal", {
            screen: "ModalViewProfile", params: {
                friendInfo: item,
                itemForRequest: 1,
                userId: userId,
                roomId: roomId,
                typeLocalLivetalk: true
            }
        })
    }

    const _renderItem = ({ item, index }: renderItem) => {
        return (
            <TouchableOpacity key={index} onPress={() => onShowProfile(item)}>
                <Image style={styles.avatarMember} source={{ uri: item.profilePicture }} />
            </TouchableOpacity>
        );
    };

    return (
        <>
            <View style={styles.container}>
                <View style={styles.wrapRoomMembers}>
                    <FlatList
                        horizontal={true}
                        keyExtractor={(item, index) => item.id}
                        data={roomMembers}
                        renderItem={_renderItem}
                    />
                </View>
            </View>
        </>
    );
}

const screen = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    wrapRoomMembers: {
        width: 300,
        // backgroundColor: colors.mainColor,
        marginVertical: 15,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    avatarMember: {
        width: 50,
        height: 50,
        borderRadius: 50,
        marginHorizontal: 5
    },
});
