
import React, { useState } from 'react';
import { StyleSheet, View, StatusBar, TouchableOpacity, Image, TextInput, Platform, ScrollView, Alert, Switch } from 'react-native';
import { colors } from '../../../config/Colors';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { Icons } from "../../../assets";
import { screen } from '../../../config/Layout';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import _ChooseImageModal from '../../../components/ChooseImageModal/_ChooseImageModal';
import { RouteProp } from '@react-navigation/native';
import DeviceInfo from 'react-native-device-info';
import _Text from '../../../components/_Text';
import { uploadFile } from '../../../api/ImageServices';
import { createNewPost } from '../../../api/PostServices';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import Environments from '../../../config/Environments';
import Icon from 'react-native-vector-icons/FontAwesome5';
const RNFS = require('react-native-fs');

type Params = {
    params: {
        resultCreatePost: any,
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
    route: RouteProp<Params, 'params'>
}

const CreateNewPost = ({ navigation, route }: Props) => {

    const [imageSelected, setImageSelected] = useState(false)
    const [isShowCamera, setShowCamera] = useState(false)
    const [imgPath, setImgPath] = useState('')
    const [txtTitle, setTxtTitle] = useState('')
    const [txtContent, setTxtContent] = useState('')
    const [gameId, setGameId] = useState('')
    const [gameName, setGameName] = useState('게시판 카테고리 선택')
    const [gameNameValue, setGameNameValue] = useState(false)
    const [isEnabledA, setIsEnableA] = useState(false)
    const [historyChooseGame, setHistoryChooseGame] = useState(null)
    const callbackCreatePost = route?.params?.resultCreatePost;
    const [imgHeight, setImgHeight] = useState(0);

    const onCloseModal = () => {
        setShowCamera(false)
    }

    const onChangeModal = (locations: string[]) => {
        if (locations.length > 0) {
            setImageSelected(true)
            setImgPath(locations[0].toString());
            Image.getSize(locations[0].toString(), (width, height) => {
                if (width > height) {
                    setImgHeight(200);
                } else {
                    setImgHeight(600);
                }
            });
        }
        setShowCamera(false);
    }

    const resultFavoriteGame = (value: any) => {
        if (value) {
            setGameId(value.id);
            setGameName(value.name);
            setGameNameValue(true);
            setHistoryChooseGame(value);
        }
    }

    const refreshGame = () => {
        setGameId('');
        setGameName('게시판 카테고리 선택');
        setGameNameValue(false);
        setHistoryChooseGame(null);
    }

    const uploadPic = (uri: string) => {
        return new Promise(async (resolve, reject) => {
            let formdata = new FormData();
            const ext = getImageExstension(uri);
            const filename = `${Date.now()}.${ext}`;
            let file = {
                uri,
                type: 'image/jpeg',
                name: filename
            };
            formdata.append('photos', file);
            await uploadFile(formdata).then((response: any) => {
                if (!response.isFailed) {
                    resolve(response.data.locations[0])
                    // resolve(true);
                }
                else {
                    resolve('');
                    Alert.alert('이미지 업로딩 실패', '용량이 너무 큰 이미지 혹은 잘못된 형식입니다.');
                    // reject(false);
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                // console.log('오류', error.message);
                reject(false);
            })
        })
    }

    const getImageExstension = (uri: string) => {
        if (uri) {
            const splitNameArr = uri.split('/');
            const filename = splitNameArr[splitNameArr.length - 1];
            const splitFilenameArr = filename.split('.');
            return splitFilenameArr[splitFilenameArr.length - 1];
        }
        return '';
    }

    const onRemoveImage = () => {
        setImageSelected(false);
        setImgPath('');
    }

    const createNewPostConfirm = async () => {
        try {
            if (txtTitle == '') return Alert.alert('오류', '제목을 입력해주세요');
            if (txtTitle.length > 256) return Alert.alert('제목이 너무 깁니다', '제목은 255자 미만으로 입력해주세요');
            if (txtContent == '') return Alert.alert('오류', '내용을 입력해주세요');

            let currentImg = imgPath;
            if (Platform.OS == 'ios' && currentImg?.length > 0) {
                let photoPATH = `assets-library://asset/asset.JPG?id=${currentImg.substring(5)}&ext=JPG`;
                const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                currentImg = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
            }

            let uploadPicRes = imgPath == '' ? { origin: '', thumbnail: '' } : await uploadPic(currentImg);
            if (uploadPicRes != '') {
                let userId: any = await keyValueStorage.get('userID');
                let body = {
                    title: txtTitle,
                    gameId: gameId,
                    text: txtContent,
                    pictures: [uploadPicRes],
                    isAnonymous: isEnabledA
                }
                let response = await createNewPost(userId, body);
                if (!response.isFailed) {
                    // come back Home
                    callbackCreatePost()
                    navigation.goBack()
                    return Alert.alert('게시글 작성 완료', '게시글이 성공적으로 등록되었습니다.');

                } else {
                    // show error
                    return Alert.alert('게시글 작성 실패', response.errors[0]);
                }
            }
            else {
                return Alert.alert('게시글 작성 실패', '조금 후에 다시 시도해주세요');
            }
        } catch (e) { }
    }

    return (
        <ContainerMain>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <_ChooseImageModal visible={isShowCamera} title={'최근에'} doneButtonText={'완료'} onClose={onCloseModal} onChange={onChangeModal} numberOfImages={1} />
            <View style={styles.header}>
                <TouchableOpacity style={styles.titleContainer}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={Icons.ic_back} />
                </TouchableOpacity>
                <_Text style={styles.txtTitle}>글쓰기</_Text>
                <TouchableOpacity style={styles.btnDone}
                    onPress={createNewPostConfirm}
                >
                    <_Text style={styles.txtDone}>완료</_Text>
                </TouchableOpacity>
            </View>
            <View style={styles.postView}>
                <TextInput style={styles.pTittle}
                    placeholder='제목'
                    onChangeText={text => { setTxtTitle(text) }}
                    defaultValue={txtTitle}
                />
                <View style={styles.line} />
                <View style={styles.anonymousImageView}>
                    <TouchableOpacity style={styles.btnSelectImage}
                        onPress={() => setShowCamera(true)}>
                        <Image source={Icons.ic_image} />
                    </TouchableOpacity>
                    <View style={styles.anonymousView}>
                        <_Text style={styles.txtAnonymous}>익명</_Text>
                        <Switch
                            trackColor={{ false: "#C4C4C6", true: '#4387EF' }}
                            thumbColor={isEnabledA ? colors.white : colors.white}
                            ios_backgroundColor={isEnabledA ? "#4387EF" : '#C4C4C6'}
                            onValueChange={() => setIsEnableA(!isEnabledA)}
                            value={isEnabledA}
                        />
                    </View>
                </View>
                <TouchableOpacity style={styles.wrapInput}
                    onPress={() => navigation.navigate('SearchFavouriteGame',
                        {
                            resultFavoriteGame,
                            numberOfChoice: 1,
                            historyChooseGame: historyChooseGame ? [historyChooseGame] : [],
                            isTier: false
                        })}>
                    <Image source={Icons.ic_search} />
                    <_Text style={gameNameValue ? styles.txtSearchBold : styles.txtSearch}>{gameName}</_Text>
                    {
                        gameId !== '' &&
                        <TouchableOpacity
                            style={styles.btnRefresh}
                            onPress={refreshGame}>
                            <Icon name="times" />
                        </TouchableOpacity>
                    }
                </TouchableOpacity>
                <_Text style={styles.txtUnderGameTag}>카테고리를 선택하지 않을 시 자유게시판에 작성됩니다.</_Text>
                <View style={styles.postContent}>
                    {!imageSelected ?
                        <TextInput style={styles.textInputContent}
                            multiline={true}
                            onChangeText={text => setTxtContent(text)}
                            defaultValue={txtContent} />
                        :
                        <ScrollView contentContainerStyle={{ paddingBottom: 450 }}
                            showsVerticalScrollIndicator={false}>
                            <TextInput style={styles.textInputContent}
                                multiline={true}
                                onChangeText={text => setTxtContent(text)}
                                defaultValue={txtContent}
                            />
                            <View style={styles.imgPostedView}>
                                <Image source={{ uri: imgPath }} resizeMode='stretch' style={{ width: '96%', height: imgHeight }} />
                                <TouchableOpacity style={styles.circleDelete} onPress={onRemoveImage}>
                                    <Image source={Icons.ic_remove} style={styles.icRemove} />
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    }
                </View>
            </View>
        </ContainerMain>
    );
};

const styles = StyleSheet.create({
    body: {
        backgroundColor: colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: colors.black,
    },
    header: {
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        paddingRight: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleContainer: {
        justifyContent: 'center',
    },
    txtTitle: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: '800',
        textAlign: 'center',
        fontSize: 16
    },
    txtDone: {
        fontSize: 17,
        color: 'white',
        fontWeight: "400",
        textAlign: 'right',
        alignSelf: 'center',
    },
    btnDone: {
        // marginTop: '3%',
    },
    postView: {
        marginTop: '10%',
        backgroundColor: 'white',
        minHeight: '90%',
    },
    pTittle: {
        color: "#65676B",
        fontSize: 14,
        fontWeight: '800',
        marginLeft: '3%',
        marginVertical: DeviceInfo.hasNotch() ? '3%' : '0%',
        height: Platform.OS == 'ios' ? 50 : 60,
        paddingHorizontal: 5,
        fontFamily: Environments.DefaultFont.Regular,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0
    },
    line: {
        backgroundColor: '#C6C6C8',
        height: 0.5,
        width: screen.widthscreen * 0.97,
        alignSelf: 'flex-end',
    },
    btnSelectImage: {
        margin: '3%',
    },
    wrapInput: {
        width: '95%',
        borderRadius: 15,
        backgroundColor: 'rgba(235, 235, 245, 0.3)',
        alignItems: 'center',
        paddingHorizontal: 10,
        marginBottom: 8,
        marginTop: '4%',
        paddingVertical: Platform.OS === 'ios' ? 20 : 10,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    txtSearch: {
        fontSize: 14,
        fontWeight: '400',
        color: '#C4C4C6',
        textAlign: 'right',
        marginLeft: '3%',
    },
    txtSearchBold: {
        fontSize: 14,
        fontWeight: '900',
        color: '#000',
        textAlign: 'right',
        marginLeft: '3%',
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: screen.widthscreen / 30,
        width: '90%',
    },
    postContent: {
        backgroundColor: 'rgba(235, 235, 245, 0.3)',
        borderRadius: 15,
        width: '95%',
        alignSelf: 'center',
        marginTop: '5%',
        padding: '3%',
    },
    textInputContent: {
        padding: '3%',
        minHeight: screen.heightscreen / 3,
        textAlignVertical: 'top',
        fontFamily: Environments.DefaultFont.Regular
    },
    imgPostedView: {
        // padding: '3.5%',
        // marginTop: '3%',
        // width: '100%',

    },
    icRemove: {
        alignSelf: 'flex-end',
    },
    imgPosted: {
        width: '100%',
        height: '100%'
    },
    scrollView: {

    },
    circleDelete: {
        alignSelf: 'flex-end',
        position: 'absolute',
        borderRadius: 50,
        top: -12,
        right: 0
    },
    anonymousImageView: {
        flexDirection: 'row',
    },
    anonymousView: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtAnonymous: {
        fontWeight: '400',
        color: '#000000',
        width: screen.widthscreen * 0.71,
        textAlign: 'right',
        marginRight: '3%',
    },
    txtUnderGameTag: {
        textAlign: 'left',
        marginLeft: '3.5%',
    },
    btnRefresh: {
        width: 20,
        height: 20,
        position: 'absolute',
        right: 0,
        justifyContent: 'center'
    }
});

export default CreateNewPost;
