import React from 'react';
import { View, StatusBar, TouchableOpacity, Image, TextInput, FlatList, Alert, Keyboard, ViewStyle } from 'react-native';
import { colors } from '../../config/Colors';
import { ContainerMain } from '../../components/Container/ContainerMain';
import { Icons } from "../../assets";
import { TabView, TabBar, SceneRendererProps, NavigationState } from 'react-native-tab-view';
import NewList from './TabScreens/newList';
import PopularList from './TabScreens/popularList';
import MineList from './TabScreens/mineList';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../@types';
import messaging from '@react-native-firebase/messaging';
import _Text from '../../components/_Text';
import LoadingIndicator from '../../components/LoadingIndicator/LoadingIndicator';
import { bulletinValueStorage } from '../../storage/bulletinValueStorage';
import _ModalShowImage from '../../components/Modal/_ModalShowImage';
import { getUserPicture } from '../../api/UserServices';
import { getPostData, getUserPostData } from '../../api/PostServices';
import styles from './styles';
import { deleteGameTag, getGameFilter } from '../../api/GameServices';
import { keyValueStorage } from '../../storage/keyValueStorage';
import Icon from 'react-native-vector-icons/FontAwesome5';

type State = NavigationState<{
    key: string;
    title: string;
}>;

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}

const limit = 10;

class BulletinScreen extends React.Component<Props> {

    _unsubscribe: any;

    pageNew: number = 1;
    pagePopular: number = 1;
    pageMyPost: number = 1;
    keyboardDidHide: any;
    keyboardDidShow: any;

    state = {
        index: 0,
        type: 'new',
        listTags: [],
        routes: [
            { key: 'new', title: '최신' },
            { key: 'popular', title: '인기' },
            { key: 'myposts', title: '내 게시글' },
        ],
        newList: [],
        popularList: [],
        myPosts: [],
        text: '', // title
        gameTag: '',
        isloading: false,
        isViewNotify: false,
        historyChooseGame: null,
        showingImage: null,
        visibleShowImg: false,
        isLoadingAll: true,
        selectedTagId: '',
        refreshing: false,
        newPostQuantity: 1,
        popularPostQuantity: 1,
        myPostQuantity: 1,
        addNewBtnVisible: true,
    };

    /// delete data search live talk
    removeKeySeacrh = async () => {
        await keyValueStorage.delete("datasearchlivetalk");
    }

    async componentDidMount() {
        try {
            const { navigation } = this.props;
            this._unsubscribe = navigation.addListener('focus', async () => {
                this.setState({ isLoadingAll: true });
                let { type }: any = this.state;
                await Promise.all([
                    this.getGameFilterTag(),
                    this.getPostOptionData(type, true)
                ]).finally(() => {
                    this.setState({ isLoadingAll: false });
                });
                this.removeKeySeacrh();
            });

            var isView = await bulletinValueStorage.get('isViewNotify');
            var isViewNoti = isView == 'false' ? false : true;
            this.setState({ isViewNotify: isViewNoti });
            messaging().onMessage(async remoteMessage => {
                if (remoteMessage?.data?.type == 'post-commented') {
                    await bulletinValueStorage.save('isViewNotify', 'false');
                    this.setState({ isViewNotify: false });
                }
            });
            this.keyboardDidHide = Keyboard.addListener('keyboardDidHide', (e) => {
                this.setState({ addNewBtnVisible: true });
            });
            this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', (e) => {
                this.setState({ addNewBtnVisible: false });
            });
        } catch (error) {

        } finally {
            this.setState({ isLoadingAll: false });
        }
    }

    componentWillUnmount() {
        this._unsubscribe();
        this.keyboardDidHide?.remove();
        this.keyboardDidShow?.remove();
    }

    private handleIndexChange = (index: number) => {
        let { routes } = this.state;
        let currentRoute = routes[index];
        this.setState({ index, type: currentRoute.key });
    }

    private backAction: any = () => {
        this.setState({ visibleShowImg: false });
    }

    getPostOptionData = async (
        postType: 'new' | 'popular' | 'myposts',
        isRefresh = false,
        isFilter = false,
        isLoadMore = false) => {
        try {
            let userId: any = await keyValueStorage.get('userID');
            let { gameTag, text, newList, popularList, myPosts, myPostQuantity, popularPostQuantity, newPostQuantity } = this.state;
            if (!isLoadMore) {
                if (!isRefresh) {
                    if (postType === 'new' && newList.length !== 0) return;
                    if (postType === 'popular' && popularList.length !== 0) return;
                    if (postType === 'myposts' && myPosts.length !== 0) return;
                } else {
                    this.pageNew = 1;
                    this.pagePopular = 1;
                    this.pageMyPost = 1;
                }
            } else {
                if (postType === 'new' && newList.length >= newPostQuantity) return;
                if (postType === 'popular' && popularList.length >= popularPostQuantity) return;
                if (postType === 'myposts' && myPosts.length >= myPostQuantity) return;
            }
            this.setState({ isloading: true });

            let page = this.pagePopular;
            if (postType === 'new') {
                page = this.pageNew;
            } else if (postType === 'myposts') {
                page = this.pageMyPost;
            }

            let params: any = {
                gameId: gameTag,
                title: text,
                page,
                limit,
            };
            if (postType !== 'myposts') {
                params = {
                    ...params,
                    sort: postType,
                };
            }
            let postResData = null;
            if (postType === 'myposts') {
                postResData = await getUserPostData(userId, params);
            } else {
                postResData = await getPostData(params);
            }
            let list = postResData.posts;
            let quantity = postResData.quantity;

            switch (postType) {
                case 'new':
                    this.setState({
                        newList: page == 1 ? list : newList.concat(list),
                        newPostQuantity: quantity,
                    });
                    this.pageNew++;
                    break;
                case 'popular':
                    this.setState({
                        popularList: page == 1 ? list : popularList.concat(list),
                        popularPostQuantity: quantity,
                    });
                    this.pagePopular++;
                    break;
                default:
                    this.setState({
                        myPosts: page == 1 ? list : myPosts.concat(list),
                        myPostQuantity: quantity
                    });
                    this.pageMyPost++;
                    break;
            }

            if (isFilter) {
                this.getGameFilterTag();
            }

        } catch (error) {
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        } finally {
            this.setState({ isloading: false });
        }
    }

    getGameFilterTag = async () => {
        let userId: any = await keyValueStorage.get('userID');
        let data = await getGameFilter(userId);
        let listTags = data;
        let newListTags = [{
            id: '',
            name: '자유',
            platform: 'mobile',
            canDelete: false
        }, ...listTags];

        this.setState({ listTags: newListTags });
    }

    deleteTag = async (gameId: string) => {
        try {
            let userId: any = await keyValueStorage.get('userID');
            let responseData = await deleteGameTag(userId, gameId);
            if (responseData) {
                let { selectedTagId, type }: any = this.state;
                let listTags = responseData.gameFilters
                let newListTags = [
                    { id: '', name: '자유', platform: 'mobile', selected: true, canDelete: false },
                    ...listTags
                ];
                this.setState({ listTags: newListTags });
                if (selectedTagId === gameId) {
                    this.setState({ selectedTagId: '', gameTag: '' }, async () => {
                        await this.getPostOptionData(type, true);
                    });
                }
            }
            else {
                Alert.alert('카테고리 삭제 실패', '잠시 후 다시 시도해주세요.');
            }
        } catch (error) {
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        }
    }

    private renderTabBar = (
        props: SceneRendererProps & { navigationState: State }
    ) => (
        <View style={styles.loadingView}>
            <TabBar
                {...props}
                scrollEnabled
                indicatorStyle={styles.indicator}
                style={styles.tabbar}
                tabStyle={styles.tab}
                labelStyle={styles.label}
                onTabPress={async ({ route }) => {
                    switch (route.key) {
                        case 'new':
                            await this.getPostOptionData('new');
                            break;
                        case 'popular':
                            await this.getPostOptionData('popular');
                            break;
                        case 'myposts':
                            await this.getPostOptionData('myposts');
                            break;
                    }
                }}
            />
        </View>
    );

    onCloseModalShowImage = () => {
        this.setState({ visibleShowImg: false })
    }

    onClickAvatar = async (id: string, userName: string) => {
        if (userName !== '익명') {
            let pictures = await getUserPicture(id);
            this.setState({
                showingImage: pictures,
                visibleShowImg: true,
            });
        }
    }

    onClickImage = (url: string) => {
        console.log('url: ', url);
        this.setState({
            showingImage: url,
            visibleShowImg: true,
        });
    }

    onChangeTag = async (item: any) => {
        let { type, selectedTagId }: any = this.state;
        if (item.id === selectedTagId) return;
        this.setState({
            gameTag: item.id,
            selectedTagId: item.id,
            newList: [],
            popularList: [],
            myPosts: [],
        }, async () => {
            await this.getPostOptionData(type, true);
        });
    }

    render() {
        const { navigation } = this.props
        let {
            visibleShowImg,
            showingImage,
            type,
            isLoadingAll,
            isloading,
            refreshing,
            newList,
            listTags,
            popularList,
            myPosts,
            addNewBtnVisible,
        }: any = this.state;

        const resultFavoriteGame = async (value: any) => {
            this.setState({ historyChooseGame: value });
            if (value) {
                var gameId = value.id;
                this.setState({ gameTag: gameId, selectedTagId: gameId }, async () => {
                    await this.getPostOptionData(type, true, true);
                });
            }
        }

        const resultCreatePost = async () => {
            await this.getPostOptionData(type, true);
        }

        const renderTagItem = ({ item, index }: any) => {
            let { selectedTagId } = this.state;
            const name = item.name || '자유';
            return (
                <TouchableOpacity style={item.id === selectedTagId ? styles.tagItemActive : styles.tagItemInActive}
                    onPress={async () => this.onChangeTag(item)}>
                    <_Text style={item.id === selectedTagId ? styles.txtTagActive : styles.txtTagInActive}>{name}</_Text>
                    {item.canDelete &&
                        <TouchableOpacity style={styles.btnDeleteTag} onPress={async () => await this.deleteTag(item.id)}>
                            <Icon name="times" style={styles.btnDeleteIcon} />
                        </TouchableOpacity>}
                </TouchableOpacity>
            );
        }

        const onChangeSearch = (text: any) => {
            this.setState({ text: text })
        }

        const onSubmitSearchText = () => {
            var text = this.state.text
            this.setState({
                text,
                newList: [],
                popularList: [],
                myPosts: [],
            }, () => {
                setTimeout(async () => {
                    await this.getPostOptionData(type, true);
                }, 500);
            });
        }

        const createPostBtnStyles: ViewStyle = {
            ...styles.btnPost,
            bottom: addNewBtnVisible ? '2%' : -500,
        }

        const onShowUserDetail = async (userName: string, friendId: string) => {
            let userId: any = await keyValueStorage.get('userID');
            if (userName !== '익명') {
                navigation.navigate('ViewFullProfile', {
                    userId,
                    friendId,
                    type: 'friendRequestNormalRecommendation',
                });
            }
        }

        return (
            <ContainerMain>
                {visibleShowImg && <_ModalShowImage
                    visible={visibleShowImg}
                    onClose={this.onCloseModalShowImage}
                    banner={showingImage} />}
                <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
                <View style={styles.header}>
                    <TouchableOpacity style={styles.titleContainer}
                        onPress={async () => {
                            navigation.navigate('Notify')
                            this.setState({ isViewNotify: true });
                            await bulletinValueStorage.save('isViewNotify', 'true');
                        }}>
                        <Image source={this.state.isViewNotify ? Icons.ic_bell_full : Icons.ic_bell} />
                    </TouchableOpacity>
                </View>

                <View style={styles.wrapInput}>
                    <Image source={Icons.ic_search} />
                    <TextInput style={styles.textInput}
                        onChangeText={onChangeSearch}
                        placeholder='검색어를 입력해주세요'
                        onSubmitEditing={onSubmitSearchText}
                    />
                </View>
                <View style={styles.tagView}>
                    {(listTags && listTags.length > 0) &&
                        <View style={styles.tagsView}>
                            <FlatList horizontal
                                contentContainerStyle={{ alignItems: 'center' }}
                                data={listTags}
                                numColumns={1}
                                showsHorizontalScrollIndicator={false}
                                renderItem={renderTagItem} />
                        </View>
                    }
                    <TouchableOpacity style={styles.btnAdd}
                        onPress={() => navigation.navigate('SearchFavouriteGame', {
                            resultFavoriteGame,
                            numberOfChoice: 1,
                            historyChooseGame: listTags || [],
                            isTier: false,
                        })}>
                        <Image source={Icons.ic_addbulletin} style={styles.imgAdd} />
                    </TouchableOpacity>
                </View>
                <View style={styles.tabViewContainer}>
                    <TabView
                        style={styles.tabView}
                        navigationState={this.state}
                        renderTabBar={this.renderTabBar}
                        onIndexChange={this.handleIndexChange}
                        renderScene={({ route }) => {
                            const generalParams = {
                                navigation,
                                onClickAvt: this.onClickAvatar,
                                onClickImg: this.onClickImage,
                                refreshing,
                                callBackNewPost: async () => await this.getPostOptionData(route.key as any, true),
                                onLoadMore: async () => await this.getPostOptionData(route.key as any, false, false, true),
                                onShowUserDetail
                            }
                            switch (route.key) {
                                case 'new':
                                    return <NewList {...generalParams} newList={newList} />
                                case 'popular':
                                    return <PopularList {...generalParams} popularList={popularList} />
                                case 'myposts':
                                    return <MineList {...generalParams} myPosts={myPosts} />
                            }
                        }} />
                </View>
                <TouchableOpacity style={createPostBtnStyles}
                    onPress={() => navigation.navigate('CreateNewPost', { resultCreatePost })}>
                    <Image source={Icons.ic_plussign} />
                </TouchableOpacity>
                {!isLoadingAll && <LoadingIndicator visible={isloading} />}
                <LoadingIndicator visible={isLoadingAll} />
            </ContainerMain>
        );
    }
}

export default BulletinScreen;
