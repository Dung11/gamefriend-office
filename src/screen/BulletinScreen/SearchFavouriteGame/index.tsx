import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack/lib/typescript/src/types';
import React, { useEffect, useState } from 'react';
import { StatusBar, TextInput, StyleSheet, View, Image, TouchableOpacity, FlatList, Alert, Platform } from "react-native";
import { ScrollView } from 'react-native-gesture-handler';
import { RootStackParamList } from '../../../@types';
import { Icons } from '../../../assets';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import DeviceInfo from 'react-native-device-info';
import LoadingIndicator from '../../../components/LoadingIndicator/LoadingIndicator';
import _Text from '../../../components/_Text';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getListGame } from '../../../api/GameServices';
import Environments from '../../../config/Environments';

type Params = {
    params: {
        result: any,
        resultFavoriteGame: any,
        numberOfChoice: number,
        historyChooseGame: any[],
        isTier: boolean
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export const SearchFavouriteGame = ({
    navigation, route
}: Props) => {
    const [fullLisDataPC, setFullListDataPC] = useState<any[]>([]);
    const [fullLisDataMB, setFullListDataMB] = useState<any[]>([]);
    const [fullLisDataST, setFullListDataST] = useState<any[]>([]);

    const [showItemPC, setShowItemPC] = useState(true);
    const [showItemMB, setShowItemMB] = useState(true);
    const [showItemST, setShowItemST] = useState(true);
    const [lisDataPC, setListDataPC] = useState<any[]>([]);
    const [lisDataMB, setListDataMB] = useState<any[]>([]);
    const [lisDataST, setListDataST] = useState<any[]>([]);
    const [search, setSearch] = useState('');
    const [dataResult, setDataResult] = useState<any>();
    const [data, setData] = useState<any[]>([]);
    const [isDisable, setIsDisable] = useState(true);
    const [isLoading, setIsLoading] = useState(true);
    const callbackFavorite = route?.params?.resultFavoriteGame;
    const callback = route?.params?.resultFavoriteGame;
    const numberOfChoice = route?.params?.numberOfChoice;

    const [lisTags, setListTags] = useState<any[]>([]);
    const [bannedTags, setBannedListTags] = useState<any[]>(route?.params?.historyChooseGame);

    const onNext = () => {
        let searchData: any = {
            name: search
        }
        if (searchData.name !== '') {
            setDataResult(searchData.search);
        }
        if (numberOfChoice === 1) {
            callbackFavorite(dataResult);
        } else {
            callback(data);
        }
        navigation.goBack();
    }

    const onDropDowmPC = () => {
        setShowItemPC(!showItemPC);
    }

    const onDropDowmMB = () => {
        setShowItemMB(!showItemMB);
    }

    const onDropDowmST = () => {
        setShowItemST(!showItemST);
    }

    const onChangeSearch = (value: any) => {
        setSearch(value);
        if (value.length !== 0) {
            let searchValue = value.toLowerCase();

            let filteredPC = fullLisDataPC.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);
            let filteredST = fullLisDataST.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);
            let filteredMB = fullLisDataMB.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);

            setListDataPC(filteredPC);
            setListDataST(filteredST);
            setListDataMB(filteredMB);

            setShowItemPC(true);
            setShowItemST(true);
            setShowItemMB(true);
        } else {
            let fullPC = [...fullLisDataPC];
            let fullST = [...fullLisDataST];
            let fullMB = [...fullLisDataMB];
            setListDataPC(fullPC);
            setListDataST(fullST);
            setListDataMB(fullMB);

            setShowItemPC(true);
            setShowItemST(true);
            setShowItemMB(true);
        }
    }

    const checkIsExisted = (_id: number) => {
        let index = data.findIndex((d) => d.id === _id);
        if (index != -1) return true;
        return false;
    }

    const checkIsBanned = (item: any) => {
        if (!item) {
            return false;
        }
        let itemIndex = bannedTags.findIndex((d: any) => d.name === item.name);
        if (itemIndex >= 0) {
            return true;
        }
        return false;
    }

    const onClickGame = (item: any, index: number) => {
        let tmpData = [...data];
        if (numberOfChoice === 1) {
            let checkExisted = checkIsExisted(item.id);
            if (!checkExisted) {
                setIsDisable(false)
                tmpData = [];
                tmpData.push(item);
                setDataResult(item)
            } else {
                setDataResult({})
                setIsDisable(true)
                tmpData = tmpData.filter((d) => d.id !== item.id);
            }
        } else {
            let checkExisted = checkIsExisted(item.id);
            if (!checkExisted) {
                setIsDisable(false)
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `${numberOfChoice}개까지만 선택 가능합니다`)
                } else {
                    tmpData.push(item);
                }
            } else {
                setIsDisable(true)
                if (tmpData.length === 1) {
                    Alert.alert('오류', '1 개 이상 선택')
                    return;
                }
                tmpData = tmpData.filter((d) => d.id !== item.id);
            }
        }
        setListTags(tmpData);
        setData(tmpData)
    }

    const getGame = () => {
        const { isTier } = route?.params
        Promise.all([
            getListGame('pc', isTier),
            getListGame('mobile', isTier),
            getListGame('steam', isTier)
        ]).then((responses: any) => {
            setIsLoading(false);

            setListDataPC(responses[0]?.data.games);
            setFullListDataPC(responses[0]?.data.games);

            setListDataMB(responses[1]?.data.games);
            setFullListDataMB(responses[1]?.data.games);

            setListDataST(responses[2]?.data.games);
            setFullListDataST(responses[2]?.data.games);
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            setIsLoading(false);
        });
    }

    useEffect(() => {
        getGame();
        const { historyChooseGame } = route?.params;
        console.log('historyChooseGame: ', historyChooseGame);
        setIsDisable(historyChooseGame ? false : true)
        historyChooseGame.length > 0 && setDataResult(historyChooseGame[0])
    }, [])

    const renderItemPC = (item: any, index: number) => {
        return (
            <TouchableOpacity key={index}
                disabled={checkIsBanned(item)}
                style={[styles.listItem, checkIsExisted(item.id) === true &&
                    { borderColor: colors.mainColor, backgroundColor: colors.white },
                { opacity: checkIsBanned(item) === true ? .5 : 1 }
                ]}
                onPress={() => onClickGame(item, index)}>
                <_Text numberOfLines={2} style={[
                    styles.nameListItem,
                    { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }
                ]}>{item.name}</_Text>
            </TouchableOpacity>

        )

    }
    const renderItemMB = (item: any, index: number) => {
        return (
            <TouchableOpacity key={index}
                disabled={checkIsBanned(item)}
                style={[styles.listItem, checkIsExisted(item.id) === true &&
                    { borderColor: colors.mainColor, backgroundColor: colors.white },
                { opacity: checkIsBanned(item) === true ? .5 : 1 }]}
                onPress={() => onClickGame(item, index)}>
                <_Text numberOfLines={2} style={[
                    styles.nameListItem,
                    { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }
                ]}>{item.name}</_Text>
            </TouchableOpacity>

        )
    }
    const renderItemST = (item: any, index: number) => {
        return (
            <TouchableOpacity key={index}
                disabled={checkIsBanned(item)}
                style={[styles.listItem, checkIsExisted(item.id) === true &&
                    { borderColor: colors.mainColor, backgroundColor: colors.white },
                { opacity: checkIsBanned(item) === true ? .5 : 1 }]}
                onPress={() => onClickGame(item, index)}>
                <_Text numberOfLines={2} style={[
                    styles.nameListItem,
                    { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }
                ]}>{item.name}</_Text>
            </TouchableOpacity>

        )
    }

    const renderTagItem = ({ item, index }: any) => {
        let dataTmp = [...lisTags];
        if (item.hasOwnProperty('canDelete') && item.canDelete === false) {
            return <></>
        }
        const render = dataTmp.length > 0 ?
            <View key={index} style={styles.tagItemActive}>
                <_Text style={styles.txtTagActive}>{item.name}</_Text>
                <TouchableOpacity onPress={() => onClickGame(item, index)} style={styles.tagCloseBtn}>
                    {/* <_Text style={styles.txtX}>x</_Text> */}
                    <Icon name="times" style={styles.tagCloseIcon} />
                </TouchableOpacity>
            </View>
            : null
        return render
    }

    return (
        <ContainerMain>
            <LoadingIndicator visible={isLoading} />
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image source={Icons.ic_back} style={styles.backIcon} />
                </TouchableOpacity>
                <TouchableOpacity disabled={isDisable} style={styles.btnDone} onPress={onNext}>
                    <_Text style={styles.txtDone}>완료</_Text>
                </TouchableOpacity>
            </View>

            <View style={styles.wrapInput}>
                <Image source={Icons.ic_search} />
                <TextInput style={styles.textInput}
                    // value={search} 
                    // onChangeText={(value: any) => { onChangeSearch(value) }} 
                    placeholder='게임명을 검색해주세요'
                    value={search}
                    onChangeText={(value: any) => { onChangeSearch(value) }}
                />
            </View>
            <View style={styles.wrapDataItem}>
                <FlatList
                    horizontal
                    data={lisTags}
                    numColumns={1}
                    renderItem={renderTagItem}
                    keyExtractor={item => item.id}
                />

            </View>
            <ScrollView indicatorStyle="white" contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false}>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmPC} style={[styles.wrapDropDowm, { backgroundColor: showItemPC ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>PC게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemPC ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_raphael_pc} />
                    </TouchableOpacity>
                    {showItemPC &&
                        <ScrollView contentContainerStyle={styles.flatList}>
                            {
                                lisDataPC.map((item: any, index: number) => renderItemPC(item, index))
                            }
                        </ScrollView>
                    }
                </View>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmMB} style={[styles.wrapDropDowm, { backgroundColor: showItemMB ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>Mobile게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemMB ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_mobile_vibration} />
                    </TouchableOpacity>
                    {showItemMB &&
                        <ScrollView contentContainerStyle={styles.flatList}>
                            {
                                lisDataMB.map((item: any, index: number) => renderItemMB(item, index))
                            }
                        </ScrollView>
                    }
                </View>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmST} style={[styles.wrapDropDowm, { backgroundColor: showItemST ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>Steam게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemST ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_cib_steam} />
                    </TouchableOpacity>
                    {
                        showItemST &&
                        <ScrollView contentContainerStyle={styles.flatList}>
                            {
                                lisDataST.map((item: any, index: number) => renderItemST(item, index))
                            }
                        </ScrollView>
                    }
                </View>
            </ScrollView>
        </ContainerMain>
    )
}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        paddingVertical: 30,
    },
    wrapInput: {
        width: screen.widthscreen - 20,
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        marginBottom: 8,
        marginTop: '7%',
        paddingVertical: DeviceInfo.hasNotch() ? 10 : 0,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    wrapDataItem: {
        width: '95%',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingVertical: DeviceInfo.hasNotch() ? 15 : 0,
        flexDirection: 'row'
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: 14,
        width: '90%',
        fontWeight: '400',
        fontFamily: Environments.DefaultFont.Regular,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        height: Platform.OS == 'android' ? 50 : 'auto',
    },
    txtSearch: {
        fontSize: 14,
        fontWeight: '400',
        color: '#C4C4C6',
        textAlign: 'right',
        marginLeft: '3%',
    },
    listItem: {
        borderColor: '#EDEDED',
        backgroundColor: '#EDEDED',
        height: screen.heightscreen / 23,
        //width: screen.widthscreen / 4,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: screen.heightscreen / 70,
        paddingVertical: 5,
        borderRadius: 30,
        borderWidth: 1,
        marginBottom: screen.heightscreen / 70,
    },
    nameListItem: {
        color: '#65676B',
        textAlign: 'center',
        fontSize: 12
    },
    wrapItem: {
        paddingHorizontal: 3,
        paddingVertical: 3,
        backgroundColor: colors.white,
        borderRadius: 10,
        marginVertical: 10,
        width: screen.widthscreen - 30,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    wrapDropDowm: {
        paddingHorizontal: 15,
        justifyContent: "flex-end",
        borderRadius: 10,
        backgroundColor: 'white',
        width: screen.widthscreen - 36,
        height: screen.heightscreen / 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    labelDropDowm: {
        color: colors.mainColor,
        fontSize: 14,
    },
    flatList: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    contentContainer: {
        flexGrow: 1,
        flexDirection: 'column',
    },
    header: {
        minHeight: 60,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.mainColor,
        paddingRight: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    backIcon: {
        width: 35,
        height: 35,
    },
    titleContainer: {
        justifyContent: 'center',
    },
    txtTitle: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: '800',
        textAlign: 'center',
        fontSize: 16,
    },
    txtDone: {
        fontSize: 20,
        color: 'white',
        fontWeight: '900',
        textAlign: 'right',
        alignSelf: 'center',
    },
    btnDone: {
        paddingLeft: 30,
    },
    tagItemActive: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        paddingLeft: screen.widthscreen / 25,
        paddingRight: screen.widthscreen / 25 + 5,
        borderColor: '#4387EF',
        backgroundColor: 'rgba(67, 135, 239, 0.24)',
        marginHorizontal: screen.widthscreen / 70,
        paddingVertical: 7
    },
    tagCloseBtn: {
        position: 'absolute',
        right: 10,
    },
    tagCloseIcon: {
        color: colors.blue,
    },
    txtTagActive: {
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 20,
        color: colors.blue,
        textAlign: 'center',
        marginRight: screen.widthscreen / 40,
    },
    txtX: {
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 20,
        color: colors.blue,
        textAlign: 'center',
    },
    flatListTag: {
        alignItems: 'center',
        marginTop: 10,
    },
});
