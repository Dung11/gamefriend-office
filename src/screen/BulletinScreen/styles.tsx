import { Platform, StyleSheet } from "react-native";
import { colors } from "../../config/Colors";
import { screen } from "../../config/Layout";
import DeviceInfo from 'react-native-device-info';
import Environments from "../../config/Environments";

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        backgroundColor: colors.mainColor,
        paddingBottom: 10,
        paddingRight: 10,
        minHeight: 60,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 10,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingLeft: 10,
    },
    btnGame: {
        position: 'absolute',
        right: 10,
        top: 10
    },
    body: {
        backgroundColor: colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: colors.black,
    },
    wrapInput: {
        width: '95%',
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        marginBottom: 8,
        marginTop: 25,
        paddingVertical: Platform.OS === 'ios' ? 15 : 0,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: screen.widthscreen / 30,
        width: '90%',
        justifyContent: 'center',
        fontWeight: '400',
        fontFamily: Environments.DefaultFont.Regular,
        alignItems: 'center',
        padding: 0,
        height: Platform.OS == 'android' ? 50 : 'auto',
    },
    tagView: {
        width: '95%',
        alignSelf: 'center',
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    btnAdd: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
    },
    imgAdd: {
        height: screen.widthscreen / 8,
        width: screen.widthscreen / 8
    },
    scene: {
        flex: 1,
    },
    tabbar: {
        backgroundColor: colors.grey,
    },
    tab: {
        width: (screen.widthscreen - 20) / 3,
    },
    indicator: {
        backgroundColor: 'white',
        height: '100%'
    },
    label: {
        fontWeight: '900',
        color: colors.blue,
        fontFamily: Environments.DefaultFont.Black,
    },
    postView: {
        width: '94%',
        backgroundColor: 'white',
        alignSelf: 'center'
    },
    btnPost: {
        justifyContent: 'center',
        alignSelf: 'flex-end',
        position: 'absolute',
        bottom: '2%',
        marginHorizontal: '5%',
        zIndex: 100,
    },
    imgPost: {

    },
    tagItemInActive: {
        height: 30,
        paddingHorizontal: screen.widthscreen / 25,
        backgroundColor: '#EBEBEB',
        borderRadius: screen.heightscreen / 10,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 10,
        borderWidth: 1,
        borderColor: '#EBEBEB',
        paddingTop: Platform.OS == 'android' ? 3 : 0
    },
    tagItemActive: {
        height: 30,
        borderRadius: screen.heightscreen / 10,
        paddingHorizontal: screen.widthscreen / 25,
        justifyContent: 'center',
        borderColor: '#4387EF',
        borderWidth: 1,
        marginRight: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: Platform.OS == 'android' ? 3 : 0
    },
    txtTag: {
        fontSize: 12,
        color: '#65676b',
        textAlign: 'center',
    },
    txtTagActive: {
        fontSize: 12,
        color: '#4387EF',
        textAlign: 'center',
    },
    txtTagInActive: {
        fontSize: 12,
        color: '#65676B',
        textAlign: 'center',
    },
    tagsView: {
        width: '88%',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    loadingView: {
        backgroundColor: '#fff',
    },
    txtDeleteTag: {
        fontSize: 16,
        color: colors.blue,
        marginHorizontal: screen.widthscreen / 90,
        alignSelf: 'center',
        textAlign: 'center',
    },
    btnDeleteTag: {
        alignSelf: 'center',
        alignItems: 'center',
        marginLeft: 10,
    },
    btnDeleteIcon: {
        color: colors.blue,
    },
    tabViewContainer: {
        flex: 1,
        marginHorizontal: 10
    },
    tabView: {
        // marginHorizontal: screen.widthscreen - 10
    }
});

export default styles;