import React, { useEffect, useRef, useState } from 'react';
import { Alert, Image, Platform, TextInput, TouchableOpacity, View, Animated, ActivityIndicator } from 'react-native';
import { ICommentItem } from '../../../@types';
import { uploadFile } from '../../../api/ImageServices';
import { addComment } from '../../../api/PostServices';
import { Icons } from '../../../assets';
import _ChooseImageModal from '../../../components/ChooseImageModal/_ChooseImageModal';
import _Text from '../../../components/_Text';
import styles from './styles';
const RNFS = require('react-native-fs');

interface Props {
    postId: string,
    selectedComment?: ICommentItem | null,
    commentList: ICommentItem[],
    onSentComment: (response: any) => void,
    onBlurInput: () => void,
    onFocusInput: () => void,
}

export const CommentInputComponent = (props: Props) => {

    let commentInputRef: any = useRef(null);
    const [imgPath, setImgPath] = useState('');
    const [typeSend, setTypeSend] = useState<'text' | 'image'>('text');
    const [isAnonymous, setisAnonymous] = useState(false);
    const [isShowCamera, setShowCamera] = useState(false);
    const [isSending, setIsSending] = useState(false);
    const [txtInputComment, setTxtInputComment] = useState('');

    const fadeAnim = useRef(new Animated.Value(0)).current;
    const fadeIn = () => {
        Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();
    }

    const fadeOut = () => {
        Animated.timing(fadeAnim, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: true,
        }).start();
    }

    useEffect(() => {
        if (props.selectedComment?.id) {
            commentInputRef.current.focus();
        }
    }, [props.selectedComment])

    const sendComment = async () => {
        try {
            if (isSending) return;
            setIsSending(true);
            const { postId, selectedComment, onSentComment } = props;
            let currentImg = imgPath;
            if (Platform.OS == 'ios' && currentImg?.length > 0) {
                let photoPATH = `assets-library://asset/asset.JPG?id=${currentImg.substring(5)}&ext=JPG`;
                const dest = `${RNFS.TemporaryDirectoryPath}${Math.random().toString(36).substring(7)}.jpg`;
                currentImg = await RNFS.copyAssetsFileIOS(photoPATH, dest, 500, 500, 1.0, 1.0, 'contain');
            }

            let pic = typeSend == 'image' ? await uploadPic(currentImg) : ''
            pic != '' ? setTypeSend('image') : setTypeSend('text')
            let content: any = {
                type: typeSend,
                value: txtInputComment,
            };
            if (typeSend == 'image') {
                content.image = pic;
            }
            let body: any = {
                content: content,
                isAnonymous,
                isSub: selectedComment != null,
            };
            if (selectedComment) {
                body.parentId = selectedComment.id || null;
            }

            let data = await addComment(postId, body);
            if (data) {
                let newComment = null;
                if (selectedComment) {
                    newComment = data.subs[0];
                } else {
                    newComment = data.comments[0];
                }
                onSentComment(newComment);
            }
        } catch (e) {
            console.log('SEND COMMENT ERROR: ', e);
            Alert.alert('죄송합니다', e);
        } finally {
            props.onBlurInput();
            setImgPath('');
            setTxtInputComment('');
            setIsSending(false);
        }
    }

    const uploadPic = (uri: string) => {
        return new Promise(async (resolve, reject) => {
            let formdata = new FormData();
            const ext = getImageExstension(uri);
            const filename = `${Date.now()}.${ext}`;
            let file = {
                uri,
                type: 'image/jpeg',
                name: filename,
            };
            formdata.append('photos', file);
            await uploadFile(formdata).then((response: any) => {
                if (!response.isFailed) {
                    resolve(response.data.locations[0].thumbnail)
                }
                else {
                    Alert.alert('이미지 업로드 실패', '잠시 후 다시 시도해주세요');
                    resolve('')
                }
            }).catch((error: Error) => {
                Alert.alert('오류', error.message);
                reject('');
            })
        })
    }

    const getImageExstension = (uri: string) => {
        if (uri) {
            const splitNameArr = uri.split('/');
            const filename = splitNameArr[splitNameArr.length - 1];
            const splitFilenameArr = filename.split('.');
            return splitFilenameArr[splitFilenameArr.length - 1];
        }
        return '';
    }

    const onToggleAnonymouse = () => {
        setisAnonymous(!isAnonymous);
        fadeIn();
        setTimeout(() => {
            fadeOut();
        }, 3000);
    }

    const onCloseModal = () => {
        setShowCamera(false);
    }
    const onChangeModal = (locations: string[]) => {
        if (locations.length > 0) {
            setImgPath(locations[0].toString());
            setTypeSend('image');
        }
        setShowCamera(false);
    }

    return (
        <View style={styles.bottomInput}>
            <_ChooseImageModal visible={isShowCamera}
                title={'최근에'}
                doneButtonText={'완료'}
                onClose={onCloseModal}
                onChange={onChangeModal}
                numberOfImages={1} />
            {imgPath != '' && <View style={styles.imgSelectedView}>
                <Image source={{ uri: imgPath }} style={styles.imgComment} />
                <TouchableOpacity
                    onPress={() => {
                        setImgPath('');
                        setTypeSend('text');
                    }}>
                    <Image source={Icons.ic_delete_circle} />
                </TouchableOpacity>
            </View>}
            <View style={styles.inputViewHorizontal}>
                <Animated.View
                    style={[styles.fadingContainer, { opacity: fadeAnim }]}>
                    <_Text style={styles.fadingText}>익명 댓글 : {isAnonymous ? '켜짐' : '꺼짐'}</_Text>
                </Animated.View>
                <View style={styles.icSelectImage}>
                    <TouchableOpacity style={styles.openImagesButton} onPress={() => setShowCamera(true)}>
                        <Image source={Icons.ic_add_image} />
                    </TouchableOpacity>
                </View>
                <View style={styles.inputView}>
                    <TextInput ref={commentInputRef}
                        style={styles.textInputComment}
                        onChangeText={setTxtInputComment}
                        onBlur={props.onBlurInput}
                        onFocus={props.onFocusInput}
                        value={txtInputComment}
                        editable={!isSending}
                        multiline />
                    <TouchableOpacity onPress={onToggleAnonymouse}>
                        <Image source={isAnonymous ? Icons.anynomous : Icons.ic_de_anynomous} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={sendComment} disabled={txtInputComment == '' || isSending == true}>
                        {
                            isSending ?
                                <ActivityIndicator color="gray" size="small" /> :
                                <Image source={txtInputComment != '' ? Icons.ic_carbon_send_alt_filled : Icons.ic_carbon_send_alt} />
                        }
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export const CommentInput = React.memo(CommentInputComponent, (prevProps, nextProps) => {
    if (prevProps.selectedComment?.id != nextProps.selectedComment?.id) {
        return false;
    }
    if (prevProps.commentList.length != nextProps.commentList.length) {
        return false;
    }
    return true;
});
