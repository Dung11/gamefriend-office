import { StyleSheet } from "react-native";
import { colors } from "../../../config/Colors";
import DeviceInfo from 'react-native-device-info';
import { screen } from "../../../config/Layout";
import { color } from "react-native-reanimated";

const AVATAR_WIDTH = 30;

const styles = StyleSheet.create({
    header: {
        backgroundColor: colors.mainColor,
        paddingRight: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    txtTitle: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: '800',
        textAlign: 'center',
        fontSize: 16,
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingLeft: 50,
        paddingRight: 50,
    },
    txtDone: {
        fontSize: 17,
        color: 'white',
        fontWeight: "400",
        textAlign: 'right',
        alignSelf: 'center',
    },
    btnDone: {
        marginTop: '3%'
    },
    textInput: {
        paddingHorizontal: 5,
        fontSize: screen.widthscreen / 30,
        width: '90%',
    },
    postContent: {
        backgroundColor: 'rgba(235, 235, 245, 0.3)',
        borderRadius: 15,
        height: '40%',
        width: '95%',
        alignSelf: 'center',
        marginTop: '5%',
        padding: '3%',
    },
    headerPost: {
        flexDirection: 'row',
        // width: screen.widthscreen,
    },
    viewTextHeader: {
        flex: .5
    },
    txtName: {
        fontSize: 16,
        fontWeight: '800',
    },
    txtTimeCreate: {
        fontSize: 13,
        color: '#8E8E92',
    },
    avatar: {
        marginRight: '4%',
        height: 50,
        width: 50,
        borderRadius: 50,
    },
    postPage: {
        backgroundColor: '#FBFBFC',
        flex: 1,
    },
    txtContain: {
        fontSize: 13,
        fontWeight: '400',
        marginTop: '7%',
    },
    imgPosted: {
        alignSelf: 'center',
        width: '100%',
        paddingVertical: '34%',
    },
    imgPostedView: {
        borderRadius: 4,
        marginVertical: '5%',
        justifyContent: 'center',

    },
    heartCommentView: {
        flexDirection: 'row',
    },
    heartView: {
        height: screen.heightscreen * 0.05,
        width: screen.widthscreen * 0.2,
        backgroundColor: 'rgba(227, 92, 92, 0.1)',
        marginRight: '2%',
        borderRadius: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    commentListContainer: {
        marginBottom: 70,
    },
    commentView: {
        height: screen.heightscreen * 0.05,
        width: screen.widthscreen * 0.2,
        backgroundColor: 'rgba(67, 135, 239, 0.1)',
        borderRadius: 18,
        flexDirection: 'row',
        alignItems: 'center',
    },
    icFilled: {
        height: screen.widthscreen * 0.06,
        width: screen.widthscreen * 0.06,
        marginHorizontal: '10%',
        marginVertical: '5%',
    },
    txtCommentCount: {
        alignSelf: 'center',
        fontSize: 15,
        color: '#2F73DB',
        fontWeight: '400',
    },
    txtHeartCount: {
        alignSelf: 'center',
        fontSize: 15,
        color: '#CF4848',
        fontWeight: '400',
    },
    line: {
        height: 1,
        backgroundColor: '#E3E3E8',
        width: '100%',
        alignSelf: 'center',
    },
    itemView: {
        backgroundColor: 'white',
        alignSelf: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#E8E8ED',
        marginHorizontal: '4%',
    },
    itemChild: {
        alignSelf: 'center',
        marginTop: '4%',
        marginHorizontal: -AVATAR_WIDTH / 4,
    },
    itemRow: {
        flexDirection: 'row',
        width: '100%',
        padding: 10,
    },
    subItemLeft: {
        flex: 1,
        flexDirection: 'row',
    },
    txtNameUserComment: {
        fontWeight: '800',
        color: '#000',
        fontSize: 13,
    },
    txtComment: {
        fontSize: 13,
        fontWeight: '400',
        color: '#1C1E21',
    },
    txtTime: {
        color: '#A6A8AA',
        fontSize: 10,
        fontWeight: '400',
    },
    avatarUserBContainer: {
        flex: 1,
        alignItems: 'center',
    },
    avatarUserB: {
        height: AVATAR_WIDTH,
        width: AVATAR_WIDTH,
        borderRadius: AVATAR_WIDTH,
    },
    commentContentContainer: {
        flex: 6,
        paddingLeft: '2%',
    },
    commentUsernameContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    postView: {
        backgroundColor: '#FFF',
        width: screen.widthscreen,
        alignSelf: 'center',
        paddingVertical: '6%',
        paddingHorizontal: '5%',
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#E8E8ED',
    },
    itemSubView: {
        flexDirection: 'row'
    },
    lineItemSub: {
        width: 3,
        backgroundColor: '#E3E3E8',
        marginVertical: '3%',
    },
    borderLeftItem: {
        borderLeftWidth: 3,
        borderLeftColor: '#E3E3E8',
        flex: .1
    },
    itemSubContaint: {
        flexDirection: 'row',
        borderLeftWidth: 2,
        borderLeftColor: '#E3E3E8',
        marginLeft: '-10%',
        paddingLeft: '10%',
        paddingVertical: 10,
    },
    avatarUserC: {
        marginRight: '4%',
        marginLeft: '2%',
        height: screen.widthscreen / 14,
        width: screen.widthscreen / 14,
        borderRadius: 50,
    },
    txtItemSubView: {
        width: screen.widthscreen * 0.59,
    },
    txtItemSubViewDel: {
        width: screen.widthscreen * 0.535,
    },
    itemSubLine: {
        height: 1,
        width: '100%',
        backgroundColor: '#E3E3E8',
    },
    bottomInput: {
        justifyContent: 'center',
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    },
    icSelectImage: {
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    openImagesButton: {
        alignItems: 'center',
    },
    inputView: {
        flex: 4,
        backgroundColor: '#EEEEEF',
        borderRadius: 20,
        paddingHorizontal: '5%',
        marginVertical: '2.5%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        minHeight: 45,
    },
    textInputComment: {
        width: '80%',
    },
    centeredView: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        backgroundColor: 'rgba(248, 248, 248, 0.95)',
        borderRadius: 13,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: screen.widthscreen * 0.8,
        justifyContent: 'center',
    },
    lineModal: {
        height: 1,
        backgroundColor: '#DADADE',
        width: screen.widthscreen * 0.8,
    },
    titleRP: {
        color: 'rgba(0, 0, 0, 0.4)',
        fontWeight: '500',
        fontSize: 13,
        margin: '3%',
    },
    questionRP: {
        color: 'rgba(0, 0, 0, 0.4)',
        fontWeight: '400',
        fontSize: 12,
        marginTop: '3%',
    },
    questionRP2: {
        color: 'rgba(0, 0, 0, 0.4)',
        fontWeight: '400',
        fontSize: 12,
        marginBottom: '9%',
    },
    txtOpt: {
        color: '#007AFF',
        fontSize: 18,
        fontWeight: '400',
        margin: '5%',
    },
    txtOptSelected: {
        color: colors.red,
        fontSize: 18,
        fontWeight: '400',
        margin: '5%',
    },
    textInputModalView: {
        marginTop: '5%',
        backgroundColor: '#ECF1F9',
        width: '95%',
        borderRadius: 4,
        height: screen.heightscreen / 9,
        paddingHorizontal: '4%',
        paddingVertical: '3%',
    },
    btnConfirmRP: {
        backgroundColor: 'white',
        width: screen.widthscreen * 0.8,
        marginTop: '3%',
        borderRadius: 13,
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        paddingVertical: '5%',
    },
    txtBtnConfirmRP: {
        textAlign: 'center',
        width: '100%',
        color: '#007AFF',
        fontWeight: '500',
        fontSize: 20,
    },
    noComment: {
        color: '#8E8E92',
        fontWeight: '500',
        marginTop: '5%',
        marginHorizontal: '2%',
        alignSelf: 'center',
    },
    icSubWarning: {
        alignSelf: 'flex-end',
    },
    imgComment: {
        height: screen.heightscreen / 10,
        width: screen.widthscreen / 7,
        marginRight: '2%',
        borderRadius: 8,
        marginVertical: '3%',
    },
    imgSelectedView: {
        height: screen.heightscreen / 8,
        width: '80%',
        flexDirection: 'row',
        paddingVertical: '3%',
        marginHorizontal: '16%',
    },
    inputViewHorizontal: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingBottom: DeviceInfo.hasNotch() ? 10 : 0,
    },
    btnOption: {
        height: '12%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtViewReply: {
        fontSize: 10,
    },
    lineHoz: {
        height: 1,
        width: screen.widthscreen,
        backgroundColor: '#E8E8ED',
        alignSelf: 'center',
    },
    sendButtonIcon: {
        width: 25,
        height: 25,
    },
    loadMore: {
        fontSize: 10,
        marginLeft: '15%',
        fontWeight: '800',
    },
    cmdReport: {
        height: screen.heightscreen * 0.04
    },
    cmdDelete: {
        height: screen.heightscreen * 0.025,
        marginTop: 2,
    },
    fadingContainer: {
        height: 30,
        backgroundColor: '#65676B',
        width: '30%',
        borderRadius: 34,
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        top: -30,
    },
    fadingText: {
        fontSize: 13,
        textAlign: "center",
        color: '#FFFFFF',
        fontWeight: '400',
    },
    buttonRow: {
        flexDirection: "row",
        marginVertical: 16,
    },
    actionGroupContainer: {
        position: 'absolute',
        right: 0,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    commentActionButton: {
        width: 32,
        height: 32,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexRowCenter: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        justifyContent: "center",
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10,
    },
    loadingContainer: {
        backgroundColor: 'white',
        paddingVertical: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    ownerContainer: {
        borderRadius: 6,
        backgroundColor: colors.blue,
        paddingHorizontal: 8,
        marginLeft: 10,
        paddingVertical: 4
    },
    ownerText: {
        color: '#F3F4F4',
        fontSize: 11,
        fontWeight: '400',
        lineHeight: 17
    },
    btnIcon: {
        color: '#ccc',
        fontSize: 16,
    },
});

export default styles;