import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Image, Alert, ActivityIndicator } from 'react-native';
import { getPostDetail, onLikePost, onUnLikePost } from '../../../api/PostServices';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import Utils from '../../../utils/Utils';
import PostActionModal from './PostActionModal';
import styles from './styles';

interface Props {
    postId: string,
    commentQuantity: number,
    onShowUserDetail: (authorId: any, username: any) => void,
    onShowAvatar: (authorId: any, username: any) => void,
    onShowImage: (imgUrl: string) => void,
    onReportPost: (post: any) => void,
    onPostChange: (post: any) => void,
}

export const PostContent = React.memo(function ({ postId, commentQuantity, onShowAvatar, onShowUserDetail, onShowImage, onReportPost, onPostChange }: Props) {

    const [state, setState] = useState<any>({
        postDetail: null,
        postIsLiked: false,
        userId: '',
    });

    useEffect(() => {
        checkMyPost();
        return () => { }
    }, [])

    const checkMyPost = async () => {
        let user = await keyValueStorage.get('userID');
        if (!user) return;
        let postDetail = await getPostDetail(postId, user);
        if (postDetail) {
            setState({
                ...state,
                userId: user,
                postDetail: postDetail,
                postIsLiked: postDetail.isLiked,
            });
            onPostChange(postDetail);
        }
    }

    const heartClickHandle = async () => {
        try {
            if (!state.postIsLiked) {
                let response = await onLikePost(state.postDetail.id);
                if (response.isFailed) {
                    Alert.alert('좋아요 실패', '잠시 후 다시 시도해주세요');
                } else {
                    setState({
                        ...state,
                        postDetail: { ...state.postDetail, likes: response.likes },
                        postIsLiked: !state.postIsLiked,
                    });
                }
            }
            else {
                let response = await onUnLikePost(postId);
                if (response.isFailed) {
                    Alert.alert('좋아요 취소 실패', '잠시 후 다시 시도해주세요');
                } else {
                    setState({
                        ...state,
                        postDetail: { ...state.postDetail, likes: response.likes },
                        postIsLiked: !state.postIsLiked,
                    });
                }
            }
        } catch (e) {
            Alert.alert('죄송합니다', e);
        }
    }

    if (!state.postDetail) {
        return <ActivityIndicator color="gray" />
    }

    return (
        <View style={styles.postView}>
            <View style={styles.headerPost}>
                <View style={{ flex: .2 }}>
                    <TouchableOpacity
                        onPress={() => onShowAvatar(state.postDetail?.author?.id, state.postDetail?.author?.userName)}>
                        <Image source={{ uri: state.postDetail?.author?.profilePicture }} style={styles.avatar} />
                    </TouchableOpacity>
                </View>
                <View style={styles.viewTextHeader}>
                    <TouchableOpacity onPress={() => onShowUserDetail(state.postDetail?.author?.id, state.postDetail?.author?.userName)}>
                        <_Text style={styles.txtName}>{state.postDetail?.author?.userName}</_Text>
                    </TouchableOpacity>
                    <_Text style={styles.txtTimeCreate}>{Utils.timeAgoHandle(state.postDetail?.timeAgo)}</_Text>
                </View>
                <View style={styles.actionGroupContainer}>
                    <View style={styles.flexRowCenter}>
                        <TouchableOpacity
                            onPress={heartClickHandle}>
                            <Image source={state.postIsLiked ? Icons.ic_aheart_full : Icons.ic_heart_line} />
                        </TouchableOpacity>
                        {/* {
                            state.userId == state.postDetail.author.id ?
                                <TouchableOpacity
                                    onPress={() => {
                                        Alert.alert('게시글을 지우겠습니까?', '', [
                                            { text: '취소', style: 'cancel' },
                                            { text: '확인', onPress: () => removeMyPost() }
                                        ], { cancelable: false });
                                    }}>
                                    <Image source={Icons.ic_delete_post} />
                                </TouchableOpacity> :
                                <TouchableOpacity onPress={() => onReportPost(state.postDetail)}>
                                    <Image source={Icons.ic_warning_top} />
                                </TouchableOpacity>
                        } */}
                        <PostActionModal userId={state.userId}
                            postDetail={state.postDetail} />
                    </View>
                </View>
            </View>
            <_Text style={styles.txtContain}>{state.postDetail?.text}</_Text>
            <TouchableOpacity
                style={styles.imgPostedView}
                onPress={() => onShowImage(state.postDetail?.picture)}>
                {
                    state.postDetail?.picture !== '' &&
                    <Image
                        style={styles.imgPosted}
                        source={{ uri: state.postDetail?.picture }}
                        resizeMode='contain' />
                }
            </TouchableOpacity>
            <View style={styles.heartCommentView}>
                <View style={styles.heartView}>
                    <Image source={Icons.ic_heart} style={styles.icFilled} />
                    <_Text style={styles.txtHeartCount}>{state.postDetail?.likes}</_Text>
                </View>
                <View style={styles.commentView}>
                    <Image source={Icons.ic_comment_blue} style={styles.icFilled} />
                    <_Text style={styles.txtCommentCount}>{commentQuantity}</_Text>
                </View>
            </View>
        </View>
    );
}, (prevProps, nextProps) => {
    if (prevProps.commentQuantity != nextProps.commentQuantity) {
        return false;
    }
    return true;
});
