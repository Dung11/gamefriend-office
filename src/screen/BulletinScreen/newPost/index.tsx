import React, { useState, useEffect, useRef } from 'react';
import { View, StatusBar, TouchableOpacity, Image } from 'react-native';
import { colors } from '../../../config/Colors';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { Icons } from "../../../assets";
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import _ChooseImageModal from '../../../components/ChooseImageModal/_ChooseImageModal';
import { RouteProp } from '@react-navigation/native';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import _Text from '../../../components/_Text';
import _ModalShowImage from '../../../components/Modal/_ModalShowImage';
import { getUserPicture } from '../../../api/UserServices';
import styles from './styles';
import _ReportModal from './ReportModal';
import CommentList from './CommentList';

const RNFS = require('react-native-fs');

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,
    route: RouteProp<Params, 'params'>
}

type Params = {
    params: {
        postId: string,
        callBackNewPost: any,
    },
}

const limit = 10;

const NewPost = ({ navigation, route }: Props) => {

    let postId = route.params.postId;

    const [userId, setUserId] = useState<any>('');
    const [postDetail, setPostDetail] = useState<any>();
    const [modalVisible, setModalVisible] = useState(false);
    const [reportType, setReportType] = useState(1);
    const [showingImage, setShowingImage] = useState<any>(null);
    const [visibleShowImg, setvisibleShowImg] = useState<boolean>(false);
    const [reportUser, setReportUser] = useState('');

    useEffect(() => {
        getUserData();
    }, []);

    const getUserData = async () => {
        let id = await keyValueStorage.get('userID');
        setUserId(id);
    }

    const onCloseModalShowImage = () => {
        setvisibleShowImg(false);
        setShowingImage(null);
    }

    useEffect(() => {
        if (showingImage) {
            setvisibleShowImg(true);
        }
    }, [showingImage])

    const onShowAvatar = async (id: string, userName: string) => {
        try {
            if (userName !== '익명') {
                let pictures = await getUserPicture(id);
                setShowingImage(pictures);
            }
        } catch (error) {
            console.log('onShowAvatar - error: ', error)
        }
    }

    const onShowImage = (url: string) => {
        try {
            setShowingImage(url);
        } catch (error) {
            console.log('onShowImage - error: ', error)
        }
    }

    const onShowUserDetail = async (friendId: string, userName: string) => {
        if (userName !== '익명') {
            if (friendId !== '익명') {
                let userId = await keyValueStorage.get('userID');
                navigation.navigate('ViewFullProfile', {
                    userId,
                    friendId,
                    type: 'friendRequestNormalRecommendation',
                });
            }
        }
    }

    const onPostChange = (post: any) => {
        setPostDetail(post);
    }

    const onReportPost = (post: any) => {
        setReportUser(post?.author?.userName);
        setReportType(1);
        setModalVisible(true);
    }

    const onReportComment = (target: any) => {
        setReportUser(target.userName);
        setReportType(2);
        setModalVisible(true);
    }

    return (
        <ContainerMain>
            <_ModalShowImage
                visible={visibleShowImg}
                onClose={onCloseModalShowImage}
                banner={showingImage} />
            <_ReportModal
                postId={postId}
                reportType={reportType}
                userId={userId}
                friendName={reportUser}
                visible={modalVisible}
                onClose={() => setModalVisible(false)}
                onSubmit={() => setModalVisible(false)} />
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <View style={styles.header}>
                <TouchableOpacity style={styles.titleContainer} onPress={() => navigation.goBack()}>
                    <Image source={Icons.ic_back} />
                </TouchableOpacity>
                <_Text style={styles.txtTitle} numberOfLines={1}>
                    {postDetail?.title}
                </_Text>
            </View>
            <CommentList
                userId={userId}
                postId={postId}
                postDetail={postDetail}
                onShowAvatar={onShowAvatar}
                onShowImage={onShowImage}
                onShowUserDetail={onShowUserDetail}
                onReportPost={onReportPost}
                onReportComment={onReportComment}
                onPostChange={onPostChange} />
        </ContainerMain>
    );
};

export default NewPost;
