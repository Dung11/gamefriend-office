import React, { useCallback, useEffect, useRef, useState } from 'react';
import { ActivityIndicator, Alert, FlatList, RefreshControl, View } from 'react-native';
import { ICommentItem } from '../../../@types';
import { getComment } from '../../../api/PostServices';
import _Text from '../../../components/_Text';
import { colors } from '../../../config/Colors';
import { CommentInput } from './CommentInput';
import CommentItem from './CommentItem';
import { PostContent } from './PostContent';
import styles from './styles';

interface State {
    commentList: any[],
    isLoading: boolean,
    refreshing: boolean,
    isHideFooter: boolean,
    selectedComment: ICommentItem | null,
    page: number,
    commentQuantity: number,
}

interface Props {
    userId: string,
    postId: string,
    postDetail?: any,
    onShowAvatar: (id: string, userName: string) => void,
    onShowUserDetail: (id: string, userName: string) => void,
    onShowImage: (url: string) => void,
    onReportPost: (post: any) => void,
    onReportComment: (author: any) => void,
    onPostChange: (post: any) => void,
}

const CommentListComponent = (props: Props) => {

    const { postId } = props;
    const limit = 10;
    let commentListRef: any = useRef(null);
    const [state, setState] = useState<State>({
        commentList: [],
        isLoading: false,
        refreshing: false,
        isHideFooter: false,
        selectedComment: null,
        page: 1,
        commentQuantity: 1,
    });

    useEffect(() => {
        if (props.postDetail || state.refreshing == true) {
            getComments();
        }
    }, [props.postDetail?.id, state.refreshing])

    const getComments = async () => {
        try {
            let comments: any = [];
            let quantity = state.commentQuantity;
            if (state.isLoading) return;
            if (state.commentList.length < quantity) {
                setState({
                    ...state,
                    isLoading: true,
                    isHideFooter: false,
                });
                let data = await getComment(postId, state.page, limit);
                if (data) {
                    if (state.page == 1) {
                        comments = data.comments;
                        quantity = data.quantity;
                    } else {
                        comments = state.commentList.concat(data.comments);
                    }
                }
                setState({
                    ...state,
                    commentList: comments,
                    isLoading: false,
                    refreshing: false,
                    isHideFooter: true,
                    page: state.page + 1,
                    commentQuantity: quantity,
                });
            }
        } catch (e) {
            Alert.alert('죄송합니다', e);
        }
    }

    const onRefresh = async () => {
        setState({ ...state, refreshing: true, page: 1, commentList: [] });
    }

    const onSentComment = useCallback((newComment: ICommentItem) => {
        let comments = [...state.commentList];
        if (comments.length > 0) {
            if (state.selectedComment?.id) {
                comments = state.commentList.map(item => {
                    if (item.id == state.selectedComment?.id) {
                        return Object.assign({}, item, { subs: item.subs + 1 });
                    }
                    return item;
                });
                setState({ ...state, commentList: comments });
            } else {
                onRefresh();
                // comments.unshift(newComment);
                // setState({ ...state, commentList: comments, commentQuantity: state.commentQuantity + 1 });
            }
        }
    }, [state.commentList]);

    const onSelectComment = (comment: ICommentItem | null = null, index?: number) => {
        setState({ ...state, selectedComment: comment });
        if (index) {
            setTimeout(() => {
                commentListRef.current.scrollToIndex({ animated: true, index: index });
            }, 200);
        }
    }

    const onDeleteComment = useCallback((deleteComment: ICommentItem, parentId?: string) => {
        if (parentId) {
            const comments = state.commentList.map(item => {
                if (item.id == parentId) {
                    return Object.assign({}, item, { subs: item.subs - 1 });
                }
                return item;
            });
            setState({ ...state, commentList: comments });
        } else {
            setState({
                ...state,
                selectedComment: null,
                commentList: state.commentList.filter(a => a.id != deleteComment.id),
                commentQuantity: state.commentQuantity - 1,
            });
        }
    }, [state.commentList]);

    const listFooterComponent = () => {
        if (!state.isHideFooter) {
            return (
                <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size={36} color={colors.gray} />
                </View>
            );
        }
        return null;
    }

    const renderItem = ({ item, index }: { item: any, index: number }) => {
        // if (item.isBlocked) {
        //     return (<></>);
        // }
        return <CommentItem {...props}
            item={item}
            index={index}
            selectedComment={state.selectedComment}
            commentList={state.commentList}
            onSelectComment={onSelectComment}
            onDeleteComment={onDeleteComment}
            onRefresh={onRefresh} />
    }

    return (
        <View style={styles.postPage}>
            <FlatList ref={commentListRef}
                style={styles.commentListContainer}
                data={state.commentList}
                keyExtractor={item => item.id.toString()}
                onEndReachedThreshold={0.3}
                onEndReached={getComments}
                renderItem={renderItem}
                refreshControl={<RefreshControl refreshing={state.refreshing} onRefresh={onRefresh} />}
                ListEmptyComponent={<_Text style={styles.noComment}>댓글이 없습니다</_Text>}
                ListHeaderComponent={<PostContent {...props} commentQuantity={state.commentQuantity} />}
                ListFooterComponent={listFooterComponent} />
            <CommentInput postId={postId}
                commentList={state.commentList}
                selectedComment={state.selectedComment}
                onSentComment={onSentComment}
                onBlurInput={onSelectComment}
                onFocusInput={() => {
                    state.commentList.length > 0 && commentListRef.current.scrollToIndex({ animated: true, index: 0 });
                }} />
        </View>
    )
}

const CommentList = React.memo(CommentListComponent, (prevProps, nextProps) => {
    // if (prevProps.postDetail?.id != nextProps.postDetail?.id) {
    //     return false;
    // }
    return false;
});
export default CommentList;
