import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FlatList, Image, TouchableOpacity, View, ViewStyle, Alert, Keyboard } from 'react-native';
import { ICommentItem } from '../../../@types';
import { delComment, getCommentSub } from '../../../api/PostServices';
import { Icons } from '../../../assets';
import _Text from '../../../components/_Text';
import Utils from '../../../utils/Utils';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { blockUser } from '../../../api/UserServices';
import ToastUtils from '../../../utils/ToastUtils';
import { useNavigation } from '@react-navigation/core';

interface Props {

    item: ICommentItem,
    index: number,
    userId: string,
    postDetail?: any,
    onShowAvatar: (id: string, userName: string) => void,
    onShowUserDetail: (id: string, userName: string) => void,
    onShowImage: (url: string) => void,
    onSelectComment: (comment: ICommentItem, index: number) => void,
    onReportComment: (author: any) => void,
    onDeleteComment: (comment: ICommentItem, parentId?: string) => void,
    parentId?: string,
    selectedComment: ICommentItem | null,
    commentList: ICommentItem[],
    onRefresh: () => void
}

interface State {
    subCommentList: ICommentItem[],
}

const CommentItemComponent = (props: Props) => {

    const navigation = useNavigation();
    const { item, userId, postDetail, selectedComment, onShowAvatar, onShowUserDetail, onShowImage } = props;
    let page = 1, limit = 10;
    const { id, author, content, subs } = Object.freeze(item);
    let timeAgo = Utils.timeAgoHandle(item.timeAgo);

    const [state, setState] = useState<State>({
        subCommentList: [],
    });

    useEffect(() => {
        if (subs) {
            getSubComments(id);
        }
    }, [props.item.subs]);

    const getSubComments = async (commentId: string) => {
        try {
            let response = await getCommentSub(postDetail.id, commentId, page, limit);
            if (!response.isFailed) {
                setState({ ...state, subCommentList: response.data.subs });
            }
        } catch (e) {

        }
    }

    const onReply = () => {
        props.onSelectComment(item, props.index);
    }

    const deleteComment = async () => {
        try {
            let res = await delComment(postDetail.id, item.id);
            if (!res.isFailed) {
                props.onDeleteComment(item, props.parentId);
            } else {
                Alert.alert('오류', '잠시 후 다시 시도해주세요');
            }
        } catch (e) {
            console.log('error: ', e);
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        }
    }

    const onBlockUser = async () => {
        try {
            Alert.alert('사용자의 게시글을 차단하겠습니까?', '', [
                { text: '취소', style: 'cancel' },
                {
                    text: '확인', onPress: async () => {
                        if (!props.userId || !props.postDetail) return;
                        var res = await blockUser(props.userId, item.author.id);
                        if (!res.isFailed) {
                            ToastUtils.showToast({
                                text1: `${item.author.userName} 사용자가 차단 되었습니다.`,
                                type: 'info',
                                position: 'bottom'
                            });
                            if (item.author.id === postDetail.author?.id) {
                                navigation.goBack();
                            } else {
                                props.onRefresh();
                            }
                        }
                        else {
                            Alert.alert('오류', '잠시 후 다시 시도해주세요');
                        }
                    }
                }
            ], { cancelable: false });
        } catch (e) {
            console.log('error: ', e);
            Alert.alert('오류', '잠시 후 다시 시도해주세요');
        }
    }

    const renderSubItem = ({ item, index }: { item: any, index: number }) => {
        const subProps = {
            ...props,
            parentId: props.item.id,
            item: item,
            index: index,
        }
        return <CommentItem {...subProps} />;
    }

    const itemStyles: ViewStyle = props.parentId ? styles.itemChild : styles.itemView;
    const itemParentRow: ViewStyle = useMemo(() => {
        return {
            ...styles.itemRow,
            backgroundColor: selectedComment?.id == item.id ? '#eee' : 'white',
        }
    }, [selectedComment?.id]);

    const showAvatar = useCallback(() => {
        onShowAvatar(author.id, author.userName);
    }, [author]);

    const showUserDetail = useCallback(() => {
        onShowUserDetail(author.id, author.userName)
    }, [author]);

    return (
        <View style={itemStyles}>
            <View style={itemParentRow}>
                <TouchableOpacity style={styles.avatarUserBContainer} onPress={showAvatar}>
                    <Image source={{ uri: author.profilePicture }} style={styles.avatarUserB} />
                </TouchableOpacity>
                <View style={styles.commentContentContainer}>
                    <TouchableOpacity style={styles.commentUsernameContainer} onPress={showUserDetail}>
                        <_Text style={styles.txtNameUserComment}>{author.userName}</_Text>
                        {
                            postDetail?.author.id === author.id &&
                            <View style={styles.ownerContainer}>
                                <_Text style={styles.ownerText}>글쓴이</_Text>
                            </View>
                        }
                    </TouchableOpacity>
                    <_Text style={styles.txtComment}>{content.value}</_Text>
                    {
                        content.type == 'image' &&
                        <TouchableOpacity onPress={() => onShowImage(content.image)}>
                            <Image source={{ uri: content.image }} style={styles.imgComment} />
                        </TouchableOpacity>
                    }
                    <_Text style={styles.txtTime}>{timeAgo}</_Text>
                    <View style={styles.actionGroupContainer}>
                        {
                            !props.parentId &&
                            <TouchableOpacity style={styles.commentActionButton} onPress={onReply}>
                                <Image source={Icons.ic_comment_send} />
                            </TouchableOpacity>
                        }
                        {
                            userId != author.id &&
                            <TouchableOpacity style={styles.commentActionButton} onPress={() => { props.onReportComment(author) }}>
                                <Image source={Icons.ic_warning} />
                            </TouchableOpacity>
                        }
                        {
                            userId == author.id &&
                            <TouchableOpacity style={styles.commentActionButton} onPress={deleteComment}>
                                <Image style={{ height: 26, width: 26, resizeMode: "contain"}} source={Icons.ic_delete_post} />
                            </TouchableOpacity>
                        }
                        {
                            userId != author.id &&
                            <TouchableOpacity style={styles.commentActionButton} onPress={onBlockUser}>
                                <Icon name="ban" style={styles.btnIcon} />
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
            {
                subs > 0 &&
                <View style={styles.itemRow}>
                    <View style={styles.subItemLeft}>
                        <View style={{ flex: 1 }} />
                        <View style={{ flex: 1, borderLeftWidth: 1, borderLeftColor: '#E8E8ED', marginLeft: -1 }} />
                    </View>
                    <View style={styles.commentContentContainer}>
                        <FlatList
                            data={state.subCommentList}
                            keyExtractor={item => item.id.toString()}
                            renderItem={renderSubItem} />
                    </View>
                </View>
            }
        </View>
    )
}

const CommentItem = React.memo(CommentItemComponent, (prevProps, nextProps) => {
    if (prevProps.item.subs != nextProps.item.subs) {
        return false;
    }
    if (prevProps.selectedComment?.id != nextProps.selectedComment?.id) {
        if (nextProps.selectedComment?.id == nextProps.item.id) {
            return false;
        } else if (prevProps.selectedComment?.id == nextProps.item.id) {
            return false;
        }
    }
    if (prevProps.commentList != nextProps.commentList) {
        return false;
    }
    return true;
});
export default CommentItem;
