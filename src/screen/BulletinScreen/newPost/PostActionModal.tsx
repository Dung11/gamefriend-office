import { useNavigation } from '@react-navigation/core';
import React, { useState } from 'react'
import { Alert, StyleSheet, TouchableOpacity, View } from 'react-native'
import { deletePost } from '../../../api/PostServices';
import Modal from 'react-native-modal';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/FontAwesome5';
import _Text from '../../../components/_Text';
import _ReportModal from './ReportModal';
import ToastUtils from '../../../utils/ToastUtils';
import { blockUser } from '../../../api/UserServices';

interface State {
    visible: boolean,
    reportVisible: boolean,
}

interface Props {
    userId: string,
    postDetail?: any,
}

const PostActionModalComponent = (props: Props) => {

    const [state, setState] = useState<State>({
        visible: false,
        reportVisible: false,
    });
    const navigation = useNavigation();

    const onToggle = () => {
        setState({ ...state, visible: !state.visible });
    }

    const onReportPost = () => {
        setState({ ...state, visible: false });
        setTimeout(() => {
            setState({ ...state, reportVisible: true });
        }, 500);
    }

    const removeMyPost = () => {
        Alert.alert('게시글을 지우겠습니까?', '', [
            { text: '취소', style: 'cancel' },
            {
                text: '확인', onPress: async () => {
                    if (!props.userId || !props.postDetail) return;
                    var res = await deletePost(props.userId, props.postDetail.id);
                    if (!res.isFailed) {
                        navigation.goBack();
                    }
                    else {
                        Alert.alert('오류', '잠시 후 다시 시도해주세요');
                    }
                }
            }
        ], { cancelable: false });
    }

    const onBlock = () => {
        Alert.alert('사용자를 차단 하시겠습니까?', '', [
            { text: '취소', style: 'cancel' },
            {
                text: '확인', onPress: async () => {
                    if (!props.userId || !props.postDetail) return;
                    var res = await blockUser(props.userId, props.postDetail.author?.id);
                    if (!res.isFailed) {
                        setState({ ...state, visible: false });
                        ToastUtils.showToast({
                            text1: `${props.postDetail.author?.userName} 사용자가 차단 되었습니다.`,
                            type: 'info',
                            position: 'bottom'
                        })
                        navigation.goBack();
                    }
                    else {
                        Alert.alert('오류', '잠시 후 다시 시도해주세요');
                    }
                }
            }
        ], { cancelable: false });
    }

    if (!props.userId || !props.postDetail) {
        return null;
    }

    return (
        <View>
            <_ReportModal reportType={1}
                postId={props.postDetail.id}
                userId={props.userId}
                friendName={props.postDetail.author.userName}
                visible={state.reportVisible}
                onClose={() => setState({ ...state, reportVisible: false })}
                onSubmit={() => setState({ ...state, reportVisible: false })} />
            <Modal style={styles.background} isVisible={state.visible} onBackButtonPress={onToggle} onBackdropPress={onToggle}>
                <Animatable.View style={styles.container}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={onToggle}>
                            <Icon name="times" style={styles.btnIcon} />
                        </TouchableOpacity>
                    </View>
                    {props.userId == props.postDetail.author.id ?
                        <TouchableOpacity onPress={removeMyPost} style={styles.elementBtn}>
                            <Icon name="trash-alt" style={styles.btnIcon} />
                            <_Text style={styles.btnText}>게시글 삭제</_Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={onReportPost} style={styles.elementBtn}>
                            <Icon name="exclamation-triangle" style={styles.btnIcon} />
                            <_Text style={styles.btnText}>게시글 신고</_Text>
                        </TouchableOpacity>
                    }
                    {props.userId != props.postDetail.author.id &&
                        <TouchableOpacity onPress={onBlock} style={styles.elementBtn}>
                            <Icon name="ban" style={styles.btnIcon} />
                            <_Text style={styles.btnText}>사용자 차단</_Text>
                        </TouchableOpacity>
                    }
                </Animatable.View>
            </Modal>
            <TouchableOpacity onPress={onToggle} style={styles.toggleBtn}>
                <Icon name="ellipsis-v" style={styles.btnIcon} />
            </TouchableOpacity>
        </View>
    )
}

const PostActionModal = React.memo(PostActionModalComponent, (prevProps, nextProps) => {
    if (prevProps.userId != nextProps.userId) {
        return false;
    } else if (prevProps.postDetail?.id != nextProps.postDetail?.id) {
        return false;
    }
    return true;
});
export default PostActionModal;

const styles = StyleSheet.create({
    toggleBtn: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnIcon: {
        color: 'gray',
        fontSize: 16,
    },
    background: {
        margin: 0,
        padding: 0,
        justifyContent: 'flex-end',
    },
    container: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: 'white',
    },
    header: {
        height: 50,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    elementBtn: {
        height: 50,
        alignItems: 'center',
        marginHorizontal: 15,
        paddingHorizontal: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        flexDirection: 'row',
    },
    btnText: {
        marginLeft: 10,
        fontSize: 16,
    },
});
