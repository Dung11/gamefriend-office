import React, { useEffect, useState } from "react";
import Modal from 'react-native-modal';
import { StyleSheet, TouchableOpacity, View, Alert, TextInput } from 'react-native';
import LoadingIndicator from "../../../components/LoadingIndicator/LoadingIndicator";
import { colors } from "../../../config/Colors";
import { addReport } from "../../../api/UserServices";
import DeviceInfo from 'react-native-device-info';
import _Text from "../../../components/_Text";
import ToastUtils from "../../../utils/ToastUtils";
import Environments from "../../../config/Environments";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

interface Props {
    visible?: boolean,
    onSubmit: () => void,
    onClose: () => void,
    userId: string,
    friendName: string,
    reportType: number,
    postId: string,
    commentId?: string
}

const _ReportModalComponent = (props: Props) => {

    const { onClose } = props;
    const [isLoadingInView, setIsLoadingInView] = useState(false);
    const [visible, setVisible] = useState(props.visible);
    const [reportText, setReportText] = useState('');
    const [selectedReport, setSelectedReport] = useState('')
    const [showToastInModal, setShowToastInModal] = useState(false);

    useEffect(() => {
        setVisible(props.visible);
    }, [props.visible]);

    const _onChangeText = (text: string) => {
        setReportText(text);
    };

    const onModalSubmit = async () => {
        if (selectedReport === '') {
            Alert.alert('오류', '내용을 입력해주세요');
            return;
        }
        setIsLoadingInView(true);

        let body: any = {
            text: reportText,
            title: selectedReport,
        }

        if (props.reportType === 1) {
            body = {
                ...body,
                postId: props.postId
            }
        } else {
            body = {
                ...body,
                commentId: props.commentId
            }
        }

        const response = await addReport(props.userId, body);
        if (!response) {
            Alert.alert('오류', response.errors);
        } else {
            setSelectedReport('');
            setReportText('');
            setIsLoadingInView(false);
            setShowToastInModal(true)
            ToastUtils.showToast({
                text1: '신고가 정상적으로 접수되었습니다',
                type: 'info',
                position: 'bottom'
            });
            props.onSubmit();
        }
        setIsLoadingInView(false);
    }

    useEffect(() => {
        setSelectedReport('');
        setReportText('');
    }, [props.visible])

    return (
        <Modal
            isVisible={visible} onBackdropPress={onClose}>
            <LoadingIndicator visible={isLoadingInView} />
            <View style={styles.wrapper}>
                <KeyboardAwareScrollView style={styles.content} enableAutomaticScroll={true} extraHeight={150}>
                    <View style={styles.header}>
                        <_Text style={[styles.headerTitle]}>{props.friendName} 님을 신고하시겠어요?</_Text>
                        <_Text style={[styles.headerTitle]}>신고 내용은 관리자 검토 후 처리됩니다.</_Text>
                    </View>
                    {
                        ['부적절한사진', '허위프로필', '욕설 및 비방', '스팸'].map((item: string, index: number) => {
                            return (
                                <TouchableOpacity activeOpacity={1}
                                    style={styles.btn} key={index.toString()}
                                    onPress={() => { setSelectedReport(item) }}>
                                    <_Text style={[styles.btnText,
                                    { color: selectedReport === item ? colors.red : colors.blue }]}>{item}</_Text>
                                </TouchableOpacity>
                            )
                        })
                    }
                    <View style={styles.bottomModal}>
                        <View style={styles.textInputModalView}>
                            <TextInput style={styles.textInput}
                                placeholder='신고 내용을 입력해주세요'
                                numberOfLines={4}
                                multiline={true}
                                textAlignVertical={'top'}
                                onChangeText={_onChangeText}
                            />
                        </View>
                    </View>
                    <TouchableOpacity activeOpacity={1} style={styles.btnSubmit} onPress={onModalSubmit}>
                        <_Text style={styles.btnSubmitText}>제출</_Text>
                    </TouchableOpacity>
                </KeyboardAwareScrollView>
            </View>
        </Modal>
    )
}

const _ReportModal = React.memo(_ReportModalComponent);
export default _ReportModal;

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 16,
        paddingTop: 50,
    },
    content: {
        borderRadius: 13,
        width: '100%',
    },
    header: {
        backgroundColor: colors.white,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 14,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    headerTitle: {
        color: 'rgba(0, 0, 0, 0.4)',
        fontSize: 12,
        lineHeight: 18,
    },
    btn: {
        borderTopWidth: 0.5,
        backgroundColor: colors.white,
    },
    btnSubmit: {
        marginVertical: 4,
        backgroundColor: colors.white,
        borderRadius: 12
    },
    btnText: {
        color: colors.blue,
        textAlign: 'center',
        paddingVertical: 16,
        fontWeight: '400',
        fontSize: 18,
        lineHeight: 25
    },
    btnSubmitText: {
        color: colors.blue,
        textAlign: 'center',
        paddingVertical: 16,
        fontWeight: '500',
        fontSize: 20,
        lineHeight: 25
    },
    bottomModal: {
        backgroundColor: colors.white,
        paddingVertical: 16,
        marginVertical: '0.05%',
        marginTop: 0.5,
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
    },
    textInputModalView: {
        backgroundColor: '#ECF1F9',
        borderRadius: 4,
        marginHorizontal: 20,
        minHeight: 50,
        padding: 5,
    },
    textInput: {
        minHeight: 50,
        fontFamily: Environments.DefaultFont.Regular,
    },
});
