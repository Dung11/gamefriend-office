import React from "react";
import { StyleSheet, View, Image, TouchableOpacity, Dimensions, FlatList, ViewProps, RefreshControl } from "react-native";
import { Icons } from "../../../assets";
import { screen } from "../../../config/Layout";
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../@types';
import _Text from "../../../components/_Text";
import { colors } from "../../../config/Colors";
import Utils from "../../../utils/Utils";

interface Props extends ViewProps {
    myPosts: any,
    navigation: StackNavigationProp<RootStackParamList>
    callBackNewPost: any,
    onClickAvt: (id: string, userName: string) => void,
    onClickImg: (url: string) => void,
    refreshing: boolean,
    onLoadMore: any,
    onShowUserDetail: (userName: string, friendId: string) => void
}

const numColumns = 1;

const renderItem = (item: any,
    navigation: StackNavigationProp<RootStackParamList>,
    onClickAvt: any,
    onClickImg: any,
    onShowUserDetail: any) => {
    if (!item?.author) {
        return <></>
    }
    let username = item.author.userName;
    let title = item.title;
    let profilePicture = { uri: item.author.profilePicture };
    let thumbnail: any = item.pictures.length > 0 && { uri: item.pictures[0].thumbnail };
    let likes = item.likes;
    let comments = item.comments;
    let timeAgo = Utils.timeAgoHandle(item.timeAgo);
    return (
        <TouchableOpacity
            style={styles.containerItem}
            onPress={() => navigation.navigate('NewPost', { postId: item.id })}>
            <TouchableOpacity
                style={{ alignSelf: 'center', margin: '3%' }}
                onPress={() => onClickAvt(item.author.id, item.author.userName)}>
                <Image style={styles.avatar} source={profilePicture} />
            </TouchableOpacity>
            <View style={styles.txtView}>
                <View style={styles.viewNameTime}>
                    <TouchableOpacity
                        style={styles.nameView}
                        onPress={() => onShowUserDetail(item.author.userName, item.author.id)}>
                        <_Text numberOfLines={1} style={styles.txtName}>{username}</_Text>
                    </TouchableOpacity>
                    <View style={styles.dot}></View>
                    <_Text style={styles.txtTime}>{timeAgo}</_Text>
                </View>
                <_Text numberOfLines={1} style={styles.txtContent}>{title}</_Text>
            </View>
            <View style={styles.heartCommentView}>
                <View style={styles.heartView}>
                    <Image style={styles.heartIc} source={Icons.ic_heart} />
                    <_Text style={styles.txtHeart}>{likes}</_Text>
                </View>
                <View style={styles.commentView}>
                    <Image style={styles.heartIc} source={Icons.ic_comment} />
                    <_Text style={styles.txtHeart}>{comments}</_Text>
                </View>
            </View>
            {(thumbnail && thumbnail.uri !== '') &&
                <TouchableOpacity style={{ alignSelf: 'center', margin: '3%' }} onPress={() => onClickImg(thumbnail.uri)}>
                    <Image style={styles.imgPosted} source={thumbnail} />
                </TouchableOpacity>
            }
        </TouchableOpacity>
    )
}

const MineList = (props: Props) => {
    const myPosts = props.myPosts != null ? props.myPosts : [];
    const navigation = props.navigation;
    const callBackNewPost = props.callBackNewPost;
    return (
        <View style={styles.container}>
            {myPosts.length > 0 &&
                <FlatList style={styles.flatL}
                    contentContainerStyle={{ paddingBottom: 50 }}
                    keyExtractor={(item, index) => index.toString()}
                    onEndReachedThreshold={0.1}
                    data={myPosts}
                    numColumns={numColumns}
                    refreshControl={
                        <RefreshControl
                            refreshing={props.refreshing}
                            onRefresh={callBackNewPost} />
                    }
                    renderItem={({ item }) => renderItem(
                        item,
                        navigation,
                        props.onClickAvt,
                        props.onClickImg,
                        props.onShowUserDetail
                    )}
                    onEndReached={props.onLoadMore}
                    extraData={[navigation, callBackNewPost]}
                />}
        </View>
    );
}

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(255,255,255,1)",
        width: width,
        height: height,
    },
    group_list: {
        width: width,
        height: height * 0.5,
        alignSelf: "center",
        marginTop: 5,
    },
    flatL: {
        paddingBottom: '30%',
    },
    containerItem: {
        borderTopWidth: 0.2,
        borderTopColor: colors.gray,
        width: width,
        height: height * 0.1,
        alignSelf: 'center',
        flexDirection: 'row',
    },
    avatar: {
        height: screen.widthscreen / 9,
        width: screen.widthscreen / 9,
        borderRadius: 50,
    },
    txtView: {
        height: screen.heightscreen / 8,
        width: screen.widthscreen / 2.8,
    },
    imgPosted: {
        height: screen.widthscreen / 9,
        width: screen.widthscreen / 7,
        borderRadius: 4,
    },
    heartCommentView: {
        height: screen.widthscreen / 9,
        width: screen.widthscreen / 5.3,
        alignSelf: 'center',
        marginLeft: '2%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    heartView: {
        height: screen.widthscreen / 20,
        width: screen.widthscreen / 10,
        flexDirection: 'row',
        marginRight: 3
    },
    commentView: {
        height: screen.widthscreen / 20,
        width: screen.widthscreen / 14,
        flexDirection: 'row',
        alignItems: 'center'
    },
    heartIc: {
        height: screen.widthscreen / 20,
        width: screen.widthscreen / 20,
        alignSelf: 'center'
    },
    txtHeart: {
        fontSize: 9,
        color: '#8F8F8F',
        alignSelf: 'center',
        margin: 2
    },
    viewNameTime: {
        height: screen.heightscreen / 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtContent: {
        fontSize: 12,
        color: "#65676B",
    },
    nameView: {
        height: screen.heightscreen / 40,
        backgroundColor: '#4387EF',
        padding: screen.widthscreen / 40,
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 5,
        marginRight: '20%',
        paddingHorizontal: screen.widthscreen / 12,
    },
    txtName: {
        color: 'white',
        fontSize: 8,
        lineHeight: 11,
        fontWeight: '800',
        alignSelf: 'center',
        position: 'absolute',
        width: screen.widthscreen / 10,
        textAlign: 'center',
    },
    dot: {
        height: screen.widthscreen / 60,
        width: screen.widthscreen / 60,
        backgroundColor: '#8F8F8F',
        borderRadius: 50,
    },
    txtTime: {
        fontSize: 8,
        color: "#8F8F8F",
        margin: 3,
    },
});

export default MineList;
