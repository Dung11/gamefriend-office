
import React, { useEffect, useState } from 'react';
import {
    View,
    StatusBar,
    TouchableOpacity,
    Image,
    FlatList,
    Alert,
    RefreshControl
} from 'react-native';
import { colors } from '../../../config/Colors';
import { ContainerMain } from '../../../components/Container/ContainerMain';
import { Icons } from "../../../assets";
import styles from './styles';

import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList, renderItem } from '../../../@types';
import _Text from '../../../components/_Text';
import { getNotification } from '../../../api/UserServices';
import { keyValueStorage } from '../../../storage/keyValueStorage';
import Utils from '../../../utils/Utils';

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}

const limit = 10;
const Notify = ({ navigation }: Props) => {

    const [notiList, setNotiList] = useState<any[]>([]);
    const [refreshing, setRefreshing] = React.useState(false);
    const [page, setPage] = useState(1);
    const [quantity, setQuantity] = useState<number>(1);


    useEffect(() => {
        getNotify();
        const unsubscribe = navigation.addListener('focus', () => {
            getNotify(true);
        });
        return unsubscribe;
    }, []);

    const getNotify = async (isRefresh = false) => {
        try {
            setRefreshing(true);
            let notiListTmp = notiList;
            let pageTmp = page;
            let quantityTmp = quantity;

            if (isRefresh) {
                pageTmp = 1;
                notiListTmp = [];
                quantityTmp = 1;

                setPage(1);
                setNotiList([]);
                setQuantity(1);
            }

            if (notiListTmp.length >= quantityTmp) return;
            setRefreshing(true);

            let userId: any = await keyValueStorage.get('userID');
            let notifyData = await getNotification(userId, pageTmp, limit);
            if (notifyData) {
                setQuantity(notifyData.quantity);
                let notificationsTmp = notiListTmp.concat(notifyData.notifications);
                setNotiList(notificationsTmp);
                ++pageTmp;
                setPage(pageTmp);
            }
        } catch (e) {
            Alert.alert('오류...', e);
        } finally {
            setRefreshing(false);
        }
    };

    const showPost = (postId: string) => {
        navigation.navigate('NewPost', { postId });
    }

    const renderItem = ({ item, index }: renderItem) => {
        const timeAgo = Utils.timeAgoHandle(item.data.timeAgo);
        const postId = item.data.post.id;
        return (
            <TouchableOpacity
                style={styles.itemView}
                key={index}
                onPress={() => showPost(postId)}>
                <View style={styles.textsView}>
                    <_Text style={styles.txtTile}>{item.type}</_Text>
                    <_Text style={styles.txtContent}>{item.data.post.title}</_Text>
                </View>
                <_Text style={styles.txtDate}>{timeAgo}</_Text>
            </TouchableOpacity>
        );
    }
    return (
        <ContainerMain>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <View style={styles.header}>
                <TouchableOpacity style={styles.titleContainer}
                    onPress={() => navigation.goBack()}>
                    <Image source={Icons.ic_back} />
                </TouchableOpacity>
                <_Text style={styles.txtTitle}>글알림 목록</_Text>
                <TouchableOpacity style={styles.btnDone}
                    onPress={() => navigation.navigate('GameItem')}>
                    <Image source={Icons.ic_game} />
                </TouchableOpacity>
            </View>
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                onEndReachedThreshold={0.3}
                onEndReached={() => getNotify()}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={() => getNotify(true)} />
                }
                data={notiList}
                renderItem={renderItem} />
        </ContainerMain>
    );
};

export default Notify;
