import { StyleSheet } from 'react-native';
import { colors } from '../../../config/Colors';
import { screen } from '../../../config/Layout';
import DeviceInfo from 'react-native-device-info';

const styles = StyleSheet.create({
    header: {
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        paddingRight: 10,
        flexDirection: 'row',
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    txtTitle: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: '800',
        textAlign: 'center',
        fontSize: 16,
        marginLeft: '7%'
    },
    txtDone: {
        fontSize: 17,
        color: 'white',
        fontWeight: "400",
        textAlign: 'right',
        alignSelf: 'center'
    },
    btnDone: {
        marginTop: '3%'
    },
    group_list: {
        width: screen.widthscreen,
        height: screen.heightscreen / 1.5,
        alignSelf: "center",
        marginTop: 5,
    },
    textsView: {
        paddingHorizontal: '6%',
        paddingVertical: '3%',
        width: '80%',
    },
    txtTile: {
        fontWeight: '400',
        color: '#8F8F8F',
        fontSize: 12
    },
    txtContent: {
        fontWeight: '800',
        color: '#8F8F8F',
        fontSize: 14
    },
    txtDate: {
        fontWeight: '400',
        color: '#8F8F8F',
        fontSize: 11,
        marginTop: '2%'
    },
    itemView: {
        flexDirection: 'row',
        marginBottom: 1,
        backgroundColor: '#fff',
    }
});

export default styles;