interface ColorProps {
      white: string,
      black: string,
      mainColor: string;
      backgroundApp: string;
      grayLight: string;
      gray: string;
      red: string;
      pink: string;
      dark: string;
      blue: string;
      buleSky: string;
      green: string;
      inActiveBlue: string;
      black05:string;
      blurPink: string;
      grey: string;
      gray8F: string;
      warning: string;
      greenActive: string;
      grayInActive: string;
      darkGray: string;
}
export const colors: ColorProps = {
      white: 'white',
      black: 'black',
      mainColor: '#4387EF',
      backgroundApp: '#ECF1F9',
      grayLight: '#E5E5EA',
      gray: '#8E8E93',
      red: '#E35C5C',
      pink: '#EA4774',
      dark: '#1C1E21',
      blue: '#4387EF',
      buleSky: '#184995',
      green: 'green',
      black05: '#050505',
      inActiveBlue: 'rgba(12, 176, 232, 0.2)',
      blurPink: '#EC6083',
      grey: '#F5F5F5',
      gray8F: '#8F8F8F',
      warning: '#ffc107',
      greenActive: '#03A714',
      grayInActive: '#C6C6C8',
      darkGray: '#ECF3FF'
}