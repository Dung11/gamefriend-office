export type ENV_TYPE = 'DEV' | 'PROD';

const ENV: ENV_TYPE = 'DEV';

const PROTOCOL = 'https://'
const DOMAIN = process.env.NODE_ENV === 'development' ? 'api-dev.gamefriend.net' : 'api.gamefriend.net';
const PREFIX = '/api';

const DEV = {
    AUTH_SERVICE_BASE_URL: `${PROTOCOL}${DOMAIN}${PREFIX}`,
    AUTH_SERVICE_BASE_URL_SOCKET: `${PROTOCOL}${DOMAIN}`,
    // AUTH_SERVICE_BASE_URL: 'https://api-dev.gamefriend.net/api',
    // AUTH_SERVICE_BASE_URL_SOCKET: 'https://api-dev.gamefriend.net'
    // AUTH_SERVICE_BASE_URL: 'https://api.gamefriend.net/api',
    // AUTH_SERVICE_BASE_URL: 'http://192.168.1.233:3000',
    // LOCAL_SERVICE_BASE_URL: 'http://192.168.1.80:3000',
    // AUTH_SERVICE_BASE_URL_SOCKET: 'http://192.168.1.233:3030'
    // AUTH_SERVICE_BASE_URL_SOCKET: 'https://api.gamefriend.net'
};

const PROD = {
    AUTH_SERVICE_BASE_URL: '',
};

export default { DEV, PROD, ENV };
