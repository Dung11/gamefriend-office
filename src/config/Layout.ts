import { Dimensions } from "react-native";

const window = Dimensions.get('window');
export const screen = {
    widthscreen: window.width,
    heightscreen: window.height
}