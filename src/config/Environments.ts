// const GOOGLE_API_KEY = "AIzaSyAg8kE_p6s4aWHlQ7STZiWlWe9GepSNjCs";
const GOOGLE_API_KEY = "AIzaSyCVaqj-5YzM729DE2U5GWMbPi7KN4RqXAc";
const AUTHENTICATED_USER = 'AUTHENTICATED_USER';
const ACCESSTOKEN = 'access-token';
const REFRESH_DATA = 'REFRESH_DATA';
const TOPICS_DATA = 'TOPICS_DATA';
const ANDROID_APP_URL = 'https://play.google.com/store/apps/details?id=com.gamefriend';
const IOS_APP_URL = 'https://apps.apple.com/us/app/%EA%B2%9C%EC%B9%9C-%EA%B2%8C%EC%9E%84%EC%B9%9C%EA%B5%AC-%ED%95%84%EC%9A%94%ED%95%A0-%EB%95%8C/id1554806725';
const CODEPUSH_STAGING_KEY = 'RJ3O41ezEB2wrZHgSmw7FRrKUUR6wNhGkcVVk';
const CODEPUSH_PRODUCTION_KEY = 'OjrnDYPTRkdDOEH7xQx6F96j-lVTin6lBQVCI';

export default {
   GOOGLE_API_KEY,
   AUTHENTICATED_USER,
   ACCESSTOKEN,
   REFRESH_DATA,
   DefaultFont: {
      Thin: 'NotoSansKR-Thin',
      Light: 'NotoSansKR-Light',
      Regular: 'NotoSansKR-Regular',
      Medium: 'NotoSansKR-Medium',
      Bold: 'NotoSansKR-Bold',
      Black: 'NotoSansKR-Black',
   },
   TOPICS_DATA,
   ANDROID_APP_URL,
   IOS_APP_URL,
   CODEPUSH_PRODUCTION_KEY,
   CODEPUSH_STAGING_KEY,
};
