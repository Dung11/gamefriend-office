import AsyncStorage from '@react-native-community/async-storage';

// export type KeyType = 'isViewNotify' |
//   'getPost' |
//   'paramsMatch' |
//   'showModalMatch' |
//   'userName' |
//   'messageIntroduction' |
//   'dataMessageIntroduction' |
//   'roomId' |
//   'refreshToken'|
//   'isFirstlogin';

export type KeyType = 'isViewNotify';

class BulletinValueStorage {
  save = (key: KeyType, value: string) => {
    return AsyncStorage.setItem(key, value);
  }

  get = async (key: KeyType) => {
    try {
      const result = await AsyncStorage.getItem(key);
      if (result) return result;
      else return undefined;
    } catch (e) {
      console.log('==>> get error: ', e)
      return undefined;
    }
  }

  delete = (key: KeyType) => {
    return AsyncStorage.removeItem(key);
  }
}

export const bulletinValueStorage = new BulletinValueStorage();
