import React, { useEffect, useState } from 'react';
import { EmitterSubscription, Keyboard, KeyboardEventListener, Platform, ViewStyle } from 'react-native';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Icons, Images } from '../assets';
import { colors } from '../config/Colors';
import { getPersonalProfileRequest } from '../redux/personal-profile/actions';
import store, { AppState } from '../redux/store';
import { connect } from 'react-redux';
import _Text from '../components/_Text';
import { screen } from '../config/Layout';
import * as Animatable from 'react-native-animatable';

interface Props extends BottomTabBarProps {
    personalProfile?: any
}

const _TabBar = (props: Props) => {

    let keyboardDidShowListener: EmitterSubscription;
    let keyboardDidHideListener: EmitterSubscription;
    const { state, descriptors, navigation } = props;
    const focusedOptions = descriptors[state.routes[state.index].key].options;
    const [personalProfile, setPersonalProfile] = useState<any>();
    const [isKeyboardVisible, setKeyboardVisible] = React.useState(false);

    const onDone = (response: any) => {
        setPersonalProfile(response);
    }

    useEffect(() => {
        store.dispatch(getPersonalProfileRequest({ callback: onDone }));
        keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
            setKeyboardVisible(true);
        });
        keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardVisible(false);
        });
        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        }
    }, [])


    const CURRENT_ROUTES = {
        ...state.routes,
        icons: [
            { index: 'LiveTalk', icon: Icons.tabs_live, active: Icons.tabs_active_live },
            { index: 'Bulletin', icon: Icons.tabs_new, active: Icons.tabs_active_new },
            { index: 'Home', icon: Icons.tabs_home, active: Icons.tabs_match },
            { index: 'Chatlist', icon: Icons.tabs_list_chat, active: Icons.tabs_active_list_chat },
            { index: 'Profile', icon: Icons.tabs_profile, active: Icons.tabs_profile },
        ]
    };

    if (focusedOptions.tabBarVisible === false) {
        return null;
    }

    const fadeIn = {
        from: {
            height: 0, opacity: 0,
        },
        to: {
            height: 68, opacity: 1,
        },
    };

    const fadeOut = {
        from: {
            height: 68, opacity: 1,
        },
        to: {
            height: 0, opacity: 0,
        }
    };

    return (
        <Animatable.View style={styles.container} duration={250} animation={isKeyboardVisible ? fadeOut : fadeIn}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label = options.tabBarLabel !== undefined ? options.tabBarLabel
                    : options.title !== undefined ? options.title : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                const textStyles = {
                    ...styles.label,
                    color: isFocused ? colors.blue : '#8E8E93',
                };

                const iconStyles = {
                    ...styles.icon,
                };

                const iconSource = isFocused ? CURRENT_ROUTES.icons[index].active : CURRENT_ROUTES.icons[index].icon;

                if (CURRENT_ROUTES.icons[index].index === 'Home') {
                    return (
                        <TouchableOpacity key={index}
                            accessibilityRole="button"
                            accessibilityState={isFocused ? { selected: true } : {}}
                            accessibilityLabel={options.tabBarAccessibilityLabel}
                            testID={options.tabBarTestID}
                            onPress={onPress}
                            style={styles.homeTabBar}
                        >
                            <Image source={iconSource} style={iconStyles} resizeMode="contain" />
                            <_Text style={textStyles}>{label}</_Text>
                        </TouchableOpacity>
                    );
                }

                return (
                    <TouchableOpacity key={index}
                        accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        style={styles.tabBar}>
                        {
                            CURRENT_ROUTES.icons[index].index !== 'Profile' ?
                                <Image source={iconSource} style={iconStyles} resizeMode="contain" />
                                :
                                <Image source={props.personalProfile?.profilePicture ?
                                    { uri: props.personalProfile?.profilePicture } :
                                    Images.avatar_default} style={styles.avatarIcon}
                                />
                        }
                        <_Text style={textStyles}>{label}</_Text>
                    </TouchableOpacity>
                );
            })}
        </Animatable.View>
    )
}

const mapStateToProps = (state: AppState): Partial<Props> => ({
    personalProfile: state.PersonalProfileReducer.profile
});

export default connect(mapStateToProps, null)(_TabBar);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 68,
    },
    containerAndroid: {
        flexDirection: 'row',
        height: 68,
        position: 'absolute',
        marginTop: '166%',
        // alignSelf: 'center',
        width: screen.widthscreen,
        backgroundColor: 'white',
        justifyContent: 'center'
    },
    tabBar:
    {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingTop: 10,
    },
    tabBarAndroid: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingTop: 10,
        width: screen.widthscreen * 0.18
    },
    homeTabBar: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabBarFirst: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
    },
    tabBarLast: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
    },
    label: {
        fontSize: 8,
        fontWeight: '400',
        color: '#8E8E93',
        paddingTop: 5,
        paddingBottom: 10,
    },
    icon: {
        resizeMode: 'contain',
    },
    avatarIcon: {
        width: 23,
        height: 23,
        borderRadius: 23,
    }
});
