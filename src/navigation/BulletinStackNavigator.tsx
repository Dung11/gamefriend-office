import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import BulletinScreen from '../screen/BulletinScreen';
import GameItemScreen from '../screen/GameItem';
import ViewFullProfile from '../screen/RecommendedFriend/ViewFullProfile';

const Bulletin = createStackNavigator();
const HideHeader = { headerShown: false };

const BulletinStackNavigator = () => {
    return (
        <Bulletin.Navigator initialRouteName="Bulletin" screenOptions={HideHeader}>
            <Bulletin.Screen name="Bulletin"
                component={BulletinScreen}
                options={{ headerShown: false }} />
            <Bulletin.Screen name="GameItem"
                component={GameItemScreen}
                options={{ headerShown: false }} />
            <Bulletin.Screen name="ViewFullProfile"
                component={ViewFullProfile}
                options={{ headerShown: false }} />
        </Bulletin.Navigator>
    )
}

export default BulletinStackNavigator;