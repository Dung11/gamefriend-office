import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import RecommendedFriend from '../screen/RecommendedFriend';
import HomeScreen from '../screen/HomeScreen';
import ViewFullProfile from '../screen/RecommendedFriend/ViewFullProfile';
import FilterRecommendedFriend from '../screen/RecommendedFriend/FilterRecommendedFriend';
import GameItemScreen from '../screen/GameItem';

const HomeStack = createStackNavigator();
const HideHeader = { headerShown: false };

const HomeStackNavigator = () => {
    return (
        <HomeStack.Navigator initialRouteName="Home" screenOptions={HideHeader}>
            <HomeStack.Screen name="Home"
                component={HomeScreen}
                options={{ headerShown: false }} />
            <HomeStack.Screen name="RecommendedFriend"
                component={RecommendedFriend}
                options={{ headerShown: false }} />
            <HomeStack.Screen name="ViewFullProfile"
                component={ViewFullProfile}
                options={{ headerShown: false }} />
            <HomeStack.Screen name="FilterRecommendedFriend"
                component={FilterRecommendedFriend}
                options={{ headerShown: false }} />
            <HomeStack.Screen name="GameItem"
                component={GameItemScreen}/>
        </HomeStack.Navigator>
    )
}

export default HomeStackNavigator;