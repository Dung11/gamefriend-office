import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LiveTalkScreen from '../screen/LiveTalkScreen';
import LiveTalkNeighborhood from '../screen/LiveTalkScreen/LiveTalkNeighborhood';
import { WattingJoinRoom } from '../screen/LiveTalkScreen/LiveTalkNeighborhood/WattingJoinRoom';
import GameItemScreen from '../screen/GameItem';
import ViewFullProfile from '../screen/RecommendedFriend/ViewFullProfile';

const LiveTalkStack = createStackNavigator();
const HideHeader = { headerShown: false };

const LiveTalkStackNavigator = () => {
    return (
        <LiveTalkStack.Navigator initialRouteName="Home" screenOptions={HideHeader}>
            <LiveTalkStack.Screen name="LiveTalkScreen"
                component={LiveTalkScreen}
                options={{ headerShown: false }} />
            <LiveTalkStack.Screen name="LiveTalkNeighborhood"
                component={LiveTalkNeighborhood}
                options={{ headerShown: false }} />
            <LiveTalkStack.Screen name="WattingJoinRoom"
                component={WattingJoinRoom}
                options={{ headerShown: false }} />
            <LiveTalkStack.Screen name="ViewFullProfile"
                component={ViewFullProfile}
                options={{ headerShown: false }} />
            <LiveTalkStack.Screen name="GameItemLiveTalk"
                component={GameItemScreen} />
        </LiveTalkStack.Navigator>
    )
}

export default LiveTalkStackNavigator;