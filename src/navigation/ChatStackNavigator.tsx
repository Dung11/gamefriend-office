import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import ChatListScreen from '../screen/ChatListScreen';
import FriendProfile from '../screen/ChatListScreen/FriendProfile';
import ViewFullProfile from '../screen/RecommendedFriend/ViewFullProfile';
import SetupPropensityChat from '../screen/ChatListScreen/SetupPropensityChat';

const ChatStack = createStackNavigator();
const HideHeader = { headerShown: false };

const ChatStackNavigator = () => {
    return (
        <ChatStack.Navigator initialRouteName="Home" screenOptions={HideHeader}>
            <ChatStack.Screen name="ChatListScreen"
                component={ChatListScreen}
                options={{ headerShown: false }} />
            <ChatStack.Screen name="FriendProfile"
                component={FriendProfile}
                options={{ headerShown: false }} />
            <ChatStack.Screen name="ViewFullProfile"
                component={ViewFullProfile}
                options={{ headerShown: false }} />
            <ChatStack.Screen name="SetupPropensityChat"
                component={SetupPropensityChat}
                options={{ headerShown: false }} />
        </ChatStack.Navigator>
    )
}

export default ChatStackNavigator;