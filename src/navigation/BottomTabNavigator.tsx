import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import ProfileScreen from '../screen/ProfileScreen';
import HomeStackNavigator from './HomeStackNavigator';
import ChatStackNavigator from './ChatStackNavigator';
import _TabBar from './_TabBar';
import NotificationHandler from '../components/Modal/NotificationHandler';
import LiveTalkStackNavigator from './LiveTalkStackNavigator';
import BulletinStackNavigator from './BulletinStackNavigator';

const BottomTab = createBottomTabNavigator();

export default function BottomTabNavigator() {
    return (
        <>
            <BottomTab.Navigator
                initialRouteName="Home"
                tabBar={props => <_TabBar {...props} keyboardHidesTabBar={true} />}
                tabBarOptions={{
                    keyboardHidesTabBar: true,
                }}>
                <BottomTab.Screen
                    name="LiveTalk"
                    component={LiveTalkStackNavigator}
                    options={{ tabBarLabel: "라이브톡" }}
                />
                <BottomTab.Screen
                    name="Bulletin"
                    component={BulletinStackNavigator}
                    options={{ tabBarLabel: "게시판" }} />
                <BottomTab.Screen
                    name="Home"
                    component={HomeStackNavigator}
                    options={{ tabBarLabel: "" }} />
                <BottomTab.Screen
                    name="Chatlist"
                    component={ChatStackNavigator}
                    options={{ tabBarLabel: "대화목록" }} />
                <BottomTab.Screen
                    name="Profile"
                    component={ProfileScreen}
                    options={{ tabBarLabel: "프로필" }} />
            </BottomTab.Navigator>
        </>
    );
}

const TabOneStack = createStackNavigator();

