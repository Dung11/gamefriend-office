import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screen/author/Login';
import BottomTabNavigator from './BottomTabNavigator';
import LoginByEmail from '../screen/author/Login/LoginByEmail';
import { colors } from '../config/Colors';
import RegisterScreen from '../screen/author/Register';
import EnterCodeScreen from '../screen/author/EnterCode';
import ForgotPasswordScreen from '../screen/author/ForgotPassword';
import NewPasswordScreen from '../screen/author/NewPassword';
import VerifiedCodeForgotPasswordScreen from '../screen/author/ForgotPassword/VerifiedCode';
import PolicyTermsScreen from '../screen/author/PolicyTerms';
import AuthLoadingScreen from '../screen/author/AuthLoadingScreen';
import PhoneNumberScreen from '../screen/author/PhoneNumber';
import PhoneNumberForgotPasswordScreen from '../screen/author/ForgotPassword/PhoneNumber';
import BirthDayScreen from '../screen/author/BirthDay';
import GenderScreen from '../screen/author/GenderScreen';
import AddressScreen from '../screen/author/Address';
import UploadImageScreen from '../screen/author/UploadImage';
import NickNameScreen from '../screen/author/NickName';
import { SearchFiterGame } from '../components/SearchFilter/SearchFiter';
import { SearchFiterGameFavorite } from '../components/SearchFilter/SearchFiterGameFavorite';
import ChooseGameScreen from '../screen/author/ChooseGame';
import ChoosePlayTimeScreen from '../screen/author/ChoosePlayTime';
import ChooseCharacterPlayScreen from '../screen/author/ChooseCharacterPlay';
import ChoosePersonalitiesScreen from '../screen/author/ChoosePersonalities';
import WaitingScreen from '../screen/author/WaitingScreen';
import ConfirmScreen from '../screen/author/ConfirmScreen';
import UploadGameImageScreen from '../screen/author/UploadGameImage';
import ModalMatchingSuccess from '../screen/HomeScreen/component/ModalMatchingSuccess';
import RecommendedFriend from '../screen/RecommendedFriend';
import CreateNewPost from '../screen/BulletinScreen/createNewPost';
import NewPost from '../screen/BulletinScreen/newPost';
import Notify from '../screen/BulletinScreen/notify';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { screen } from '../config/Layout';
import CustomMenuChat from '../screen/ChatDetailMatch/CustomMenuchat';
import ChatDetailScreen from '../screen/ChatDetailScreen';
import Menuchat from '../screen/ChatDetailScreen/Menuchat';
import ListFriendRequest from '../screen/ChatListScreen/FriendRequestList';
import ChatDetailMatch from '../screen/ChatDetailMatch';
import { SearchFavouriteGame } from '../screen/BulletinScreen/SearchFavouriteGame';
import Propensity from '../screen/ProfileScreen/Propensity';
import SetupPropensity from '../screen/ProfileScreen/SetupPropensity';
import SuggestedFriendSetting from '../screen/ProfileScreen/SuggestedFriendSetting';
import Noti from '../screen/ProfileScreen/Noti';
import Terms from '../screen/ProfileScreen/Terms/Terms';
import EditProfile from '../screen/ProfileScreen/EditProfile';
import AppInfo from '../screen/ProfileScreen/AppInfo';
import AppInfoMenu from '../screen/ProfileScreen/AppInfoMenu';
import ModalDisConnect from '../components/Modal/ModalDisConnect';
import ModalNoItem from '../components/Modal/NoItemModal';
import SetupPropensityChat from '../screen/ChatListScreen/SetupPropensityChat';
import AlertView from '../components/Modal/AlertView';
import _ModalConfirmItem from '../components/Modal/_ModalConfirmItem';
import LiveTalkChatScreen from '../screen/LiveTalkChatScreen';
import NotificationHandler from '../components/Modal/NotificationHandler';
import Environments from '../config/Environments';
import EmailInputScreen from '../screen/author/ForgotPassword/EmailInput';
import ModalViewProfile from '../screen/LiveTalkChatScreen/ModalViewProfile';
import { ViewFullProfile } from '../screen/RecommendedFriend/ViewFullProfile';
import ViewProfileModal from '../screen/RecommendedFriend/ViewProfileModal';

const MainStack = createStackNavigator();
const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const Stack = createStackNavigator();

const headerDefault = (title: string) => ({
    headerStyle: {
        backgroundColor: colors.mainColor,
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontWeight: '900',
        textAlign: 'center',
        fontFamily: Environments.DefaultFont.Bold,
        fontSize: 16 
    },
    headerTitle: title,
    headerLeft: null
})
const customModal = () => ({
    headerShown: false,
    cardStyle: { backgroundColor: 'rgba(0, 0, 0, 0.3)' },
    // cardOverlayEnabled: true,
    cardStyleInterpolator: ({ current: { progress } }: any) => ({
        cardStyle: {
            opacity: progress.interpolate({
                inputRange: [0, 0.5, 0.9, 1],
                outputRange: [0, 0.25, 0.7, 1],
            }),
        },
        overlayStyle: {
            opacity: progress.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 0.2],
                extrapolate: 'clamp',
            }),
        },
    }),
})

export const Navigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Root" component={RootStackScreen} options={{ headerShown: false }} />
                <Stack.Screen name="CreateNewPost" component={CreateNewPost} options={{ headerShown: false }} />
                <Stack.Screen name="NewPost" component={NewPost} options={{ headerShown: false }} />
                <Stack.Screen name="Notify" component={Notify} options={{ headerShown: false }} />
                <Stack.Screen name="SearchFavouriteGame" component={SearchFavouriteGame} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    );

    function ChatMatch() {
        return (
            <Drawer.Navigator
                initialRouteName="ChatDetailMatch"
                drawerPosition="right"
                drawerContent={(props) => <CustomMenuChat {...props} />}
                drawerStyle={{
                    borderTopLeftRadius: 24,
                    borderBottomLeftRadius: 24
                }}
                drawerType={screen.widthscreen >= 768 ? 'permanent' : 'front'}
            >
                <Drawer.Screen name="ChatDetailMatch" component={ChatDetailMatch} />
            </Drawer.Navigator>
        );
    }

    function ChatScreen() {
        return (
            <Drawer.Navigator
                initialRouteName="ChatDetail"
                drawerPosition="right"
                drawerContent={(props) => <Menuchat {...props} />}
                drawerStyle={{
                    borderTopLeftRadius: 24,
                    borderBottomLeftRadius: 24
                }}
                drawerType={screen.widthscreen >= 768 ? 'permanent' : 'front'}
            >
                <Drawer.Screen name="ChatDetail" component={ChatDetailScreen} />
            </Drawer.Navigator>
        );
    }

    function LiveTalk() {
        return (
            <Drawer.Navigator
                initialRouteName="LiveTalkChat"
                drawerPosition="right"
                drawerContent={(props) => <Menuchat {...props} />}
                drawerStyle={{
                    borderTopLeftRadius: 24,
                    borderBottomLeftRadius: 24
                }}
                drawerType={screen.widthscreen >= 768 ? 'permanent' : 'front'}
            >
                <Drawer.Screen name="LiveTalkChat" component={LiveTalkChatScreen} />
            </Drawer.Navigator>
        );
    }

    function AuthorStackScreen() {
        return (
            <>
                <MainStack.Navigator initialRouteName="Login">
                    <MainStack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
                    <MainStack.Screen name="LoginByEmail" component={LoginByEmail} options={headerDefault('로그인') as Object} />

                    <MainStack.Screen name="Register" component={RegisterScreen} options={headerDefault('회원가입') as Object} />
                    <MainStack.Screen name="EnterCode" component={EnterCodeScreen} options={headerDefault('회원가입') as Object} />
                    <MainStack.Screen name="ForgotPassword" component={ForgotPasswordScreen} options={headerDefault('계정 찾기') as Object} />
                    <MainStack.Screen name="NewPassword" component={NewPasswordScreen} options={headerDefault('로그인') as Object} />
                    <MainStack.Screen name="PhoneNumber" component={PhoneNumberScreen} options={headerDefault('회원가입') as Object} />
                    <MainStack.Screen name="VerifiedCodeForgotPassword" component={VerifiedCodeForgotPasswordScreen} options={headerDefault('계정 찾기') as Object} />
                    <MainStack.Screen name="PhoneNumberForgotPassword" component={PhoneNumberForgotPasswordScreen} options={headerDefault('계정 찾기') as Object} />
                    <MainStack.Screen name="EmailInput" component={EmailInputScreen} options={headerDefault('계정 찾기') as Object} />
                    <MainStack.Screen name="PolicyTerms" component={PolicyTermsScreen} options={headerDefault('회원가입') as Object} />

                    <MainStack.Screen name="BirthDay" component={BirthDayScreen} options={headerDefault('1/10') as Object} />
                    <MainStack.Screen name="Gender" component={GenderScreen} options={headerDefault('2/10') as Object} />
                    <MainStack.Screen name="Address" component={AddressScreen} options={headerDefault('3/10') as Object} />
                    <MainStack.Screen name="UploadImage" component={UploadImageScreen} options={headerDefault('4/10') as Object} />
                    <MainStack.Screen name="UploadGameImage" component={UploadGameImageScreen} options={headerDefault('5/10') as Object} />
                    <MainStack.Screen name="NickName" component={NickNameScreen} options={headerDefault('6/10') as Object} />
                    <MainStack.Screen name="ChooseGame" component={ChooseGameScreen} options={headerDefault('7/10') as Object} />

                    <MainStack.Screen name="SearchFiterGameFavorite" component={SearchFiterGameFavorite} options={headerDefault('7/10') as Object} />

                    <MainStack.Screen name="ChoosePlayTime" component={ChoosePlayTimeScreen} options={headerDefault('8/10') as Object} />
                    <MainStack.Screen name="ChooseCharacterPlay" component={ChooseCharacterPlayScreen} options={headerDefault('9/10') as Object} />
                    <MainStack.Screen name="ChoosePeronalities" component={ChoosePersonalitiesScreen} options={headerDefault('10/10') as Object} />
                    <MainStack.Screen name="Waiting" component={WaitingScreen} options={{ headerShown: false }} />
                    <MainStack.Screen name="Confirm" component={ConfirmScreen} options={{ headerShown: false }} />


                </MainStack.Navigator>
                <AlertView />
            </>
        );
    }

    function MainStackScreen() {
        return (
            <MainStack.Navigator initialRouteName="Tabs">
                <MainStack.Screen name="Tabs" component={BottomTabNavigator} options={{ headerShown: false }} />
                <MainStack.Screen name="ChatMatch" component={ChatMatch} options={{ headerShown: false }} />
                <MainStack.Screen name="ChatScreen" component={ChatScreen} options={{ headerShown: false }} />
                <MainStack.Screen name="LiveTalkChatScreen" component={LiveTalk} options={{ headerShown: false }} />
                <MainStack.Screen name="SearchFiter" component={SearchFiterGame} options={{ headerShown: false }} />
                <MainStack.Screen name="ListFriendRequest" component={ListFriendRequest} options={{ headerShown: false }} />
                <MainStack.Screen name="Propensity" component={Propensity} options={{ headerShown: false }} />
                <MainStack.Screen name="SetPropensity" component={SetupPropensity} options={{ headerShown: false, gestureEnabled: false }} />
                <MainStack.Screen name="SetupPropensityChat" component={SetupPropensityChat} options={{ headerShown: false, gestureEnabled: false }} />
                <MainStack.Screen name="SuggestedFriendSetting" component={SuggestedFriendSetting} options={{ headerShown: false }} />
                <MainStack.Screen name="Noti" component={Noti} options={{ headerShown: false }} />
                <MainStack.Screen name="Terms" component={Terms} options={{ headerShown: false }} />
                <MainStack.Screen name="EditProfile" component={EditProfile} options={{ headerShown: false }} />
                <MainStack.Screen name="AppInfo" component={AppInfo} options={{ headerShown: false }} />
                <MainStack.Screen name="AppInfoMenu" component={AppInfoMenu} options={{ headerShown: false }} />
            </MainStack.Navigator>
        );
    }
    function ModalStackScreen() {
        return (
            <MainStack.Navigator mode="modal">
                <MainStack.Screen name="AlertMatchingSuccess" component={ModalMatchingSuccess} options={customModal} />
                <MainStack.Screen name="AlertDisconnect" component={ModalDisConnect} options={customModal} />
                <MainStack.Screen name="AlertNoItem" component={ModalNoItem} options={customModal} />
                <MainStack.Screen name="AlertConfirmItem" component={_ModalConfirmItem} options={customModal} />
                <MainStack.Screen  name="ModalViewProfile" component={ModalViewProfile} options={customModal}/>
                <MainStack.Screen  name="ModalFullViewProfile" component={ViewFullProfile} options={customModal}/>
                <MainStack.Screen  name="ModalViewProfileRecommend" component={ViewProfileModal} options={customModal}/>

            </MainStack.Navigator>
        );
    }

    function RootStackScreen() {
        return (
            <>
                <NotificationHandler />
                <RootStack.Navigator mode="modal" initialRouteName="AuthLoading">
                    <RootStack.Screen
                        name="Main"
                        component={MainStackScreen}
                        options={{
                            headerShown: false, cardStyle: {
                                backgroundColor: 'transparent'
                            },
                        }}
                    />

                    <RootStack.Screen name="Author" component={AuthorStackScreen} options={{ headerShown: false }} />
                    <RootStack.Screen
                        name="AuthLoading"
                        component={AuthLoadingScreen}
                        options={{ headerShown: false }}
                    />

                    <RootStack.Screen
                        name="Modal"
                        component={ModalStackScreen}
                        options={customModal}
                    />

                </RootStack.Navigator>
            </>
        );
    }
}