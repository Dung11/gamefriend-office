import { ProfileActionTypes, PROFILE_LOCAL_REQUEST, PROFILE_LOCAL_SUCCESS } from "./types";

export interface ProfileState {
  profile_local: any;
  loading : boolean
}
const initialState: ProfileState = {
  profile_local: {},
  loading: false
};
export default function(
  state = initialState,
  action: ProfileActionTypes
): ProfileState {
  switch (action.type) {
    case PROFILE_LOCAL_REQUEST:
      return {...state, loading: true}
    case PROFILE_LOCAL_SUCCESS:
      return { ...state, profile_local: action.payload.user ,loading: false};
    default:
      return state;
  }
}
