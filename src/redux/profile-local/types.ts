
export const PROFILE_LOCAL_REQUEST = "PROFILE_LOCAL_REQUEST";
export const PROFILE_LOCAL_SUCCESS = "PROFILE_LOCAL_SUCCESS";
export const PROFILE_LOCAL_ERROR = "PROFILE_LOCAL_ERROR";
///
export interface SetProfileLocalRequestType {
  type: typeof PROFILE_LOCAL_REQUEST;
  payload: {
    data: any;
  };
}
export interface SetProfileLocalSuccessType {
  type: typeof PROFILE_LOCAL_SUCCESS;
  payload: {
    user: any;
  };
}
export interface SetProfileLocalErrorType {
  type: typeof PROFILE_LOCAL_ERROR;
  payload: {
    error: {
      message: string;
    };
  };
}

export type ProfileActionTypes =
  | SetProfileLocalRequestType
  | SetProfileLocalSuccessType
  | SetProfileLocalErrorType;
