import { call, put, all, takeLatest } from "redux-saga/effects";
import { SetProfileLocalRequestType, PROFILE_LOCAL_REQUEST } from "./types";
import { profileLocalRequest, profileLocalRequestError, profileLocalRequestSuccess } from "./actions";
// import { matchDone } from "../../socketio/socketController";

import { getLocalRoomMember } from "../../api/LivetalkServices";

function* profileLocalRequestSaga(action: SetProfileLocalRequestType) {
    try {
        const profile: any = yield call(getLocalRoomMember, action.payload.data)
        Object.assign(profile)

        yield put(profileLocalRequestSuccess(profile))
    } catch (error) {
        yield put(profileLocalRequestError(error))
    }
}

export default function* actionWatcher() {
    yield all([
        yield takeLatest(PROFILE_LOCAL_REQUEST, profileLocalRequestSaga)
    ])
}