import { ProfileActionTypes, PROFILE_LOCAL_REQUEST, PROFILE_LOCAL_SUCCESS, PROFILE_LOCAL_ERROR } from "./types";

export const profileLocalRequest = (data: any): ProfileActionTypes => ({
  type: PROFILE_LOCAL_REQUEST,
  payload: {
    data
  }
});
//

export const profileLocalRequestSuccess = (
  user: any
): ProfileActionTypes => ({
  type: PROFILE_LOCAL_SUCCESS,
  payload: {
    user
  },
})
export const profileLocalRequestError = (error: {
  message: string;
}): ProfileActionTypes => ({
  type: PROFILE_LOCAL_ERROR,
  payload: {
    error
  }
});
