import { MemberLivetalkActionTypes, MEMBER_LIVETALK_ERROR, MEMBER_LIVETALK_REQUEST, MEMBER_LIVETALK_SUCCESS } from "./types";

export const memberLivetalkRequest = (data: any): MemberLivetalkActionTypes => ({
  type: MEMBER_LIVETALK_REQUEST,
  payload: {
    data
  }
});
//

export const memberLivetalkRequestSuccess = (
  user: any
): MemberLivetalkActionTypes => ({
  type: MEMBER_LIVETALK_SUCCESS,
  payload: {
    user
  },
})
export const memberLivetalkRequestError = (error: {
  message: string;
}): MemberLivetalkActionTypes => ({
  type: MEMBER_LIVETALK_ERROR,
  payload: {
    error
  }
});
