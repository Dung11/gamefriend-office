
export const MEMBER_LIVETALK_REQUEST = "MEMBER_LIVETALK_REQUEST";
export const MEMBER_LIVETALK_SUCCESS = "MEMBER_LIVETALK_SUCCESS";
export const MEMBER_LIVETALK_ERROR = "MEMBER_LIVETALK_ERROR";
///
export interface SetMemberLivetalkRequestType {
  type: typeof MEMBER_LIVETALK_REQUEST;
  payload: {
    data: any;
  };
}
export interface SetMemberLivetalkSuccessType {
  type: typeof MEMBER_LIVETALK_SUCCESS;
  payload: {
    user: any;
  };
}
export interface SetMemberLivetalkErrorType {
  type: typeof MEMBER_LIVETALK_ERROR;
  payload: {
    error: {
      message: string;
    };
  };
}

export type MemberLivetalkActionTypes =
  | SetMemberLivetalkRequestType
  | SetMemberLivetalkSuccessType
  | SetMemberLivetalkErrorType;
