import { MemberLivetalkActionTypes, MEMBER_LIVETALK_ERROR, MEMBER_LIVETALK_REQUEST, MEMBER_LIVETALK_SUCCESS } from "./types";

export interface MemberLivetalkState {
  listMemberlivetalk: any;
  loading: boolean,
  error: any
}
const initialState: MemberLivetalkState = {
  listMemberlivetalk: {},
  loading: false,
  error: {}
};
export default function (
  state = initialState,
  action: MemberLivetalkActionTypes
): MemberLivetalkState {
  switch (action.type) {
    case MEMBER_LIVETALK_REQUEST:
      return { ...state, loading: true }
    case MEMBER_LIVETALK_SUCCESS:
      return { ...state, listMemberlivetalk: action.payload.user, loading: false };
    case MEMBER_LIVETALK_ERROR:
      return { ...state, error: action.payload.error.message, loading: false };
    default:
      return state;
  }
}
