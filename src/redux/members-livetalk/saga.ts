import { call, put, all, takeLatest } from "redux-saga/effects";

import { getRoomInfo } from "../../api/LivetalkServices";
import { memberLivetalkRequestError, memberLivetalkRequestSuccess } from "./actions";
import { MEMBER_LIVETALK_REQUEST, SetMemberLivetalkRequestType } from "./types";

function* getListMemberRequestSaga(action: SetMemberLivetalkRequestType) {
    try {
        const data: any = yield call(getRoomInfo, action.payload.data)
        Object.assign(data)

        yield put(memberLivetalkRequestSuccess(data))
    } catch (error) {
        yield put(memberLivetalkRequestError(error))
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(MEMBER_LIVETALK_REQUEST, getListMemberRequestSaga)
    ])
}