import {
    GameFilterActionTypes,
    GAME_FILTER_REQUEST,
    GAME_FILTER_SUCCESS,
    GAME_FILTER_ERROR,
} from "./types";

export const gameFilterRequest = (data: any): GameFilterActionTypes => ({
    type: GAME_FILTER_REQUEST,
    payload: {
        data
    }
});

export const gameFilterRequestSuccess = (
    filters: any
): GameFilterActionTypes => ({
    type: GAME_FILTER_SUCCESS,
    payload: {
        filters
    },
});

export const gameFilterRequestError = (error: {
    message: string;
}): GameFilterActionTypes => ({
    type: GAME_FILTER_ERROR,
    payload: {
        error
    }
});