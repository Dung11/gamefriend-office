import { call, put, all, takeLatest } from "redux-saga/effects";
import { GAME_FILTER_REQUEST, SetGameFilterRequestType } from "./types";
import { gameFilterRequestSuccess, gameFilterRequestError } from "./actions";
import { getGameFilterPayload } from "../../api/UserServices";

function* gameFilterRequestSaga(action: SetGameFilterRequestType) {
    try {
        const requests: any = yield call(getGameFilterPayload, action.payload);
        Object.assign(requests);
console.log('clm,', requests)
        yield put(gameFilterRequestSuccess(requests));
        action.payload.data.callback && action.payload.data.callback(requests);
    } catch (error) {
        yield put(gameFilterRequestError(error));
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(GAME_FILTER_REQUEST, gameFilterRequestSaga)
    ])
}