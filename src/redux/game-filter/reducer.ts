import {
    GameFilterActionTypes,
    GAME_FILTER_REQUEST,
    GAME_FILTER_SUCCESS,
} from "./types";

export interface GameFilterReqState {
    filters: any;
    loading: boolean
}

const initialState: GameFilterReqState = {
    filters: {},
    loading: false
};

export default function (
    state = initialState,
    action: GameFilterActionTypes
): GameFilterReqState {
    switch (action.type) {
        case GAME_FILTER_REQUEST:
            return { ...state, loading: true }
        case GAME_FILTER_SUCCESS:
            return { ...state, filters: action.payload.filters, loading: false };
        default:
            return state;
    }
}
