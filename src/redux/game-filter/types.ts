
export const GAME_FILTER_REQUEST = "GAME_FILTER_REQUEST";
export const GAME_FILTER_SUCCESS = "GAME_FILTER_SUCCESS";
export const GAME_FILTER_ERROR = "GAME_FILTER_ERROR";
export const GET_GAME_FILTER = "GET_GAME_FILTER";

export interface SetGameFilterRequestType {
    type: typeof GAME_FILTER_REQUEST;
    payload: any;
    callback?: any
}
export interface SetGameFilterSuccessType {
    type: typeof GAME_FILTER_SUCCESS;
    payload: {
        filters: any;
    };
}
export interface GetGameFilterType {
    type: typeof GET_GAME_FILTER;
    payload: any
}
export interface SetGameFilterErrorType {
    type: typeof GAME_FILTER_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}

export type GameFilterActionTypes =
    | SetGameFilterRequestType
    | SetGameFilterSuccessType
    | SetGameFilterErrorType
    | GetGameFilterType;
