import {
    GET_PERSONAL_TENDENCY,
    PersonalTendencyActionTypes,
    PERSONAL_TENDENCY_REQUEST,
    PERSONAL_TENDENCY_SUCCESS
} from "./types";

export interface FriendReqState {
    tendency: any;
    loading: boolean
}

const initialState: FriendReqState = {
    tendency: {},
    loading: false
};

export default function (
    state = initialState,
    action: PersonalTendencyActionTypes
): FriendReqState {
    switch (action.type) {
        case PERSONAL_TENDENCY_REQUEST:
            return { ...state, loading: true }
        case PERSONAL_TENDENCY_SUCCESS:
            state.tendency = action.payload.tendency;
            return { ...state, tendency: action.payload.tendency, loading: false };
        case GET_PERSONAL_TENDENCY:
            action.payload.callback(state.tendency);
            return { ...state, tendency: state.tendency, loading: false };
        default:
            return state;
    }
}
