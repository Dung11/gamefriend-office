import { call, put, all, takeLatest } from "redux-saga/effects";
import { PERSONAL_TENDENCY_REQUEST, SetPersonalTendencyRequestType } from "./types";
import { personalTendencyRequestSuccess, personalTendencyRequestError } from "./actions";
import { personalTendency } from "../../api/UserServices";

function* personalTendencyRequestSaga(action: SetPersonalTendencyRequestType) {
    try {
        const requests: any = yield call(personalTendency, action.payload);
        Object.assign(requests);

        yield put(personalTendencyRequestSuccess(requests));
        action.payload.data.callback && action.payload.data.callback(requests);
    } catch (error) {
        yield put(personalTendencyRequestError(error));
    }
}

export default function* actionWatcher() {
    yield all([
        yield takeLatest(PERSONAL_TENDENCY_REQUEST, personalTendencyRequestSaga)
    ])
}