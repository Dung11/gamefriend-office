import {
    PersonalTendencyActionTypes,
    PERSONAL_TENDENCY_REQUEST,
    PERSONAL_TENDENCY_SUCCESS,
    PERSONAL_TENDENCY_ERROR,
    GET_PERSONAL_TENDENCY
} from "./types";

export const personalTendencyRequest = (data: any): PersonalTendencyActionTypes => ({
    type: PERSONAL_TENDENCY_REQUEST,
    payload: {
        data
    }
});

export const personalTendencyRequestSuccess = (
    tendency: any
): PersonalTendencyActionTypes => ({
    type: PERSONAL_TENDENCY_SUCCESS,
    payload: {
        tendency
    },
});

export const personalTendencyRequestError = (error: {
    message: string;
}): PersonalTendencyActionTypes => ({
    type: PERSONAL_TENDENCY_ERROR,
    payload: {
        error
    }
});

export const getPersonalTendencyRequest = (data: any): PersonalTendencyActionTypes => ({
    type: GET_PERSONAL_TENDENCY,
    payload: data
});