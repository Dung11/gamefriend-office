
export const PERSONAL_TENDENCY_REQUEST = "PERSONAL_TENDENCY_REQUEST";
export const PERSONAL_TENDENCY_SUCCESS = "PERSONAL_TENDENCY_SUCCESS";
export const GET_PERSONAL_TENDENCY = "GET_PERSONAL_TENDENCY";
export const PERSONAL_TENDENCY_ERROR = "PERSONAL_TENDENCY_ERROR";

export interface SetPersonalTendencyRequestType {
    type: typeof PERSONAL_TENDENCY_REQUEST;
    payload: any;
    callback?: any
}
export interface SetPersonalTendencySuccessType {
    type: typeof PERSONAL_TENDENCY_SUCCESS;
    payload: {
        tendency: any;
    };
}
export interface GetPersonalTendencyType {
    type: typeof GET_PERSONAL_TENDENCY;
    payload: any;
}
export interface SetPersonalTendencyErrorType {
    type: typeof PERSONAL_TENDENCY_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}

export type PersonalTendencyActionTypes =
    | SetPersonalTendencyRequestType
    | SetPersonalTendencySuccessType
    | SetPersonalTendencyErrorType
    | GetPersonalTendencyType;
