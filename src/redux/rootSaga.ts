import { all, call, spawn, takeEvery } from 'redux-saga/effects';

import profileSaga from '../redux/profile/saga';
import profileLocalSaga from '../redux/profile-local/saga';
import chatListSaga from '../redux/chatlist/saga'
import tierGameSaga from '../redux/tiergame/saga'
import friendRequestsSaga from '../redux/friendrequest/saga'
import reFriendListReqRequestSaga from '../redux/recommendedfriendlist/saga'
import personProfileSaga from '../redux/personal-profile/saga'
import personTendencySaga from '../redux/personal-tendency/saga'
import gameFilterSaga from '../redux/game-filter/saga'
import bulletinSage from '../redux/bulletin/saga'
import gameRuleSage from '../redux/gamerule/saga'
import memberLivetalkSaga from '../redux/members-livetalk/saga'
import memberLivetalkLocalSaga from '../redux/members-livetalklocal/saga'


// authenticate

function* rootSaga() {
    const sagas = [
        profileSaga,
        profileLocalSaga,
        chatListSaga,
        tierGameSaga,
        friendRequestsSaga,
        reFriendListReqRequestSaga,
        personProfileSaga,
        personTendencySaga,
        gameFilterSaga,
        bulletinSage,
        gameRuleSage,
        memberLivetalkSaga,
        memberLivetalkLocalSaga
    ];

    yield all(
        sagas.map(saga =>
            spawn(function* () {
                while (true) {
                    try {
                        yield call(saga);
                        break;
                    } catch (e) {
                        console.log(e);
                    }
                }
            }),
        ),
    );
}

export default rootSaga;
