import { ProfileActionTypes, PROFILE_REQUEST, PROFILE_SUCCESS, PROFILE_ERROR } from "./types";

export const profileRequest = (data: any): ProfileActionTypes => ({
  type: PROFILE_REQUEST,
  payload: {
    data
  }
});
//

export const profileRequestSuccess = (
  user: any
): ProfileActionTypes => ({
  type: PROFILE_SUCCESS,
  payload: {
    user
  },
})
export const profileRequestError = (error: {
  message: string;
}): ProfileActionTypes => ({
  type: PROFILE_ERROR,
  payload: {
    error
  }
});
