
export const PROFILE_REQUEST = "PROFILE_REQUEST";
export const PROFILE_SUCCESS = "PROFILE_SUCCESS";
export const PROFILE_ERROR = "PROFILE_ERROR";
///
export interface SetProfileRequestType {
  type: typeof PROFILE_REQUEST;
  payload: {
    data: any;
  };
}
export interface SetProfileSuccessType {
  type: typeof PROFILE_SUCCESS;
  payload: {
    user: any;
  };
}
export interface SetProfileErrorType {
  type: typeof PROFILE_ERROR;
  payload: {
    error: {
      message: string;
    };
  };
}

export type ProfileActionTypes =
  | SetProfileRequestType
  | SetProfileSuccessType
  | SetProfileErrorType;
