import { call, put, all, takeLatest } from "redux-saga/effects";
import { SetProfileRequestType, PROFILE_REQUEST } from "./types";
import { profileRequestSuccess, profileRequestError } from "./actions";
import { getRoomMemberInformation } from "../../api/ChatServices";

function* profileRequestSaga(action: SetProfileRequestType) {
    try {
        const profile: any = yield call(getRoomMemberInformation, action.payload.data)
        Object.assign(profile)

        yield put(profileRequestSuccess(profile))
    } catch (error) {
        yield put(profileRequestError(error))
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(PROFILE_REQUEST, profileRequestSaga)
    ])
}