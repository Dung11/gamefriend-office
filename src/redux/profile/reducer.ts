import { ProfileActionTypes, PROFILE_REQUEST, PROFILE_SUCCESS } from "./types";

export interface ProfileState {
  profile: any;
  loading : boolean
}
const initialState: ProfileState = {
  profile: {},
  loading: false
};
export default function(
  state = initialState,
  action: ProfileActionTypes
): ProfileState {
  switch (action.type) {
    case PROFILE_REQUEST:
      return {...state, loading: true}
    case PROFILE_SUCCESS:
      return { ...state, profile: action.payload.user ,loading: false};
    default:
      return state;
  }
}
