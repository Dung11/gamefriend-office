import { TierGameActionTypes, TIERGAME_ERROR, TIERGAME_REQUEST, TIERGAME_SUCCESS } from "./types";

export const tierGameRequest = (data: any): TierGameActionTypes => ({
  type: TIERGAME_REQUEST,
  payload: data
});
//

export const tierGameRequestSuccess = (
  data: any
): TierGameActionTypes => ({
  type: TIERGAME_SUCCESS,
  payload: {
    data
  },
})
export const tierGameRequestError = (error: {
  message: string;
}): TierGameActionTypes => ({
  type: TIERGAME_ERROR,
  payload: {
    error
  }
});
