import { call, put, all, takeLatest } from "redux-saga/effects";
import { RequestType, TIERGAME_REQUEST } from "./types";
import { tierGameRequestError, tierGameRequestSuccess } from "./actions";
import { getTiesGame } from "../../api/GameServices";

function* TiergameRequestSaga(action: RequestType) {
    try {
        const list: any = yield call(getTiesGame, action.payload)
        Object.assign(list)

        yield put(tierGameRequestSuccess(list))
    } catch (error) {
        yield put(tierGameRequestError(error))
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(TIERGAME_REQUEST, TiergameRequestSaga)
    ])
}