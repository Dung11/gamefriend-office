import {  TierGameActionTypes, TIERGAME_REQUEST, TIERGAME_SUCCESS } from "./types";

export interface TierGameState {
  tierGame: any;
  loading : boolean
}
const initialState: TierGameState = {
  tierGame: [],
  loading: false
};
export default function(
  state = initialState,
  action: TierGameActionTypes
): TierGameState {
  switch (action.type) {
    case TIERGAME_REQUEST:
      return {...state, loading: true}
    case TIERGAME_SUCCESS:
      return { ...state, tierGame: action.payload.data ,loading: false};
    default:
      return state;
  }
}
