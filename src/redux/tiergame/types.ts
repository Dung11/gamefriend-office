
export const TIERGAME_REQUEST = "TIERGAME_REQUEST";
export const TIERGAME_SUCCESS = "TIERGAME_SUCCESS";
export const TIERGAME_ERROR = "TIERGAME_ERROR";
///
export interface RequestType {
  type: typeof TIERGAME_REQUEST;
  payload: any
}
export interface SuccessType {
  type: typeof TIERGAME_SUCCESS;
  payload: {
    data: any;
  };
}
export interface ErrorType {
  type: typeof TIERGAME_ERROR;
  payload: {
    error: {
      message: string;
    };
  };
}

export type TierGameActionTypes =
  | RequestType
  | SuccessType
  | ErrorType;
