
export const MEMBER_LIVETALK_LOCAL_REQUEST = "MEMBER_LIVETALK_LOCAL_REQUEST";
export const MEMBER_LIVETALK_LOCAL_SUCCESS = "MEMBER_LIVETALK_LOCAL_SUCCESS";
export const MEMBER_LIVETALK_LOCAL_ERROR = "MEMBER_LIVETALK_LOCAL_ERROR";
///
export interface SetMemberLivetalkLocalRequestType {
  type: typeof MEMBER_LIVETALK_LOCAL_REQUEST;
  payload: {
    data: any;
  };
}
export interface SetMemberLivetalkLocalSuccessType {
  type: typeof MEMBER_LIVETALK_LOCAL_SUCCESS;
  payload: {
    user: any;
  };
}
export interface SetMemberLivetalkLocalErrorType {
  type: typeof MEMBER_LIVETALK_LOCAL_ERROR;
  payload: {
    error: {
      message: string;
    };
  };
}

export type MemberLivetalkLocalActionTypes =
  | SetMemberLivetalkLocalRequestType
  | SetMemberLivetalkLocalSuccessType
  | SetMemberLivetalkLocalErrorType;
