import { MEMBER_LIVETALK_ERROR } from "../members-livetalk/types";
import { MemberLivetalkLocalActionTypes, MEMBER_LIVETALK_LOCAL_ERROR, MEMBER_LIVETALK_LOCAL_REQUEST, MEMBER_LIVETALK_LOCAL_SUCCESS } from "./types";

export interface MemberLivetalkLocaState {
  listMemberivetalkLocal: any;
  error: any
  loading: boolean
}
const initialState: MemberLivetalkLocaState = {
  listMemberivetalkLocal: {},
  loading: false,
  error: {}
};
export default function (
  state = initialState,
  action: MemberLivetalkLocalActionTypes
): MemberLivetalkLocaState {
  switch (action.type) {
    case MEMBER_LIVETALK_LOCAL_REQUEST:
      return { ...state, loading: true }
    case MEMBER_LIVETALK_LOCAL_SUCCESS:
      return { ...state, listMemberivetalkLocal: action.payload.user, loading: false };
    case MEMBER_LIVETALK_LOCAL_ERROR:
      return { ...state, error: action.payload.error.message, loading: false };
    default:
      return state;
  }
}
