import { call, put, all, takeLatest } from "redux-saga/effects";
import { getLocalRoomInfo } from "../../api/LivetalkServices";

import { memberLivetalkLocalRequestError, memberLivetalkLocalRequestSuccess } from "./actions";
import { MEMBER_LIVETALK_LOCAL_REQUEST, SetMemberLivetalkLocalRequestType } from "./types";


function* getListMemberRequestSaga(action: SetMemberLivetalkLocalRequestType) {
    try {
        const data: any = yield call(getLocalRoomInfo, action.payload.data)
        Object.assign(data)

        yield put(memberLivetalkLocalRequestSuccess(data))
    } catch (error) {
        yield put(memberLivetalkLocalRequestError(error))
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(MEMBER_LIVETALK_LOCAL_REQUEST, getListMemberRequestSaga)
    ])
}