import { MemberLivetalkLocalActionTypes, MEMBER_LIVETALK_LOCAL_ERROR, MEMBER_LIVETALK_LOCAL_REQUEST, MEMBER_LIVETALK_LOCAL_SUCCESS } from "./types";

export const memberLivetalkLocalRequest = (data: any): MemberLivetalkLocalActionTypes => ({
  type: MEMBER_LIVETALK_LOCAL_REQUEST,
  payload: {
    data
  }
});
//

export const memberLivetalkLocalRequestSuccess = (
  user: any
): MemberLivetalkLocalActionTypes => ({
  type: MEMBER_LIVETALK_LOCAL_SUCCESS,
  payload: {
    user
  },
})
export const memberLivetalkLocalRequestError = (error: {
  message: string;
}): MemberLivetalkLocalActionTypes => ({
  type: MEMBER_LIVETALK_LOCAL_ERROR,
  payload: {
    error
  }
});
