
export const PERSONAL_PROFILE_REQUEST = "PERSONAL_PROFILE_REQUEST";
export const PERSONAL_PROFILE_SUCCESS = "PERSONAL_PROFILE_SUCCESS";
export const PERSONAL_PROFILE_ERROR = "PERSONAL_PROFILE_ERROR";
export const GET_PERSONAL_PROFILE = "GET_PERSONAL_PROFILE";

export interface SetPersonalProfileRequestType {
    type: typeof PERSONAL_PROFILE_REQUEST;
    payload: any;
    callback?: any
}
export interface SetPersonalProfileSuccessType {
    type: typeof PERSONAL_PROFILE_SUCCESS;
    payload: {
        profile: any;
    };
}
export interface GetPersonalProfileType {
    type: typeof GET_PERSONAL_PROFILE;
    payload: any
}
export interface SetPersonalProfileErrorType {
    type: typeof PERSONAL_PROFILE_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}

export type PersonalProfileActionTypes =
    | SetPersonalProfileRequestType
    | SetPersonalProfileSuccessType
    | SetPersonalProfileErrorType
    | GetPersonalProfileType;
