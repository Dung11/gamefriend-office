import {
    PersonalProfileActionTypes,
    PERSONAL_PROFILE_REQUEST,
    PERSONAL_PROFILE_SUCCESS,
    PERSONAL_PROFILE_ERROR,
    GET_PERSONAL_PROFILE
} from "./types";

export const personalProfileRequest = (data: any): PersonalProfileActionTypes => ({
    type: PERSONAL_PROFILE_REQUEST,
    payload: {
        data
    }
});

export const personalProfileRequestSuccess = (
    profile: any
): PersonalProfileActionTypes => ({
    type: PERSONAL_PROFILE_SUCCESS,
    payload: {
        profile
    },
});

export const personalProfileRequestError = (error: {
    message: string;
}): PersonalProfileActionTypes => ({
    type: PERSONAL_PROFILE_ERROR,
    payload: {
        error
    }
});

export const getPersonalProfileRequest = (data: any): PersonalProfileActionTypes => ({
    type: GET_PERSONAL_PROFILE,
    payload: data
});