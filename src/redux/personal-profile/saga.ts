import { call, put, all, takeLatest } from "redux-saga/effects";
import { PERSONAL_PROFILE_REQUEST, SetPersonalProfileRequestType } from "./types";
import { personalProfileRequestSuccess, personalProfileRequestError } from "./actions";
import { personalProfile } from "../../api/UserServices";

function* personalProfileRequestSaga(action: SetPersonalProfileRequestType) {
    try {
        const requests: any = yield call(personalProfile, action.payload);
        Object.assign(requests);

        yield put(personalProfileRequestSuccess(requests));
        action.payload.data.callback && action.payload.data.callback(requests);
    } catch (error) {
        yield put(personalProfileRequestError(error));
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(PERSONAL_PROFILE_REQUEST, personalProfileRequestSaga)
    ])
}