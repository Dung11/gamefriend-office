import {
    PersonalProfileActionTypes,
    PERSONAL_PROFILE_REQUEST,
    PERSONAL_PROFILE_SUCCESS,
    GET_PERSONAL_PROFILE
} from "./types";

export interface FriendReqState {
    profile: any;
    loading: boolean
}

const initialState: FriendReqState = {
    profile: {},
    loading: false
};

export default function (
    state = initialState,
    action: PersonalProfileActionTypes
): FriendReqState {
    switch (action.type) {
        case PERSONAL_PROFILE_REQUEST:
            return { ...state, loading: true }
        case PERSONAL_PROFILE_SUCCESS:
            return { ...state, profile: action.payload.profile, loading: false };
        case GET_PERSONAL_PROFILE:
            action.payload.callback(state.profile);
            return { ...state, profile: state.profile, loading: false };
        default:
            return state;
    }
}
