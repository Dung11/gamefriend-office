import store from "../store";
import {
    ReFriendReqActionTypes,
    RECOMMENDED_FRIEND_LIST_REQ_REQUEST,
    RECOMMENDED_FRIEND_LIST_REQ_SUCCESS,
    EDIT_RECOMMENDED_FRIEND_REQ
} from "./types";

export interface ReFriendListReqState {
    recommendations: any;
    loading: boolean
}

const initialState: ReFriendListReqState = {
    recommendations: {},
    loading: false
};

export default function (
    state = initialState,
    action: ReFriendReqActionTypes
): ReFriendListReqState {
    switch (action.type) {
        case RECOMMENDED_FRIEND_LIST_REQ_REQUEST:
            return { ...state, loading: true }
        case RECOMMENDED_FRIEND_LIST_REQ_SUCCESS:
            state.recommendations = action.payload.recommendations;
            return { ...state, recommendations: action.payload.recommendations, loading: false };
        case EDIT_RECOMMENDED_FRIEND_REQ:
            let friendId = action.payload.friendId;
            let recommendationRes: any = state.recommendations;
            if (recommendationRes && Object.keys(recommendationRes).length > 0) {
                let recommendations: any[] = recommendationRes.recommendations;
    
                let selectedFriendIndex = recommendations.findIndex((d: any) => d?.recommendedUser.id === friendId);
                if (selectedFriendIndex >= 0){
                    let isRequested = recommendations[selectedFriendIndex].isRequested;
                    recommendations[selectedFriendIndex].isRequested = !isRequested;
                    state.recommendations.recommendations = recommendations;
                }
            }
            return { ...state, recommendations: action.payload.recommendations, loading: false };
        default:
            return state;
    }
}
