
export const RECOMMENDED_FRIEND_LIST_REQ_REQUEST = "RECOMMENDED_FRIEND_LIST_REQ_REQUEST";
export const RECOMMENDED_FRIEND_LIST_REQ_SUCCESS = "RECOMMENDED_FRIEND_LIST_REQ_SUCCESS";
export const RECOMMENDED_FRIEND_LIST_REQ_ERROR = "RECOMMENDED_FRIEND_LIST_REQ_ERROR";
export const EDIT_RECOMMENDED_FRIEND_REQ = "EDIT_RECOMMENDED_FRIEND_REQ";

export interface SetReFriendListReqRequestType {
    type: typeof RECOMMENDED_FRIEND_LIST_REQ_REQUEST;
    payload: any;
    callback?: any
}
export interface SetReFriendListReqSuccessType {
    type: typeof RECOMMENDED_FRIEND_LIST_REQ_SUCCESS;
    payload: {
        recommendations: any;
    };
}
export interface SetReFriendListReqErrorType {
    type: typeof RECOMMENDED_FRIEND_LIST_REQ_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}

export interface EditReFriendRequest {
    type: typeof EDIT_RECOMMENDED_FRIEND_REQ;
    payload: any
}

export type ReFriendReqActionTypes =
    | SetReFriendListReqRequestType
    | SetReFriendListReqSuccessType
    | SetReFriendListReqErrorType
    | EditReFriendRequest;
