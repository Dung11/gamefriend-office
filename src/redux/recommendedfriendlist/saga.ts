import { call, put, all, takeLatest } from "redux-saga/effects";
import {
    SetReFriendListReqRequestType,
    RECOMMENDED_FRIEND_LIST_REQ_REQUEST,
    EDIT_RECOMMENDED_FRIEND_REQ
}
    from "./types";
import { recommendedFriendListReqRequestSuccess, recommendedFriendListReqRequestError } from "./actions";
import { getRecommendationListPayload } from "../../api/RecommendedServices";

function* reFriendListReqRequestSaga(action: SetReFriendListReqRequestType) {
    try {
        const requests: any = yield call(getRecommendationListPayload, action.payload);
        Object.assign(requests);

        yield put(recommendedFriendListReqRequestSuccess(requests));
        action.payload.data.callback && action.payload.data.callback(requests);
    } catch (error) {
        yield put(recommendedFriendListReqRequestError(error));
    }
}

export default function* actionWatcher() {
    yield all([
        yield takeLatest(RECOMMENDED_FRIEND_LIST_REQ_REQUEST, reFriendListReqRequestSaga),
    ])
}