import {
    ReFriendReqActionTypes,
    RECOMMENDED_FRIEND_LIST_REQ_REQUEST,
    RECOMMENDED_FRIEND_LIST_REQ_SUCCESS,
    RECOMMENDED_FRIEND_LIST_REQ_ERROR,
    EDIT_RECOMMENDED_FRIEND_REQ
}
    from "./types";

export const recommendedFriendListReqRequest = (data: any): ReFriendReqActionTypes => ({
    type: RECOMMENDED_FRIEND_LIST_REQ_REQUEST,
    payload: {
        data
    }
});

export const recommendedFriendListReqRequestSuccess = (
    requests: any
): ReFriendReqActionTypes => ({
    type: RECOMMENDED_FRIEND_LIST_REQ_SUCCESS,
    payload: {
        recommendations: requests
    },
});

export const recommendedFriendListReqRequestError = (error: {
    message: string;
}): ReFriendReqActionTypes => ({
    type: RECOMMENDED_FRIEND_LIST_REQ_ERROR,
    payload: {
        error
    }
});

export const editRecommendedFriendRequest = (data: any): ReFriendReqActionTypes => ({
    type: EDIT_RECOMMENDED_FRIEND_REQ,
    payload: data
});