import {createStore, applyMiddleware} from 'redux';
// import thunk from 'redux-./counter/reducer
import reducers from './rootReducer';
import rootSaga from './rootSaga';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './rootReducer';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export type AppState = ReturnType<typeof rootReducer>;

export default store;
