import { combineReducers } from 'redux';
import profileReducer from '../redux/profile/reducer';
import profileLocalReducer from '../redux/profile-local/reducer';
import chatListReducer from '../redux/chatlist/reducer';
import tireGameReducer from '../redux/tiergame/reducer';
import friendRequestsReducer from '../redux/friendrequest/reducer';
import recommendedFriendListRequestsReducer from '../redux/recommendedfriendlist/reducer';
import personalProfileReducer from '../redux/personal-profile/reducer';
import personalTendencyReducer from '../redux/personal-tendency/reducer';
import gameFilterReducer from '../redux/game-filter/reducer';
import bulletinReducer from '../redux/bulletin/reducer';
import gameRuleReducer from '../redux/gamerule/reducer';
import memberLivetalkReducer from '../redux/members-livetalk/reducer'
import memberLivetalkLocalReducer from '../redux/members-livetalklocal/reducer'

export default combineReducers({
    MemberLivetalkReducer: memberLivetalkReducer,
    MemberLivetalkLocalReducer: memberLivetalkLocalReducer,
    ProfileReducer: profileReducer,
    ProfileLocalReducer: profileLocalReducer,
    ChatListReducer: chatListReducer,
    TireGameReducer: tireGameReducer,
    FriendRequestsReducer: friendRequestsReducer,
    RecommendedFriendListRequestsReducer: recommendedFriendListRequestsReducer,
    PersonalProfileReducer: personalProfileReducer,
    PersonalTendencyReducer: personalTendencyReducer,
    GameFilterReducer: gameFilterReducer,
    BulletinReducer :bulletinReducer,
    GameRuleReducer :gameRuleReducer
});
