export const FRIEND_REQ_REQUEST = "FRIEND_REQ_REQUEST";
export const FRIEND_REQ_SUCCESS = "FRIEND_REQ_SUCCESS";
export const FRIEND_REQ_ERROR = "FRIEND_REQ_ERROR";

export interface SetFriendReqRequestType {
    type: typeof FRIEND_REQ_REQUEST;
    payload: any;
    callback?: any
}
export interface SetFriendReqSuccessType {
    type: typeof FRIEND_REQ_SUCCESS;
    payload: {
        requests: any;
    };
}
export interface SetFriendReqErrorType {
    type: typeof FRIEND_REQ_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}

export type FriendReqActionTypes =
    | SetFriendReqRequestType
    | SetFriendReqSuccessType
    | SetFriendReqErrorType;
