import { FriendReqActionTypes, FRIEND_REQ_REQUEST, FRIEND_REQ_SUCCESS } from "./types";

export interface FriendReqState {
    friendRequests: any;
    loading: boolean
}

const initialState: FriendReqState = {
    friendRequests: {},
    loading: false
};

export default function (
    state = initialState,
    action: FriendReqActionTypes
): FriendReqState {
    switch (action.type) {
        case FRIEND_REQ_REQUEST:
            return { ...state, loading: true }
        case FRIEND_REQ_SUCCESS:
            return { ...state, friendRequests: action.payload.requests, loading: false };
        default:
            return state;
    }
}
