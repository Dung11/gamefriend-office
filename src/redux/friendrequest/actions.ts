import { FriendReqActionTypes, FRIEND_REQ_REQUEST, FRIEND_REQ_SUCCESS, FRIEND_REQ_ERROR } from "./types";

export const friendReqRequest = (data: any): FriendReqActionTypes => ({
    type: FRIEND_REQ_REQUEST,
    payload: {
        data
    }
});

export const friendReqRequestSuccess = (
    requests: any
): FriendReqActionTypes => ({
    type: FRIEND_REQ_SUCCESS,
    payload: {
        requests
    },
});

export const friendReqRequestError = (error: {
    message: string;
}): FriendReqActionTypes => ({
    type: FRIEND_REQ_ERROR,
    payload: {
        error
    }
});