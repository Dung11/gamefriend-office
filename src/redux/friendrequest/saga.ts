import { call, put, all, takeLatest } from "redux-saga/effects";
import { SetFriendReqRequestType, FRIEND_REQ_REQUEST } from "./types";
import { friendReqRequestSuccess, friendReqRequestError } from "./actions";
import { getUserRequestsQuantity } from "../../api/UserServices";

function* friendReqRequestSaga(action: SetFriendReqRequestType) {
    try {
        const requests: any = yield call(getUserRequestsQuantity, action.payload);
        Object.assign(requests);

        yield put(friendReqRequestSuccess(requests));
        action.payload.data.callback && action.payload.data.callback(requests);
    } catch (error) {
        yield put(friendReqRequestError(error));
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(FRIEND_REQ_REQUEST, friendReqRequestSaga)
    ])
}