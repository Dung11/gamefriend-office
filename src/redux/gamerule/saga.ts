import { call, put, all, takeLatest } from "redux-saga/effects";
import { getGameRule } from "../../api/GeneralServices";
import { gameRuleRequestError, gameRuleRequestSuccess } from "./actions";
import { GAME_RULE_REQUEST, SetGameRuleRequestType } from "./types";

function* gameRuleRequestSaga(action: SetGameRuleRequestType) {
    try {
        const rules: any = yield call(getGameRule)
        Object.assign(rules)

        yield put(gameRuleRequestSuccess(rules));
        action.payload.callback && action.payload.callback(rules);
    } catch (error) {
        yield put(gameRuleRequestError(error))
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(GAME_RULE_REQUEST, gameRuleRequestSaga)
    ])
}