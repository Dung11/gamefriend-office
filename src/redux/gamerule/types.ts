
export const GAME_RULE_REQUEST = "GAME_RULE_REQUEST";
export const GAME_RULE_SUCCESS = "GAME_RULE_SUCCESS";
export const GAME_RULE_ERROR = "GAME_RULE_ERROR";
export const GET_GAME_RULE = "GET_GAME_RULE";

export interface SetGameRuleRequestType {
    type: typeof GAME_RULE_REQUEST;
    payload: any,
    callback?: any
}
export interface SetGameRuleSuccessType {
    type: typeof GAME_RULE_SUCCESS;
    payload: {
        data: any;
    };
}
export interface SetGameRuleErrorType {
    type: typeof GAME_RULE_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}
export interface GetGameRuleType {
    type: typeof GET_GAME_RULE;
    payload: any
}

export type GameRuleActionTypes =
    | SetGameRuleRequestType
    | SetGameRuleSuccessType
    | SetGameRuleErrorType
    | GetGameRuleType;
