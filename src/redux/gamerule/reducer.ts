import { GAME_RULE_REQUEST, GAME_RULE_SUCCESS, GameRuleActionTypes, GET_GAME_RULE } from "./types";

export interface GameRuleState {
    rules: any;
    loading: boolean
}
const initialState: GameRuleState = {
    rules: {},
    loading: false
};
export default function (
    state = initialState,
    action: GameRuleActionTypes
): GameRuleState {
    switch (action.type) {
        case GAME_RULE_REQUEST:
            return { ...state, loading: true }
        case GAME_RULE_SUCCESS:
            state.rules = action.payload.data;
            return { ...state, rules: action.payload.data, loading: false };
        case GET_GAME_RULE:
            action.payload.callback(state.rules);
            return { ...state, rules: state.rules, loading: false };
        default:
            return state;
    }
}
