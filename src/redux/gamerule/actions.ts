import { GAME_RULE_ERROR, GAME_RULE_REQUEST, GAME_RULE_SUCCESS, GameRuleActionTypes, GET_GAME_RULE } from "./types";

export const gameRuleRequest = (data: any): GameRuleActionTypes => ({
    type: GAME_RULE_REQUEST,
    payload: data
});

export const gameRuleRequestSuccess = (
    data: any
): GameRuleActionTypes => ({
    type: GAME_RULE_SUCCESS,
    payload: {
        data
    },
})
export const gameRuleRequestError = (error: {
    message: string;
}): GameRuleActionTypes => ({
    type: GAME_RULE_ERROR,
    payload: {
        error
    }
});

export const getGameRuleRequest = (data: any): GameRuleActionTypes => ({
    type: GET_GAME_RULE,
    payload: data
});
