
export const CHATLIST_REQUEST = "CHATLIST_REQUEST";
export const CHATLIST_SUCCESS = "CHATLIST_SUCCESS";
export const CHATLIST_ERROR = "CHATLIST_ERROR";
///
export interface SetChatListRequestType {
    type: typeof CHATLIST_REQUEST;
    payload: any,
    callback?: any
}
export interface SetChatListSuccessType {
    type: typeof CHATLIST_SUCCESS;
    payload: {
        data: any;
    };
}
export interface SetChatListErrorType {
    type: typeof CHATLIST_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}

export type ProfileActionTypes =
    | SetChatListRequestType
    | SetChatListSuccessType
    | SetChatListErrorType;
