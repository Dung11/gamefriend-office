import { call, put, all, takeLatest } from "redux-saga/effects";
import { getChatListDataPayload } from "../../api/ChatServices";
import { chatListRequestError, chatListRequestSuccess } from "./actions";
import { CHATLIST_REQUEST, SetChatListRequestType } from "./types";

function* profileRequestSaga(action: SetChatListRequestType) {
    try {
        const list: any = yield call(getChatListDataPayload, action.payload)
        Object.assign(list)

        yield put(chatListRequestSuccess(list));
        action.payload.callback && action.payload.callback(list);
    } catch (error) {
        yield put(chatListRequestError(error))
    }
}
export default function* actionWatcher() {
    yield all([
        yield takeLatest(CHATLIST_REQUEST, profileRequestSaga)
    ])
}