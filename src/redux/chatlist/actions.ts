import { CHATLIST_ERROR, CHATLIST_REQUEST, CHATLIST_SUCCESS, ProfileActionTypes } from "./types";

export const chatListRequest = (data: any): ProfileActionTypes => ({
    type: CHATLIST_REQUEST,
    payload: data
});
//

export const chatListRequestSuccess = (
    data: any
): ProfileActionTypes => ({
    type: CHATLIST_SUCCESS,
    payload: {
        data
    },
})
export const chatListRequestError = (error: {
    message: string;
}): ProfileActionTypes => ({
    type: CHATLIST_ERROR,
    payload: {
        error
    }
});
