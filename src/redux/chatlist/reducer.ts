import { CHATLIST_REQUEST, CHATLIST_SUCCESS, ProfileActionTypes } from "./types";

export interface ChatListState {
  chatlist: any;
  loading : boolean
}
const initialState: ChatListState = {
  chatlist: [],
  loading: false
};
export default function(
  state = initialState,
  action: ProfileActionTypes
): ChatListState {
  switch (action.type) {
    case CHATLIST_REQUEST:
      return {...state, loading: true}
    case CHATLIST_SUCCESS:
      return { ...state, chatlist: action.payload.data ,loading: false};
    default:
      return state;
  }
}
