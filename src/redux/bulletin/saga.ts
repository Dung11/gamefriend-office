import { call, put, all, takeLatest } from "redux-saga/effects";
import { bulletinListRequestError, bulletinListRequestSuccess } from "./actions";
import { BULLETIN_LIST_REQUEST, SetBulletinListRequestType } from "./types";

function* bulletinRequestSaga(action: SetBulletinListRequestType) {
    try {
        // const list: any = yield call(apiBulletinBoard.getPostsR, action.payload)
        // Object.assign(list)
        // yield put(bulletinListRequestSuccess(list));
        // action.payload.callback && action.payload.callback(list);
    } catch (error) {
        yield put(bulletinListRequestError(error))
    }
}
export default function* actionWatcher() {
    yield all([
        // yield takeLatest(BULLETIN_LIST_REQUEST, bulletinRequestSaga)
    ])
}