import { BULLETIN_LIST_ERROR, BULLETIN_LIST_REQUEST, BULLETIN_LIST_SUCCESS, BulletinActionTypes } from "./types";

export const bulletinListRequest = (data: any): BulletinActionTypes => ({
    type: BULLETIN_LIST_REQUEST,
    payload: data
});
//

export const bulletinListRequestSuccess = (data: any): BulletinActionTypes => ({
    type: BULLETIN_LIST_SUCCESS,
    payload: {
        data
    },
})

export const bulletinListRequestError = (error: { message: string; }): BulletinActionTypes => ({
    type: BULLETIN_LIST_ERROR,
    payload: {
        error
    }
});
