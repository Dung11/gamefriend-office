
export const BULLETIN_LIST_REQUEST = "BULLETIN_LIST_REQUEST";
export const BULLETIN_LIST_SUCCESS = "BULLETIN_LIST_SUCCESS";
export const BULLETIN_LIST_ERROR = "BULLETIN_LIST_ERROR";
///
export interface SetBulletinListRequestType {
    type: typeof BULLETIN_LIST_REQUEST;
    payload: any,
    callback?: any
}
export interface SetBulletinListSuccessType {
    type: typeof BULLETIN_LIST_SUCCESS;
    payload: {
        data: any;
    };
}
export interface SetBulletinListErrorType {
    type: typeof BULLETIN_LIST_ERROR;
    payload: {
        error: {
            message: string;
        };
    };
}

export type BulletinActionTypes =
    | SetBulletinListRequestType
    | SetBulletinListSuccessType
    | SetBulletinListErrorType;
