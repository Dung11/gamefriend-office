import { BULLETIN_LIST_REQUEST, BULLETIN_LIST_SUCCESS, BulletinActionTypes } from "./types";

export interface BulletinListState {
  bulletinList: any;
  loading : boolean
}
const initialState: BulletinListState = {
  bulletinList: [],
  loading: false
};
export default function(
  state = initialState,
  action: BulletinActionTypes
): BulletinListState {
  switch (action.type) {
    case BULLETIN_LIST_REQUEST:
      return {...state, loading: true}
    case BULLETIN_LIST_SUCCESS:
      return { ...state, bulletinList: action.payload.data ,loading: false};
    default:
      return state;
  }
}
