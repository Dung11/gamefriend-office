import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack/lib/typescript/src/types';
import React, { useEffect, useState } from 'react';
import { TextInput, StyleSheet, View, Platform, Image, TouchableOpacity, FlatList, Alert } from "react-native";
import { ScrollView } from 'react-native-gesture-handler';
import { RootStackParamList } from '../../@types';
import { getListGame } from '../../api/GameServices';
import { Icons } from '../../assets';
import { colors } from '../../config/Colors';
import Environments from '../../config/Environments';
import { screen } from '../../config/Layout';
import { Container } from '../Container/InputApp';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';
import _Text from '../_Text';

type Params = {
    params: {
        result: any,
        resultFavoriteGame: any,
        numberOfChoice: number,
        bannedGames: any[],
        choices: any[],
        type: 'favorite-game' | 'game'
    },
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export const SearchFiterGameFavorite = ({
    navigation, route
}: Props) => {

    const [isLoading, setLoading] = useState<boolean>(true);
    const [fullLisDataPC, setFullListDataPC] = useState<any[]>([]);
    const [fullLisDataMB, setFullListDataMB] = useState<any[]>([]);
    const [fullLisDataST, setFullListDataST] = useState<any[]>([]);

    const [showItemPC, setShowItemPC] = useState(true);
    const [showItemMB, setShowItemMB] = useState(true);
    const [showItemST, setShowItemST] = useState(true);
    const [lisDataPC, setListDataPC] = useState<any[]>([]);
    const [lisDataMB, setListDataMB] = useState<any[]>([]);
    const [lisDataST, setListDataST] = useState<any[]>([]);

    const [bannedGames, setBannedGames] = useState<any[]>([]);

    const [search, setSearch] = useState('');
    const [dataResult, setDataResult] = useState<any>();
    const [data, setData] = useState<any[]>([]);
    const callbackFavorite = route?.params?.resultFavoriteGame;
    const callback = route?.params?.result;
    const numberOfChoice = route?.params?.numberOfChoice;

    const onBack = () => {
        navigation.goBack()
    }

    const onNext = () => {
        let searchData: any = {
            name: search
        }
        if (searchData.name !== '') {
            setDataResult(searchData.search);
        }
        if (numberOfChoice === 1) {
            callbackFavorite(dataResult);
        } else {
            callback(data);
        }
        navigation.goBack();
    }

    const onDropDowmPC = () => {
        setShowItemPC(!showItemPC);
    }

    const onDropDowmMB = () => {
        setShowItemMB(!showItemMB);
    }

    const onDropDowmST = () => {
        setShowItemST(!showItemST);
    }

    const onChangeSearch = (value: any) => {
        setSearch(value);
        onSubmitSearchEdit(value)
    }

    const checkIsExisted = (_id: number) => {
        let index = data.findIndex((d) => d.id === _id);
        if (index != -1) return true;
        return false;
    }

    const onClickPC = (id: any, name: string, index: number) => {
        const selectedItem = lisDataPC[index];
        let tempData = [...lisDataPC];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };
        setListDataPC(tempData);
        if (numberOfChoice === 1) {
            setDataResult({ name, id });
        } else {
            let tmpData = [...data];
            let checkExisted = checkIsExisted(id);
            if (!checkExisted) {
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `좋아하는 게임은 ${numberOfChoice}개까지만 선택 가능합니다`)
                } else {
                    tmpData.push({ name, id });
                    setData(tmpData);
                }
            } else {
                tmpData = tmpData.filter((d) => d.id !== id);
                setData(tmpData);
            }
        }
    }

    const onClickMB = (id: any, name: string, index: number) => {
        const selectedItem = lisDataMB[index];
        let tempData = [...lisDataMB];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };
        setListDataMB(tempData);
        if (numberOfChoice === 1) {
            setDataResult({ name, id });
        } else {
            let tmpData = [...data];
            let checkExisted = checkIsExisted(id);
            if (!checkExisted) {
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `좋아하는 게임은 ${numberOfChoice}개까지만 선택 가능합니다`);
                } else {
                    tmpData.push({ name, id });
                    setData(tmpData);
                }
            } else {
                tmpData = tmpData.filter((d) => d.id !== id);
                setData(tmpData);
            }
        }
    }

    const onClickST = (id: any, name: string, index: number) => {
        const selectedItem = lisDataST[index];
        let tempData = [...lisDataST];
        tempData[index] = { ...tempData[index], isCheck: !selectedItem.isCheck };
        setListDataST(tempData);
        if (numberOfChoice === 1) {
            setDataResult({ name, id });
        } else {
            let tmpData = [...data];
            let checkExisted = checkIsExisted(id);
            if (!checkExisted) {
                if (data.length >= numberOfChoice) {
                    Alert.alert('오류', `좋아하는 게임은 ${numberOfChoice}개까지만 선택 가능합니다`);
                } else {
                    tmpData.push({ name, id });
                    setData(tmpData);
                }
            } else {
                tmpData = tmpData.filter((d) => d.id !== id);
                setData(tmpData);
            }
        }
    }

    const onSubmitSearchEdit = (value: string) => {
        try {
            if (value.length !== 0) {
                let searchValue = value.toLowerCase();

                let filteredPC = fullLisDataPC.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);
                let filteredST = fullLisDataST.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);
                let filteredMB = fullLisDataMB.filter((d) => d.name.toLowerCase().indexOf(searchValue) >= 0);

                setListDataPC(filteredPC);
                setListDataST(filteredST);
                setListDataMB(filteredMB);

                setShowItemPC(true);
                setShowItemST(true);
                setShowItemMB(true);
            } else {
                let fullPC = [...fullLisDataPC];
                let fullST = [...fullLisDataST];
                let fullMB = [...fullLisDataMB];

                setListDataPC(fullPC);
                setListDataST(fullST);
                setListDataMB(fullMB);

                setShowItemPC(true);
                setShowItemST(true);
                setShowItemMB(true);
            }
        } catch (error) {
            console.log("error >>>", error);
        }
    }

    const checkIsBanned = (item: any) => {
        if (!item) {
            return false;
        }
        let itemIndex = bannedGames.findIndex((d: any) => d.name === item.name);
        if (itemIndex >= 0) {
            return true;
        }
        return false;
    }

    useEffect(() => {
        Promise.all([
            getListGame('pc'),
            getListGame('mobile'),
            getListGame('steam')
        ]).then((responses: any) => {
            setListDataPC(responses[0]?.data.games);
            setFullListDataPC(responses[0]?.data.games);

            setListDataMB(responses[1]?.data.games);
            setFullListDataMB(responses[1]?.data.games);

            setListDataST(responses[2]?.data.games);
            setFullListDataST(responses[2]?.data.games);
        }).catch((error: Error) => {
            Alert.alert('오류', error.message);
        }).finally(() => {
            setLoading(false);
        });
        let { bannedGames, type, choices } = route?.params;
        setBannedGames(bannedGames);
        if (type === 'favorite-game') {
            choices.length > 0 && setDataResult(choices[0])
        } else {
            choices.length > 0 && setData(choices)
        }

    }, [])

    const renderItemPC = (item: any, index: number) => {
        if (numberOfChoice === 1) {
            return (
                <TouchableOpacity key={index}
                    disabled={checkIsBanned(item)}
                    style={[styles.listItem,
                    item.id === dataResult?.id && { borderColor: colors.mainColor, backgroundColor: colors.white },
                    { opacity: checkIsBanned(item) === true ? .5 : 1 }
                    ]}
                    onPress={() => onClickPC(item.id, item.name, index)}>
                    <_Text
                        style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>
                        {item.name}
                    </_Text>
                </TouchableOpacity>

            )
        } else {
            return (
                <TouchableOpacity key={index}
                    disabled={checkIsBanned(item)}
                    style={[styles.listItem,
                    checkIsExisted(item.id) === true && { borderColor: colors.mainColor, backgroundColor: colors.white },
                    { opacity: checkIsBanned(item) === true ? .5 : 1 }
                    ]}
                    onPress={() => onClickPC(item.id, item.name, index)}>
                    <_Text
                        style={[styles.nameListItem,
                        { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }]}>
                        {item.name}
                    </_Text>
                </TouchableOpacity>

            )
        }
    }
    const renderItemMB = (item: any, index: number) => {
        if (numberOfChoice === 1) {
            return (
                <TouchableOpacity key={index}
                    disabled={checkIsBanned(item)}
                    style={[styles.listItem,
                    item.id === dataResult?.id && { borderColor: colors.mainColor, backgroundColor: colors.white },
                    { opacity: checkIsBanned(item) === true ? .5 : 1 }
                    ]}
                    onPress={() => onClickMB(item.id, item.name, index)}>
                    <_Text
                        style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>
                        {item.name}
                    </_Text>
                </TouchableOpacity>

            )
        } else {
            return (
                <TouchableOpacity key={index}
                    disabled={checkIsBanned(item)}
                    style={[styles.listItem,
                    checkIsExisted(item.id) === true && { borderColor: colors.mainColor, backgroundColor: colors.white },
                    { opacity: checkIsBanned(item) === true ? .5 : 1 }
                    ]}
                    onPress={() => onClickMB(item.id, item.name, index)}>
                    <_Text numberOfLines={2}
                        style={[styles.nameListItem,
                        { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }]}>
                        {item.name}
                    </_Text>
                </TouchableOpacity>

            )
        }
    }
    const renderItemST = (item: any, index: number) => {
        if (numberOfChoice === 1) {
            return (
                <TouchableOpacity key={index}
                    disabled={checkIsBanned(item)}
                    style={[styles.listItem,
                    item.id === dataResult?.id && { borderColor: colors.mainColor, backgroundColor: colors.white },
                    { opacity: checkIsBanned(item) === true ? .5 : 1 }
                    ]}
                    onPress={() => onClickST(item.id, item.name, index)}>
                    <_Text numberOfLines={2} style={[styles.nameListItem, { color: item.id === dataResult?.id ? colors.mainColor : '#65676B' }]}>{item.name}</_Text>
                </TouchableOpacity>

            )
        } else {
            return (
                <TouchableOpacity key={index}
                    disabled={checkIsBanned(item)}
                    style={[styles.listItem,
                    checkIsExisted(item.id) === true && { borderColor: colors.mainColor, backgroundColor: colors.white },
                    { opacity: checkIsBanned(item) === true ? .5 : 1 }
                    ]}
                    onPress={() => onClickST(item.id, item.name, index)}>
                    <_Text numberOfLines={2}
                        style={[styles.nameListItem,
                        { color: checkIsExisted(item.id) === true ? colors.mainColor : '#65676B' }]}>
                        {item.name}
                    </_Text>
                </TouchableOpacity>

            )
        }
    }

    return (
        <Container isBack isNext={dataResult || data.length > 0 ? true : false} onGoBack={onBack} onNext={onNext} styles={styles.container}>
            <LoadingIndicator visible={isLoading} />
            <View style={styles.wrapInput}>
                <Image source={Icons.ic_search} />
                <TextInput style={styles.textInput}
                    placeholder="게임 이름을 입력해주세요"
                    value={search} onChangeText={(value: any) => { onChangeSearch(value) }} />
                {/* <Image source={Icons.ic_microphone} /> */}
            </View>
            <ScrollView indicatorStyle="white" contentContainerStyle={styles.contentContainer} showsVerticalScrollIndicator={false}>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmPC} style={[styles.wrapDropDowm, { backgroundColor: showItemPC ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>PC게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemPC ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_raphael_pc} />
                    </TouchableOpacity>
                    {showItemPC &&
                        <ScrollView contentContainerStyle={styles.flatList}>
                            {
                                lisDataPC.map((item: any, index: number) => renderItemPC(item, index))
                            }
                        </ScrollView>
                    }
                </View>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmMB} style={[styles.wrapDropDowm, { backgroundColor: showItemMB ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>Mobile게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemMB ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_mobile_vibration} />
                    </TouchableOpacity>
                    {showItemMB &&
                        <ScrollView contentContainerStyle={styles.flatList}>
                            {
                                lisDataMB.map((item: any, index: number) => renderItemMB(item, index))
                            }
                        </ScrollView>
                    }
                </View>
                <View style={styles.wrapItem}>
                    <TouchableOpacity onPress={onDropDowmST} style={[styles.wrapDropDowm, { backgroundColor: showItemST ? '#EEF5FF' : colors.white }]}>
                        <_Text style={styles.labelDropDowm}>Steam게임</_Text>
                        <Image style={{ marginHorizontal: 5, transform: showItemST ? [{ rotate: "180deg" }] : [{ rotate: "0deg" }] }} source={Icons.ic_select} />
                        <Image source={Icons.ic_cib_steam} />
                    </TouchableOpacity>
                    {showItemST &&
                        <ScrollView contentContainerStyle={styles.flatList}>
                            {
                                lisDataST.map((item: any, index: number) => renderItemST(item, index))
                            }
                        </ScrollView>
                    }
                </View>
            </ScrollView>
        </Container>
    )
}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        paddingVertical: 30
    },
    wrapInput: {
        width: screen.widthscreen - 20,
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingVertical: Platform.OS === 'ios' ? 15 : 0,
        flexDirection: 'row'
    },
    textInput: {
        fontSize: 14,
        width: '90%',
        fontWeight: '400',
        fontFamily: Environments.DefaultFont.Regular,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        height: Platform.OS == 'android' ? 50 : 'auto',
    },
    iconLeft: {
        resizeMode: 'contain',
        height: 20,
        width: 20,
    },
    listItem: {
        borderColor: '#EDEDED',
        backgroundColor: '#EDEDED',
        height: screen.heightscreen / 23,
        // width: screen.widthscreen / 4,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
        marginVertical: 5,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 30,
        borderWidth: 1
    },
    nameListItem: {
        color: '#65676B',
        textAlign: 'center',
        fontSize: 12
    },
    wrapItem: {
        width: screen.widthscreen - 30,
        paddingHorizontal: 3,
        paddingVertical: 3,
        backgroundColor: colors.white,
        borderRadius: 10,
        marginVertical: 20,
        paddingBottom: 10
    },
    wrapDropDowm: {
        paddingHorizontal: 15,
        justifyContent: "flex-end",
        borderRadius: 10,
        backgroundColor: 'white',
        width: screen.widthscreen - 36,
        height: screen.heightscreen / 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    labelDropDowm: {
        color: colors.mainColor,
        fontSize: 16
    },
    flatList: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    contentContainer: {
        flexGrow: 1,
        flexDirection: 'column',
    },
    wrappEmpty: {
        width: '100%',
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        paddingVertical: 10,
    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        fontStyle: "italic",
        color: colors.grayLight
    },
})
