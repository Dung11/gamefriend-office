import React from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import { colors } from '../../config/Colors';
import _Text from '../_Text';

const AnimatedView = Animated.createAnimatedComponent(View);

const width = 60;

const LabelBase = (props: any) => {
    const { position, value, pressed } = props;
    const scaleValue = React.useRef(new Animated.Value(0.1));
    const cachedPressed = React.useRef(pressed);

    React.useEffect(() => {
        Animated.timing(scaleValue.current, {
            toValue: 1,
            duration: 200,
            delay: 0 ,
            useNativeDriver: false,
        }).start();
        cachedPressed.current = pressed;
    }, [pressed]);

    return (
        Number.isFinite(position) &&
        Number.isFinite(value) && (
            <AnimatedView
                style={[
                    styles.sliderLabel,
                    {
                        left: position - width / 2,
                        transform: [
                            { translateY: width },
                            { scale: scaleValue.current },
                            { translateY: -width },
                        ],
                    },
                ]}
            >
                <_Text style={styles.sliderLabelText}>{value >= 400 ? '∞ ' : value}km</_Text>
            </AnimatedView>
        ) || <></>
    );
}

const CustomLabel = (props: any) => {
    const {
        leftDiff,
        oneMarkerValue,
        twoMarkerValue,
        oneMarkerLeftPosition,
        twoMarkerLeftPosition,
        oneMarkerPressed,
        twoMarkerPressed,
    } = props;

    return (
        <View style={styles.parentView}>
            <LabelBase
                position={oneMarkerLeftPosition}
                value={oneMarkerValue}
                leftDiff={leftDiff}
                pressed={oneMarkerPressed}
            />
            <LabelBase
                position={twoMarkerLeftPosition}
                value={twoMarkerValue}
                leftDiff={leftDiff}
                pressed={twoMarkerPressed}
            />
        </View>
    );
}

export default CustomLabel;

const styles = StyleSheet.create({
    parentView: {
        position: 'relative',
    },
    sliderLabel: {
        position: 'absolute',
        justifyContent: 'center',
        bottom: '100%',
        width: width,
        height: width - 10,
    },
    sliderLabelText: {
        textAlign: 'center',
        lineHeight: width,
        flex: 1,
        fontSize: 12,
        color: colors.blue,
        fontWeight: '400',
    },
});