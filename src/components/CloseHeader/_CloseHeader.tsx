import { NavigationProp } from "@react-navigation/native";
import React from "react";
import { Alert, Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RootStackParamList } from "../../@types";
import { Icons } from "../../assets";
import { colors } from "../../config/Colors";
import DeviceInfo from 'react-native-device-info';
import _Text from "../_Text";

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
    title: string,
    onSubmit?: () => void,
    selectedTitle: string,
    canSubmit?: boolean
}

export const _CloseHeader = (props: Props) => {

    const onBack = () => {
        props.navigation.goBack();
    }

    return (
        <>
            <StatusBar barStyle="light-content" />
            <View style={styles.header}>
                <TouchableOpacity style={styles.btnBack} onPress={onBack}>
                    <Image source={Icons.ic_white_closed} />
                </TouchableOpacity>
                <View style={styles.titleContainer}>
                    <_Text style={styles.headerTitle}>{props.title}</_Text>
                </View>
                <TouchableOpacity disabled={
                    typeof props.canSubmit !== 'undefined' ?
                        (!props.canSubmit) :
                        false}
                    style={[styles.btnGame, {
                        opacity: typeof props.canSubmit !== 'undefined' ?
                            (props.canSubmit === true ? 1 : .5) :
                            1
                    }]}
                    onPress={props.onSubmit}>
                    <_Text style={styles.selectedText}>{props.selectedTitle}</_Text>
                </TouchableOpacity>
            </View>
        </>
    )
};

export default _CloseHeader;

const styles = StyleSheet.create({
    header: {
        backgroundColor: colors.mainColor,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    btnGame: {
        position: 'absolute',
        right: 15,
        bottom: 10,
    },
    btnBack: {
        position: 'absolute',
        left: 15,
        bottom: 10,
        zIndex: 1000,
    },
    headerTitle: {
        fontSize: 16,
        fontWeight: '900',
        lineHeight: 23,
        color: colors.white
    },
    selectedText: {
        fontWeight: '400',
        fontSize: 17,
        color: colors.white
    }
});