import React, { useEffect, useState } from 'react'
import { Button, DeviceEventEmitter, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Modal from 'react-native-modal';
import DeviceInfo from 'react-native-device-info';
import { colors } from '../../config/Colors';
import _Text from '../_Text';
import { useNavigation, useRoute } from '@react-navigation/native';

interface IEventData {
    title: string,
    body: string,
    errorCode?: string,
}

const AlertView = () => {

    const [visible, setVisible] = useState(false);
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [btnEvent, setBtnEvent] = useState<any>(() => { setVisible(false) });
    const navigation = useNavigation();

    let subscription: any;
    let buttonText: string = '확인';

    useEffect(() => {
        subscription = DeviceEventEmitter.addListener('ShowAlert', onActiveEvent);
        return () => {
            subscription.remove();
        }
    }, [])

    const onActiveEvent = (data: IEventData) => {
        setTitle(data.title);
        setBody(data.body);
        setVisible(true);
        if (data?.errorCode == '401') {
            buttonText = '로그인';
            setBtnEvent(() => {
                navigation.navigate('Author');
                setVisible(false);
            });
        } else {
            buttonText = '확인';
        }
    }

    return (
        <Modal isVisible={visible} style={styles.background}>
            <View style={styles.container}>
                <_Text style={styles.title}>{title}</_Text>
                <_Text style={styles.body}>{body}</_Text>
                <View style={styles.footer}>
                    <Button title={buttonText} onPress={btnEvent} />
                </View>
            </View>
        </Modal>
    );
}

export default AlertView

const styles = StyleSheet.create({
    background: {
        margin: 0,
        padding: 0,
    },
    container: {
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
        paddingTop: 20,
        alignItems: 'center',
    },
    title: {
        fontWeight: '800',
        fontSize: 15,
        color: colors.black,
    },
    body: {
        fontSize: 15,
        color: colors.gray,
    },
    footer: {
        backgroundColor: 'white',
        paddingBottom: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        justifyContent: 'center',
        marginTop: 10,
    },
});
