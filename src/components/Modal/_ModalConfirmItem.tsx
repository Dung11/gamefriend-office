import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import {
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    ActivityIndicator
} from "react-native";
import { RootStackParamList } from "../../@types";
import { colors } from "../../config/Colors";
import Modal from 'react-native-modal';
import _Text from "../_Text";
import { Images } from "../../assets";
import store, { AppState } from "../../redux/store";
import { getGameRuleRequest } from "../../redux/gamerule/actions";
import { connect } from "react-redux";
import { RouteProp } from "@react-navigation/native";
import { getUserItem } from "../../api/GameServices";
import { keyValueStorage } from "../../storage/keyValueStorage";

type Params = {
    params: {
        type: 'friendRequestNormalRecommendation' |
        'friendRequestSpecialRecommendation' |
        'leaveLiveTalkRoom' |
        'match' |
        'priorMatch' |
        'refreshNormalRecommendation' |
        'refreshSpecialRecommendation' |
        'viewDetailSpecialRecommendation' |
        'friendRequestLiveTalk',
        onSubmit: () => void,
        onCancel?: () => void,
    }
}

interface Props {
    navigation: StackNavigationProp<RootStackParamList>,

    rules: any,
    route: RouteProp<Params, 'params'>
}
const _ModalConfirmItem = (props: Props) => {

    const [visible, setVisible] = useState<boolean>(true);
    const [itemCoin, setItemCoin] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(true)

    const onDone = async (response: any) => {
        const { route } = props;
        const { type } = route.params;
        await checkUserItem(response.rules[type]);
        setItemCoin(response.rules[type]);
        setIsLoading(false);
    }

    const getData = () => {
        store.dispatch(getGameRuleRequest({ callback: onDone }));
    };

    useEffect(() => {
        const { route } = props;
        console.log('PARAMS: ', route.params);

        getData();
    }, [props.navigation]);

    const checkUserItem = async (ruleItem: number) => {
        let userId: any = await keyValueStorage.get('userID');
        let item: number = await getUserItem(userId);
        if (ruleItem > item) {
            onClose();
            setTimeout(() => {
                props.navigation.navigate("Modal", { screen: "AlertNoItem" });
            }, 500);
        }
    }

    const onClose = () => {
        const { route } = props;
        const { onCancel } = route.params;
        setVisible(false);
        props.navigation.pop();
        onCancel && onCancel();
    }

    if (isLoading) {
        return (
            <Modal isVisible={true}>
                <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size={54} color="#FFFFFF" />
                </View>
            </Modal>
        )
    }

    const onModalSubmit = () => {
        const { route } = props;
        const { onSubmit } = route.params;
        onClose();
        onSubmit();
    }

    return (
        <Modal isVisible={visible} onBackdropPress={onClose} onBackButtonPress={onClose}>
            <View style={styles.wrapper}>
                {/* <Image source={Images.confirm_item} /> */}
                <View style={styles.content}>
                    <_Text style={styles.title}>아이템 사용</_Text>
                    <_Text style={styles.subTitle}>아이템을 {itemCoin}개 사용 하시겠습니까?</_Text>
                    <View style={styles.buttonGroupContainer}>
                        <TouchableOpacity style={[styles.btnChange, { backgroundColor: '#EDF1F7' }]} onPress={onClose}>
                            <_Text style={[styles.txt, { color: '#6D6F72', }]}>취소</_Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btnDel, { backgroundColor: '#4F91F3' }]} onPress={onModalSubmit}>
                            <_Text style={[styles.txt, { color: colors.white }]}>확인</_Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    );
};

const mapStateToProps = (state: AppState): Partial<Props> => ({
    rules: state.GameRuleReducer.rules,
});

export default connect(mapStateToProps, null)(_ModalConfirmItem);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: 'rgba(0,0,0,0)',
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 24,
        // paddingTop: 50,
        backgroundColor: "white",
        borderRadius: 20,
    },
    content: {
        width: '100%',
        paddingVertical: 10,
    },
    wrapperItem: {
        paddingTop: 50,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        width: '100%',
    },
    title: {
        fontWeight: '900',
        fontSize: 20,
        lineHeight: 26,
        marginTop: 25,
    },
    subTitle: {
        color: '#757575',
        fontSize: 14,
        lineHeight: 21,
        marginTop: 36,
    },
    buttonGroupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 48,
        marginBottom: 26
    },
    btnChange: {
        flex: 1,
        marginRight: 10,
        borderRadius: 10
    },
    btnDel: {
        flex: 1,
        marginLeft: 10,
        borderRadius: 10
    },
    txt: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 21,
        textAlign: 'center',
        paddingVertical: 10
    },
});
