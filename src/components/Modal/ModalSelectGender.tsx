import React from "react";
import {
    View,
    TouchableOpacity,
    Image,
    StyleSheet
} from "react-native";
import { Icons } from "../../assets";
import Modal from 'react-native-modal';
import _Text from "../_Text";

export default ({
    visible,
    onSellect,
    onBackdropPress
}: {
    visible: boolean;
    onSellect: (name: string, key: string) => void;
    onBackdropPress: () => void;
}) => {
    return (
        <Modal isVisible={visible} onBackdropPress={onBackdropPress}>
                <View style={styles.Content}>
                    <TouchableOpacity style={styles.WrapperItem} onPress={() => { onSellect("상관없음", "any") }}>
                        <_Text style={styles.Text}>상관없음</_Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.WrapperItem, { paddingVertical: 15 }]} onPress={() => { onSellect("남성", "male") }}>
                        <Image style={styles.icon} source={Icons.ic_male} />
                        <_Text style={styles.Text}>남성</_Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.WrapperItem, { borderBottomColor: 'white' }]} onPress={() => { onSellect("여성", "female") }}>
                        <Image style={styles.icon} source={Icons.ic_female} />
                        <_Text style={styles.Text}>여성</_Text>
                    </TouchableOpacity>
                </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    Wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        paddingHorizontal: 16
    },
    Content: {
        backgroundColor: "white",
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10
    },
    WrapperItem: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e3f0ee',
        width: '100%',
        flexDirection: 'row',
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10
    },
    Text: {
        fontWeight: '900'
    }
});
