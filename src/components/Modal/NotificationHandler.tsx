import React, { useEffect, useState } from 'react'
import { DeviceEventEmitter, ImageStyle, StyleSheet, TextStyle, TouchableOpacity, ViewStyle } from 'react-native'
import messaging from '@react-native-firebase/messaging';
import { useNavigation } from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';
import { bulletinValueStorage } from '../../storage/bulletinValueStorage';
import _Text from '../_Text';
import { sendIntroMatch } from '../../socketio/socketController';
import { colors } from '../../config/Colors';

const NotificationHandler = () => {

    const navigation = useNavigation();
    const [notification, setNotification] = useState<{ title?: string, body?: string }>({});
    const [notificationData, setNotificationData] = useState({});
    const [animation, setAnimation] = useState<{ from: TextStyle & ViewStyle & ImageStyle, to: TextStyle & ViewStyle & ImageStyle }>({ from: {}, to: {} });

    const onShowNotification = () => {
        setAnimation({
            from: {
                top: -500,
            },
            to: {
                top: 100,
            },
        });
        setTimeout(() => {
            setAnimation({
                to: {
                    top: -500,
                },
                from: {
                    top: 100,
                },
            });
        }, 3000);
    }

    const onDetail = () => {
        navigateToTargetNotiScreen(notificationData);
    }

    const navigateToTargetNotiScreen = (notiData: any) => {
        if (notiData.type == 'profile-approved') {
            navigation.navigate('Login');
        } else if (notiData.type == "recommendation-refreshed") {
            navigation.navigate('RecommendedFriend');
        } else if (notiData.type == "post-commented") {
            navigation.navigate('Notify');
        } else if (notiData.type == "room-chat-created") {

            const data = JSON.parse(notiData.data);
            const roomId = data.room.id;
            const nameFriend = data.sender.userName;
            const beRoomType = data.roomType;
            const roomName = data.room.name;

            let params: any = {
                roomId: roomId,
                nameFriend: nameFriend,
                roomName: roomName,
            }
            if (beRoomType === 'duet') {
                navigation.navigate('ChatScreen', {
                    screen: 'ChatDetail',
                    params
                });
            } else if (beRoomType === 'group') {
                params = {
                    ...params,
                    roomType: 'livetalk',
                }
                navigation.navigate('LiveTalkChatScreen', {
                    screen: 'LiveTalkChat', params
                });
            } else {
                params = {
                    ...params,
                    roomType: 'livetalk-local',
                }
                navigation.navigate('LiveTalkChatScreen', {
                    screen: 'LiveTalkChat', params
                });
            }
        } else if (notiData.type == "request-received") {
            navigation.navigate('ListFriendRequest');
        } else if (notiData.type === 'invitation-received') {
            navigation.navigate('Chatlist');
        } else if (notiData.type == 'match-done') {
            let dataMatchDone = JSON.parse(notiData.data)

            DeviceEventEmitter.emit("showWattingMatch", { visible: false });
            DeviceEventEmitter.emit("showWattingMatchTwoPoint", { visible: false });
            navigation.navigate('Modal', { screen: "AlertMatchingSuccess", params: dataMatchDone });
            sendIntroMatch(dataMatchDone.roomId);
        }
    }

    useEffect(() => {
        messaging().setBackgroundMessageHandler(async remoteMessage => {
            if (remoteMessage?.data) {
                navigateToTargetNotiScreen(remoteMessage.data);
            }
        });
        messaging().getInitialNotification().then(remoteMessage => {
            if (remoteMessage?.data) {
                navigateToTargetNotiScreen(remoteMessage.data);
            }
        });
        messaging().onNotificationOpenedApp(remoteMessage => {
            if (remoteMessage?.data) {
                navigateToTargetNotiScreen(remoteMessage.data);
            }
        });
        const unsubscribe = messaging().onMessage(async remoteMessage => {
            remoteMessage?.notification && setNotification(remoteMessage.notification);
            setNotificationData(remoteMessage?.data || {});
            onShowNotification();
            if (remoteMessage?.data?.type == 'post-commented') {
                await bulletinValueStorage.save('isViewNotify', 'false')
            } else if (remoteMessage?.data?.type == 'profile-approved') {
                navigation.navigate('Login');
            }
        });
        return unsubscribe;
    }, [])

    return (
        <Animatable.View style={styles.background} animation={animation}>
            <TouchableOpacity onPress={onDetail}>
                <_Text style={styles.title}>{notification?.title}</_Text>
                <_Text>{notification?.body}</_Text>
            </TouchableOpacity>
        </Animatable.View>
    )
}

export default NotificationHandler;

const styles = StyleSheet.create({
    background: {
        position: 'absolute',
        top: -500,
        left: 10,
        right: 10,
        padding: 10,
        borderRadius: 10,
        zIndex: 9999,
        borderWidth: 1,
        backgroundColor: colors.white,
        borderColor: '#a0a0a0',
    },
    container: {
        width: '100%',
    },
    title: {
        fontWeight: '900',
        marginBottom: 10,
        color: 'black',
    },
    body: {
        color: 'black',
    },
});
