import React, { PureComponent } from 'react';
import { DeviceEventEmitter, StyleSheet, View } from 'react-native';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';
import Modal from 'react-native-modal';
import DeviceInfo from 'react-native-device-info';

interface Props {

}

export class ModalDisConnectLoading extends PureComponent<Props> {
    
    subscription: any;

    constructor(props: Props) {
        super(props);
        this.subscription = DeviceEventEmitter.addListener('showDisConnectLoadingModal', this.onEvent);
        this.state = {
            visible: false,
        };
    }

    componentWillUnmount() {
        this.subscription.remove();

    }

    onEvent = (args: any) => {
        this.setState({ visible: args.visible })
    }

    render() {
        const { visible } = this.state as any
        return (
            <Modal isVisible={visible} style={styles.container}>
                <View style={styles.content}>
                    <LoadingIndicator visible={true} />
                </View>
            </Modal>
        )
    }

}


export default ModalDisConnectLoading;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    smallLayout: {
        height: screen.widthscreen / 8,
        position: 'absolute',
        top: 60,
        bottom: 0,
        left: 4,
        right: 0,
        backgroundColor: "#65676B",
        borderRadius: 8,
        width: '98%',
        paddingVertical: 10,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'

    },
    countSmall: { color: colors.white },
    wrapCountNumberSmall: { alignItems: 'center', flexDirection: 'row' },
    wrap: {
        flex: 1
    },
    content: {
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        alignItems: 'center',
        paddingBottom: 10,
        paddingRight: 5,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    label: {
        color: colors.grayLight
    },
    wrapAction: { width: '100%', height: screen.heightscreen / 2, justifyContent: 'space-between' },
    wrapTip: { backgroundColor: 'white', paddingHorizontal: 10, paddingVertical: 10, borderRadius: 7 },
    wrapLableTip: { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', },
    tip: { fontWeight: '900' },
    wrapCountNumber: { width: '100%', alignItems: 'center' },
    row: { flexDirection: 'row', alignItems: 'center' },
    countNumber: { fontSize: 28, fontWeight: '900' },
    textWatting: { color: '#B7B7B7', fontSize: 16 },
    exits: { paddingHorizontal: 10, paddingVertical: 5, borderRadius: 3 },
    textExits: { color: colors.white, fontWeight: '900' }
});
