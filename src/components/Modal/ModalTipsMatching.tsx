import React from "react";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Icons } from "../../assets";
import { colors } from "../../config/Colors";
import Modal from 'react-native-modal';
import _Text from "../_Text";

export default ({
    visible,
    onClose,
    onBackdropPress,
    typeFast,
    typeDetail,
    typeNeightborhood
}: {
    visible: boolean;
    onClose: () => void;
    onBackdropPress: () => void,
    typeFast?: boolean,
    typeDetail?: boolean,
    typeNeightborhood?: boolean
}) => {
    return (
        <Modal isVisible={visible} onBackdropPress={onBackdropPress} propagateSwipe>
            {/* <View style={styles.wrapper}> */}
            <View style={styles.content}>
                <View style={styles.headerContent}>
                    <View style={styles.wrapTitle}>
                        {typeFast && <Image style={styles.icon} source={Icons.ic_matching_fast} />}
                        {typeDetail && <Image style={styles.icon} source={Icons.ic_matching_detail} />}
                        {typeNeightborhood && <Image style={styles.icon} source={Icons.ic_matching_neighborhood} />}
                        {typeFast && <_Text style={styles.title}>빠른 매칭</_Text>}
                        {typeDetail && <_Text style={styles.titleDetail}>맞춤 매칭</_Text>}
                        {typeNeightborhood && <_Text style={styles.titleNeightborhood}>동네 매칭</_Text>}
                    </View>
                    <TouchableOpacity style={styles.btn} onPress={onClose}>
                        <Image resizeMode="contain" style={[styles.iconbtn, { width: 23, height: 23 }]} source={Icons.ic_closed} />
                    </TouchableOpacity>
                </View>
                <View style={styles.contentItem}>
                    {typeFast && <_Text style={styles.text}>
                        {'빠른 매칭은 실시간으로 접속 중인 겜친을 만나볼 수 있어요!\n원하는 성별, 나이, 게임을 선택하고 겜친을 바로 만나보세요 :)'}
                    </_Text>}
                    {typeNeightborhood && <_Text style={styles.text}>
                        {'동네 매칭은 보다 가까이에 있는 겜친들을 만나볼 수 있어요!\n우리 동네에 있는 겜친을 만나고 싶다면?\n거리를 좁게 설정해보세요!\n\n단, 너무 가까운 거리일 경우 매칭 시간이 길어질 수 있답니다ᅲᅲ'}
                    </_Text>}
                    {typeDetail && <_Text style={styles.text}>
                        {'게임을 즐겨하는 당신을 위한 맞춤 매칭!\n플레이하는 게임과 만나고 싶은 티어를 선택해 나와 딱 맞는 겜친을 찾아보세요!\n단, 티어가 맞아야 매칭되는 만큼 시간이 소요될 수 있는 점 양해 부탁드려요 ᅲᅲ\n\n내가 하는 게임이 맞춤매칭 리스트에 없다면?\n추가 게임 문의는 프로필 > 고객문의를 통해 문의해주세요 :)'}
                    </_Text>}
                </View>

            </View>
            {/* </View> */}
        </Modal>
    );
};
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        paddingHorizontal: 16
    },
    content: {
        backgroundColor: "white",
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10
    },
    headerContent: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.grayLight
    },
    wrapTitle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '85%'
    },
    btn: {
        paddingHorizontal: 15,
        paddingVertical: 5
    },
    contentItem: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    icon: {
        width: 36,
        height: 36,
        marginRight: 10
    },
    iconbtn: {
    },
    title: {
        fontWeight: '900',
        color: '#AF52DE'
    },
    titleDetail: {
        fontWeight: '900',
        color: '#5856D6'
    },
    titleNeightborhood: {
        fontWeight: '900',
        color: '#007AFF'
    },
    text: {
        textAlign: 'center',
        fontSize: 16
    }
});
