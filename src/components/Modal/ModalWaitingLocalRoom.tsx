import React, { PureComponent } from 'react';
import { View, DeviceEventEmitter, TouchableOpacity, StyleSheet, Image, Platform, ViewStyle, Dimensions } from 'react-native';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import DeviceInfo from 'react-native-device-info';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { colors } from '../../config/Colors';
import { Icons } from '../../assets';
import { screen } from '../../config/Layout';
import { addHandler, removeHandler } from '../../socketio/socketController';
import _Text from '../_Text';
import * as Animatable from 'react-native-animatable';

interface Props {

}

interface State {
    visible: boolean,
    curMember: number,
    limitMember: number,
    roomId: string,
    isMinimized: boolean,
};

const minimize = {
    from: {
        right: 4,
        paddingHorizontal: 15,
    },
    to: {
        right: Dimensions.get('screen').width + 4 - 50,
        paddingHorizontal: 0,
    },
};

const large = {
    from: {
        right: Dimensions.get('screen').width + 4 - 50,
        paddingHorizontal: 0,
    },
    to: {
        right: 4,
        paddingHorizontal: 15,
    },
};

export class ModalWaitingLocalRoom extends PureComponent<Props, State> {

    subscription: any;
    time: any;
    totalSeconds: any = 0;

    constructor(props: Props) {
        super(props);
        this.subscription = DeviceEventEmitter.addListener('showWaitingLocalRoom', this.onEvent);

        this.state = {
            visible: false,
            curMember: 1,
            limitMember: 5,
            roomId: '',
            isMinimized: false,
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.subscription.remove();
    }

    handleMemberJoinedEvent = async () => {
        console.log('Waiting popup listen room update');
        addHandler('local-room-update' as any, (data: any) => {
            console.log('Number of member in pop up: ', data.members);
            // setUserList(data.members);
            this.setState({ curMember: data.members.length });
        });
    }

    handleMemberFullEvent = async () => {
        console.log('Waiting popup listen room full');
        addHandler('local-room-full' as any, (data: any) => {
            console.log(`Room ${data.roomId} is full !!!!`);
            // removeHandler('local-room-update' as any);
            // removeHandler('local-room-full' as any);
            // removeHandler('local-room-terminate' as any);

            this.setState({ visible: false, curMember: this.state.limitMember });
        });
    }

    handleRoomTerminated = async () => {
        console.log('listen room terminated');
        addHandler('local-room-terminate' as any, (data: any) => {
            // console.log(`Room ${data.roomId} has been terminated !!!!`);
            // props.navigation.goBack()

            this.setState({ visible: false });
        });
    }

    onEvent = (args: any) => {
        this.setState({
            visible: args.visible,
            curMember: args.curMember,
            limitMember: args.limitMember,
            roomId: args.roomId
        });
        if (args.visible === true) {
            this.handleMemberJoinedEvent();
            this.handleMemberFullEvent();
            this.handleRoomTerminated();
        } else {
            removeHandler('local-room-update' as any);
            removeHandler('local-room-full' as any);
            removeHandler('local-room-terminate' as any);
        }
    }

    goToWaitingLocalRoom = () => {
        this.setState({ visible: false });

        console.log('cur mem: ', this.state.curMember);
        console.log('limit mem: ', this.state.limitMember);

        let isFull = false
        if (this.state.curMember === this.state.limitMember) {
            isFull = true;
        }

        console.log('go to waiting room is full: ', isFull);

        DeviceEventEmitter.emit("enterWaitingLocalRoom", {
            roomId: this.state.roomId,
            isFull: isFull
        });
    }

    onToggle = () => {
        this.setState({ isMinimized: !this.state.isMinimized });
    }

    render() {
        const { visible, curMember, limitMember, isMinimized } = this.state as any;
        if (visible) {
            return (
                <Animatable.View animation={isMinimized ? minimize : large} duration={700} style={styles.wrapWaitingRoomPopup}>
                    {isMinimized || <>
                        <View style={styles.wrapCountNumberSmall}>
                            <View style={styles.row}>
                                <TouchableOpacity onPress={this.onToggle}>
                                    <Icon name="angle-double-left" style={styles.hideIcon} />
                                </TouchableOpacity>
                                <_Text 
                                    style={styles.textWaiting}
                                    onPress={this.goToWaitingLocalRoom}>동네 친구 채팅방 대기중</_Text>
                                <AnimatedEllipsis style={styles.textWatting} useNativeDriver/>
                            </View>
                            <View style={[styles.row, { marginLeft: 10 }]}>
                                <_Text style={styles.txtNumber}>{`${curMember}/${limitMember}`}</_Text>
                            </View>
                        </View>
                        <TouchableOpacity onPress={this.goToWaitingLocalRoom}>
                            <Image source={Icons.ic_arrow_right} />
                        </TouchableOpacity>
                    </>}
                    {isMinimized && <TouchableOpacity style={styles.minimizedBtn} onPress={this.onToggle}>
                        <_Text style={styles.txtNumber}>{`${curMember}/${limitMember}`}</_Text>
                    </TouchableOpacity>}
                </Animatable.View>
            );
        }
        return <View />
    };
}

export default ModalWaitingLocalRoom;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    wrapWaitingRoomPopup: {
        height: 45,
        position: 'absolute',
        top: 80,
        bottom: 0,
        left: 4,
        right: 4,
        backgroundColor: "#65676B",
        borderRadius: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
    },
    txtNumber: {
        color: colors.white,
        fontSize: 16
    },
    wrapCountNumberSmall: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    wrap: {
        flex: 1,
    },
    content: {
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        alignItems: 'center',
        paddingBottom: 10,
        paddingRight: 5,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    label: {
        color: colors.grayLight,
    },
    wrapAction: {
        width: '100%',
        height: screen.heightscreen / 2,
        justifyContent: 'space-between',
    },
    wrapTip: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 7,
    },
    wrapLableTip: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
    tip: {
        fontWeight: '900',
    },
    wrapCountNumber: {
        width: '100%',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    countNumber: {
        fontSize: 28,
        fontWeight: '900',
    },
    textWaiting: {
        color: colors.white,
        fontSize: 16,
        paddingLeft: 15
    },
    textWatting: {
        color: colors.white,
        fontSize: 16,
    },
    exits: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 3,
    },
    textExits: {
        color: colors.white,
        fontWeight: '900',
    },
    hideIcon: {
        color: 'white',
        fontSize: 18,
        marginRight: 5,
    },
    minimizedBtn: {
        width: '100%',
        alignItems: 'center',
    },
});