import React, { useEffect, useState } from "react";
import {
    View,
    StyleSheet,
} from "react-native";
import Modal from 'react-native-modal';
import ButtonApp from "../../components/Button/ButtonApp";
import _Text from "../../components/_Text";
import { colors } from "../../config/Colors";

export default ({
    visible,
    onBackdropPress,
    onConfirm,
}: {
    visible: boolean;
    onBackdropPress: () => void;
    onConfirm: () => void;
}) => {

    const [isVisible, setIsvisible] = useState(visible)

    useEffect(() => {
        setIsvisible(visible);
    }, [visible]);

    const onSubmit = async () => {
        onConfirm();
        setIsvisible(false);
    }

    return (
        <Modal isVisible={isVisible} onBackdropPress={onBackdropPress} avoidKeyboard>
            <View style={styles.content}>
                <View style={styles.wrapTitle}>
                    <_Text style={styles.title}>정말 나가시겠습니까?</_Text>
                </View>
                <View style={styles.wrapTxt}>
                    <_Text style={styles.txt}>다시 입장할 경우 </_Text>
                    <_Text style={styles.txt}>유료 아이템이 사용됩니다</_Text>
                </View>
                <View style={styles.wrapBtn}>
                    <ButtonApp title="나가기" backgroundColor={colors.red} onPress={() => { onSubmit() }} />
                </View>
            </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    content: {
        backgroundColor: colors.white,
        borderRadius: 13,
        width: '100%',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
    wrapTitle: {
        marginTop: 20,
        width: '100%',
        alignItems: 'center'
    },
    title: {
        fontSize: 16,
        color: colors.red,
        fontWeight: '800',
    },
    wrapTxt: {},
    txt: {
        fontWeight: '400',
        fontSize: 14,
        textAlign: 'center'
    },
    wrapBtn: {
        marginVertical: 10,
        width: '100%',
        paddingHorizontal: 50
    },

});
