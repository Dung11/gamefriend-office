import React, { useEffect, useState } from "react";
import {
    View,
    TouchableOpacity,
    StyleSheet
} from "react-native";
import { colors } from "../../config/Colors";
import Modal from 'react-native-modal';
import _Text from "../_Text";

interface Props {
    onSubmit: (status: 'logout' | 'withdrawl') => void,
    onClose?: () => void,
    visible: boolean,
    title: string,
    content: string,
    status: 'logout' | 'withdrawl'
}
const _ModalLogout = (props: Props) => {

    const [visible, setVisible] = useState<boolean>(props.visible);
    const [content, setContent] = useState<string>('');
    const [title, setTitle] = useState<string>('')

    useEffect(() => {
        setVisible(props.visible);
        setContent(props.content);
        setTitle(props.title);
    }, [props.visible])

    return (
        <Modal isVisible={visible} onBackdropPress={props.onClose} onBackButtonPress={props.onClose}>
            <View style={styles.wrapper}>
                <View style={styles.content}>
                    <_Text style={styles.title}>{title}</_Text>
                    <_Text style={styles.txt}>{content}</_Text>
                    <View style={styles.buttonGroupContainer}>
                        <TouchableOpacity style={[styles.btnChange, { backgroundColor: '#EDF1F7' }]} onPress={props.onClose}>
                            <_Text style={[styles.txt, { color: '#6D6F72', }]}>취소</_Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btnDel, { backgroundColor: '#4F91F3' }]} onPress={() => props.onSubmit(props.status)}>
                            <_Text style={[styles.txt, { color: colors.white }]}>확인</_Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    );
};

export default _ModalLogout;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: 'rgba(0,0,0,0)',
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    },
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 24,
        paddingTop: 50,
        backgroundColor: "white",
        borderRadius: 20,
    },
    content: {
        width: '100%',
        paddingVertical: 10,
    },
    wrapperItem: {
        paddingTop: 50,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        width: '100%',
    },
    title: {
        fontWeight: '900',
        fontSize: 20,
        lineHeight: 26,
        textAlign: 'center',
    },
    subTitle: {
        color: '#757575',
        fontSize: 14,
        lineHeight: 21,
        marginTop: 36,
    },
    buttonGroupContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 48,
        marginBottom: 26
    },
    btnChange: {
        flex: 1,
        marginRight: 10,
        borderRadius: 10
    },
    btnDel: {
        flex: 1,
        marginLeft: 10,
        borderRadius: 10
    },
    txt: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 21,
        textAlign: 'center',
        paddingVertical: 10
    },
});
