import React, { PureComponent } from 'react';
import { View, DeviceEventEmitter, TouchableOpacity, StyleSheet, Image, Platform, ViewStyle, Dimensions } from 'react-native';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import DeviceInfo from 'react-native-device-info';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { colors } from '../../config/Colors';
import ButtonMatch from '../../screen/HomeScreen/component/ButtonMatch';
import PulseLoader from '../../screen/HomeScreen/MatchingLoading/PulseLoading';
import { ContainerMain } from '../Container/ContainerMain';
import { Icons } from '../../assets';
import { screen } from '../../config/Layout';
import { addHandler } from '../../socketio/socketController';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';
import _Text from '../_Text';
import * as Animatable from 'react-native-animatable';
import { keyValueStorage } from '../../storage/keyValueStorage';
import { TipMatch } from '../../screen/HomeScreen/component/TipMatching';

interface Props {

}

interface State {
    visible: boolean,
    isBack: boolean,
    number: number,
    isLoading: boolean,
    isMinimized: boolean,
};

const minimize = {
    from: {
        right: 4,
        paddingHorizontal: 15,
    },
    to: {
        right: Dimensions.get('screen').width + 4 - 50,
        paddingHorizontal: 0,
    },
};

const large = {
    from: {
        right: Dimensions.get('screen').width + 4 - 50,
        paddingHorizontal: 0,
    },
    to: {
        right: 4,
        paddingHorizontal: 15,
    },
};

export class ModalWatting extends PureComponent<Props, State> {

    subscription: any;
    time: any;
    totalSeconds: any = 0;

    constructor(props: Props) {
        super(props);
        this.subscription = DeviceEventEmitter.addListener('showWattingMatch', this.onEvent);

        this.state = {
            visible: false,
            isBack: false,
            number: 0,
            isLoading: false,
            isMinimized: false,
        };
    }

    componentDidMount() {
        addHandler('match-done', (data: any) => {
            this.setState({ isBack: false });
            clearInterval(this.time);
            this.totalSeconds = 0;
        });
    }

    componentWillUnmount() {
        this.subscription.remove();
        // removeHandler('match-done')
        clearInterval(this.time);
    }

    
    onEvent = (args: any) => {
        this.setState({ visible: args.visible });
        if (args.visible === true) {
            this.time = setInterval(this.countUp, 1000);
            this.totalSeconds = 0;
        } else {
            this.setState({ isBack: false, isLoading: false });
            clearInterval(this.time);
            this.totalSeconds = 0;
        }
    }

    hide = () => {
        this.setState({ visible: false });
    }

    goToWaittingTwoPoint = () => {
        clearInterval(this.time);
        this.setState({ visible: false });
        DeviceEventEmitter.emit("submitMatchTwoPoint", {
            onCancel: () => {
                this.time = setInterval(this.countUp, 1000);
                this.setState({
                    visible: true,
                    isBack: false,
                });
                DeviceEventEmitter.emit("backWatingMatch");
                keyValueStorage.save('disableWaitingRoom', 'true');
            }
        });
    }

    onBack = () => {
        this.setState({ isBack: true });
        DeviceEventEmitter.emit("backWatingMatch");
        keyValueStorage.save('disableWaitingRoom', 'true');
    }

    onArrowRight = () => {
        this.setState({ isBack: false });
    }

    onExit = async () => {
        clearInterval(this.time);
        this.totalSeconds = 0;
        this.setState({ isLoading: true, number: 0 });
        // removeHandler('match-done')
        DeviceEventEmitter.emit("exitMatch");
        keyValueStorage.save('disableWaitingRoom', 'false');
    }

    countUp = () => {
        this.setState({ number: ++this.totalSeconds });
    }

    onToggle = () => {
        this.setState({ isMinimized: !this.state.isMinimized });
    }

    render() {
        const { visible, isBack, isLoading, number, isMinimized } = this.state as any;
        let containerStyles: ViewStyle = { ...styles.container };
        if (Platform.OS == 'ios') {
            containerStyles = {
                ...styles.container,
                bottom: isBack ? undefined : 0,
                backgroundColor: 'red',
            };
        }
        return (
            <>
                <LoadingIndicator visible={isLoading} />
                { visible &&
                    <View style={containerStyles}>
                        {!isBack && <View style={styles.wrap}>
                            <View style={styles.header}>
                                <TouchableOpacity onPress={this.onBack}>
                                    <Image source={Icons.ic_back} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.exits} onPress={this.onExit} activeOpacity={0.1}>
                                    <_Text style={styles.textExits}>취소</_Text>
                                </TouchableOpacity>
                            </View>
                            <ContainerMain styles={styles.content}>
                                <TipMatch/>
                                <View style={styles.wrapAction}>
                                    <ButtonMatch onPress={this.goToWaittingTwoPoint} title="더 빠른 매칭하기" backgroundColor={colors.pink} iconLeft iconRight poin={2} />
                                    <View style={styles.wrapCountNumber}>
                                        <PulseLoader pressDuration={100} avatar={''} />
                                        <View style={styles.row}>
                                            <_Text style={styles.countNumber}>{number}</_Text>
                                            <_Text style={styles.countNumber}>초</_Text>
                                        </View>
                                        <View style={styles.row}>
                                            <_Text style={styles.textWatting}>실시간 빠른 매칭 중</_Text>
                                            <AnimatedEllipsis style={styles.textWatting} />
                                        </View>
                                    </View>
                                </View>
                            </ContainerMain>

                        </View>}
                        {isBack && <Animatable.View animation={isMinimized ? minimize : large} duration={700} style={styles.smallLayout}>
                            {isMinimized || <>
                                <View style={styles.wrapCountNumberSmall}>
                                    <View style={styles.row}>
                                        <TouchableOpacity onPress={this.onToggle}>
                                            <Icon name="angle-double-left" style={styles.hideIcon} />
                                        </TouchableOpacity>
                                        <_Text style={styles.textWatting}>실시간 빠른 매칭 중</_Text>
                                        <AnimatedEllipsis style={styles.textWatting} />
                                    </View>
                                    <View style={[styles.row, { marginLeft: 10 }]}>
                                        <_Text style={styles.countSmall}>{`${number}s`}</_Text>
                                    </View>
                                </View>
                                <TouchableOpacity onPress={this.onArrowRight}>
                                    <Image source={Icons.ic_arrow_right} />
                                </TouchableOpacity>
                            </>}
                            {isMinimized && <TouchableOpacity style={styles.minimizedBtn} onPress={this.onToggle}>
                                <_Text numberOfLines={1} style={styles.countSmall}>{`${number}s`}</_Text>
                            </TouchableOpacity>}
                        </Animatable.View>}
                    </View>
                }
            </>
        );
    };
}


export default ModalWatting;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    smallLayout: {
        height: 45,
        position: 'absolute',
        top: 80,
        bottom: 0,
        left: 4,
        right: 4,
        backgroundColor: "#65676B",
        borderRadius: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
    },
    countSmall: {
        color: colors.white,
    },
    wrapCountNumberSmall: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    wrap: {
        flex: 1,
    },
    content: {
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        alignItems: 'center',
        paddingBottom: 10,
        paddingRight: 5,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    label: {
        color: colors.grayLight,
    },
    wrapAction: {
        width: '100%',
        height: screen.heightscreen / 2,
        justifyContent: 'space-between',
    },
    wrapTip: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 7,
    },
    wrapLableTip: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
    tip: {
        fontWeight: '900',
    },
    wrapCountNumber: {
        width: '100%',
        alignItems: 'center',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    countNumber: {
        fontSize: 28,
        fontWeight: '900',
    },
    textWatting: {
        color: '#B7B7B7',
        fontSize: 16,
    },
    exits: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 3,
    },
    textExits: {
        color: colors.white,
        fontWeight: '900',
    },
    hideIcon: {
        color: 'white',
        fontSize: 18,
        marginRight: 5,
    },
    minimizedBtn: {
        width: '100%',
        alignItems: 'center',
    },
});
