import React, { useEffect, useState } from 'react';
import Modal from 'react-native-modal';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import _Text from '../_Text';
interface Props {
    visible?: boolean,
    onSubmit?: (value: number) => void,
    onClose: () => void,
}

const ModalUserReport = (props: Props) => {

    const { onSubmit, onClose } = props;
    const [visible, setVisible] = useState(props.visible);

    useEffect(() => {
        setVisible(props.visible);
    })

    return (
        <Modal isVisible={visible} onBackdropPress={onClose}>
            <View style={styles.wrapper}>
                <View style={styles.content}>
                    <TouchableOpacity
                        style={[styles.wrapperItem, { paddingVertical: 15 }]}
                        onPress={() => { onSubmit ? onSubmit(1) : onClose }}>
                        <_Text style={styles.text}>신고</_Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.wrapperItem, { borderBottomColor: 'white' }]}
                        onPress={() => { onSubmit ? onSubmit(2) : onClose }}>
                        <_Text style={styles.text}>차단</_Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
}

export default ModalUserReport;

const styles = StyleSheet.create({
    wrapper: {
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 16
    },
    content: {
        backgroundColor: "white",
        borderRadius: 13,
        width: '100%',
        paddingVertical: 10
    },
    wrapperItem: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#e3f0ee',
        width: '100%',
        flexDirection: 'row',
    },
    icon: {
        width: 18,
        height: 18,
        marginRight: 10
    },
    text: {
        color: '#65676B',
        fontSize: 16,
    }
});