import React, { useEffect, useState } from "react";
import Modal from 'react-native-modal';
import { ActivityIndicator, BackHandler, StyleSheet, TouchableOpacity, View } from "react-native";
import ImageViewer from 'react-native-image-zoom-viewer';
import { colors } from "../../config/Colors";
import LoadingIndicator from "../LoadingIndicator/LoadingIndicator";
import Icon from "react-native-vector-icons/FontAwesome5";

type Banner = {
    url: string
}

interface Props {
    visible: boolean,
    banner?: string | string[],
    onClose: () => void
}

const _ModalShowImageComponent = (props: Props) => {

    const [listImages, setListImages] = useState<Banner[]>([]);

    const loadingRender = () => {
        return (
            <View style={{ width: 50, height: 50, zIndex: 99 }}>
                <ActivityIndicator color={colors.white} />
            </View>
        )
    }

    useEffect(() => {
        if (!props.banner) {
            console.log('props.visible', props.visible);
            return;
        }
        if (typeof props.banner === 'object') {
            let listImagesTmp: Banner[] = [];
            for (let i = 0; i < props.banner.length; i++) {
                listImagesTmp.push({ url: props.banner[i] });
            }
            setListImages(listImagesTmp);
        }
    }, [props.visible]);

    if (props.visible && typeof props.banner === 'object' && listImages.length === 0) {
        return (<LoadingIndicator visible={true} />);
    }

    return (
        <Modal isVisible={props.visible}
            onBackdropPress={props.onClose}
            onBackButtonPress={props.onClose}
            style={styles.background}>
            <View style={styles.container}>
                <TouchableOpacity style={styles.closeButton} onPress={props.onClose}>
                    <Icon name="times" style={styles.closeButtonIcon} />
                </TouchableOpacity>
                {
                    props.banner &&
                    <ImageViewer
                        imageUrls={typeof props.banner === 'string' ? [{ url: props.banner }] : listImages}
                        enableSwipeDown
                        loadingRender={loadingRender}
                        onSwipeDown={props.onClose}
                    />
                }
            </View>
        </Modal >
    );
}

const _ModalShowImage = React.memo(_ModalShowImageComponent, (prevProps, nextProps) => {
    if (prevProps.visible != nextProps.visible) {
        return false;
    } else {
        if (typeof prevProps.banner != typeof nextProps.banner) {
            return false;
        } else {
            if (typeof nextProps.banner == 'string') {
                return prevProps != nextProps;
            }
        }
    }
    return true;
});

export default _ModalShowImage;

const styles = StyleSheet.create({
    background: {
        margin: 0,
        padding: 0,
    },
    container: {
        flex: 1,
    },
    closeButton: {
        position: 'absolute',
        right: 30,
        top: 30,
        height: 35,
        width: 35,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1000,
    },
    closeButtonIcon: {
        color: 'white',
        fontSize: 18,
    },
});
