import { StackNavigationProp } from "@react-navigation/stack";
import React, { useState } from "react";
import {
    View,
    TouchableOpacity,
    StyleSheet
} from "react-native";
import { RootStackParamList } from "../../@types";
import { colors } from "../../config/Colors";
import Modal from 'react-native-modal';
import _Text from "../_Text";

interface Props {
    navigation: StackNavigationProp<RootStackParamList>
}
export default (props: Props) => {

    const [visible, setVisible] = useState<boolean>(true)

    const onBuyItem = () => {
        props.navigation.navigate('GameItem');
    }

    const onClose = () => {
        setVisible(false);
        props.navigation.pop();
    }

    return (
        <Modal isVisible={visible} onBackdropPress={onClose} style={styles.container} onBackButtonPress={onClose}>
            <View style={styles.content}>
                <View style={styles.info}>
                    <_Text style={styles.title}>사용가능한 아이템이 없습니다!</_Text>
                    <_Text style={styles._Text}>추가 아이템 구매를 해주세요.</_Text>
                </View>
                <View style={styles.wrapBtn}>
                    <TouchableOpacity style={styles.btn} onPress={onBuyItem}>
                        <_Text style={styles.titleBtn}>페이지로 이동</_Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: "center"
    },
    content: {
        backgroundColor: colors.white,
        alignItems: 'center',
        paddingVertical: 20,
        borderRadius: 10
    },
    info: {
        marginVertical: 10,
        width: '100%',
    },
    title: {
        fontWeight: '900',
        fontSize: 20,
        marginVertical: 10
    },
    _Text: {
        fontSize: 14,
        color: colors.gray,
        marginVertical: 10
    },
    btn: {
        width: '100%',
        backgroundColor: colors.mainColor,
        paddingHorizontal: 20,
        paddingVertical: 20,
        borderRadius: 10,
        marginHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleBtn: {
        color: colors.white
    },
    wrapBtn: {
        marginTop: 10,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: "center"
    }
});
