import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect, useState } from "react";
import {
    View,
    TouchableOpacity,
    Image,
    StyleSheet,
} from "react-native";
import { RootStackParamList } from "../../@types";
import { Images } from "../../assets";
import { colors } from "../../config/Colors";
import { screen } from "../../config/Layout";
import RNExitApp from "react-native-exit-app";
import Modal from 'react-native-modal';
import _Text from "../_Text";

type Params = {
    params: {
        roomId: any
    }
}
interface Props {
    navigation: StackNavigationProp<RootStackParamList>
    route: RouteProp<Params, 'params'>
}
export default ({
    navigation,
    route
}: Props) => {

    const [visible, setVisible] = useState<boolean>(true)

    const onClose = () => {
        setVisible(false);
        navigation.pop();
    }

    return (
        <Modal isVisible={visible} onBackdropPress={onClose} style={styles.container} onBackButtonPress={onClose}>
            <View style={styles.content}>
                <Image source={Images.no_internet} />
                <View style={styles.info}>
                    <_Text style={styles.title}>Connection lost!</_Text>
                    <_Text style={styles._Text}>It seems like there is no Internet. Please re-check or try again later.</_Text>
                </View>

                <View style={styles.wrapBtn}>
                    <TouchableOpacity style={styles.btn} onPress={() => {
                        RNExitApp.exitApp()
                    }}>
                        <_Text style={styles.titleBtn}>Quit Application</_Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};
const styles = StyleSheet.create({
    container: { flex: 1, paddingHorizontal: 16, alignItems: 'center', justifyContent: "center", backgroundColor: 'transparent' },
    content: { backgroundColor: colors.white, width: screen.widthscreen - 20, alignItems: 'center', paddingVertical: 20, borderRadius: 10 },
    info: {
        marginVertical: 10,
        paddingHorizontal: 20
    },
    title: {
        fontWeight: '900',
        fontSize: 20,
        marginVertical: 10
    },
    _Text: {
        fontSize: 14,
        color: colors.gray,
        marginVertical: 10
    },
    btn: {
        width: '100%',
        backgroundColor: colors.mainColor,
        paddingHorizontal: 20,
        paddingVertical: 20,
        borderRadius: 10,
        marginHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleBtn: {
        color: colors.white
    },
    wrapBtn: {
        marginTop: 10,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: "center"
    }
});
