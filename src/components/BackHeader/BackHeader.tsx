import { NavigationProp } from "@react-navigation/native";
import React from "react";
import { StyleSheet, StatusBar, View, Image, TouchableOpacity } from "react-native";
import { RootStackParamList } from "../../@types";
import { Images, Icons } from "../../assets";
import { colors } from '../../config/Colors';
import DeviceInfo from 'react-native-device-info';

interface Props {
    navigation: NavigationProp<RootStackParamList, any>,
}

const BackHeader = (props: Props) => {

    const goBack = () => {
        const { navigation } = props;
        navigation.goBack();
    }

    return (
        <>
            <StatusBar barStyle="light-content" />
            <View style={styles.header}>
                <TouchableOpacity style={styles.titleContainer} onPress={goBack}>
                    <Image source={Icons.ic_back} />
                </TouchableOpacity>
            </View>
        </>
    );
}

export default BackHeader;

const styles = StyleSheet.create({
    header: {
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        paddingBottom: 10,
        paddingRight: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems:  'flex-start',
        paddingLeft: 10
    },
    btnGame: {
        position: 'absolute',
        right: 10,
        top: 10
    }
});