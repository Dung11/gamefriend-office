import React from 'react';
import { TextStyle, TouchableOpacity, ViewStyle, Dimensions, StyleSheet, Text, Image, View } from 'react-native';
import { Icons, Images } from '../../assets';
import { colors } from '../../config/Colors';
import _Text from '../_Text';

export default ({
	title,
	onPress,
	style,
	titleBlack,
	backgroundColor,
	borderRadius,
	iconRight,
	iconLeft,
	urlIcon
}: {
	title: string;
	backgroundColor?: string;
	borderRadius?: any;
	onPress?: () => any;
	style?: ViewStyle;
	titleBlack?: boolean;
	iconRight?: boolean;
	iconLeft?: boolean;
	urlIcon?: any;
}) => (
	<TouchableOpacity style={[style, styles.wrapper, { backgroundColor: backgroundColor }, borderRadius ? { borderRadius: borderRadius } : {}]} onPress={onPress}>
		{iconLeft && <Image resizeMode="contain" style={styles.iconLeft} source={urlIcon} />}
		<View style={styles.wrapTitle}>
			<_Text style={{
				...styles.title,
				color: titleBlack ? colors.black : colors.white,
				marginLeft: iconRight ? 15 : 0
			}}>
				{title}
			</_Text>
		</View>
		{iconRight && <Image resizeMode="contain" style={styles.iconRight} source={Icons.ic_arrow_right} />}
	</TouchableOpacity>
);
const screen = Dimensions.get("window");
const styles = StyleSheet.create({
	wrapper: {
		borderRadius: 30,
		paddingVertical: 10,
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%',
		marginVertical: 5,
		flexDirection: 'row'
	},
	title: {
		fontSize: screen.width / 27,
		color: colors.white,
		fontWeight: '500',
		textAlign: 'left',
	},
	wrapTitle: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	iconLeft: {
		width: screen.width / 15,
		height: screen.width / 15,
		marginLeft: 20
	},
	iconRight: {
		marginRight: 15,
		marginVertical: 5
	}

});
