import React from 'react'

// Import react-native components
import {
    StyleSheet,
    View,
    ScrollView
} from 'react-native'

import Svg, { Path } from 'react-native-svg'

import { moderateScale } from 'react-native-size-matters'
import { colors } from '../../config/Colors'
import { screen } from '../../config/Layout'
import _Text from '../_Text'

class MessageBubble extends React.Component<any, any> {

    render() {
        return (
            <View style={[styles.message, styles.mine]}>
                <View style={styles.cloud}>
                    <ScrollView nestedScrollEnabled={true}
                        contentContainerStyle={{ maxWidth: moderateScale(screen.widthscreen, 2) }}>
                        <_Text style={styles.text}>{this.props.content || ''}</_Text>
                    </ScrollView>
                    <View style={[styles.arrow_container, styles.arrow_right_container]}>
                        <Svg
                            style={styles.arrow_right}
                            width={40}
                            height={21}
                            viewBox="0 0 100 100"
                        >
                            <Path
                                d={"M80,0 L80,100 L25,0"}
                                fill={'#FFFFFF'}
                                strokeWidth="4"
                                stroke={colors.blue}
                            />
                        </Svg>
                    </View>
                </View>
            </View>
        )
    }
}

export default MessageBubble

const styles = StyleSheet.create({
    message: {
        // flexDirection: 'row',
    },
    mine: {
        marginHorizontal: 20,
    },
    cloud: {
        maxWidth: moderateScale(250, 2),
        paddingHorizontal: moderateScale(10, 2),
        paddingTop: moderateScale(5, 2),
        paddingBottom: moderateScale(7, 2),
        borderColor: colors.blue,
        borderWidth: 1,
        borderRadius: 12,
        maxHeight: 100
    },
    text: {
        fontSize: 12,
        lineHeight: 17,
        fontWeight: '500',
    },
    arrow_container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -1,
        flex: 1
    },
    arrow_left_container: {
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    arrow_right_container: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    arrow_left: {
        left: moderateScale(-6, 0.5)
    },
    arrow_right: {
        top: 20.6,
        right: moderateScale(16, 0.5),
    }

})