import React, { Component } from 'react'
import {
    Dimensions,
    Image,
    StyleSheet,
    TouchableOpacity,
    View,
    ViewProps
} from 'react-native'
import Modal from 'react-native-modal';
import { Icons } from '../../assets';
import DeviceInfo from 'react-native-device-info';

interface Props extends ViewProps {
    visible: boolean,
    onClose?: any,
    imgPath?: string,
}

export class ImageModal extends Component<Props, any> {

    end_cursor: any;
    state = {
        visible: false,
        imgPath: '',
        isShowCloseBtn: false
    }

    componentDidMount() {
    }


    render() {
        let { visible, onClose, imgPath } = this.props;
        return (
            <Modal isVisible={visible} style={styles.background}>
                <View style={styles.container}>
                    <TouchableOpacity
                        onPress={() => { this.setState({ isShowCloseBtn: !this.state.isShowCloseBtn }) }}
                    >
                        <Image
                            source={{ uri: imgPath }}
                            style={styles.imgView}
                            resizeMode='contain'
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        onClose()
                        this.setState({ isShowCloseBtn: false })
                    }

                    } style={this.state.isShowCloseBtn ? styles.btnClose : styles.btnCloseHidden}>
                        <Image source={Icons.ic_white_closed} />
                    </TouchableOpacity>
                </View>

            </Modal>
        )
    }
}

export default ImageModal;

const styles = StyleSheet.create({
    background: {
        margin: 0,
        padding: 0,
    },
    container: {
        flex: 1,
    },
    imgView:
    {
        height: '100%',
        width: '100%',
        alignSelf: 'center'
    },
    btnCloseHidden:
    {
        width: 0
    },
    btnClose:
    {
        position: 'absolute',
        marginHorizontal: 20,
        marginTop: DeviceInfo.hasNotch() ? 40 : 20,
    }
});