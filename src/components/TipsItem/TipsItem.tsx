import React from 'react';
import { StyleSheet, View, Image } from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icons } from '../../assets';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';
import _Text from '../_Text';

interface Props {
    text: String,
    text1?: String,
    dateTime?: String,
    onClose?: () => void
}

export const TipsItem = (props: Props) => {

    return (
        <View style={styles.container}>
            <View style={styles.wrapTip}>
                {
                    props.onClose &&
                    <View style={styles.wrapClose}>
                        <TouchableOpacity onPress={props.onClose}>
                            <Image source={Icons.ic_close_black} />
                        </TouchableOpacity>
                    </View>
                }
                <View style={styles.wrapLableTip}>
                    <View style={styles.row}>
                        <Image source={Icons.ic_game_controller} />
                        <_Text style={styles.label}>{" "}겜친</_Text>
                    </View>
                    <_Text style={styles.label}>지금</_Text>
                </View>
                <_Text style={styles.tip}>팁:</_Text>
                <View>
                    <_Text>{props.text}</_Text>
                    {props.text1 &&<_Text>{props.text1}</_Text>}
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        width: screen.widthscreen,
        alignItems: 'center',
    },
    dateTime: { marginVertical: 15, color: colors.gray },
    label: {
        color: colors.grayLight
    },
    wrapTip: {
        backgroundColor: colors.white,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 7,
        width: screen.widthscreen - 14
    },
    wrapLableTip: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
    tip: {
        fontWeight: '900',
        fontSize: 15
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    wrapClose: {
        alignItems: 'flex-end',
        width: '100%',
        position: 'absolute',
        top: -5,
        right: -5
    }
})
