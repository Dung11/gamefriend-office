import React, { useEffect } from 'react';
import _Text from '../_Text';
import Toast, { BaseToast } from 'react-native-toast-message';
import Environments from '../../config/Environments';
import { colors } from '../../config/Colors';
import { StyleSheet } from 'react-native';

interface Props {
    onHide: any,
    text1: string,
    text2?: string
    type: string,
    position: string,
    modalToastRef?: any
}
const toastConfig = {
    success: ({ text1, text2, props, ...rest }: any) => (
        <BaseToast
            {...rest}
            style={{ borderLeftColor: colors.greenActive }}
            contentContainerStyle={{ paddingHorizontal: 15 }}
            text1Style={{
                fontSize: 15,
                fontWeight: '400',
                fontFamily: Environments.DefaultFont.Regular,
            }}
            text2Style={{
                fontSize: 15,
                fontWeight: '400',
                fontFamily: Environments.DefaultFont.Regular,
            }}
            text1={text1}
            text2={text2}
        />
    ),
    info: ({ text1, text2, props, ...rest }: any) => (
        <BaseToast
            {...rest}
            style={{ borderLeftColor: colors.blue }}
            contentContainerStyle={{ paddingHorizontal: 15 }}
            text1Style={{
                fontSize: 15,
                fontWeight: '400',
                fontFamily: Environments.DefaultFont.Regular,
            }}
            text2Style={{
                fontSize: 15,
                fontWeight: '400',
                fontFamily: Environments.DefaultFont.Regular,
            }}
            text1={text1}
            text2={text2}
        />
    ),
    error: ({ text1, text2, props, ...rest }: any) => (
        <BaseToast
            {...rest}
            style={{ borderLeftColor: colors.red }}
            contentContainerStyle={{ paddingHorizontal: 15 }}
            text1Style={{
                fontSize: 15,
                fontWeight: '400',
                fontFamily: Environments.DefaultFont.Regular,
            }}
            text2Style={{
                fontSize: 15,
                fontWeight: '400',
                fontFamily: Environments.DefaultFont.Regular,
            }}
            text1={text1}
            text2={text2}
        />
    ),
};

export const ToastInModal = (props: Props) => {
    
    const privateConfig = {
        visibilityTime: 1000,
        autoHide: true,
        topOffset: 30,
        bottomOffset: -10,
        text1Style: styles.text1Style,
        text2Style: styles.text1Style,
    }

    function hideToast() {
        props.modalToastRef.current.hide();
    }

    useEffect(() => {
        props.modalToastRef.current.show({
            text1: props.text1,
            text2: props?.text2,
            type: props.type,
            position: props.position,
            onHide: props.onHide,
            onPress: () => hideToast(),
            ...privateConfig
        })
    }, [])

    return (
        <Toast config={toastConfig} ref={props.modalToastRef} />
    )
}

const styles = StyleSheet.create({
    text1Style: {
        fontSize: 30,
        fontFamily: Environments.DefaultFont.Regular
    }
});