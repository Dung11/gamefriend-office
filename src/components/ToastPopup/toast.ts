import Toast from 'react-native-root-toast';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  successContainer: {
    marginTop:30,
    backgroundColor: '#4BB543',
    paddingHorizontal: 6,
    paddingVertical: 5
  },
  successText: {
    color: '#FFFFFF'
  },
  errorContainer: {
    marginTop:30,
    backgroundColor: '#CC0000',
    paddingHorizontal: 6,
    paddingVertical: 5
  },
  errorText: {
    color: '#FFFFFF'
  }
})

class ToastPopup {

  error = (error: string) => {
    Toast.show(error, {
      duration: Toast.durations.LONG,
      position: Toast.positions.TOP,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      containerStyle: styles.errorContainer,
      textStyle: styles.errorText,
      opacity: 1
    });
  }

  success = (message: string) => {
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position: Toast.positions.TOP,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      containerStyle: styles.successContainer,
      textStyle: styles.successText,
      opacity: 1
    });
  }
}

export const toastPopup = new ToastPopup();
