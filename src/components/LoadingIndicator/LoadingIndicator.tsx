import React, { useEffect, useState } from "react";
import { ActivityIndicator, Modal, StyleSheet, View } from "react-native";

interface Props {
    visible: boolean,
}

const LoadingIndicator = (props: Props) => {

    const [visible, setVisible] = useState(props.visible);

    useEffect(() => {
        setVisible(props.visible);
        return () => { }
    }, [props.visible])

    return (
        <Modal visible={visible} transparent={true} style={{ zIndex: 1000 }}>
            <View style={styles.container}>
                <View style={styles.indicatorContainer}>
                    <ActivityIndicator color="gray" size="large" />
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.7)',
    },
    indicatorContainer: {
        width: 100,
        height: 100,
        borderRadius: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default LoadingIndicator;