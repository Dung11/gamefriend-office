import CameraRoll from '@react-native-community/cameraroll';
import React, { Component } from 'react'
import { Dimensions, FlatList, Image, StyleSheet, TouchableOpacity, View, ViewProps, Platform, StatusBar } from 'react-native'
import { checkReadStoragePermission } from '../../utils/common';
import Modal from 'react-native-modal';
import { Icons, Images } from '../../assets';
import DeviceInfo from 'react-native-device-info';
import MaskedView from '@react-native-community/masked-view';
import _Text from '../_Text';

interface State {
    visible: boolean,
    data: any[],
    selectedImages: any[],
    currentImage: string,
    statusTextColor: 'dark-content' | 'light-content',
}

interface Props extends ViewProps {
    visible: boolean,
    title?: string,
    doneButtonText?: string,
    numberOfImages?: number,
    onChange?: (item: any) => void,
    onClose?: () => void,
}
const width = Dimensions.get('screen').width;

export class _ChooseImageModal extends Component<Props, State> {

    end_cursor: any;


    constructor(props: Props) {
        super(props);
        this.state = {
            visible: false,
            data: [],
            selectedImages: [],
            currentImage: '',
            statusTextColor: 'dark-content',
        }
    }

    componentDidMount() {
        checkReadStoragePermission()?.then(isGranted => {
            if (isGranted) {
                CameraRoll.getPhotos({
                    first: 18,
                    assetType: 'Photos',
                    after: this.end_cursor || undefined,
                }).then((values) => {
                    this.end_cursor = values.page_info.end_cursor;
                    if (values?.edges instanceof Array && values?.edges.length > 0
                        || values.page_info.has_next_page === true) {
                        this.setState({
                            currentImage: values.edges[0].node.image.uri,
                            selectedImages: [values.edges[0].node.image.uri],
                            data: values.edges,
                        });
                    }
                });
            }
        }).catch(err => {
            console.log(err.message);
        });
    }

    onDone = () => {
        let { selectedImages } = this.state;
        this.props.onChange && this.props.onChange(selectedImages);
    }

    loadLazyMedia = () => {
        if (this.end_cursor) {
            CameraRoll.getPhotos({
                first: 18,
                assetType: 'Photos',
                after: this.end_cursor ? this.end_cursor : null,
            }).then((values) => {
                this.end_cursor = values.page_info.end_cursor;
                this.setState({
                    data: [
                        ...this.state.data,
                        ...values.edges,
                    ],
                });
            });
        }
    };

    selectImage = (item: any) => {
        let numMaxImages = this.props.numberOfImages || 5;
        let selected: string[] = [...this.state.selectedImages];
        let uri: any = item.node.image.uri;
        let index = selected.indexOf(uri);
        if (numMaxImages === 1) {
            selected = [uri];
        } else {
            if (index == -1 && selected.length < numMaxImages) {
                this.setState({
                    currentImage: uri
                });
                selected.push(uri);
            } else if (index != -1 && selected.length > 1) {
                selected.splice(index, 1);
                this.setState({
                    currentImage: selected[selected.length - 1],
                });
            }
        }
        this.setState({ selectedImages: selected, currentImage: uri });
    }

    renderItem = ({ item }: any) => {
        let { selectedImages }: any = this.state;
        let uri: any = item.node.image.uri;
        let index = selectedImages.indexOf(uri);
        return (
            <TouchableOpacity style={{
                width: '33%'
            }} onPress={() => this.selectImage(item)}>
                { index > -1 && <Image style={styles.icon} source={Icons.yellow_checked} />}
                <Image style={styles.userImg} resizeMode="cover" source={{ uri: uri }} />
            </TouchableOpacity>
        )
    }

    render() {
        let { visible, onClose, title, doneButtonText } = this.props;
        let { currentImage, data, statusTextColor } = this.state;
        return (
            <Modal isVisible={visible} style={styles.background} onBackdropPress={onClose}>
                <View style={styles.container}>
                    <StatusBar barStyle={statusTextColor} />
                    <View style={styles.header}>
                        <TouchableOpacity onPress={onClose} style={styles.closeButtonContainer}>
                            <_Text>닫기</_Text>
                        </TouchableOpacity>
                        <View style={styles.titleContainer}>
                            <_Text style={styles.title}>
                                {/* {title} */}
                            </_Text>
                        </View>
                        <TouchableOpacity onPress={this.onDone} style={styles.doneButtonContainer}>
                            <_Text style={styles.doneTitle}>{doneButtonText}</_Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.maskedViewContainer}>
                        <MaskedView style={styles.maskedView}
                            maskElement={
                                <View style={styles.maskedElementContainer}>
                                    <View style={styles.maskedElement} />
                                </View>
                            }>
                            <Image source={currentImage ? {
                                uri: currentImage
                            } : Images.avatar_default}
                                style={styles.maskedElementImage} />
                        </MaskedView>
                    </View>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        onEndReachedThreshold={0.1}
                        onEndReached={this.loadLazyMedia}
                        data={data}
                        numColumns={3}
                        renderItem={this.renderItem} />
                </View>
            </Modal>
        )
    }
}

export default _ChooseImageModal;

const styles = StyleSheet.create({
    contentContainer: {
        flexGrow: 1,
        justifyContent: 'space-between',
        flexDirection: 'column',
    },
    background: {
        margin: 0,
        padding: 0,
    },
    container: {
        flex: 1,
        paddingTop: Platform.OS === 'ios' ? (DeviceInfo.hasNotch() ? 50 : 20) : 0,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        paddingVertical: 15,
        paddingHorizontal: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        color: 'black',
        fontSize: 16,
        fontWeight: '800',
    },
    closeButtonContainer: {
        position: 'absolute',
        left: 15,
        top: 15,
        width: 50,
        height: 30,
        justifyContent: 'center',
    },
    doneButtonContainer: {
        position: 'absolute',
        top: 15,
        right: 5,
        width: 50,
        height: 30,
        justifyContent: 'center',
    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
    },
    icon: {
        position: 'absolute',
        height: 15,
        width: 15,
        top: 5,
        right: 5,
        zIndex: 1,
    },
    userImg: {
        height: 120,
        margin: 2,
        zIndex: 0
    },
    doneTitle: {
        color: '#4387EF',
        fontSize: 16,
        fontWeight: '800',
    },
    currentImage: {
        height: 400,
        width: '100%',
        resizeMode: 'contain'
    },
    maskedViewContainer: {
        width: width,
        height: 400,
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    },
    maskedView: {
        height: 400,
        width: width,
    },
    maskedElementContainer: {
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        alignItems: 'center',
        justifyContent: 'center',
        height: width,
        width: '100%',
    },
    maskedElement: {
        width: width - (width / 10),
        height: width - (width / 10),
        borderRadius: (width - (width / 10)) / 2,
        backgroundColor: 'black',
    },
    maskedElementImage: {
        width: '100%',
        height: 400,
    },
});
