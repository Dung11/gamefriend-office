import React from 'react'
import { Platform, Text, TextProps, TextStyle } from 'react-native';
import Environments from '../config/Environments';

interface Props extends TextProps {
    style?: TextStyle | any,
    children?: Element,
}

const _Text = (props: Props) => {
    const customStyles = props.style;
    let lineHeight = 20;
    if (customStyles?.lineHeight) {
        lineHeight = customStyles.lineHeight;
    } else if (customStyles?.fontSize) {
        lineHeight = customStyles.fontSize + 10;
    }

    let textStyles: TextStyle = {
        fontSize: 14,
        fontFamily: Environments.DefaultFont.Medium,
        lineHeight: lineHeight,
        color: '#4A5568',
        // marginBottom: -10,
        // paddingBottom: Platform.OS == 'ios' ? 5 : 0,
    }
    if (props.style && props.style.fontWeight) {
        const fontWeight = props.style.fontWeight;
        if (fontWeight == '100' || fontWeight == '200') {
            textStyles.fontFamily = Environments.DefaultFont.Thin;
        } else if (fontWeight == '300') {
            textStyles.fontFamily = Environments.DefaultFont.Light;
        } else if (fontWeight == '400') {
            textStyles.fontFamily = Environments.DefaultFont.Regular;
        } else if (fontWeight == '500' || fontWeight == '600') {
            textStyles.fontFamily = Environments.DefaultFont.Medium;
        } else if (fontWeight == '700' || fontWeight == '800') {
            textStyles.fontFamily = Environments.DefaultFont.Bold;
        } else if (fontWeight == '900') {
            textStyles.fontFamily = Environments.DefaultFont.Black;
        } else if (fontWeight == 'bold') {
            textStyles.fontFamily = Environments.DefaultFont.Bold;
        } else {
            textStyles.fontFamily = Environments.DefaultFont.Medium;
        }
    }

    return (
        <Text {...props} style={[textStyles, props.style]}>
            {props.children}
        </Text>
    )
}

export default _Text;
