import React from 'react';
import { View, Image, StyleSheet, TouchableOpacity, ViewStyle } from 'react-native';
import { colors } from '../../config/Colors';
import { screen } from '../../config/Layout';
import _Text from '../_Text';
interface Props {
    item: any;
    userName?: string;
    textColor?: string;
    profilePicture?: string;
    isSender?: boolean,
    onShowImage: (url: string) => void,
    contentContainerStyle?: ViewStyle,
}
export const ChatLineHolder = ({ profilePicture, item, userName, textColor, isSender, onShowImage, contentContainerStyle }: Props) => {
    if (item?.chatContent?.value !== null) {
        return (
            <View style={[styles.container,{}]}>
                { profilePicture && <Image style={styles.avatar} source={{ uri: profilePicture }} />}
                <View>
                    {
                        item?.chatContent?.value !== '' &&
                        <View style={[styles.item, contentContainerStyle]} >
                            {userName && <_Text style={styles.usernameText}>{userName}</_Text>}
                            <_Text style={{ color: textColor ? textColor : colors.black }}>
                                {item?.chatContent?.value.replace('introGF-','')}
                            </_Text>
                        </View>
                    }
                    {
                        item?.chatContent?.chatType === 'image' &&
                        <TouchableOpacity style={[styles.imageMessage, { justifyContent: isSender ? 'flex-end' : 'flex-start' }]}
                            onPress={() => onShowImage(item?.chatContent?.image?.origin)}>
                            <Image style={styles.imgComment} source={{ uri: item?.chatContent?.image?.thumbnail }} />
                        </TouchableOpacity>
                    }
                </View>
            </View>
        );
    }
    return null;
    
};
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 16,
    },
    item: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        paddingHorizontal: 12.5,
        paddingVertical: 7.5,
        backgroundColor: colors.grayLight,
        borderRadius: 15,
        marginLeft: 5,
        marginTop: 5,
    },
    avatar: {
        marginLeft: 10,
        marginRight: 10,
        height: 35,
        width: 35,
        borderRadius: 35,
    },
    imgComment: {
        height: screen.heightscreen / 4,
        width: screen.widthscreen / 2,
        borderRadius: 8,
        transform: [{ scale: 0.9 }],
    },
    usernameText: {
        color: '#005ce6',
        marginBottom: 5,
    },
    imageMessage: {
        flexDirection: 'row',
        marginHorizontal: 5,
    },
});
