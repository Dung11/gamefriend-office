import React, { useState } from 'react';
import { TextInput, ReturnKeyTypeOptions, StyleSheet, View, StyleProp, ViewStyle, TextStyle, Platform, Image, TouchableOpacity, ActivityIndicator } from "react-native";
import { Icons } from '../../assets';
import { colors } from '../../config/Colors';
import Environments from '../../config/Environments';
import { screen } from '../../config/Layout';
import _Text from '../_Text';

interface Props {
    returnKeyType?: ReturnKeyTypeOptions;
    returnKeyLabel?: string;
    onSubmitEditing?: () => void
    errorMessage?: string;
    placeholder?: string;
    onChangeText?: (text: string) => void;
    containerStyle?: StyleProp<ViewStyle>;
    inputStyle?: StyleProp<TextStyle>;
    isError?: boolean,
    value: string,
    label?: string,
    iconLabel?: any,
    iconRight?: boolean,
    keyboardType?: any,
    onBlur?: () => void,
    checkNickName?: boolean
}

export const InputApp = (props: Props) => {
    const [focusBorder, setFocusBorder] = useState(false)
    const [showPass, setShowPass] = useState(false)
    return (
        <>
            <View style={styles.label}>
                <Image source={props.iconLabel} />
                <_Text style={styles.textLabel}>{props.label}</_Text>
            </View>
            <View style={[styles.container, props.containerStyle, focusBorder ? {
                borderColor: colors.mainColor,
                borderWidth: 1
            } : {}]}>
                <TextInput {...props}
                    autoCapitalize="none"
                    style={[styles.textInput, props.inputStyle]}
                    onFocus={() => { setFocusBorder(!focusBorder) }}
                    secureTextEntry={props.iconRight ? !showPass : false} />
                {props.iconRight &&
                    <ShowHideBtn showed={showPass} onPress={() => setShowPass(!showPass)} />
                }
                {props.checkNickName &&
                    <ActivityIndicator color={colors.mainColor} />
                }
            </View>
            {props.isError && <View style={styles.wrapError}>
                <_Text style={styles.errorText}>{props.errorMessage}</_Text>
            </View>}
        </>
    )
}
const ShowHideBtn = ({
    showed,
    onPress,
}: {
    showed?: boolean;
    onPress?: (a: any) => any;
}) => (
    <TouchableOpacity onPress={onPress}>
        {showed ? <Image source={Icons.ic_eye_off} style={styles.iconLeft} /> : <Image style={[styles.iconLeft]} source={Icons.ic_eye} />}
    </TouchableOpacity>
);


const styles = StyleSheet.create({
    container: {
        width: '100%',
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        // marginVertical: 5,
        paddingVertical: Platform.OS === 'ios' ? 15 : 0,
        flexDirection: 'row'
    },
    label: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        fontWeight: '900',
        marginTop: 10
    },
    textLabel: { marginLeft: 10, fontSize: 14, fontWeight: '900', color: '#666666' },
    textInput: {
        paddingHorizontal: 5,
        fontSize: 14,
        width: '90%',
        fontWeight: '400',
        fontFamily: Environments.DefaultFont.Regular,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        height: Platform.OS == 'android' ? 50 : 'auto',
    },
    wrapError: {
        width: '100%', alignItems: 'flex-end'
    },
    errorText: {
        color: '#CC0000',
    },
    iconLeft: {
        resizeMode: 'contain',
        height: screen.widthscreen / 16,
        width: screen.widthscreen / 16,

    },

})
