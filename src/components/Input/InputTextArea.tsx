import React, { PureComponent } from "react";
import { View, TextInput, ViewStyle, TextInputProps, StyleSheet } from "react-native";
import { screen } from "../../config/Layout";
import _Text from "../_Text";
const DEFAULT_TINT_COLOR = "#828282";
const DEFAULT_SECOND_COLOR = "rgba(255, 255, 255, 0.6)";
const DEFAULT_ERROR_COLOR = "#E23244";

interface Props extends TextInputProps {
	style?: ViewStyle;
	onChangeText?: (text: string) => void;
	value: string;
	error?: boolean;
	borderColor?: string;
	errorBorderColor?: string;
	tintColor?: string;
	errorColor?: string;
	label?: string;
	ulrAvt?: string;
}

export default class InputTextArea extends PureComponent<Props> {
	_onChangeText = (text: string) => {
		this.props.onChangeText && this.props.onChangeText(text);
	};

	render() {
		const {
			tintColor = DEFAULT_TINT_COLOR,
			errorColor = DEFAULT_ERROR_COLOR,
			style,
			value,
			error,
			borderColor = tintColor,
			errorBorderColor = errorColor,
			label,
			ulrAvt,
			...rest
		} = this.props;
		return (
			<View
				style={[
					{ borderColor: error === true ? errorBorderColor : borderColor },
					styles.Wrapper
				]}
			>
				<View style={styles.LabelWrapper}>
					<_Text style={styles.LabelText}>{label}</_Text>
				</View>
				<View style={[styles.WrapInput, style]}>
					<TextInput multiline ref="textarea"
						onChangeText={this._onChangeText}
						style={styles.textArea}
						value={value}
						{...rest}
					/>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	Wrapper: {
		justifyContent: "center",
	},
	LabelWrapper: {
		marginBottom: 10,
		alignItems: 'center'
	},
	LabelText: {
		fontSize: 16,
		flexDirection: "row"
	},
	WrapInput: {
		width: "100%",
		height: screen.heightscreen / 4,
		backgroundColor: "#FFFFFF",
		borderRadius: 12,
		paddingHorizontal: 10,
		paddingVertical: 5,
	},
	textArea: {
		width: "100%",
		height: "100%",
		textAlignVertical: "top",
	},
});
