
import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image, ViewStyle, TextStyle, Dimensions } from 'react-native';
import { colors } from '../../config/Colors';
import { Icons } from '../../assets';
import _Text from '../_Text';
import DeviceInfo from 'react-native-device-info';

interface Props {
    title: string,
    onBack?: () => void,
    onPress?: () => void,
    styleContainer?: ViewStyle,
    styleTitle?: TextStyle,
}

export const HeaderApp = (Props: Props) => {

    const containerStyle: ViewStyle = {
        ...styles.wrapper,
        ...Props.styleContainer,
        justifyContent: Props.onBack === undefined || Props.onPress === undefined ? 'center' : 'space-between',
    };

    const titleStyle: ViewStyle = {
        ...styles.title,
        ...Props.styleTitle,
    };

    return (
        <View style={containerStyle}>
            {Props.onBack && <TouchableOpacity onPress={Props.onBack} >
                <Image source={Icons.ic_back} style={styles.backIcon} />
            </TouchableOpacity>}
            <_Text numberOfLines={1} style={titleStyle}>
                {Props.title}
            </_Text>
            {Props.onPress && <TouchableOpacity onPress={Props.onPress} >
                <Image source={Icons.ic_more} style={styles.burgerIcon} />
            </TouchableOpacity>}
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        backgroundColor: colors.mainColor,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 25,
        paddingBottom: 10,
    },
    title: {
        color: colors.white,
        fontSize: 16,
        fontWeight: '900',
        paddingBottom: DeviceInfo.hasNotch() ? 10 : 0,
        width: Dimensions.get('screen').width * 0.7,
        textAlign: 'center',
    },
    backIcon: {
        width: 30,
        height: 30,
        marginLeft: 5,
    },
    burgerIcon: {
        width: 30,
        height: 30,
        marginRight: 5,
    },
});
