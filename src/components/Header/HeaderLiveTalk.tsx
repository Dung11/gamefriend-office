import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity, Platform, ViewStyle } from 'react-native';
import _Text from '../_Text';
import { colors } from '../../config/Colors';
import { Icons } from '../../assets';
import DeviceInfo from 'react-native-device-info';

interface Props {
    onBack?: () => void,
    onPress?: () => void,
    styleContainer?: ViewStyle,
    styleTitle?: any
}

export const HeaderLiveTalk = (Props: Props) => {
    return (
        <View style={[styles.wrapper, Props.styleContainer]}>
            <View style={styles.headerLeftBtn}>
                {/* <Image style={styles.headerLeftBtnIcon} source={Icons.ic_live} />
                <_Text style={styles.txtLiveHeader}>추천 친구</_Text> */}
            </View>
            <TouchableOpacity style={styles.titleContainer} onPress={() => { }}>
                <Image source={Icons.ic_live_talk} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.headerRightBtn} onPress={Props.onPress} >
                <Image source={Icons.ic_game} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 15,
        backgroundColor: colors.mainColor,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,
        paddingBottom: 10,
    },
    headerLeftBtn: {
        alignItems: 'center',
    },
    headerLeftBtnIcon: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
    },
    title: {
        color: colors.white,
        fontSize: 16,
        fontWeight: '900',
        paddingBottom: 10,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtLiveHeader: {
        fontSize: 10,
        color: colors.white
    },
    headerRightBtn: {
        position: 'absolute',
        right: 10,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
});