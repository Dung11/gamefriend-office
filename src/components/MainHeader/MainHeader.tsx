import { NavigationProp, useNavigation } from "@react-navigation/native";
import React, { useEffect, useRef, useState } from "react";
import { StyleSheet, StatusBar, View, Image, TouchableOpacity, Animated, Platform } from "react-native";
import { RootStackParamList } from "../../@types";
import { Icons } from "../../assets";
import { colors } from '../../config/Colors';
import DeviceInfo from 'react-native-device-info';
import _Text from "../_Text";
import { keyValueStorage } from "../../storage/keyValueStorage";

interface Props {
    isBack?: boolean
}

const MainHeader = (props: Props) => {

    const [fadeAnim, setFadeAnim] = useState(new Animated.Value(1));
    const [isAni, setIsAni] = useState<boolean>(true)
    const navigation = useNavigation();

    const goToRecommendedFriend = async () => {
        stopAnimation();
        setFadeAnim(new Animated.Value(1));
        setIsAni(false);
        await keyValueStorage.save('isFirstlogin', 'false');
        navigation.navigate('RecommendedFriend');
    }
    const goToGameItem = () => {
        navigation.navigate('GameItem');
    }

    const onGoBack = () => {
        navigation.goBack();
    }

    const runAnimation = () => {
        Animated.loop(
            Animated.sequence([
                Animated.timing(fadeAnim, {
                    toValue: 0.2,
                    duration: 1000,
                    delay: 1000,
                    useNativeDriver: false
                }),
                Animated.timing(fadeAnim, {
                    toValue: 1,
                    duration: 1000,
                    delay: 1000,
                    useNativeDriver: false
                })
            ])
        ).start()
    }

    const stopAnimation = () => {
        setIsAni(false);
        Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 0,
            delay: 0,
            useNativeDriver: false
        }).stop();
    }

    useEffect(() => {
        keyValueStorage.get('isFirstlogin').then((isFirstlogin: any) => {
            console.log('isFirstlogin: ', isFirstlogin)
            let value = JSON.parse(isFirstlogin);
            if (value === true) {
                runAnimation();
            } else {
                setIsAni(false);
            }
        })
    }, [navigation])

    return (
        <>
            <StatusBar barStyle="light-content" />
            <View style={styles.header}>
                {
                    (props?.isBack && props?.isBack === true) &&
                    <TouchableOpacity style={styles.btnBack} onPress={onGoBack}>
                        <Image source={Icons.ic_back} />
                    </TouchableOpacity>
                }
                {
                    isAni ?
                        <Animated.View style={[styles.headerContainer, { opacity: fadeAnim }]}>
                            <View style={styles.circleStatus} />
                            <TouchableOpacity style={styles.titleContainer} onPress={goToRecommendedFriend}>
                                <Image source={Icons.ic_header_diamond} />
                                <_Text style={styles.titleHeader}>추천 친구</_Text>
                                <View style={styles.underline} />
                            </TouchableOpacity>
                        </Animated.View> :
                        <View style={[styles.headerContainer]}>
                            <View style={styles.circleStatus} />
                            <TouchableOpacity style={styles.titleContainer} onPress={goToRecommendedFriend}>
                                <Image source={Icons.ic_header_diamond} />
                                <_Text style={styles.titleHeader}>추천 친구</_Text>
                                <View style={styles.underline} />
                            </TouchableOpacity>
                        </View>
                }
                <TouchableOpacity style={styles.btnGame} onPress={goToGameItem}>
                    <Image source={Icons.ic_game} />
                </TouchableOpacity>
            </View>
        </>
    );
}

export default MainHeader;

const styles = StyleSheet.create({
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    titleHeader: {
        fontWeight: '900',
        fontSize: 20,
        lineHeight: 26,
        color: colors.white,
    },
    header: {
        justifyContent: 'space-between',
        backgroundColor: colors.mainColor,
        alignItems: 'center',
        paddingBottom: 10,
        paddingRight: 10,
        paddingTop: DeviceInfo.hasNotch() ? 50 : 20,

    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnGame: {
        position: 'absolute',
        right: 10,
        bottom: 0
    },
    btnBack: {
        position: 'absolute',
        left: 10,
        bottom: 10
    },
    underline: {
        height: 0,
        width: 100,
        borderTopColor: colors.white,
        borderTopWidth: 2,
    },
    circleStatus: {
        height: 8,
        width: 8,
        backgroundColor: '#EBEA64',
        borderRadius: 10,
        marginTop: 10
    }
});