import { StyleSheet } from "react-native";
import { colors } from "../../config/Colors";
import { screen } from "../../config/Layout";

const styles = StyleSheet.create({
    commentContainer: {
        paddingBottom: 30,
        // paddingHorizontal: 24,
        maxHeight: 400
    },
    title: {
        color: colors.blue,
        fontWeight: '800',
        fontSize: 14,
        lineHeight: 20
    },
    subtitle: {
        fontWeight: '800',
        fontSize: 12,
        lineHeight: 20
    },
    commentItem: {
        backgroundColor: '#F5F5F5',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 12,
        marginBottom: 10,
    },
    reviewName: {
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 20,
        color: colors.dark
    },
    reviewContent: {
        fontSize: 10,
        lineHeight: 14,
        paddingRight: 10,
    },
    rowFriendAvt: {
        marginVertical: 10,
        marginLeft: 12,
        width: screen.widthscreen / 8,
        height: screen.widthscreen / 8,
        borderRadius: screen.widthscreen / 8 / 2,
    },
});

export default styles;