import { Alert, FlatList, Image, View, ViewProps, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from 'react'
import styles from "./styles";
import { Icons, Images } from "../../assets";
import { getUserReview } from "../../api/UserServices";
import _Text from "../_Text";

interface Props extends ViewProps {
    friendId: string,
    canRemove?: boolean,
    onRemove?: (reviewId: string) => void,
    onClickAvatar?: (reviewerId: string) => void,
}

const _CommentContainer = (props: Props) => {

    let page = 1;
    const limitPage = 10;
    const [reviews, setReviews] = useState<any[]>([]);
    const [friendId, setFriendId] = useState<any>(props.friendId);
    const [quantity, setQuantity] = useState<number>(1);

    const getReview = () => {
        let reviewsTmp = [...reviews];
        if (reviews.length >= quantity) return;

        getUserReview(friendId, page, limitPage)
            .then((response: any) => {
                setQuantity(response.quantity);
                let reiviewData = reviewsTmp.concat(response.reviews);
                setReviews(reiviewData);
                if (response.reviews !== 0) {
                    page++;
                }
            })
            .catch((error: Error) => {
                Alert.alert('오류', error.message);
            });
    }

    const onRemove = (reviewId: any) => {
        if (props.onRemove) {
            props.onRemove(reviewId);
            let reviewTmps = [...reviews];
            let filterReviewTmps = reviewTmps.filter((review: any) => review.id !== reviewId);
            setReviews(filterReviewTmps);
        }
    }

    const onClickAvatar = (id: string) => {
        if (id && props.onClickAvatar) {
            props.onClickAvatar(id);
        }
    }

    useEffect(() => {
        setFriendId(props.friendId);
        friendId && getReview();
    }, [props.friendId]);

    const renderReviewItem = ({ item, index }: any) => {
        if (item.author) {
            return (
                <View style={styles.commentItem} key={index}>
                    <TouchableOpacity style={{ flex: .2 }} onPress={() => onClickAvatar(item?.author?.id)}>
                        <Image style={styles.rowFriendAvt}
                            source={item?.author?.profilePicture ?
                                { uri: item?.author?.profilePicture } :
                                Images.avatar_default} />
                    </TouchableOpacity>
                    <View style={{ flex: .3 }}>
                        <_Text style={styles.reviewName} numberOfLines={1} >{item?.author?.userName}</_Text>
                    </View>
                    <View style={{ flex: .5, marginLeft: 10 }}>
                        <_Text style={styles.reviewContent} numberOfLines={2} >{item?.text}</_Text>
                    </View>
                    {
                        props.canRemove &&
                        <View style={{ position: 'absolute', top: 3, right: 3, padding: 10 }}>
                            <TouchableOpacity onPress={props?.onRemove && (() => onRemove(item.id))}>
                                <Image source={Icons.ic_gray_x_sign} />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            )
        } else {
            return (<></>);
        }
    }
    return (
        <View style={styles.commentContainer}>
            { !props.canRemove && <_Text style={[styles.title, { marginVertical: 20, textAlign: 'center' }]}>매칭 후기</_Text>}
            <FlatList
                data={reviews}
                nestedScrollEnabled={true}
                onEndReached={getReview}
                onEndReachedThreshold={0.3}
                renderItem={renderReviewItem}
                keyExtractor={(item, index) => String(index)}
            />
            {
                (reviews && reviews.length === 0) &&
                <_Text style={[styles.subtitle, { textAlign: 'center' }]}>등록된 댓글이 없습니다</_Text>
            }
        </View>
    )
}

export default _CommentContainer;