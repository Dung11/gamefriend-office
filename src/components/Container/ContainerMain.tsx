import React from 'react';
import { StyleSheet, View, ViewStyle, Platform, Image, TouchableOpacity, KeyboardAvoidingView, ImageBackground, TouchableWithoutFeedback, Keyboard, StatusBar, ScrollView } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';
import { Icons, Images } from '../../assets';
import { colors } from '../../config/Colors';

interface Props {
    styles?: ViewStyle;
    children: React.ReactNode;
}

export const ContainerMain = (props: Props) => {
    return (
        <KeyboardAvoidingView style={styles.container} behavior={'padding'} enabled={Platform.OS == 'ios'}>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <ImageBackground resizeMode="stretch" source={Images.background} style={[props.styles, styles.content]}>
                    {props.children}
                </ImageBackground>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundApp,
    },
    content: {
        flex: 1,
    },
});
