import React from 'react';
import { StyleSheet, View, ViewStyle, Platform, Image, TouchableOpacity, KeyboardAvoidingView, ImageBackground, TouchableWithoutFeedback, Keyboard, StatusBar } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';
import { Icons, Images } from '../../assets';
import { colors } from '../../config/Colors';

interface Props {
    onNext?: () => void;
    onGoBack?: () => void;
    styles?: ViewStyle;
    isBack?: boolean;
    isNext?: boolean;
    isNotBack?: boolean;
    children: React.ReactNode;
    isAuthor?: boolean;
}

export const Container = (props: Props) => {
    return (
        <>
            <StatusBar backgroundColor={colors.mainColor} barStyle="light-content" />
            <KeyboardAvoidingView style={styles.container} behavior={Platform.OS == "ios" ? "padding" : "height"}>
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <ImageBackground resizeMode="stretch" source={Images.background} style={styles.content}>
                        <View style={[styles.contentItem, {
                            paddingHorizontal: props.isAuthor ? 50 : 0
                        },props.styles]}>
                            {props.children}
                        </View>
                        <View style={[styles.btnBottomGroup, props.isNotBack ? { justifyContent: 'flex-end' } : {}]} >
                            {props.isBack ? <TouchableOpacity style={styles.button} onPress={props.onGoBack}>
                                <Image style={styles.icon} source={Icons.ic_back} />
                            </TouchableOpacity> : null}
                            {props.isNext ? <TouchableOpacity style={styles.button} onPress={props.onNext}>
                                <Image style={styles.icon} source={Icons.ic_next} />
                            </TouchableOpacity> : null}
                        </View>
                    </ImageBackground>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundApp,
    },
    content: {
        flex: 1,
    },
    contentItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnBottomGroup: {
        width: '100%',
        paddingHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'transparent',
        marginBottom: 16,
        position: 'absolute',
        bottom: 0
    },
    button: { backgroundColor: 'transparent'},
    icon: { resizeMode: 'contain' }

})
