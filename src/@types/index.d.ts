export type RootStackParamList = {
    Author?: any;
    Login?: any;
    Home?: any;
    Bulletin?: any;
    LiveTalk: any;
    Match?: any;
    Register?: any;
    Main: any;
    Chat?: any;
    LoginByEmail?: any;
    EnterCode?: any;
    NewPassword?: any;
    VerifiedCodeForgotPassword?: any;
    PhoneNumberForgotPassword?: any;
    EmailInput?: any;
    PolicyTerms?: any;
    AuthLoading?: any;
    PhoneNumber?: any;
    confirmation?: any;
    BirthDay: any;
    Gender?: any;
    Address?: any;
    UploadImage?: any;
    UploadGameImage?: any;
    NickName?: any;
    ChooseGame?: any;
    SearchFiter?: any;
    SearchFiterGameFavorite?: any;
    ChoosePlayTime?: any;
    ChooseCharacterPlay?: any;
    ChoosePeronalities?: any;
    Waiting?: any;
    Confirm?: any;
    IntroducingTextInput?: any;
    LoadingMatch?: any;
    WaitingMatchOnePoint?: any;
    WaitingMatchTwoPoint?: any;
    RoomChat?: any;
    AlertMatchingSuccess?: any;
    RecommendedFriend: any;
    ViewFullProfile: any;
    ModalFullViewProfile: any;
    openDrawer: () => void;
    CreateNewPost?: any;
    NewPost?: any;
    Notify?: any;
    ChatDetail?: any;
    ChatDetailMatch?: any;
    ChatScreen?: any;
    ChatMatch?: any;
    ListFriendRequest?: any;
    Chatlist?: any;
    Tabs?: any,
    Modal?: any,
    FilterRecommendedFriend?: any,
    FriendProfile?: any,
    SearchFavouriteGame?: any,
    Propensity?: any,
    SetPropensity?: any,
    SuggestedFriendSetting?: any,
    Noti?: any,
    Terms?: any,
    EditProfile?: any,
    AlertDisconnect?: any,
    GameItem?: any
    Profile?: any,
    AppInfo?: any,
    AppInfoMenu?: any,
    AlertNoItem?: any,
    SetupPropensityChat?: any,
    AlertConfirmItem?: any,
    LiveTalkNeighborhood?: any,
    LiveTalkChatScreen?: any
    WattingJoinRoom?: any,
    ViewFullProfileLiveTalk?: any,
};

export type renderItem = {
    item: any,
    index: number,
}

export interface ICommentItem {
    id: string,
    author: {
        id: string,
        userName: string,
        profilePicture: string,
    },
    content: {
        value: string,
        type: 'text' | 'image',
        image: string,
    },
    subs: number,
    timeAgo: any,
}
