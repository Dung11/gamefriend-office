import NetInfo from '@react-native-community/netinfo';
import { DeviceEventEmitter } from 'react-native';
import io from 'socket.io-client';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../@types';
import { keyValueStorage } from '../storage/keyValueStorage';
import UrlApi from '../config/UrlApi';
import { refreshToken, verifyToken } from '../api/AuthorServices';
import { onLogout } from '../utils/UserUtils';
import ToastUtils from '../utils/ToastUtils';

// @ts-ignore

let socket: SocketIOClient.Socket;

export type Events = 'match-done' | 'room' | 'chat' | 'match-start' | 'prior-match-start' | 'match-status' | 'room-in-use' | 'room-not-in-use' | 'room-leave' | 'local-room-update' | 'local-room' | 'local-chat' | 'local-room-status' | 'room-terminate' | 'local-room-in-use' | 'local-room-not-in-use';
export const connectToServer = (token: string, navigation: StackNavigationProp<RootStackParamList>) => new Promise<SocketIOClient.Socket>((resolve, reject) => {

    socket = io(UrlApi.DEV.AUTH_SERVICE_BASE_URL_SOCKET, {
        path: '/ws/socket.io',
        transports: ['websocket'],
        query: `token=${token}`
    });

    socket.on('connect', () => {
        console.log("==>> Connesct Socket Successfully!");
        DeviceEventEmitter.emit("showDisConnectLoadingModal", { visible: false });
    });

    socket.on('match-done', (data: any) => {
        console.log("mathdone ==>>>>>>>>", data)
    });

    socket?.on('prior-match-start', (data: any) => {
        DeviceEventEmitter.emit("showWattingMatchTwoPoint", { visible: true })
        DeviceEventEmitter.emit("showWattingMatch", { visible: false })
    });

    socket?.on('match-start', (data: any) => {
        console.log('MATCH START');
        DeviceEventEmitter.emit("showWattingMatch", { visible: true })
    });

    socket?.on('invalid-token', (data: any) => {
        console.log('invalid-token');
    });

    socket.on('disconnect', async (reason: 'io server disconnect'
        | 'io client disconnect'
        | 'ping timeout'
        | 'transport close'
        | 'transport error') => {
        console.log('reason', reason);
        if (reason === 'transport error') {
            console.log('Disconnect Socket timeout 10 second!');
            console.log('socket.connected: ', socket.connected);
            if (!socket.connected) {
                let status = await NetInfo.fetch();
                if (status.isConnected) {
                    DeviceEventEmitter.emit("showDisConnectLoadingModal", { visible: true });
                    let responseVerify = await verifyToken();
                    if (responseVerify.status === 404) {
                        ToastUtils.showToast({
                            type: 'error',
                            text1: '오류',
                            text2: '네트워크 상태를 확인해주세요',
                            position: 'top'
                        });
                        await onLogout(navigation);
                        return;
                    }
                    let isValid = responseVerify.data.isValid;
                    if (!isValid) {
                        socket.disconnect();
                        let newAccessToken = await refreshToken();
                        await connectToServer(newAccessToken, navigation);
                    }
                }
            }
        }
    });

    socket.on('connect_error', async (e: any) => {
        ToastUtils.showToast({
            type: 'error',
            text1: '오류',
            text2: '네트워크 상태를 확인해주세요',
            position: 'top'
        });
        await onLogout(navigation);
    });

    resolve(socket);
})

export const connect = async (token: string, navigation: StackNavigationProp<RootStackParamList>) => {
    try {
        if (!socket || !socket?.connected) {
            socket = await connectToServer(token, navigation);
        }
        return true;
    } catch (e) {
        console.log('===>> Error: ', e)
        return false;
    }
}
export const updateMessage = (message: any) => {
    socket?.emit('UPDATE_MESSAGE', message);
}

export const emitEvent = (eventName: Events, data: any) => {
    socket?.emit(eventName, data);
}

export const ReceiveMessage = async (eventName: Events, handler: Function) => {
    socket?.on(eventName, handler);
}

export const sendIntroMatch = async (roomId: any) => {
    const messageIntroduction = await keyValueStorage.get("messageIntroduction") as any
    const data = {
        roomId: roomId, // id of the that message is sent to
        chatContent: {
            chatType: 'text',  // "text" or "image"
            value: `introGF-${messageIntroduction}`
        }
    }

    if (messageIntroduction) {
        socket?.emit('chat', data);
    }
}

export const addHandler = (eventName: Events, handler: Function) => {
    socket?.on(eventName, handler);
}

export const removeHandler = (eventName: Events) => {
    socket?.off(eventName);
}

export const disconnect = () => {
    socket?.disconnect()
}