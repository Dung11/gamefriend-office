/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useState } from 'react';
import { StyleSheet, View, StatusBar, ActivityIndicator } from 'react-native';
import { colors } from './src/config/Colors';
import { Navigation } from './src/navigation/navigation';
import { Provider } from 'react-redux';
import store from './src/redux/store';
import ModalWatting from './src/components/Modal/ModalWattingMatch';
import ModalWaitingLocalRoom from './src/components/Modal/ModalWaitingLocalRoom';
import ModalWattingTwoPoint from './src/components/Modal/ModalWattingMatchTwoPoint';
import ModalDisConnectLoading from './src/components/Modal/ModalDisConnectLoading';
import CodePush, { LocalPackage } from 'react-native-code-push';
import Environments from './src/config/Environments';
import Toast, { BaseToast } from 'react-native-toast-message';
import _Text from './src/components/_Text';

declare const global: { HermesInternal: null | {} };

interface State {
    syncMessage: string,
    progress: boolean,
};

class App extends React.Component<any, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            syncMessage: '',
            progress: true,
        };
    }

    codePushStatusDidChange = (syncStatus: CodePush.SyncStatus) => {
        switch (syncStatus) {
            case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
                this.setState({ syncMessage: "업데이트 확인 중." });
                break;
            case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
                this.setState({ syncMessage: "패키지 다운로드 중." });
                break;
            case CodePush.SyncStatus.AWAITING_USER_ACTION:
                this.setState({ syncMessage: "사용자 작업을 기다리고 있습니다." });
                break;
            case CodePush.SyncStatus.INSTALLING_UPDATE:
                this.setState({ syncMessage: "업데이트 설치." });
                break;
            case CodePush.SyncStatus.UP_TO_DATE:
                this.setState({ syncMessage: "앱이 최신 상태입니다.", progress: false });
                break;
            case CodePush.SyncStatus.UPDATE_IGNORED:
                this.setState({ syncMessage: "사용자가 업데이트를 취소했습니다.", progress: false });
                break;
            case CodePush.SyncStatus.UPDATE_INSTALLED:
                this.setState({ syncMessage: "업데이트가 설치되었으며 다시 시작할 때 적용됩니다.", progress: false });
                break;
            case CodePush.SyncStatus.UNKNOWN_ERROR:
                this.setState({ syncMessage: "알 수 없는 오류가 발생했습니다.", progress: false });
                break;
        }
    }

    getUpdateMetadata() {
        CodePush.getUpdateMetadata(CodePush.UpdateState.RUNNING)
            .then((metadata: LocalPackage) => {
                console.log('META DATA: ', metadata);
                // this.setState({ syncMessage: metadata ? JSON.stringify(metadata) : "Running binary version", progress: false });
            }, (error: any) => {
                this.setState({ syncMessage: "Error: " + error, progress: false });
            });
    }

    componentDidMount() {
        // CodePush.sync({ deploymentKey: Environments.CODEPUSH_STAGING_KEY });
        // this.getUpdateMetadata();
        this.syncImmediate();
    }

    codePushDownloadDidProgress(progress: any) {
        this.setState({ progress });
    }

    sync() {
        CodePush.sync(
            {},
            this.codePushStatusDidChange.bind(this),
            this.codePushDownloadDidProgress.bind(this)
        );
    }

    /** Update pops a confirmation dialog, and then immediately reboots the app */
    syncImmediate() {
        CodePush.sync(
            {
                deploymentKey: Environments.CODEPUSH_PRODUCTION_KEY,
                installMode: CodePush.InstallMode.IMMEDIATE,
                updateDialog: {
                    title: '경보',
                    optionalUpdateMessage: '업데이트가 있습니다. 설치 하시겠습니까?',
                    optionalInstallButtonLabel: '설치',
                    optionalIgnoreButtonLabel: '무시',
                },
            },
            this.codePushStatusDidChange.bind(this),
            this.codePushDownloadDidProgress.bind(this)
        );
    }

    render() {
        const { progress, syncMessage } = this.state;
        if (progress) {
            return (
                <View style={styles.waitingContainer}>
                    <ActivityIndicator color="white" size="large" />
                    <_Text style={styles.waitingText}>{syncMessage}</_Text>
                </View>
            )
        }
        const toastConfig = {
            success: ({ text1, text2, props, ...rest }: any) => (
                <BaseToast
                    {...rest}
                    style={{ borderLeftColor: colors.greenActive }}
                    contentContainerStyle={{ paddingHorizontal: 15 }}
                    text1Style={{
                        fontSize: 15,
                        fontWeight: '400',
                        fontFamily: Environments.DefaultFont.Regular,
                    }}
                    text2Style={{
                        fontSize: 15,
                        fontWeight: '400',
                        fontFamily: Environments.DefaultFont.Regular,
                    }}
                    text1={text1}
                    text2={text2}
                />
            ),
            info: ({ text1, text2, props, ...rest }: any) => (
                <BaseToast
                    {...rest}
                    style={{ borderLeftColor: colors.blue ,zIndex: 10}}
                    contentContainerStyle={{ paddingHorizontal: 15 }}
                    text1Style={{
                        fontSize: 15,
                        fontWeight: '400',
                        fontFamily: Environments.DefaultFont.Regular,
                    }}
                    text2Style={{
                        fontSize: 15,
                        fontWeight: '400',
                        fontFamily: Environments.DefaultFont.Regular,
                    }}
                    text1={text1}
                    text2={text2}
                />
            ),
            error: ({ text1, text2, props, ...rest }: any) => (
                <BaseToast
                    {...rest}
                    style={{ borderLeftColor: colors.red }}
                    contentContainerStyle={{ paddingHorizontal: 15 }}
                    text1Style={{
                        fontSize: 15,
                        fontWeight: '400',
                        fontFamily: Environments.DefaultFont.Regular,
                    }}
                    text2Style={{
                        fontSize: 15,
                        fontWeight: '400',
                        fontFamily: Environments.DefaultFont.Regular,
                    }}
                    text1={text1}
                    text2={text2}
                />
            ),
        };

        return (
            <Provider store={store} >
                <StatusBar barStyle="light-content" backgroundColor={colors.mainColor} />
                <View style={styles.container}>
                    <Navigation />
                    <ModalWatting />
                    <ModalWaitingLocalRoom />
                    <ModalWattingTwoPoint />
                    <ModalDisConnectLoading />
                    <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
                </View>
            </Provider>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundApp,
    },
    waitingContainer: {
        flex: 1,
        backgroundColor: colors.mainColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    indicatorContainer: {
        width: 100,
        height: 100,
        borderRadius: 10,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    waitingText: {
        fontSize: 18,
        color: 'white',
        marginTop: 10,
    },
});

export default CodePush(App);
